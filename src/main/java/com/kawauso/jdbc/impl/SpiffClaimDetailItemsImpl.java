package com.kawauso.jdbc.impl;

import com.kawauso.jdbc.domain.ClaimItems;
import com.kawauso.jdbc.domain.SpiffClaimDetails;
import com.kawauso.jdbc.domain.SpiffClaimMaster;
import com.kawauso.jdbc.domain.SpiffClaimSerial;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.*;


/**
 * Created by cbyrd on 5/15/15.
 */
public class SpiffClaimDetailItemsImpl {


    private DataSource dataSource;

    private final Logger logger = LoggerFactory.getLogger(SpiffClaimDetailItemsImpl.class);
    private final int ALL_YEARS = -999;


    public void setDataSource(DataSource dataSource) throws SQLException {

        this.dataSource = dataSource;
        //this.dataSource.getConnection().setAutoCommit(true);

    }



    /**
     * This is for CoolRay SPIFF processing.Eventully this will evolve to focus on all
     * customers that have multiple accounts
     */
    public List<Object[]> getClaimAndSerial(Integer cono, String product, String serialNo) {


        StringBuilder coolraySql = new StringBuilder();
        coolraySql.append("select s.claim_no, concat(i.orderno, '-', RIGHT(REPLICATE('0', 2) + LEFT(i.ordersuf, 2), 2)) as ORDER_NO ");
        coolraySql.append("from nxt_icets i ");
        coolraySql.append("left join spiff_claim_items c ON (c.product=i.prod and c.serial_number=i.serialno and c.deleted=0) ");
        coolraySql.append("left join spiff_claims s ON (s.id=c.spiff_claim_id and s.deleted=0) ");
        coolraySql.append("left join contacts con ON (con.id = s.contact_id) ");
        coolraySql.append("where i.cono = :cono and i.custno IN ('21675','107569','107570') and i.prod = :prod and  i.serialno = :serial_no and i.returnfl=0");

        logger.info("=========================  Executing getClaimAndSerial() for coolray product/serial number " + product + "/" + serialNo);
        logger.info(coolraySql.toString());

        SpiffClaimSerial claimSerial = new SpiffClaimSerial();
        List<Object[]> list = new ArrayList<Object[]>();

        try {
            NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
            MapSqlParameterSource sqlParams = new MapSqlParameterSource();

            sqlParams.addValue("cono", cono, Types.INTEGER);
            sqlParams.addValue("prod", product, Types.VARCHAR);
            sqlParams.addValue("serial_no", serialNo, Types.VARCHAR);

            List<SpiffClaimSerial> claims = jdbcTemplate.query(coolraySql.toString(),
                    sqlParams,
                    new BeanPropertyRowMapper(SpiffClaimSerial.class));

            Object[] obj = new Object[2];
            if (claims.size() > 0) {

                SpiffClaimSerial s = claims.get(0);

                obj = new Object[]{s.getClaimNo(), s.getOrderNo()};

                list.add(obj);
            }
            else if (claims.size() == 0){

                obj = new Object[2];

                //OK this is another hack.  I'm adding an empty object to the list.
                //The SpiffController finds an object array, whether it has data or not.
                //if the claims list size is zero, that means the query return no rows.
                list.add(obj);
            }

            logger.info("==================  Finished executing getClaimAndSerial() for coolray product/serial number  num " + product + "/" + serialNo );

        }
        catch(Exception ex){

            logger.error("Could not execute query in getClaimAndSerial()...", ex);


        }

        return list;

    }


    /**
     * This is for non CoolRay Spiff
     * @param cono
     * @param customerNo
     * @param product
     * @param serialNo
     * @return
     */
    public List<Object[]> getClaimAndSerial(Integer cono, String customerNo, String product, String serialNo){

        StringBuilder sql = new StringBuilder();
        sql.append("select s.claim_no, concat(i.orderno, '-', RIGHT(REPLICATE('0', 2) + LEFT(i.ordersuf, 2), 2)) As order_no ");
        sql.append(" from nxt_icets i ");
        sql.append(" left join spiff_claim_items c on (c.product=i.prod and c.serial_number=i.serialno and c.deleted=0) ");
        sql.append(" left join spiff_claims s on (s.id=c.spiff_claim_id and s.deleted=0) ");
        sql.append(" left join contacts con ON (con.id = s.contact_id) " );
        sql.append(" where i.cono = ? and i.custno = ? and i.prod = ? and i.serialno = ? and i.returnfl = 0 ");


        logger.info("=========================  Executing getClaimAndSerial() for customer num " + customerNo + " ===========");
        logger.info(sql.toString());

        SpiffClaimSerial claimSerial = new SpiffClaimSerial();
        Object[] obj = new Object[2];
        List<Object[]> list = new ArrayList<Object[]>();
        try{

           JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

           Object[] params = new Object[] {cono,customerNo,product,serialNo};

           claimSerial =  (SpiffClaimSerial) jdbcTemplate.queryForObject(sql.toString(),params, new BeanPropertyRowMapper(SpiffClaimSerial.class));


           obj[0] = claimSerial.getClaimNo();
           obj[1] = claimSerial.getOrderNo();

           list.add(obj);

           logger.info("=========================  Finished executing getClaimAndSerial() for customer num " + customerNo + " ===========");

       }
           catch(Exception ex){

               logger.error("Could not execute query in getClaimAndSerial()...", ex);

           }

        return list;



    }


    public Map getClaimMaster (String claim_id){


        SpiffClaimMaster master   = new SpiffClaimMaster();

        //Get master info
        StringBuilder masterQuery = new StringBuilder();
        masterQuery.append("select id,year,claim_no,contact_id,installed,entered,");
        masterQuery.append("release_dt,approved,paid,customer_name,customer_address,");
        masterQuery.append("customer_address_city ,customer_address_state,customer_address_zip,");
        masterQuery.append("multiplier,deleted,claim_total,deleted_date from spiff_claims ");
        masterQuery.append("where id = ?");


        Map map = new HashMap();
        try{


            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            map = jdbcTemplate.queryForMap(masterQuery.toString(), claim_id);


        }
        catch(Exception ex){

            logger.error("Error in retriving query from getClaims for SpiffClaimsDetil info...",ex);


        }

        return map;
    }

    public List<ClaimItems> getClaimsDetails(String id){

        List<ClaimItems> list = new ArrayList<ClaimItems>();
        DateTime dt = new DateTime();

        StringBuilder query = new StringBuilder();
        query.append("select classification,product_group,product,serial_number ");
        query.append("from spiff_claim_items where spiff_claim_id = ? " );//and year = " + dt.getYear());

        try{


            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            list =  jdbcTemplate.query(query.toString(),
                    new Object[]{id},
                    new BeanPropertyRowMapper(ClaimItems.class));

        }
        catch(Exception ex){

            logger.error("Error in retriving query from getClaims for SpiffClaimsDetil info...",ex);


        }

        return list;


    }


    public List<SpiffClaimDetails> findAllClaims(String contactId, Integer year ){

        List<SpiffClaimDetails> list = new ArrayList<SpiffClaimDetails>();
        StringBuilder queryString = new StringBuilder();
        queryString.append("select id, contact_id, claim_no, customer_name, entered, approved, paid, claim_total from spiff_claims ");
        queryString.append("where contact_id = ? and installed between ? and ? ");
        queryString.append("order by claim_no ");

        String startDate,endDate;

        if (year.intValue() == -999){

            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            //getTime() returns the current date in default time zone
            Date date = calendar.getTime();
            int current_year = calendar.get(Calendar.YEAR);

            startDate = String.format("%d-01-01",new Integer(1901));
            endDate   = String.format("%d-12-31",new Integer(current_year));

        }else {
            startDate = String.format("%d-01-01", 2016);
            endDate = String.format("%d-12-31", year);
        }

        try{

                JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);


                List<Map<String,Object>> rows = jdbcTemplate.queryForList(queryString.toString(), new Object[]{contactId,startDate,endDate});

                for(Map row : rows){

                    SpiffClaimDetails details = new SpiffClaimDetails();
                    details.setId((String)row.get("id"));
                    details.setClaimNumber((Integer) row.get("claim_no"));
                    details.setContactId((String) row.get("contact_id"));
                    details.setCustomerName((String) row.get("customer_name"));
                    details.setEnteredDate((Date) row.get("entered"));
                    details.setApprovedDate((Date) row.get("approved"));
                    details.setPaidDate((Date) row.get("paid"));
                    details.setAmount((Double) row.get("claim_total"));
                    list.add(details);

                }
//                list = jdbcTemplate.query(queryString.toString(),
//                                         new Object[]{contactId, startDate, endDate},
//                                         new BeanPropertyRowMapper(SpiffClaimDetails.class));

        }
        catch(Exception ex){


            logger.error("Error in findAllClaims() method ... ", ex);


        }
        return list;
    }

    public static void main(String[] args) {


            SpiffClaimDetailItemsImpl i = new SpiffClaimDetailItemsImpl();
//
//            List<Object[]> list = i.getClaimAndSerial(1, "67945", "24VNA936A003", "3914E06311");
//
//            System.out.println("size = " + list.size());


//
//
//             Map m  = i.getClaimMaster("554a7b51-afb0-4ca0-8f44-0456c0a80a21");
//
//             m.get("claim_id");



            List<SpiffClaimDetails> l = i.findAllClaims("a674704d-704d-4479-8136-42a37f000101",2015);

                    System.out.println("size = " + l.size());

        }

}
