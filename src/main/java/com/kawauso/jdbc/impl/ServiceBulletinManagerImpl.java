//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.kawauso.jdbc.impl;

import com.kawauso.jdbc.dao.ServiceBulletin;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ServiceBulletinManagerImpl implements ServiceBulletin {
    private final Logger logger = LoggerFactory.getLogger(ServiceBulletinManagerImpl.class);
    private JdbcTemplate jdbc = new JdbcTemplate(this.getDBCPDataSource());

    public ServiceBulletinManagerImpl() {
    }

    private BasicDataSource getDBCPDataSource() {
        BasicDataSource dbcp = new BasicDataSource();
        dbcp.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dbcp.setUrl("jdbc:sqlserver://hivedbsrv.mingledorffs.com:1433;databaseName=hive;integratedSecurity=false;");
        dbcp.setUsername("monty");
        dbcp.setPassword("python_669");
        return dbcp;
    }

    private String createUniqueID() {
        String uid = null;

        try {
            uid = UUID.randomUUID().toString();
        } catch (Exception var3) {
            this.logger.error("Unable to generate a unique service bulletin id ");
        }

        return uid;
    }

    public Boolean insert(final String companyNumber, final String smbid, String description, Integer year, String keywords, Integer fileSize, CommonsMultipartFile fileData) {
        Boolean isSuccess = Boolean.valueOf(false);
        final int vYear = year.intValue();
        final String vDesc = description;
        final String vKeywords = keywords;
        final int vFileSize = fileSize.intValue();
        final CommonsMultipartFile vFileData = fileData;
        final DefaultLobHandler lobHandler = new DefaultLobHandler();
        StringBuilder sql = new StringBuilder();
        sql.append("insert into dbo.service_bulletins (uuid,erpcono,smbid,year,subject,keywords,filesize,filedata) ");
        sql.append("VALUES (?,?,?,?,?,?,?,?) ");

        try {
            final String ex = this.createUniqueID();
            this.jdbc.execute(sql.toString(), new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
                protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                    ps.setString(1, ex);
                    ps.setInt(2, Integer.valueOf(companyNumber).intValue());
                    ps.setString(3, smbid);
                    ps.setInt(4, vYear);
                    ps.setString(5, vDesc);
                    lobCreator.setClobAsString(ps, 6, vKeywords);
                    ps.setInt(7, vFileSize);

                    try {
                        ps.setBinaryStream(8, vFileData.getInputStream());
                    } catch (IOException var4) {
                        var4.printStackTrace();
                    }

                }
            });
            isSuccess = Boolean.valueOf(true);
        } catch (Exception var17) {
            var17.printStackTrace();
        }

        return isSuccess;
    }

    public Boolean delete(String uuid) {
        //String sql = "DELETE FROM service_bulletins where uuid = ?";
        String sql = "update service_bulletins set status=0 where uuid = ?";
        Boolean itWorked = Boolean.valueOf(false);

        try {
            int ex = this.jdbc.update(sql, new Object[]{uuid});
            if(ex == 1) {
                itWorked = Boolean.valueOf(true);
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }

        return itWorked;
    }
}
