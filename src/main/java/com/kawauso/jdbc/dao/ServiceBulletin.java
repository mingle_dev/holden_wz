package com.kawauso.jdbc.dao;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public interface ServiceBulletin {
    Boolean insert(String var1, String var2, String var3, Integer var4, String var5, Integer var6, CommonsMultipartFile var7);

    Boolean delete(String var1);
}