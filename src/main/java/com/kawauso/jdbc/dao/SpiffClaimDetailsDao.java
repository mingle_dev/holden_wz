package com.kawauso.jdbc.dao;

import com.kawauso.jdbc.domain.SpiffClaimDetails;

/**
 * Created by cbyrd on 5/14/15.
 */
public interface SpiffClaimDetailsDao {


     SpiffClaimDetails getClaimDetails(String id);


}
