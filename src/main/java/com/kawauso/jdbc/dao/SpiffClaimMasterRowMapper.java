package com.kawauso.jdbc.dao;


import com.kawauso.jdbc.domain.SpiffClaimMaster;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is a custom row mapper object for use with Spring jdBC
 */
public class SpiffClaimMasterRowMapper implements RowMapper {


     public SpiffClaimMaster mapRow(ResultSet rs, int rowNum) throws SQLException {

         SpiffClaimMaster master = new SpiffClaimMaster();

         master.setId(rs.getString("id"));
         master.setYear(rs.getInt("year"));
         master.setClaimNumber(rs.getInt("claim_no"));
         master.setContactId(rs.getString("contact_id"));
         master.setInstallDate(rs.getDate("installed"));
         master.setCustomerName(rs.getString("customer_name"));
         master.setStreet(rs.getString("customer_address"));
         master.setCity(rs.getString("customer_address_city"));
         master.setState(rs.getString("customer_address_state"));
         master.setZipCode(rs.getString("customer_address_zip"));
         master.setApprovedDate(rs.getDate("approved"));
         master.setEnteredDate(rs.getDate("entered"));
         master.setPaidDate(rs.getDate("paid"));
         master.setClaimTotal(rs.getDouble("claim_total"));
         master.setMultiplier(rs.getInt("multiplier"));
         master.setReleaseDate(rs.getDate("release_dt"));
         master.setDeleted(rs.getInt("deleted_date"));
         master.setMultiplier(rs.getInt("multiplier"));

         return master;

     }

}


//StringBuilder query = new StringBuilder();
//query.append("select id,year,claim_no,contact_id,installed,entered,");
//        query.append("release_dt,approved,paid,customer_name,customer_address,");
//        query.append("customer_address_city ,customer_address_state,customer_address_zip,");
//        query.append("multiplier,deleted,claim_total,deleted_date from spiff_claims ");
//        query.append("where id = :id");