/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base;

import com.kawauso.base.bean.Session.Role;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author chris.weaver
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SecurityAnnotation
{
    boolean requireLogin();
    Role[] allowedRole() default Role.ANONYMOUS;
}