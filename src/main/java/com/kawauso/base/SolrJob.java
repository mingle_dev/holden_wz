/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base;

import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.bean.Kit;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.nonmanaged.SolrProduct;
import com.kawauso.base.bean.nonmanaged.SolrProductAttribute;
import com.kawauso.base.dataobject.SolrDao;
import com.kawauso.base.service.ProductService;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author chris
 */
public class SolrJob extends TimerTask
{
    private static final Logger log = Logger.getLogger( SolrJob.class );
    private final String solrUrl = ConfigReader.getProperty("solr.url");

    private int counter = 0;

    @Autowired
    ProductService productService;

    //@Override
    public void run() 
    {
        //don't run the indexer on startup of the site
        counter++;
        if(counter == 1) {
            log.info("SolrJob bypassed on startup");
            return;
        }

        log.info("SolrJob Started for " + solrUrl);

        boolean isIndexServer = false;

        try {
            String idxIpAddr = ConfigReader.getProperty("sol.idxIpAddr");

            //loop through all ip addresses looking for a match to the
            //assigned solr index server (only one per cluster)
            Enumeration en = NetworkInterface.getNetworkInterfaces();
            while(en.hasMoreElements()){
                NetworkInterface ni=(NetworkInterface) en.nextElement();
                Enumeration ee = ni.getInetAddresses();
                while(ee.hasMoreElements()) {
                    InetAddress ia = (InetAddress) ee.nextElement();
                    if(ia.getHostAddress().equalsIgnoreCase(idxIpAddr))
                        isIndexServer = true;
                }
            }

        }catch(Exception ex) {
            log.error("Error determining ip address; Solr failed to index due to: " + ex);
        }

        if(isIndexServer == false) {
            log.info("Solr Indexing not active on this server");
            return;
        }

        try {
            SolrServer server = SolrDao.getInstance(solrUrl).getServer();

            server.deleteByQuery("*:*");
            server.commit();

            //get products
            List<Product> products = productService.getAll();
            log.info("Solr will index " + products.size() + " records");

            int size = 0;

            List<SolrProduct> spList = new ArrayList<SolrProduct>();
            for (Product p : products) {
                try {
                    SolrProduct sp = new SolrProduct();

                    sp.setId(ELUtilities.base64Encode(p.getProduct()));
                    sp.setSku(p.getProduct());
                    sp.setDescription(p.getDescription());
                    sp.setDetails(p.getDetails());
                    sp.setType("product");
                    sp.setCategory(String.valueOf(p.getCategoryId()));

                    sp.setImage(p.getImage());
                    //String manufacturer = productService.getManufacturer( p.getProduct() );
                    //sp.setManufacturer(manufacturer);

                    //fetch attributes, bind manufacturer and build dynamic fields
                    Map<String, String> attrs = productService.getAttributes(p.getProduct());

                    //if(attrs.containsKey("Manufacturer")) {
                    //    sp.setManufacturer(attrs.get("Manufacturer"));
                    //    attrs.remove("Manufacturer");
                    //}

                    if(attrs != null && !attrs.isEmpty()) {
                        Map<String, String> spAttrs = new HashMap<String, String>();
                        Iterator it = attrs.keySet().iterator();
                        while (it.hasNext()) {
                            String key = it.next().toString();

                            //manufacturer isn't an attribute but is stored in the attr table
                            if (key.equals("Manufacturer")) {
                                sp.setManufacturer(attrs.get("Manufacturer"));
                            } else {
                                spAttrs.put("facet_" + key + "_s", attrs.get(key));
                            }
                        }
                        sp.setAttributes(spAttrs);
                    }
                    spList.add(sp);

                }catch(Exception pex) {
                    //log.error("Solr Indexing encountered an error on prod=" + p.getProduct() + " : " + pex);
                    log.error("Solr Indexing encountered an error: " + pex);
                }
                size++;

                if(size % 1000 == 0)
                    log.info("SolrJob processed: " + size);
            }

            server.addBeans(spList);
            server.commit();

            //get kits
            List<Kit> kits = productService.getKits();
            spList = new ArrayList<SolrProduct>();
            for (Kit k : kits) {
                SolrProduct sp = new SolrProduct();
                sp.setId(ELUtilities.base64Encode(k.getName()));
                sp.setSku(k.getName());
                sp.setDescription(k.getDescription());
                sp.setType("kit");

                spList.add(sp);
            }

            server.addBeans(spList);
            server.commit();

            //optimize
            server.optimize();

        }catch(SolrServerException ex) {
            log.error("SolrServerException: " + ex);
        }catch(IOException ex) {
            log.error("IOException: " + ex);
        }catch(Exception ex) {
            log.error("Exception: " + ex);
        }

        log.info("SolrJob Finished");
    }

}