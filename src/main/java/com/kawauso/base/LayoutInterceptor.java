 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base;

import com.kawauso.base.Utilities.Emailer;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.Session;
import com.kawauso.base.bean.UserFavorite;
import com.kawauso.base.service.*;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

 @Component
public class LayoutInterceptor extends HandlerInterceptorAdapter
{
    private static final Logger log = Logger.getLogger( LayoutInterceptor.class );

    //@Autowired
    //private MenuService menuService;

    @Autowired
    private PricebookService pricebookService;

    @Autowired
    private ListService listService;
    
    @Autowired
    private LocationService locationService;    
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionFactory sessionfactory;

    //private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(LayoutInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        Session session = (Session) request.getSession().getAttribute("session");

        if(session == null) {
            session = new Session();
            session.setRemoteAddress(request.getHeader("X-FORWARDED-FOR"));

            request.getSession().setAttribute("session", session);
        }

        //get current page
        String currPage = request.getRequestURI() + "?" + request.getQueryString();
        request.getSession().setAttribute("currPage", currPage);

        //@todo; performance hit; setup product menu - should cache this or set into application singleton
        //request.setAttribute("headProdMenu", menuService.getMenu(1));
        request.setAttribute("headProdMenu", pricebookService.getTemplateNames(15));

        //@todo; performance hit; get current location - tie into user session
        request.setAttribute("currentLocation", locationService.getLocation( session.getWarehouse() ));

        if (request.getAttribute("productNames") == null || request.getAttribute("productNames").equals(""))
        {
            //load the product SKU numbers in memory, if it doesn't exist
            request.setAttribute("productNames", productService.getAllSku(session.getCono(), null));
        }


        //user is logged in
        if(session.getUserId() != null && !session.getUserId().isEmpty())
        {
            //@todo; performance hit; setup lists
            request.setAttribute("saveLists", listService.getLists(session.getUserId(), null, false));

            //get shopping cart lines
            //@todo; performance hit; this was taken directly from CartController:getItems()
            List<CartItem> items = cartService.getCartItems( session.getUserId() );

            HashMap<String,String> lineItemErrors = new HashMap<String, String>();

            for (Iterator<CartItem> iter = items.listIterator(); iter.hasNext(); ) {


                CartItem item = iter.next();
                Product p = productService.getProduct(session.getCono(), OPER, session.getCustomerNumber(), session.getShipTo(), session.getWarehouse(), item.getProductCode());
                if (p != null) {

                    item.setProductCode(item.getProductCode());
                    item.setUnitPrice(p.getPrice());
                    item.setProductDetail(p);
                }
                else  {

                    String userId = session.getUserId();
                    String productCode = item.getProductCode();

                    //email WeatherZone Support Team with product information
                    Emailer email = new Emailer(ConfigReader.getProperty("email.smtpserver"));
                    int smtpPort = Integer.valueOf(ConfigReader.getProperty("email.smtpPort"));
                    email.setSmtpPort(smtpPort);
                    email.setFromAddress(ConfigReader.getProperty("email.sender"));
                    email.setFromName(ConfigReader.getProperty("site.name"));

                    List<String> sendToList = new ArrayList<String>();

                    String sendToProp = ConfigReader.getProperty("email.sendTo.errors");
                    if (sendToProp != null && !sendToProp.equals("")) {
                        StringTokenizer st = new StringTokenizer(sendToProp, ";");
                        while (st.hasMoreElements()) {
                           sendToList.add((String) st.nextToken());
                        }
                    }

                    Iterator<String> iterator = sendToList.iterator();

                    while(iterator.hasNext())
                    {
                        String sendTo = iterator.next();
                        System.out.println(sendTo);
                        email.addToAddress(sendTo);
                    }

                    email.setSubject(ConfigReader.getProperty("site.name") + " -- Shopping Cart Add ");

                    String message = "Cart item add failed for product code "+ productCode + ".  Currently, this product is not available online.";
                    message += "\n\nPlease contact " + session.getSlsRepName() + " to purchase item.";

                    message += "\n\n***********************************************************";
                    message += "\ncustomer: " + session.getCustomerName() + "\nusername: " + session.getUser().getUsername()  + "\nuserid: " + session.getUserId();

                    message += "\n\n***********************************************************";
                    email.setMessage(message);
                    email.sendMessage();

                    //add error products to the request
                    lineItemErrors.put(productCode, productCode);

                    logger.info("LayoutInterceptor preHandle() - cart item product code "
                            + item.getProductCode() + " invalid.");
                }
            }
            request.setAttribute("cartLines", items);

            String cartLineErrors = "";

            Set<String> keySet = lineItemErrors.keySet();
            Iterator<String> keySetIterator = keySet.iterator();

            if (keySetIterator.hasNext()){

                cartLineErrors = keySetIterator.next();
            }

            while (keySetIterator.hasNext()){

                cartLineErrors = cartLineErrors + ";" + keySetIterator.next() ;
            }

            request.getSession().setAttribute("cartLineErrors", cartLineErrors);

            //is current page userFavorite
            UserFavorite fav = userService.getFavorite(session.getUserId(), currPage.replace("/web",""));
            request.getSession().setAttribute("favorite", fav);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
    }    
    
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {
    }
}