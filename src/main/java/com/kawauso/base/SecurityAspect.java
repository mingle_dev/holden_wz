/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base;

import com.google.gson.Gson;
import com.kawauso.base.bean.JsonResult;
import com.kawauso.base.bean.Session;
import com.kawauso.base.bean.Session.Role;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author chris.weaver
 */
@Aspect
public class SecurityAspect
{
    private static final Logger log = Logger.getLogger( SecurityAspect.class );

    @Around("within (com.kawauso.base.controller..*)")
    public Object security(ProceedingJoinPoint pjp) throws Throwable 
    {
        log.debug("security() begin");

        try {
            MethodSignature methodSignature = (MethodSignature)pjp.getSignature();
            Method targetMethod = methodSignature.getMethod();
            SecurityAnnotation annotation = targetMethod.getAnnotation(SecurityAnnotation.class);

            boolean isMvcResponse = targetMethod.getReturnType().isAssignableFrom(ModelAndView.class);
            //todo delete call below: ServletRequestAttributes junk = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            ServletRequestAttributes junk = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            //ensure annotation is set on method before continuing
            if(annotation != null) {
                
                //is login required?
                if(annotation.requireLogin() == true) {
                    
                    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
                    HttpSession session = attr.getRequest().getSession(false);
                    
                    Session userSession = (Session) session.getAttribute("session");

                    if(userSession == null || userSession.getCustomerNumber() == null)
                    {
                        if(isMvcResponse) {
                            ModelMap model = new ModelMap();
                            model.put("message", "You must be logged in to access this section.");       
                            return new ModelAndView("/user/login", model);
                        }else{
                            JsonResult res = new JsonResult();

                            res.setStatus(false);
                            res.setMessage("You must first be logged on");
                            return new Gson().toJson(res);
                        }
                    }                    
                    
                    Role[] roles = annotation.allowedRole();

                    //test for security role(s)
                    boolean roleFound = false;
                    if(roles.length == 1 && roles[0].equals(Role.ANONYMOUS)) {
                        roleFound = true;
                    }else{
                        for(int i=0; i<roles.length; i++)
                            if(roles[i].equals(userSession.getType()))
                                roleFound = true;
                    }
                    
                    if( roleFound == false ) {
                        if(isMvcResponse) {
                            ModelMap model = new ModelMap();
                            model.put("messageTitle", "Unauthorized");
                            model.put("messageNum", "401");
                            model.put("message", "You do not have the appropriate security to access this section.");       
                            return new ModelAndView("/error/service", model);
                        }else{
                            JsonResult res = new JsonResult();
                            res.setStatus(false);
                            res.setMessage("Unauthorized Access");
                            return new Gson().toJson(res);
                        }
                    }
                }
            }
            
        }catch(Exception ex) {
            log.error(ex);
            
            //@this could cause some problems if exception caught and return type is String
            ModelMap model = new ModelMap();
            model.put("messageTitle", "Critical Error");
            model.put("messageNum", "500");
            model.put("message", "A critical error has occurred. Close your browser and try again. If the problem persists, please contact support.");       
            return new ModelAndView("/error/service", model);            
        }
        
        Object output = pjp.proceed();
        
        log.debug("security() end");
        
        return output;
    }
}
