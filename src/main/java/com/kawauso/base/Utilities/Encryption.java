/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.Utilities;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author chris.weaver
 */
public class Encryption
{
    public static String DES3Encrypt(String input, String key) throws Exception
    {
        SecretKey sk = new SecretKeySpec(key.getBytes(), "DESede");
    	IvParameterSpec iv = new IvParameterSpec(new byte[8]);
    	
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, sk, iv);
        
        //encrypt
        byte[] encrypted = cipher.doFinal(input.getBytes("UTF-8"));

        //encode
        byte[] encoded = Base64.encodeBase64(encrypted); //encrypted

        return new String(encoded, "UTF-8");
    }
    
    public static String DES3Decrypt(String input, String key) throws Exception
    {
        SecretKey ks = new SecretKeySpec(key.getBytes(), "DESede");
    	IvParameterSpec iv = new IvParameterSpec(new byte[8]);
    	
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

        //decode
        byte[] decoded = Base64.decodeBase64(input);
        
        //decrypt
        cipher.init(Cipher.DECRYPT_MODE, ks, iv);
        byte[] decrypted = cipher.doFinal(decoded);

        return new String(decrypted, "UTF-8");
    }
    
    public static String DES3GenKey()
    {
        Random rand = new Random();
        String letters[] = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        
        String key = "";
        for(int i=0; i<24; i++) {
            int r = rand.nextInt(16);
            key += letters[r];
        }
        
        return key;
    }
    
    public static String SHA1(String s) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(s.getBytes("iso-8859-1"), 0, s.length());
        sha1hash = md.digest();

        return convertToHex(sha1hash);
    }

    private static String convertToHex(byte[] data)
    {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        }
        return buf.toString();
    }
}