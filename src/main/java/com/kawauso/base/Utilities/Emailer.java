/*
 * Emailer.java
 *
 * Created on January 29, 2007, 11:22 AM
 *
 * @author chrisw
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.kawauso.base.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Emailer {

    private String smtpServer;
    private List<String> to, cc, bcc;
    private String subject, message;
    private String fromAddress, fromName;
    private boolean isHtml;
    private int smtpPort;
    private String username="its@mingledorffs.com";
    private String password="M1ngledorffs";

    public int getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(int smtpPort) {
        this.smtpPort = smtpPort;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /** Creates a new instance of Emailer */
    public Emailer(String smtpServer)
    {
        this.smtpServer = smtpServer;
        this.to = new ArrayList<String>();
        this.cc = new ArrayList<String>();
        this.bcc = new ArrayList<String>();
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public void addToAddress(String s) {
        //make sure there is something there
        //if( s != null && !s.equals("") )
        if(s != null && s.length() > 0)
            this.to.add(s);
    }

    public void addCcAddress(String s) {
        //make sure there is something there
        if(s != null && s.length() > 0)
            this.cc.add(s);
    }

    public void addBccAddress(String s) {
        //make sure there is something there
        if(s != null && s.length() > 0)
            this.bcc.add(s);
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * sends an email message
     *
     * @param recipients String[]
     * @param subject String
     * @param message String
     * @param from String
     * @throws MessagingException
     */
    public void sendMessage()  throws Exception
    {
        //boolean debug = false;

        //Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        // create some properties and get the default Session
        Session session = Session.getInstance(props, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
                //session.setDebug(debug);

                // create a message
        Message msg = new MimeMessage(session);
        
        if(isHtml)
            msg.setHeader("Content-Type", "text/html; charset=UTF-8");
        else
            msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
        // set the from address
        if(fromName != null)
          msg.setFrom( new InternetAddress(this.fromAddress, this.fromName) );
        else
          msg.setFrom( new InternetAddress(this.fromAddress) );

        // set the to address(es)
        for (int i = 0; i < this.to.size(); i++)
            msg.addRecipient( Message.RecipientType.TO, new InternetAddress( this.to.get(i) ) );

        // set the cc address(es)
        for (int i = 0; i < this.cc.size(); i++)
            msg.addRecipient( Message.RecipientType.CC, new InternetAddress( this.cc.get(i) ) );

        // set the bcc address(es)
        for (int i = 0; i < this.bcc.size(); i++)
            msg.addRecipient( Message.RecipientType.BCC, new InternetAddress( this.bcc.get(i) ) );

        // Optional : You can also set your custom headers in the Email if you Want
        //msg.addHeader("MyHeaderName", "myHeaderValue");

        // Setting the Subject and Content Type
        msg.setSubject(this.subject);

        if(isHtml)
            msg.setContent(this.message, "text/html");
        else
            msg.setContent(this.message, "text/plain");

        //Transport tr = session.getTransport("smtp");
        //tr.connect(smtpServer,smtpPort, username, password);
        //msg.saveChanges();
        //tr.sendMessage(msg, msg.getAllRecipients());
        //tr.close();
        Transport.send(msg);
    }

    /**
     * @return the isHtml
     */
    public boolean isIsHtml() {
        return isHtml;
    }

    /**
     * @param isHtml the isHtml to set
     */
    public void setIsHtml(boolean isHtml) {
        this.isHtml = isHtml;
    }
}