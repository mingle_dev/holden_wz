package com.kawauso.base.service;

import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.User;
import com.kawauso.base.bean.UserFavorite;
import com.kawauso.base.dataobject.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService
{
    @Autowired
    UserDao userDao;

    //@Autowired
    //private MongoTemplate mongoTemplate; //connector to portal database
    
    @Autowired
    private MongoTemplate mongoInflux; //connector to Influx database (might merge with portal)
    
    @Override
    public void saveUser(User user, boolean encyrptPassword)
    {
        userDao.saveUser(user, encyrptPassword);
    }

    @Override
    public User getUser(String userId)
    {
        return userDao.getUser(userId);
    }
    
    @Override
    public User getUser(String username, String password)
    {
        return userDao.getUser(username, password);
    }

    @Override
    public User getUser(String username, String password, int cono)
    {
        return userDao.getUser(username, password, cono);
    }

    @Override
    public User getUserFromToken(String token) { return userDao.getUserFromToken(token); }
    @Override
    public boolean validUserName(String username)
    {
        return userDao.validUserName(username);
    }
    
    @Override
    public String getContactId(BigDecimal custno, String cono, String firstName, String lastName)
    {
        return userDao.getContactId(custno, cono, firstName, lastName);
    }
    
    @Override
    public void saveContact(Contact contact)
    {
        userDao.saveContact(contact);
    }   
    
    @Override
    public Contact getContact(String contactId)
    {
        return userDao.getContact(contactId);
    }
    
    @Override
    public String getUserGroup(String groupId)
    {
        return userDao.getUserGroup(groupId);
    }
    
    @Override
    public com.influx.bean.User getInfluxById(String userId)
    {
        return mongoInflux.findById(userId, com.influx.bean.User.class, "user_authorization");
    }

    @Override
    public List<UserFavorite> getFavorites(String userId)
    {
        return userDao.getFavorites(userId);
    }

    @Override
    public UserFavorite getFavorite(String id)
    {
        return userDao.getFavorite(id);
    }


    @Override
    public UserFavorite getFavorite(String userId, String url)
    {
        return userDao.getFavorite(userId, url);
    }

    @Override
    public void addFavorite(String userId, String url, String name, String linkIcon)
    {
        userDao.addFavorite(userId, url, name, linkIcon);
    }

    @Override
    public void removeFavorite(String id)
    {
        userDao.removeFavorite(id);
    }

}