package com.kawauso.base.service;

import com.kawauso.base.bean.Location;
import com.kawauso.base.bean.LocationV18;

import java.math.BigDecimal;
import java.util.List;

public interface LocationService 
{
    List<Location> findAll();

    List<LocationV18> getLocations(BigDecimal custno, int cono);

    Location getLocation(String whseId);

    Double[] getCoordinates(int zipCd);

    Double[] getCoordinates(String city, String state);
}
