/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.bean.*;
import com.kawauso.base.dataobject.hibernate.PricebookDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional
public class PricebookServiceImpl implements PricebookService
{
    @Autowired
    PricebookDao pricebookDao;

    @Autowired
    ProductService productService;

    @Override
    public List<PricebookTemplate> getTemplates(String type, String[] categories)
    {
        return pricebookDao.getTemplates(type, categories);
    }
    
    @Override
    public void run(BigDecimal custno, String shipto, String emailAddr, String type, String name, String[] templates) throws Exception
    {
        pricebookDao.run(custno, shipto, emailAddr, type, name, templates);
    }

    @Override
    public String getActiveRetailProfileId(int cono, BigDecimal custno)
    {
        return pricebookDao.getActiveRetailProfileId(cono, custno);
    }

    @Override
    public void updateImage(String id, String type, String data)
    {
        pricebookDao.updateImage(id, type, data);
    }

    @Override
    public List<MenuCategory> getTemplateNames(int limit)
    {
        List<MenuCategory> categories = new ArrayList<MenuCategory>();

        for(Object[] item : pricebookDao.getTemplateNames(limit)) {
            MenuCategory cat = new MenuCategory();
            cat.setId(ELUtilities.base64Encode(item[0].toString()));
            cat.setName(item[0].toString());
            cat.setImage( item[1] != null ? item[1].toString() : null );
            categories.add(cat);
        }

        return categories;
    }

    @Override
    public List<MenuCategory> getSheetNames(String tid)
    {
        List<MenuCategory> categories = new ArrayList<MenuCategory>();

        for(Object[] item : pricebookDao.getSheetNames(tid)) {
            MenuCategory cat = new MenuCategory();
            cat.setId(ELUtilities.base64Encode(item[0].toString()));
            cat.setName(item[0].toString());
            cat.setImage( item[1] != null ? item[1].toString() : null);
            categories.add(cat);
        }

        return categories;
    }

    @Override
    public List<Product> getItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid)
    {
        List<Product> products = pricebookDao.getProducts(tid, sid);

        //fetch whse price/avail
        if(products != null && custno != null && whse != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            products = pd.getProducts(cono, oper, custno, shipTo, whse, products);
        }

        productService.applyNetAvail(cono, oper, products);

        return products;
    }

    @Override
    public List<Product> getSupersedeItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid)
    {
        List<Product> products = pricebookDao.getSupersedeProducts(cono, oper, tid, sid);

        //fetch whse price/avail
        if(products != null && custno != null && whse != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            products = pd.getProducts(cono, oper, custno, shipTo, whse, products);
        }

        productService.applyNetAvail(cono, oper, products);

        return products;
    }

    @Override
    public List<Product> getAllItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid)
    {
        List<Product> products = pricebookDao.getAllProducts(tid, sid);

        //fetch whse price/avail
        if(products != null && custno != null && whse != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            products = pd.getProducts(cono, oper, custno, shipTo, whse, products);
        }

        productService.applyNetAvail(cono, oper, products);

        return products;
    }

    @Override
    public RetailPricebookProfile getRetailProfile(String profileId) throws Exception
    {
        return pricebookDao.getRetailProfile(profileId);
    }

    @Override
    public List<RetailPricebookProfile> getRetailProfiles(int cono, BigDecimal custno) throws Exception
    {
        return pricebookDao.getRetailProfiles(cono, custno);
    }

    @Override
    public List<RetailPricebookItemGroup> getRetailItemGroups(int cono, String itemGroupId, String[] rootCategories) throws Exception
    {
        return pricebookDao.getRetailItemGroups(cono, itemGroupId, rootCategories);
    }

    @Override
    public void disableRetailProfiles(int cono, BigDecimal custno) throws Exception
    {
        pricebookDao.disableRetailProfiles(cono, custno);
    }

    @Override
    public void enableRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception
    {
        pricebookDao.enableRetailProfile(cono, custno, profileId);
    }

    @Override
    public void deleteRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception
    {
        pricebookDao.deleteRetailProfile(cono, custno, profileId);
    }

    @Override
    public void cloneRetailPricebookProfile(String profileId) throws Exception
    {
        pricebookDao.cloneRetailPricebookProfile(profileId);
    }

    @Override
    public void createRetailProfile(int cono, BigDecimal custno, String profileName, double taxRate, Map<String, String[]> map) throws Exception
    {
        pricebookDao.createRetailProfile(cono, custno, profileName, taxRate, map);
    }

    @Override
    public void updateRetailProfile(String profileId, BigDecimal custno, String profileName, double taxRate, HashMap<String, String[]> map) throws Exception
    {
        pricebookDao.updateRetailProfile(profileId, custno, profileName, taxRate, map);
    }
}