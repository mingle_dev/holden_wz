package com.kawauso.base.service;

import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.User;
import com.kawauso.base.bean.UserFavorite;

import java.math.BigDecimal;
import java.util.List;

public interface UserService 
{
    void saveUser(User user, boolean encyrptPassword);

    User getUser(String userId);

    User getUser(String username, String password);

    User getUser(String username, String password, int cono);

    User getUserFromToken(String token);

    boolean validUserName(String username);
    
    String getContactId(BigDecimal custno, String cono, String firstName, String lastName);

    Contact getContact(String contactId);

    void saveContact(Contact contact);

    String getUserGroup(String groupId);
    
    com.influx.bean.User getInfluxById(String userId);

    List<UserFavorite> getFavorites(String userId);

    UserFavorite getFavorite(String id);

    UserFavorite getFavorite(String userId, String url);

    void addFavorite(String userId, String url, String name, String linkIcon);

    void removeFavorite(String id);
}
