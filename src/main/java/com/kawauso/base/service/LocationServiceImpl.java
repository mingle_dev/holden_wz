package com.kawauso.base.service;

import com.kawauso.base.bean.Location;
import com.kawauso.base.bean.LocationV18;
import com.kawauso.base.dataobject.hibernate.LocationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LocationServiceImpl implements LocationService
{
    @Autowired
    LocationDao locationDao;

    @Override
    public List<Location> findAll()
    {
        return locationDao.findAll();
    }

    @Override
    public List<LocationV18> getLocations(BigDecimal custno, int cono)
    {
        return locationDao.getLocations(custno, cono);
    }
    
    @Override
    public Location getLocation(String whseId)
    {
        return locationDao.getLocation(whseId);
    }
    
    @Override
    public Double[] getCoordinates(int zipCd) {
        return locationDao.getCoordinates(zipCd);
    }
    
    @Override
    public Double[] getCoordinates(String city, String state) {
        return locationDao.getCoordinates(city, state);
    }    
}