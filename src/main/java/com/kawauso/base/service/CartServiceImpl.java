/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.influx.bean.SyncCart;
import com.kawauso.base.bean.Cart;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.ShipVia;
import com.kawauso.base.dataobject.hibernate.CartDao;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class CartServiceImpl implements CartService
{
    @Autowired
    CartDao cartDao;

    @Autowired
    private MongoTemplate mongoInflux; //connector to Influx database (might merge with portal)

    @Override
    public void saveItem(String userId, String productCode, String note, int qty, boolean overrideQty, BigDecimal price)
    {
        cartDao.saveItem(userId, productCode, note, qty, overrideQty, price);
    }
    
    @Override
    public List<CartItem> getCartItems(String userId)
    {
        return cartDao.getCartItems(userId);
    }
    
    @Override
    public boolean clear(String userId) {
        return cartDao.clear(userId);
    }
    
    @Override
    public List<ShipVia> getShipViaList()
    {
        return cartDao.getShipViaList();
    }
    
    @Override
    public Cart getCart(String userId)
    {
        return cartDao.getCart(userId);
    }

    @Override
    public String saveCart(Cart cart) {
        return cartDao.saveCart(cart);
    }

    @Override
    public SyncCart getExternalCart(String id)
    {
        return mongoInflux.findById(id, SyncCart.class, "external_cart");
    }
}