/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chris.weaver
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService
{
    @Autowired
    com.kawauso.base.dataobject.hibernate.OrderDao orderDao;

    @Autowired
    com.kawauso.base.dataobject.hibernate.AccountDao accountDao;

    @Override
    public Account getAccountInfo(int cono, String oper, BigDecimal custno) throws Exception
    {
        com.kawauso.base.dataobject.sx.AccountDao ad = new com.kawauso.base.dataobject.sx.AccountDao();
        return ad.getAccountInfo(cono, oper, custno);
    }

    @Override
    public List<Order> getAllOrders(int cono, BigDecimal custno)
    {
        return orderDao.getAll(cono, custno);
    }

    @Override
    public List<Order> getOrders(int cono, BigDecimal custno, int order, String whse, String po, String prod, String serial, Date fromDt, Date toDt, String type, int stage, int page, int limit)
    {
        return orderDao.search(cono, custno, order, whse, po, prod, serial, fromDt, toDt, type, stage, page, limit);
    }

    @Override
    public List<Order> getOrders(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit, String filter_typ, Date fromDt, Date toDt)
    {
        return orderDao.search(cono, custno, term, type, stage, sort, page, limit, filter_typ, fromDt, toDt);
    }

    @Override
    public List<Order> getOrders(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit)
    {
        return orderDao.search(cono, custno, term, type, stage, sort, page, limit);
    }

    //Chris originally wanted to merge the two getOrder methods. Since the other checks for the
    //cust no, its simple to just do an overload
    @Override
    public Order getOrder(int cono, String oper, BigDecimal custno, int orderno, int ordersuf)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        Order order = dao.getOrder(cono, oper, custno, orderno, ordersuf);

        List<Object[]> list = orderDao.getSerialNumbers(order);

        for(OrderLine line : order.getOrderLines()) {
            for(Object[] obj : list) {
                try {
                    int sl = (int) obj[0];
                    if(sl == line.getLineNumber())
                        line.addSerialNumber(obj[1].toString());
                }catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return order;
    }

    @Override
    public Order getOrder(int cono, String oper, int orderno, int ordersuf)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();

        Order order = dao.getOrder(cono, oper, orderno, ordersuf);

        //fetch serial #'s from data whse and tie to order
        //my last week, i'm terribly lazy...
        List<Object[]> list = orderDao.getSerialNumbers(order);
        for(OrderLine line : order.getOrderLines()) {
            for(Object[] obj : list) {
                try {
                    int sl = (int) obj[0];
                    if(sl == line.getLineNumber())
                        line.addSerialNumber(obj[1].toString());
                }catch(Exception ex) {}
            }
        }

        return order;
    }

    @Override
    public int getSearchResultCount()
    {
        return orderDao.getSearchResultCount();
    }

    @Override
    public List<Order> getPeriodOrders(int cono, String oper, BigDecimal custno, int period)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.getPeriodOrders(cono, oper, custno, period);
    }

    @Override
    public List<ShipTo> getShipToList(int cono, String oper, BigDecimal custno, boolean includeMain)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.getShipToList(cono, oper, custno, includeMain);
    }

    @Override
    public ShipTo getShipTo(int cono, String oper, BigDecimal custno, String shipto)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.getShipTo(cono, oper, custno, shipto);
    }

    @Override
    public Map<String, Object> getOrderingInfo(int cono, String oper, BigDecimal custno)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.getOrderingInfo(cono, oper, custno);
    }

    @Override
    public OrderResults placeOrder(int cono, String oper, String userName, BigDecimal custno, Cart cart, List<CartItem> lines) throws Exception
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.placeOrder(cono, oper, userName, custno, cart, lines);
    }

    @Override
    public List<Account> getAccounts(int cono, String oper, String key, String val, int limit)
    {
        com.kawauso.base.dataobject.sx.AccountDao dao = new com.kawauso.base.dataobject.sx.AccountDao();
        return dao.getAccounts(cono, oper, key, val, limit);
    }

    @Override
    public SalesRep getSalesRep(int cono, String slsRepId)
    {
        return accountDao.getSalesRep(cono, slsRepId);
    }


    public List<Account> getAccountsforSalesRep(int cono, String name)
    {
        return accountDao.getAccountsforSalesRep(cono, name);
    }

    public List<Account> getAccounts(int cono, String key, String val, int limit)
    {
        return accountDao.getAccounts(cono, key, val, limit);
    }

    @Override
    public List<String[]> getAccounts(int cono, String term) {
        return accountDao.getAccounts(cono, term);
    }

    @Override
    public List<Contact> getContacts(String custno, String term) { return accountDao.getContacts(custno, term); }
}