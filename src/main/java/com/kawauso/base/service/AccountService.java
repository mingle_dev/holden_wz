/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.Cart;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.Order;
import com.kawauso.base.bean.OrderResults;
import com.kawauso.base.bean.SalesRep;
import com.kawauso.base.bean.ShipTo;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chris.weaver
 */
public interface AccountService
{
    Account getAccountInfo(int cono, String oper, BigDecimal custno) throws Exception;

    List<Order> getAllOrders(int cono, BigDecimal custno);

    List<Order> getOrders(int cono, BigDecimal custno, int order, String whse, String po, String prod, String serial, Date fromDt, Date toDt, String type, int stage, int page, int limit);

    List<Order> getOrders(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit);

    List<Order> getOrders(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit, String filter_typ, Date fromDt, Date toDt);

    Order getOrder(int cono, String oper, int orderno, int ordersuf);

    Order getOrder(int cono, String oper, BigDecimal custno, int orderno, int ordersuf);

    List<Order> getPeriodOrders(int cono, String oper, BigDecimal custno, int period);

    ShipTo getShipTo(int cono, String oper, BigDecimal custno, String shipto);

    List<ShipTo> getShipToList(int cono, String oper, BigDecimal custno, boolean includeMain);

    Map<String, Object> getOrderingInfo(int cono, String oper, BigDecimal custno);

    int getSearchResultCount();

    SalesRep getSalesRep(int cono, String slsRepId);

    List<Account> getAccountsforSalesRep(int cono, String name);

    List<Account> getAccounts(int cono, String key, String val, int limit);

    OrderResults placeOrder(int cono, String oper, String userName, BigDecimal custno, Cart cart, List<CartItem> lines) throws Exception;

    List<Account> getAccounts(int cono, String oper, String key, String val, int limit);

    List<String[]> getAccounts(int cono, String term);

    List<Contact> getContacts(String custno, String term);
}