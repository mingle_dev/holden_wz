/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.JobApplication;
import com.kawauso.base.bean.Position;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris.weaver
 */
@Repository
public class CareerServiceImpl implements CareerService
{
    @Autowired
    private MongoTemplate mongoTemplate;
     
    public static final String COLLECTION_NAME = "career_position";
     
    @Override
    public String add(Position position)
    {
        String uuid = UUID.randomUUID().toString();
        
        if(!mongoTemplate.collectionExists(COLLECTION_NAME))
            mongoTemplate.createCollection(COLLECTION_NAME);
        
        position.setId(uuid);
        mongoTemplate.insert(position, COLLECTION_NAME);
        
        return uuid;
    }
     
    @Override
    public String addApplicant(JobApplication application)
    {
        String uuid = UUID.randomUUID().toString();
        
        if(!mongoTemplate.collectionExists("career_application"))
            mongoTemplate.createCollection("career_application");
        
        application.setId(uuid);
        mongoTemplate.insert(application, "career_application");
        
        return uuid;
    }    
    
    @Override
    public JobApplication getApplicant(String id)
    {
        return mongoTemplate.findById(id, JobApplication.class, "career_application");
    }
    
    @Override
    public List<Position> list() {
        return mongoTemplate.findAll(Position.class, COLLECTION_NAME);
    }
    
    @Override
    public Position find(String id)
    {
        return mongoTemplate.findById(id, Position.class, COLLECTION_NAME);
    }
     
    @Override
    public void delete(Position position) {
        mongoTemplate.remove(position, COLLECTION_NAME);
    }
     
    @Override
    public void update(Position position) {
        mongoTemplate.insert(position, COLLECTION_NAME);      
    }
}