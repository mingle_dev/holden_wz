/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.JobApplication;
import com.kawauso.base.bean.Position;
import java.util.List;

/**
 *
 * @author chris.weaver
 */
public interface CareerService
{
    String add(Position position);

    String addApplicant(JobApplication application);

    JobApplication getApplicant(String id);

    List<Position> list();

    Position find(String id);

    void delete(Position position);

    void update(Position position);
}