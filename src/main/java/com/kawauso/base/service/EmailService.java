package com.kawauso.base.service;

/**
 * Created by cbyrd on 5/4/15.
 */
public interface EmailService {

    String send(Object obj);
}
