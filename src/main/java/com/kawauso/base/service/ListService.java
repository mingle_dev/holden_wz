package com.kawauso.base.service;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.ListCategory;
import com.kawauso.base.bean.ListItem;
import java.math.BigDecimal;
import java.util.List;

public interface ListService 
{
    List<ListCategory> getLists(String userId, Integer limit, boolean includeExtra);

    ListCategory getList(String listId, boolean includeExtra);
    
    String saveList(ListCategory list);

    String updateList(ListCategory list);
            
    String renameList(String id, String name);
    
    int getItemCount(ListCategory cat);
    
    String addItem(ListItem item);

    String addItem(ListCategory item);

    boolean deleteItem(String listId, String itemId);

    boolean updateItemQty(String listId, String prod, BigDecimal qty);

    boolean deleteList(String listId);

    String cloneList(String listId);
    
    boolean isShared(String listId, BigDecimal custno);

    List<Account> getShares(String listId);

    boolean addShare(String listId, String ownerId, String accountId, int cono, BigDecimal custno);

    boolean removeShare(String listId);

    boolean removeShare(String listId, int cono, BigDecimal custno);

    List<ListCategory> getSharedLists(int cono, BigDecimal custno);
}
