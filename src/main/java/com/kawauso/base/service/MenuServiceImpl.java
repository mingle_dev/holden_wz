package com.kawauso.base.service;

import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.MenuCategory;
import com.kawauso.base.bean.Product;
import com.kawauso.base.dataobject.MenuDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MenuServiceImpl implements MenuService
{
    @Autowired
    MenuDao menuDao;

    @Override
    public List<MenuCategory> getMenu(int id)
    {
        return menuDao.getMenu(id);
    }
    
    @Override
    public List<Breadcrumb> getNavTree(int id)
    {
        return menuDao.getNavTree(id);
    }
    
    @Override
    public List<Breadcrumb> getNavTree(String productCode)
    {
        return menuDao.getNavTree(productCode);
    }
    
    @Override
    public List<Product> getMenuItems(int id)
    {
        return menuDao.getMenuItems(id);
    }
}