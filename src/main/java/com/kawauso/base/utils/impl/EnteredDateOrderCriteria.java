package com.kawauso.base.utils.impl;

import com.kawauso.base.bean.Order;
import com.kawauso.base.interfaces.OrderCriteria;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by twall on 9/15/15.
 */
public class EnteredDateOrderCriteria implements OrderCriteria{

    @Override
    public List<Order> meetCriteria(List<Order> orders, Date fromDt, Date toDt) {

        List<Order> entered_dates_filter = new ArrayList<Order>();

        for (Order order : orders) {

            //convert the string date of the order to a date object
            Date orderDt = order.getEnterDt();
            System.out.println("trying to push code using mercurial");
            //create a date object representing to date
            //create a date object representing from date
            if(orderDt.after(fromDt) && orderDt.before(toDt)){
                entered_dates_filter.add(order);
            }
        }
        return entered_dates_filter;
    }
}
