package com.kawauso.base.utils.dto;

/**
 * Created by cbyrd on 4/29/15.
 */
public interface TrainingEmailService {


    String send(TrainingEmail trainingEmail);

}
