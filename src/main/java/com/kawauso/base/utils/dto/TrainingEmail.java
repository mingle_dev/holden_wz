package com.kawauso.base.utils.dto;

/**
 * Created by cbyrd on 4/29/15.
 */
public class TrainingEmail {

    private String recipientName;
    private String recipientEmailAddr;
    private String senderName;
    private String senderEmailAddr;
    private String hostName;

    public String getSenderName() {
        return senderName;
    }

    private String message;

    public String getRecipientName() {
        return recipientName;
    }

    public String getRecipientEmailAddr() {
        return recipientEmailAddr;
    }

    public String getSenderEmailAddr() {
        return senderEmailAddr;
    }

    public String getHostName() {
        return hostName;
    }

    public String getMessage() {
        return message;
    }


    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public void setRecipientEmailAddr(String recipientEmailAddr) {
        this.recipientEmailAddr = recipientEmailAddr;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setSenderEmailAddr(String senderEmailAddr) {
        this.senderEmailAddr = senderEmailAddr;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
