/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.form;

/**
 *
 * @author chris.weaver
 */
public class ProfileForm
{
    private String customerNumber;
    private String warehouse;
    private String jobAccount;
    private String shipVia;
    private String emailAddr;
    private String officePhone;
    private String availType;
    private int pageLimit;
    private boolean resetWhse;
    private String shipNotifyType;
    
    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse the warehouse to set
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the jobAccount
     */
    public String getJobAccount() {
        return jobAccount;
    }

    /**
     * @param jobAccount the jobAccount to set
     */
    public void setJobAccount(String jobAccount) {
        this.jobAccount = jobAccount;
    }

    /**
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * @param shipVia the shipVia to set
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * @param emailAddr the emailAddr to set
     */
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    /**
     * @return the officePhone
     */
    public String getOfficePhone() {
        return officePhone;
    }

    /**
     * @param officePhone the officePhone to set
     */
    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    /**
     * @return the pageLimit
     */
    public int getPageLimit() {
        return pageLimit;
    }

    /**
     * @param pageLimit the pageLimit to set
     */
    public void setPageLimit(int pageLimit) {
        this.pageLimit = pageLimit;
    }

    /**
     * @return the resetWhse
     */
    public boolean isResetWhse() {
        return resetWhse;
    }

    /**
     * @param resetWhse the resetWhse to set
     */
    public void setResetWhse(boolean resetWhse) {
        this.resetWhse = resetWhse;
    }

    /**
     * @return the shipNotifyType
     */
    public String getShipNotifyType() {
        return shipNotifyType;
    }

    /**
     * @param shipNotifyType the shipNotifyType to set
     */
    public void setShipNotifyType(String shipNotifyType) {
        this.shipNotifyType = shipNotifyType;
    }

    /**
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * @param customerNumber the customerNumber to set
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getAvailType() {
        return availType;
    }

    public void setAvailType(String availType) {
        this.availType = availType;
    }
}
