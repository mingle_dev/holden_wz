/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.form;

import com.kawauso.base.bean.Cart;

/**
 *
 * @author chris.weaver
 */
public class CartForm extends Cart
{
    private String step;
    private int shipMonth;
    private int shipDay;
    private int shipYear;

    /**
     * @return the shipMonth
     */
    public int getShipMonth() {
        return shipMonth;
    }

    /**
     * @param shipMonth the shipMonth to set
     */
    public void setShipMonth(int shipMonth) {
        this.shipMonth = shipMonth;
    }

    /**
     * @return the shipDay
     */
    public int getShipDay() {
        return shipDay;
    }

    /**
     * @param shipDay the shipDay to set
     */
    public void setShipDay(int shipDay) {
        this.shipDay = shipDay;
    }

    /**
     * @return the shipYear
     */
    public int getShipYear() {
        return shipYear;
    }

    /**
     * @param shipYear the shipYear to set
     */
    public void setShipYear(int shipYear) {
        this.shipYear = shipYear;
    }

    /**
     * @return the step
     */
    public String getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(String step) {
        this.step = step;
    }
}
