/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.form;

import java.util.List;

/**
 *
 * @author chris
 */
public class CheckboxList
{
    private List<String> checkboxItems;

    public List<String> getCheckboxItems() {
        return checkboxItems;
    }

    public void setCheckboxItems(List<String> checkboxItems) {
        this.checkboxItems = checkboxItems;
    }
}