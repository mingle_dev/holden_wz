/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.form;

/**
 *
 * @author chris.weaver
 */
public class RegisterForm
{
    private String firstName, lastName, phoneNo, emailAddr;
    private String username, password1, password2;
    private String cono, companyName, accountNo, invoice1, invoice2, statementToken;
    private String tos;

    public String getCono() {
        return cono;
    }

    public void setCono(String cono) {
        this.cono = cono;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * @param phoneNo the phoneNo to set
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * @param emailAddr the emailAddr to set
     */
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password1
     */
    public String getPassword1() {
        return password1;
    }

    /**
     * @param password1 the password1 to set
     */
    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    /**
     * @return the password2
     */
    public String getPassword2() {
        return password2;
    }

    /**
     * @param password2 the password2 to set
     */
    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the invoice1
     */
    public String getInvoice1() {
        return invoice1;
    }

    /**
     * @param invoice1 the invoice1 to set
     */
    public void setInvoice1(String invoice1) {
        this.invoice1 = invoice1;
    }

    /**
     * @return the invoice2
     */
    public String getInvoice2() {
        return invoice2;
    }

    /**
     * @param invoice2 the invoice2 to set
     */
    public void setInvoice2(String invoice2) {
        this.invoice2 = invoice2;
    }

    /**
     * @return the statementToken
     */
    public String getStatementToken() {
        return statementToken;
    }

    /**
     * @param statementToken the statementToken to set
     */
    public void setStatementToken(String statementToken) {
        this.statementToken = statementToken;
    }

    /**
     * @return the tos
     */
    public String getTos() {
        return tos;
    }

    /**
     * @param tos the tos to set
     */
    public void setTos(String tos) {
        this.tos = tos;
    }
    
    
}
