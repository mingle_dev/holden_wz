package com.kawauso.base.dataobject;

import java.math.BigDecimal;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("bluedartDao")
public class BluedartDaoImpl implements BluedartDao
{
    @Autowired
    private SessionFactory sessionfactory;

    @Override
    public String getAccountId(int cono, BigDecimal custno)
    {
        String sql = "SELECT id FROM accounts WHERE cono=:cono AND accountno=:custno";
        Session session = sessionfactory.getCurrentSession();

        SQLQuery query = session.createSQLQuery(sql);
        query.addScalar("id", Hibernate.STRING);
        query.setInteger("cono", cono);
        query.setBigDecimal("custno", custno);

        return (String) query.uniqueResult();
    }
}