/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.sx;

import com.infor.sxapi2.callobject.ICallObject;
import com.infor.sxapi2.paramclass.ICGetOptionalProductList.input.ICGetOptionalProductListRequest;
import com.infor.sxapi2.paramclass.ICGetOptionalProductList.output.ICGetOptionalProductListResponse;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataCosts.input.ICGetWhseProductDataCostsRequest;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataCosts.output.ICGetWhseProductDataCostsResponse;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.input.ICGetWhseProductDataGeneralV2Request;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.output.ICGetWhseProductDataGeneralV2Response;
import com.infor.sxapi2.paramclass.ICProductAvailByWhse.input.ICProductAvailByWhseRequest;
import com.infor.sxapi2.paramclass.ICProductAvailByWhse.output.ICProductAvailByWhseResponse;
import com.infor.sxapi2.paramclass.ICProductMnt.input.FieldModification;
import com.infor.sxapi2.paramclass.ICProductMnt.input.ICProductMntRequest;
import com.infor.sxapi2.paramclass.ICProductMnt.output.ICProductMntResponse;
import com.infor.sxapi2.paramclass.OEPricingMultiple.input.OEPricingMultipleRequest;
import com.infor.sxapi2.paramclass.OEPricingMultiple.output.OEPricingMultipleResponse;
import com.infor.sxapi2.paramclass.OEPricingV4.input.OEPricingV4Request;
import com.infor.sxapi2.paramclass.OEPricingV4.output.OEPricingV4Response;
import com.infor.sxapi2.paramclass.SxApiAuthenticationException;
import com.infor.sxapi2.paramclass.SxApiAuthorizationException;
import com.infor.sxapi2.paramclass.SxApiConnectionException;
import com.infor.sxapi2.paramclass.SxApiGeneralException;
import com.infor.sxapi2.proxy.ApiAppObj;
import com.infor.sxapi2.services.callobject.*;
import com.kawauso.base.bean.Availability;
import com.kawauso.base.bean.Product;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author chris.weaver
 */
public class ProductDao extends SxApiHandler
{
    private static Logger log = Logger.getLogger( ProductDao.class );
    
    public Product getProduct(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode)
    {
        ICallObject connection = null;
        Product p = null;

        try {
            connection = getPool().getPooledConnection();


            p = new Product();
            
            String priceWhse = null;
            
            //get a list of whses where product is available
            List<Availability> availList = getAvailability(cono, oper, productCode, true);
            
            //iterate list to see if product exists in requested whse
            for(Availability avail : availList)
                if(avail.getWhse().equalsIgnoreCase(whse))
                    priceWhse = avail.getWhse();

            //if not exists, use first found, unless empty
            if(priceWhse == null && !availList.isEmpty())
                priceWhse = availList.get(0).getWhse();

            //if prod doesn't exist in any whse, return null
            if(priceWhse == null) {
                if (connection != null)
                    getPool().returnPooledConnection(connection);
                return null;
            }

            //fetch a price
            OEPricingV4Request req = new OEPricingV4Request();
            req.setCustomerNumber(custno);
            req.setShipTo( (shipTo == null) ? "" : shipTo );
            req.setWarehouse(whse);
            req.setProductCode(productCode);

            OEPricingV4 obj = new OEPricingV4();
            OEPricingV4Response res = (OEPricingV4Response) connection.getResponse(req);
            
            p.setProduct( productCode );
            p.setPrice( res.getPrice() );
            p.setPriceWhse(priceWhse);
            p.setAvail(res.getNetAvailable());

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getProduct: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }
        
        return p;            
    }

    public List<Product> getProducts(int cono, String oper, BigDecimal custno, String shipTo, String whse, List<Product> products)
    {
        ICallObject connection = null;

        try {
            connection = getPool().getPooledConnection();

            OEPricingMultipleRequest req = new OEPricingMultipleRequest();
            req.setCustomerNumber(custno);
            req.setShipTo(shipTo);
            
            //add products to sxapi product class
            List<com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product> inList = new ArrayList<com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product>(products.size());
            for(Product p : products) {
                com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product prod = new com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product();
                prod.setProductCode(p.getProduct());
                prod.setQuantity(BigDecimal.ONE);
                prod.setWarehouse(whse);
                inList.add(prod);
            }
            req.setListProduct(inList);
            
            OEPricingMultiple obj = new OEPricingMultiple();
            OEPricingMultipleResponse res = (OEPricingMultipleResponse) connection.getResponse(req);
            
            //tie pricing back to products
            List<com.infor.sxapi2.paramclass.OEPricingMultiple.output.Price> prices = res.getListPrice();
            for(com.infor.sxapi2.paramclass.OEPricingMultiple.output.Price p : prices) {

                //@todo; there's a chance that the price = 0 due to not setup in pricing whse
                //follow logic in getProduct() to deal with this issue
                
                int idx = products.indexOf( new Product( cono, p.getProductCode() ));
                if(idx >= 0) {
                    Product fp = products.get(idx);
                    fp.setPrice(p.getPrice());
                    fp.setPriceWhse(whse);                    
                    fp.setAvail(p.getNetAvailable());
                }
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            log.error(ex.getMessage());
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getProducts: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }
        
        return products;
    }
    
    public List<Availability> getAvailability(int cono, String oper, String productCode, boolean includeZeroQty)
    {
        ICallObject connection = null;
        List<Availability> list = null;

        try {
            connection = getPool().getPooledConnection();

            ICProductAvailByWhseRequest req = new ICProductAvailByWhseRequest();
            req.setProductCode(productCode);
            
            ICProductAvailByWhse obj = new ICProductAvailByWhse();
            ICProductAvailByWhseResponse res = (ICProductAvailByWhseResponse) connection.getResponse(req);
            
            list = new ArrayList<Availability>();
            
            for(com.infor.sxapi2.paramclass.ICProductAvailByWhse.output.Availability avail : res.getListAvailability()) {
                //standard warehouse (not damaged)
                if(Character.isDigit(avail.getWarehouse().charAt(0)) == true) {
                    if(includeZeroQty == true || !avail.getNetAvailable().equals(BigDecimal.ZERO)) {
                        Availability a = new Availability();
                        a.setCono(cono);
                        a.setProductCode(avail.getProductCode());
                        a.setWhse(avail.getWarehouse());
                        a.setAvailable(avail.getNetAvailable());
                        list.add(a);
                    }
                }
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getAvailability: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }
        
        return list;
    }
    
    public List<String> getAccessories(int cono, String oper, String whse, String productCode)
    {
        ICallObject connection = null;
        List<String> products = null;
        
        try {
            connection = getPool().getPooledConnection();

            ICGetOptionalProductListRequest req = new ICGetOptionalProductListRequest();
            req.setProduct(productCode);
            req.setWhse(whse);
                        
            ICGetOptionalProductList obj = new ICGetOptionalProductList();
            ICGetOptionalProductListResponse res = (ICGetOptionalProductListResponse) connection.getResponse(req);

            for(com.infor.sxapi2.paramclass.ICGetOptionalProductList.output.Product p : res.getListProduct()) {
                if(products == null)
                    products = new ArrayList<String>();
                
                products.add( p.getProd() );
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getAccessories: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }
        
        return products;
    } 
    
    public String createWarehouseProduct(int cono, String oper, String productCode, String destWhse)
    {
        String srcWhse = null;
        String results = "";

        try {
            List<Availability> whseAvail = getAvailability(cono, oper, productCode, true);

            for(Availability whse : whseAvail) {

                System.out.println("whse: " + whse);
                
                //make the first whse we find the source whse
                if(srcWhse == null)
                    srcWhse = whse.getWhse();

                //quit if we find the destination whse in the whse list. This means
                //that the product exists in the warehouse that we are trying to create it in
                if(whse.getWhse().equals(destWhse))
                    return null;
            }

        }catch(Exception ex) {
            log.error(ex);
            return null;
        }

        //doesn't exist anywhere, just quit
        if(srcWhse == null)
            return null;

        ICallObject connection = null;
        Map<String,String> keys = new HashMap<String,String>();
            
        try {
            connection = getPool().getPooledConnection();

            /*
             * get general info about item
             */
            ICGetWhseProductDataGeneralV2Request reqGen = new ICGetWhseProductDataGeneralV2Request();
            reqGen.setProduct(productCode);
            reqGen.setWhse(srcWhse);

            ICGetWhseProductDataGeneralV2Response resGen = (ICGetWhseProductDataGeneralV2Response) connection.getResponse(reqGen);
            
            keys.put( "arptype", resGen.getARPType() );
            keys.put( "arpwhse", resGen.getARPWhse() );
            keys.put( "arpvendno", String.valueOf( resGen.getVendorNumber() ) );
            keys.put( "pricetype", resGen.getPriceType() );
            keys.put( "prodline", resGen.getProductLine() );
            keys.put( "baseprice", String.valueOf( resGen.getBasePrice() ) );
            keys.put( "listprice", String.valueOf( resGen.getListPrice() ) );
            keys.put( "vendprod", resGen.getVendorProduct() );
            keys.put( "icswstatustype", "O" );
            keys.put( "serlottype", resGen.getSerialLotType()); //added to copy serial # flag

            /*
             * get item costs
             */
            ICGetWhseProductDataCostsRequest reqCost = new ICGetWhseProductDataCostsRequest();
            reqCost.setProduct(productCode);
            reqCost.setWhse(srcWhse);

            ICGetWhseProductDataCostsResponse resCost = (ICGetWhseProductDataCostsResponse) connection.getResponse(reqCost);
            
            keys.put( "stndcost", String.valueOf( resCost.getStandardCost() ) );
            keys.put( "avgcost", String.valueOf( resCost.getAverageCost() ) );
            keys.put( "replcost", String.valueOf(resCost.getReplacementCost() ) );
            keys.put("taxablety", "V"); //keys.put("taxablety", "1");
            keys.put("taxgroup", "1");  //keys.put("taxgroup", "V");           

            /*
             * create warehouse product
             */
            ICProductMntRequest reqMnt = new ICProductMntRequest();

            int i = 1;
            
            List<FieldModification> fields = new ArrayList<FieldModification>();
            Iterator it = keys.entrySet().iterator();
            while(it.hasNext())
            {
                Map.Entry pairs = (Map.Entry)it.next();

                FieldModification field = new FieldModification();
                field.setSetNumber(1);
                field.setSequenceNumber(i++);                
                field.setUpdateMode("add");
                field.setKey1(productCode);
                field.setKey2(destWhse);
                field.setFieldName( pairs.getKey().toString() );
                field.setFieldValue( pairs.getValue().toString() );
                
                fields.add(field);
            }
            reqMnt.setListFieldModification(fields);
            
            ICProductMnt obj = new ICProductMnt();
            ICProductMntResponse resMnt = (ICProductMntResponse) connection.getResponse(reqMnt);

            results += resMnt.getErrorMessage() + "\n";
            results += resMnt.getReturnData();

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("createWarehouseProduct: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return results;
    }
    public static void main (String args[]){

        try {

            //String connectionString="AppServerDC://172.0.0.6:7190/sxapiappsrv";
            String connectionString="AppServerDC://192.168.10.119:7190/sxapiappsrv";
            int appserverRetryConnectTimes=2;
            int appserverRetryConnectSleepSeconds=6;

            int cono=1;
            String oper="CUST";

            //ProductDao productDao = new ProductDao();
            //Product product = productDao.getProduct(1, "CUST", new BigDecimal(92295), null, "d61", "114cna030000");

            //System.out.println("product price: " + product.getPrice());
            com.infor.sxapi2.callobject.CallObject connectionObject = new com.infor.sxapi2.callobject.CallObject(connectionString, cono, oper, "", appserverRetryConnectTimes, appserverRetryConnectSleepSeconds);

            //fetch a price
            OEPricingV4Request req = new OEPricingV4Request();
            /*
            req.setCustomerNumber(new BigDecimal(92295))
            req.setShipTo(("" == null) ? "" : "");
            req.setWarehouse("61");
            req.setProductCode("114cna030000");
            */

            //req.setCustomerNumber(new BigDecimal(21015));
            //req.setShipTo("");
            //req.setWarehouse("32");
            req.setCustomerNumber(new BigDecimal(3));
            req.setShipTo("");
            req.setWarehouse("21");
            req.setProductCode("7057");

            OEPricingV4 obj = new OEPricingV4();
            OEPricingV4Response res = (OEPricingV4Response) connectionObject.getResponse(req);

            System.out.println("price per unit: " + res.getPrice());
            System.out.println("net available: " + res.getNetAvailable());
            System.out.println("pricing record number: " + res.getPricingRecordNumber());
            System.out.println("units per stocking text: " + res.getUnitsPerStockingText());

            if(!res.getErrorMessage().isEmpty())
                System.out.println("record has error");

            ICGetWhseProductDataGeneralV2Request req2 = new ICGetWhseProductDataGeneralV2Request();
            req2.setProduct("7057");
            req2.setWhse("21");
            System.out.println("prod: " + "7059");
            System.out.println("whse: " + "21");

            ICGetWhseProductDataGeneralV2 obj2 = new ICGetWhseProductDataGeneralV2();
            ICGetWhseProductDataGeneralV2Response res2 = obj2.prepareResults(new ApiAppObj(connectionString, "","","", appserverRetryConnectTimes, appserverRetryConnectSleepSeconds), "CONO=" + cono + "|OPER=CUST", "", req2);

            System.out.println("status: " + res2.getStatusType());

        }catch (Exception e){
            System.out.println("Error: SXe API method call failed");
        }
    }
}