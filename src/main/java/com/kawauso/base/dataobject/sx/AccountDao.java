package com.kawauso.base.dataobject.sx;



import com.infor.sxapi2.callobject.CallObject;
import com.infor.sxapi2.callobject.ICallObject;
import com.infor.sxapi2.paramclass.ARGetCustomerBalanceV2.input.ARGetCustomerBalanceV2Request;
import com.infor.sxapi2.paramclass.ARGetCustomerBalanceV2.output.ARGetCustomerBalanceV2Response;
import com.infor.sxapi2.paramclass.ARGetCustomerDataCredit.input.ARGetCustomerDataCreditRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataCredit.output.ARGetCustomerDataCreditResponse;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneral.input.ARGetCustomerDataGeneralRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneral.output.ARGetCustomerDataGeneralResponse;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneralV2.input.ARGetCustomerDataGeneralV2Request;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneralV2.output.ARGetCustomerDataGeneralV2Response;
import com.infor.sxapi2.paramclass.ARGetCustomerDataOrdering.input.ARGetCustomerDataOrderingRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataOrdering.output.ARGetCustomerDataOrderingResponse;
import com.infor.sxapi2.paramclass.ARGetCustomerListV2.input.ARGetCustomerListV2Request;
import com.infor.sxapi2.paramclass.ARGetCustomerListV2.output.ARGetCustomerListV2Response;
import com.infor.sxapi2.paramclass.ARGetCustomerListV2.output.Customer;
import com.infor.sxapi2.paramclass.ARGetInvoiceListV2.input.ARGetInvoiceListV2Request;
import com.infor.sxapi2.paramclass.ARGetInvoiceListV2.output.ARGetInvoiceListV2Response;
import com.infor.sxapi2.paramclass.ARGetInvoiceListV2.output.Transaction;
import com.infor.sxapi2.paramclass.ARGetShipToListV2.input.ARGetShipToListV2Request;
import com.infor.sxapi2.paramclass.ARGetShipToListV2.output.ARGetShipToListV2Response;
import com.infor.sxapi2.paramclass.ARGetShipToListV2.output.Shipto;
import com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.OEFullOrderMntV6Request;
import com.infor.sxapi2.paramclass.OEFullOrderMntV6.output.OEFullOrderMntV6Response;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.input.OEGetSingleOrderV3Request;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.AvailableField;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.LineItem;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.OEGetSingleOrderV3Response;
import com.infor.sxapi2.paramclass.OEPricingV4.input.OEPricingV4Request;
import com.infor.sxapi2.paramclass.OEPricingV4.output.OEPricingV4Response;
import com.infor.sxapi2.paramclass.SxApiAuthenticationException;
import com.infor.sxapi2.paramclass.SxApiAuthorizationException;
import com.infor.sxapi2.paramclass.SxApiConnectionException;
import com.infor.sxapi2.paramclass.SxApiGeneralException;
import com.infor.sxapi2.services.callobject.*;
import com.kawauso.base.bean.*;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class AccountDao extends SxApiHandler
{
    private static Logger log = Logger.getLogger( AccountDao.class );

    public List<Account> getAccounts(int cono, String oper, String key, String val, int limit)
    {
        ICallObject connection = null;
        List<Account> results = null;

        if(key == null || key.isEmpty() || val == null || val.isEmpty())
            return null;

        try {
            connection = getPool().getPooledConnection();

            results = new ArrayList<Account>();

            ARGetCustomerListV2Request req = new ARGetCustomerListV2Request();
            if (key.equals("custno")) {
                req.setCustomerNumber(new BigDecimal(val));
            }
            if (key.equals("name")) {
                req.setName(val);
            }
            if (key.equals("city")) {
                req.setCity(val);
            }
            if (key.equals("state")) {
                req.setState(val);
            }
            if (key.equals("zip")) {
                req.setPostalCode(val);
            }

            req.setRecordLimit(limit);

            ARGetCustomerListV2Response res = (ARGetCustomerListV2Response) connection.getResponse(req);

            for(Customer c : res.getListCustomer())
            {
                Account a = new Account();
                a.setCustno(c.getCustomerNumber());
                a.setName(c.getName());
                a.setCity(c.getCity());
                a.setState(c.getState());
                a.setZip(c.getPostalCode());
                a.setSalesYtd(c.getSalesYTD());
                a.setLastSalesYtd(c.getLastYearSalesYTD());
                results.add(a);
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getAccounts: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return results;
    }

    public Account getAccountInfo(int cono, String oper, BigDecimal custno) throws Exception
    {
        ICallObject connection = null;
        Account acct = new Account();

        try {
            connection = getPool().getPooledConnection();

            //get and set general data
            ARGetCustomerDataGeneralRequest reqGen = new ARGetCustomerDataGeneralRequest();
            reqGen.setCustomerNumber(custno);
            ARGetCustomerDataGeneralResponse resGen = (ARGetCustomerDataGeneralResponse) connection.getResponse(reqGen);
            acct.setCustno(custno);
            acct.setName(resGen.getName());
            acct.setAddress1(resGen.getAddress1());
            acct.setAddress2(resGen.getAddress2());
            acct.setCity(resGen.getCity());
            acct.setState(resGen.getState());
            acct.setZip(resGen.getZipCd());
            acct.setStatusType(resGen.getStatusType());
            acct.setPhoneNumber(resGen.getPhone());

            //get and set credit data
            ARGetCustomerDataCreditRequest reqCredit = new ARGetCustomerDataCreditRequest();
            reqCredit.setCustomerNumber(custno);
            ARGetCustomerDataCreditResponse resCredit = (ARGetCustomerDataCreditResponse) connection.getResponse(reqCredit);
            acct.setCredLimit( resCredit.getCreditLimit() );
            acct.setLastPayamt( resCredit.getLastPaymentAmount() );
            acct.setTermsType( resCredit.getTermsType() );
            acct.setTermsDescrip( resCredit.getTermsTypeDescription() );
            acct.setOrdBal( resCredit.getOrderBalance() );

            //get and set balances
            ARGetCustomerBalanceV2Request reqBal = new ARGetCustomerBalanceV2Request();
            reqBal.setCustomerNumber(custno);
            ARGetCustomerBalanceV2Response resBal = (ARGetCustomerBalanceV2Response) connection.getResponse(reqBal);
            acct.setFutBal( resBal.getFutureBalance() );
            acct.setPeriod1Bal( resBal.getPeriod1Balance() );
            acct.setPeriod1Text( resBal.getPeriod1Text().replace("(1)", "Current - ") );
            acct.setPeriod2Bal( resBal.getPeriod2Balance() );
            acct.setPeriod2Text( resBal.getPeriod2Text().replace("(2)", "30 Days - ") );
            acct.setPeriod3Bal( resBal.getPeriod3Balance() );
            acct.setPeriod3Text( resBal.getPeriod3Text().replace("(3)", "60 Days - ") );
            acct.setPeriod4Bal( resBal.getPeriod4Balance() );
            acct.setPeriod4Text( resBal.getPeriod4Text().replace("(4)", "90 Days - ") );
            acct.setPeriod5Bal( resBal.getPeriod5Balance() );
            acct.setPeriod5Text( resBal.getPeriod5Text().replace("(5)", "120+ Days - ") );
            acct.setTotalBal( resBal.getTotal2Balance() );

            //get and set ordering info
            ARGetCustomerDataOrderingRequest reqOrd = new ARGetCustomerDataOrderingRequest();
            reqOrd.setCustomerNumber(custno);
            ARGetCustomerDataOrderingResponse resOrd = (ARGetCustomerDataOrderingResponse) connection.getResponse(reqOrd);
            acct.setShipToRequiredFlag( resOrd.getShipToRequiredFlag() );
            acct.setDefaultShipTo(resOrd.getDefaultShipTo());
            acct.setPoRequiredFlag(resOrd.getPoRequiredFlag());
            acct.setCustomerPoNo(resOrd.getCustomerPoNo());
            acct.setSalesTerritory(resOrd.getSalesTerritory());
            acct.setSlsRepOut(resOrd.getSlsRepOut());

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getAccountInfo: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return acct;
    }

    public Order getOrder(int cono, String oper, BigDecimal custno, int orderno, int ordersuf)
    {
        Order order = this.getOrder(cono, oper, orderno, ordersuf);

        //got an order but it isn't for the requesting customer #
        if(custno.equals(order.getCustNo()) == false)
            return null;

        return order;
    }

    public Order getOrder(int cono, String oper, int orderno, int ordersuf)
    {
        ICallObject connection = null;
        Order order = null;

        try {
            order = new Order();
            order.setCono(cono);
            order.setOrderNo(orderno);
            order.setOrderSuf(ordersuf);

            connection = getPool().getPooledConnection();

            OEGetSingleOrderV3Request req = new OEGetSingleOrderV3Request();
            req.setOrderNumber(orderno);
            req.setOrderSuffix(ordersuf);
            req.setIncludeHeaderData(true);
            req.setIncludeLineData(true);
            req.setIncludeTaxData(true);
            req.setIncludeTotalData(true);

            OEGetSingleOrderV3Response res = (OEGetSingleOrderV3Response) connection.getResponse(req);

            //all dates in SX are in format mm/dd/yy
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");

            //header
            for(AvailableField field : res.getListAvailableField()) {
                if(field.getFieldName().equals("stage"))
                {
                    if(field.getFieldValue().equals("Ord"))
                        order.setStageCode(1);
                    else if(field.getFieldValue().equals("Pkp"))
                        order.setStageCode(2);
                    else if(field.getFieldValue().equals("Shp"))
                        order.setStageCode(3);
                    else if(field.getFieldValue().equals("Inv"))
                        order.setStageCode(4);
                    else if(field.getFieldValue().equals("Pd"))
                        order.setStageCode(5);
                    else if(field.getFieldValue().equals("Can"))
                        order.setStageCode(9);
                    else
                        order.setStageCode(-1);
                }

                if(field.getFieldName().equals("transtype")) { order.setTransType(field.getFieldValue()); }

                try { if(field.getFieldName().equals("enterdt")) { order.setEnterDt(formatter.parse(field.getFieldValue())); } }catch(Exception ig) {}
                try { if(field.getFieldName().equals("shipdt")) { order.setShipDt(formatter.parse(field.getFieldValue())); } }catch(Exception ig) {}
                //retrieved additional order information for dates
                try { if(field.getFieldName().equals("reqshipdt")) { order.setReqShipDt(formatter.parse(field.getFieldValue())); } }catch(Exception ig) {}

                if(field.getFieldName().equals("custpo")) { order.setCustPo(field.getFieldValue()); }
                if(field.getFieldName().equals("custno")) { order.setCustNo( new BigDecimal(field.getFieldValue()) ); }
                if(field.getFieldName().equals("shipviatydesc")) { order.setShipVia(field.getFieldValue()); }
                if(field.getFieldName().equals("termstypedesc")) { order.setTermsDesc(field.getFieldValue()); }
                if(field.getFieldName().equals("whse")) { order.setWhse(field.getFieldValue()); }
                if(field.getFieldName().equals("shipinstr")) { order.setInstructions(field.getFieldValue()); }
                if(field.getFieldName().equals("refer")) { order.setReference(field.getFieldValue()); }

                if(field.getFieldName().equals("totlineamt")) {
                    if(field.getFieldValue().endsWith("-"))
                        order.setSubTotal( new BigDecimal( field.getFieldValue().substring( 1,field.getFieldValue().length()-1 ).trim() ).negate() );
                    else
                        order.setSubTotal( new BigDecimal( field.getFieldValue().trim() ) );
                }
                if(field.getFieldName().equals("taxamt")) {
                    if(field.getFieldValue().endsWith("-"))
                        order.setTaxAmount( new BigDecimal( field.getFieldValue().substring( 1,field.getFieldValue().length()-1 ).trim() ).negate() );
                    else
                        order.setTaxAmount( new BigDecimal( field.getFieldValue().trim() ) );
                }
                if(field.getFieldName().equals("totinvamt")) {
                    if(field.getFieldValue().endsWith("-"))
                        order.setInvAmount( new BigDecimal( field.getFieldValue().substring( 1,field.getFieldValue().length()-1 ).trim() ).negate() );
                    else
                        order.setInvAmount( new BigDecimal( field.getFieldValue().trim() ) );
                }

                if(field.getFieldName().equals("soldtonm")) { order.setSoldToName(field.getFieldValue()); }
                if(field.getFieldName().equals("soldtoaddr1")) { order.setSoldToAddr1(field.getFieldValue()); }
                if(field.getFieldName().equals("soldtoaddr2")) { order.setSoldToAddr2(field.getFieldValue()); }
                if(field.getFieldName().equals("soldtocity")) { order.setSoldToCity(field.getFieldValue()); }
                if(field.getFieldName().equals("soldtost")) { order.setSoldToState(field.getFieldValue()); }
                if(field.getFieldName().equals("soldtozipcd")) { order.setSoldToZip(field.getFieldValue()); }

                if(field.getFieldName().equals("shiptonm")) { order.setShipToName(field.getFieldValue()); }
                if(field.getFieldName().equals("shiptoaddr1")) { order.setShipToAddr1(field.getFieldValue()); }
                if(field.getFieldName().equals("shiptoaddr2")) { order.setShipToAddr2(field.getFieldValue()); }
                if(field.getFieldName().equals("shiptocity")) { order.setShipToCity(field.getFieldValue()); }
                if(field.getFieldName().equals("shiptost")) { order.setShipToState(field.getFieldValue()); }
                if(field.getFieldName().equals("shiptozip")) { order.setShipToZip(field.getFieldValue()); }
            }


            //check whether the company number is Holden...
            //Holden orders do not have line items so bypass line item add
            //boolean isHolden = ConfigReader.getProperty("sxe.distr.Holden").equals(cono);
            //if (isHolden) { return order;}
            List<LineItem> lineItems = new ArrayList<LineItem>();

            try {

                lineItems = res.getListLineItem();

            }catch (Exception ex){

                ex.printStackTrace();;

            }

            //lines -- could throw exception if res.getListLineItem() == null
            if (lineItems.size() > 0) {
                for (LineItem item : lineItems) {
                    OrderLine line = new OrderLine();
                    line.setShipProd(item.getProductCode());
                    line.setLineNumber(item.getLineNumber());
                    line.setDescription(item.getDescription1() + " " + item.getDescription2());
                    line.setQtyOrder(item.getQuantityOrdered());
                    line.setQtyShip(item.getQuantityShip());
                    line.setPrice(item.getPrice());
                    line.setNetAmount(item.getNetAmount());

                    order.addOrderLine(line);
                }
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getOrder: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return order;
    }
    
    public List<Order> getPeriodOrders(int cono, String oper, BigDecimal custno, int period)
    {
        ICallObject connection = null;
        List<Order> orders = null;
        
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        
        try {
            orders = new ArrayList<Order>();

            connection = getPool().getPooledConnection();

            ARGetInvoiceListV2Request req = new ARGetInvoiceListV2Request();
            req.setCustomerNumber(custno);

            req.setIncludeInvoices(false);
            req.setIncludeServiceCharges(false);
            req.setIncludeRebates(false);
            req.setIncludeUnappliedCash(false);
            req.setIncludeCOD(false);
            req.setIncludeMiscCredits(false);
            req.setIncludeCreditMemos(false);
            req.setIncludeChecks(false);
            req.setIncludeDebitMemos(false);
            req.setIncludeScheduledPayments(false);            

            switch(period) {
                case 0: req.setIncludeFutureInvoices(true); break;
                case 1: req.setIncludePeriod1(true); break;
                case 2: req.setIncludePeriod2(true); break;
                case 3: req.setIncludePeriod3(true); break;
                case 4: req.setIncludePeriod4(true); break;
                case 5: req.setIncludePeriod5(true); break;
                case 6:
                    req.setIncludeInvoices(true);
                    req.setIncludeServiceCharges(true);
                    req.setIncludeRebates(true);
                    req.setIncludeUnappliedCash(true);
                    req.setIncludeCOD(true);
                    req.setIncludeMiscCredits(true);
                    req.setIncludeCreditMemos(true);
                    req.setIncludeChecks(true);
                    req.setIncludeDebitMemos(true);
                    req.setIncludeScheduledPayments(true);
                    break;
            }
            
            ARGetInvoiceListV2 obj = new ARGetInvoiceListV2();
            ARGetInvoiceListV2Response res = (ARGetInvoiceListV2Response) connection.getResponse(req);
            
            for(Transaction t : res.getListTransaction()) {
                Order order = new Order();
                order.setCono(cono);
                order.setOrderNo(t.getInvoiceNumber());
                order.setOrderSuf(t.getInvoiceSuffix());
                
                if(period != 6) {
                    try {
                        if(t.getAmount().endsWith("-"))
                            order.setInvAmount( new BigDecimal( t.getAmount().substring(1, t.getAmount().length()-1).trim() ).negate() );
                        else
                            order.setInvAmount( new BigDecimal( t.getAmount().trim() ) );
                    }catch(Exception ignore) {}
                    
                    order.setCustPo( t.getCustomerPurchaseOrder() );

                    //invoice date
                    try {
                        Date d = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse(t.getInvoiceDate().month + "/"+t.getInvoiceDate().date+"/"+t.getInvoiceDate().year);
                        order.setInvoiceDt(d);
                    }catch(Exception ignore) {}

                    //due date
                    try {
                        Date d = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse(t.getDueDate().month + "/"+t.getDueDate().date+"/"+t.getDueDate().year);
                        order.setDueDt(d);
                    }catch(Exception ignore) {}                    
                    
                    order.setReference( t.getReference() );
                }

                orders.add(order);
            }

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getPeriodOrders: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return orders;
    }
    
    /**
     * 
     * @param cono
     * @param oper
     * @param custno
     * @return
     * @throws Exception 
     */
    public List<ShipTo> getShipToList(int cono, String oper, BigDecimal custno, boolean includeMain)
    {

        List<ShipTo> list = null;

        if(includeMain) {
            ICallObject connection = null;
            try {
                connection = getPool().getPooledConnection();

                ARGetCustomerDataGeneralV2Request req = new ARGetCustomerDataGeneralV2Request();
                req.setCustomerNumber(custno);

                ARGetCustomerDataGeneralV2 obj = new ARGetCustomerDataGeneralV2();
                ARGetCustomerDataGeneralV2Response res = (ARGetCustomerDataGeneralV2Response) connection.getResponse(req);

                if (res != null) {
                    list = new ArrayList<ShipTo>();

                    ShipTo s = new ShipTo();
                    s.setName(res.getName());
                    s.setId("main");
                    s.setAddr1(res.getAddress1());
                    s.setAddr2(res.getAddress2());
                    s.setCity(res.getCity());
                    s.setState(res.getState());
                    s.setZip(res.getZipCd());
                    list.add(s);
                }
            } catch (SxApiConnectionException
                    |SxApiAuthorizationException
                    |SxApiAuthenticationException
                    |SxApiGeneralException ex) {
                getPool().destroyEntirePool(connection);
                return null;
            } catch (Exception e) {
                log.error("getShipToList(main): " + e);
                return null;
            } finally {
                if (connection != null)
                    getPool().returnPooledConnection(connection);
            }
        }

        ICallObject connection = null;
        try {
            connection = getPool().getPooledConnection();

            ARGetShipToListV2Request req = new ARGetShipToListV2Request();
            req.setCustomerNumber(custno);

            ARGetShipToListV2 obj = new ARGetShipToListV2();
            ARGetShipToListV2Response res = (ARGetShipToListV2Response) connection.getResponse(req);
            
            List<Shipto> resList = res.getListShipto();

            if(list == null && resList.size() > 0)
                list = new ArrayList<ShipTo>();

            for(Shipto shipto : resList) {
                ShipTo a = new ShipTo();
                a.setId(shipto.getShipTo());
                a.setName(shipto.getName());
                a.setAddr1(shipto.getAddress1());
                a.setAddr2(shipto.getAddress2());
                a.setCity(shipto.getCity());
                a.setState(shipto.getState());
                a.setZip(shipto.getPostalCode());

                list.add(a);
            }
        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getShipToList: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return list;
    }

    public ShipTo getShipTo(int cono, String oper, BigDecimal custno, String shipto)
    {
        ShipTo shipTo = null;

        if(shipto.equalsIgnoreCase("main"))
        {
            ICallObject connection = null;
            try {
                connection = getPool().getPooledConnection();

                ARGetCustomerDataGeneralV2Request req = new ARGetCustomerDataGeneralV2Request();
                req.setCustomerNumber(custno);

                ARGetCustomerDataGeneralV2 obj = new ARGetCustomerDataGeneralV2();
                ARGetCustomerDataGeneralV2Response res = (ARGetCustomerDataGeneralV2Response) connection.getResponse(req);

                shipTo = new ShipTo();
                shipTo.setName(res.getName());
                shipTo.setId("main");
                shipTo.setAddr1(res.getAddress1());
                shipTo.setAddr2(res.getAddress2());
                shipTo.setCity(res.getCity());
                shipTo.setState(res.getState());
                shipTo.setZip(res.getZipCd());

            } catch (SxApiConnectionException
                    |SxApiAuthorizationException
                    |SxApiAuthenticationException
                    |SxApiGeneralException ex) {
                getPool().destroyEntirePool(connection);
                return null;
            } catch (Exception e) {
                log.error("getShipTo(main): " + e);
                return null;
            } finally {
                if (connection != null)
                    getPool().returnPooledConnection(connection);
            }

            return shipTo;
        }

        ICallObject connection = null;
        try {
            connection = getPool().getPooledConnection();

            ARGetShipToListV2Request req = new ARGetShipToListV2Request();
            req.setCustomerNumber(custno);
            req.setShipTo(shipto);
            req.setIncludeClosedJobs(false);

            ARGetShipToListV2Response res = (ARGetShipToListV2Response) connection.getResponse(req);

            List<Shipto> resList = res.getListShipto();
            for(Shipto s : resList) {
                if (s.getShipTo().equals(shipto)) {
                    shipTo = new ShipTo();
                    shipTo.setId(s.getShipTo());
                    shipTo.setName(s.getName());
                    shipTo.setAddr1(s.getAddress1());
                    shipTo.setAddr2(s.getAddress2());
                    shipTo.setCity(s.getCity());
                    shipTo.setState(s.getState());
                    shipTo.setZip(s.getPostalCode());
                }
            }
        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getShipTo: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return shipTo;
    }

    /**
     * 
     * @param cono
     * @param oper
     * @param custno
     * @return 
     */
    public Map<String, Object> getOrderingInfo(int cono, String oper, BigDecimal custno)
    {
        ICallObject connection = null;
        Map<String, Object> map = new HashMap<String, Object>();

        try {
            connection = getPool().getPooledConnection();

            ARGetCustomerDataOrderingRequest req = new ARGetCustomerDataOrderingRequest();
            req.setCustomerNumber(custno);

            ARGetCustomerDataOrdering obj = new ARGetCustomerDataOrdering();
            ARGetCustomerDataOrderingResponse res = (ARGetCustomerDataOrderingResponse) connection.getResponse(req);

            map.put("shipToReq", res.getShipToRequiredFlag());
            map.put("shipToDflt", res.getDefaultShipTo());
            map.put("poReq", res.getPoRequiredFlag());
            map.put("poDfltValue", res.getCustomerPoNo());
            map.put("dfltWhse", res.getSalesTerritory());
            map.put("slsRepId", res.getSlsRepOut());
        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("getOrderingInfo: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return map;
    }   

    public OrderResults placeOrder(int cono, String oper, String userName, BigDecimal custno, Cart cart, List<CartItem> lines) throws Exception
    {
        ICallObject connection = null;
        OrderResults results = null;

        try {
            connection = getPool().getPooledConnection();

            //probably should validate the data coming in
            //if(placeorder data error)
            //    throw new GeneralException("placeOrder", new FaultBean("Order data invalid or incomplete. Create cancelled."));        

            OEFullOrderMntV6Request req = new OEFullOrderMntV6Request();

            //--setup payment (tendering)
            //not that we apply tendering online, this is how you would do it
            //List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra> headExtraList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra>();
            //com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra headExtra = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra();
            //headExtra.setFieldName("Tender_Payment");
            //headExtra.setFieldValue("paytype=2\tamt=3.40");
            ///headExtra.setSequenceNumber(1);
            //headExtraList.add(headExtra);            
            //req.setListHeaderExtra(headExtraList);

            //--setup the customer
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Customer> customerList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Customer>();
            com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Customer customer = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Customer();
            customer.setCustomerNumber( custno.toString() );
            customerList.add(customer);
            req.setListCustomer(customerList);

            //--build shipto
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.ShipTo> shiptoList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.ShipTo>();
            com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.ShipTo shipto = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.ShipTo();
            shipto.setShipToNumber( cart.getJobId() ); //set shipto # = job account id (billto accct)
            shipto.setAddress1(cart.getShipToAddr1());
            shipto.setAddress2(cart.getShipToAddr2());
            shipto.setCity(cart.getShipToCity());
            shipto.setState(cart.getShipToState());
            shipto.setPostalCode(cart.getShipToZip());
            shiptoList.add(shipto);
            req.setListShipTo(shiptoList);

            //--build order head
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Order> orderList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Order>();
            com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Order order = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Order();
            order.setTransactionType("SO");
            order.setActionType("storefront");
            order.setShipVia( cart.getShipVia() );
            order.setWarehouse( cart.getShipWhseId() );
            order.setPurchaseOrderNumber( (cart.getCustPo() != null) ? cart.getCustPo().toUpperCase() : "" );
            order.setReference( (cart.getReference() != null) ? cart.getReference().toUpperCase() : "" );
            order.setShipInstructions( (cart.getInstructions() != null) ? cart.getInstructions().toUpperCase() : "");
            order.setBuyer( userName );
            order.setTakenBy( oper );

            //try to figure out what format ship date is in (should be yyyy-mm-dd)
            try {
                String[] shipDt = cart.getShipDate().split("/|-");
                if(shipDt != null && shipDt.length == 3) { //good so far
                    String shipDtStr = shipDt[0] + "/" + shipDt[1] + "/" + shipDt[2].substring(2,4);
                    order.setRequestedShipDate(shipDtStr);
                }
            }catch(Exception ex) {
                Calendar cal = Calendar.getInstance();
                String shipDtStr = (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.YEAR) - 2000);
                //System.out.println("shipDt Revised: " + shipDtStr);
            }

            //if(o.get != null && !o.getNotes().isEmpty())
            //    order.setNotesIndicator("all");

            orderList.add(order);
            req.setListOrder(orderList);

            //if(o.getNotes() != null && !o.getNotes().isEmpty()) {
            //    List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra> extraList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra>();
            //    com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra extra = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.HeaderExtra();
            //    extra.setFieldName("notes");
            //    extra.setFieldValue(o.getNotes());
            //    extraList.add(extra);
            //    req.setListHeaderExtra(extraList);
            //}

            //--build order lines
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item> itemList = new ArrayList<com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item>();
            for(CartItem line : lines) {
                com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item item = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item();

                try {
                    item.setSellerProductCode(line.getProductCode());
                    item.setQuantityOrdered(String.valueOf(line.getQty()));
                    item.setLineComments(line.getNote());

                    //only allow override of item price if priceType not (S)tandard
                    if (line.getPriceType().equalsIgnoreCase("S") == false)
                        item.setUnitCost( String.valueOf(line.getUnitPrice()) );

                    itemList.add(item);
                }catch(Exception ex) {
                    log.error("buildorderlines: " + ex);
                    throw new Exception("Error adding product to order: " + line.getProductCode());
                }                
            }

            //mod-141117a; add freight to all orders placed by Carrier National
            if(custno.equals(new BigDecimal(100981)))
            {
                if(!cart.getShipVia().equals("PKP")) {
                    com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item item = new com.infor.sxapi2.paramclass.OEFullOrderMntV6.input.Item();
                    item.setSellerProductCode("FREIGHT OUTBOUND");
                    item.setQuantityOrdered("1");
                    item.setUnitCost("85.00");
                    itemList.add(item);
                }
            }
            //end mod-141117a

            req.setListItem(itemList);

            //--process order
            OEFullOrderMntV6 obj = new OEFullOrderMntV6();
            OEFullOrderMntV6Response res = (OEFullOrderMntV6Response) connection.getResponse(req);

            results = new OrderResults();
            results.setErrorMsg(res.getErrorMessage());
            log.error("exceptmsg: " + res.getExceptionError() + "; errormsg: " + res.getErrorMessage());

            //--get results
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.output.Acknowledgement> ackList = res.getListAcknowledgement();
            for(com.infor.sxapi2.paramclass.OEFullOrderMntV6.output.Acknowledgement a : ackList) {
                results.setAckMsg(a.getMessage());
                results.setAckData(a.getData1());
                results.setAckErrorNum(a.getErrorNumber());
                log.error("msg: " + a.getMessage() + "; data: " + a.getData1() + "; error: " + a.getErrorNumber());
            }

            //build result orderhead
            List<com.infor.sxapi2.paramclass.OEFullOrderMntV6.output.Header> headList = res.getListHeader();
            Order o = new Order();
            for(com.infor.sxapi2.paramclass.OEFullOrderMntV6.output.Header h : headList) {
                o.setOrderNo( Integer.parseInt(h.getInvoiceNumber()) );
                o.setOrderSuf( Integer.parseInt(h.getInvoiceSuffix()) );
            }
            results.setOrderDetails(o);

        } catch (SxApiConnectionException
                |SxApiAuthorizationException
                |SxApiAuthenticationException
                |SxApiGeneralException ex) {
            getPool().destroyEntirePool(connection);
            return null;
        } catch (Exception e) {
            log.error("placeOrder: " + e);
            return null;
        } finally {
            if (connection != null)
                getPool().returnPooledConnection(connection);
        }

        return results;
    }

    public static void main(String args[]){

        try {
            InetAddress iAddress = InetAddress.getLocalHost();
            String client_IP = iAddress.getHostAddress();
            System.out.println("Current IP address : " +client_IP);
        } catch (UnknownHostException e) {}

        try {

            //String connectionString="AppServerDC://172.0.0.6:7190/sxapiappsrv";
            String connectionString="AppServerDC://192.168.10.119:7190/sxapiappsrv";
            int appserverRetryConnectTimes=2;
            int appserverRetryConnectSleepSeconds=6;

            int cono=1;
            String oper="CUST";

            CallObject connectionObject = new CallObject(connectionString, cono, oper, "", appserverRetryConnectTimes, appserverRetryConnectSleepSeconds);

            OEPricingV4Request req = new OEPricingV4Request();
            req.setCustomerNumber(new BigDecimal(465));
            req.setShipTo("");
            req.setWarehouse("41");
            req.setProductCode("FB4-20-1");

            OEPricingV4 obj = new OEPricingV4();
            OEPricingV4Response res = (OEPricingV4Response) connectionObject.getResponse(req);

System.out.println("This is just so I can see the response");
            //connectionString, cono, oper, "", appserverRetryConnectTimes, appserverRetryConnectSleepSeconds);
            //Account acct = new Account();
            //get and set general data
            //SRReceivePORequest reqGen = new SRReceivePORequest();
            //reqGen.setPurchaseOrderNumber(2000169);
            //reqGen.setPurchaseOrderSuffix(0);
            //reqGen.setReference("");
            //List<Line> list = new ArrayList<Line>();
            //reqGen.setListLine(list);

            //SRReceivePOResponse resGen = (SRReceivePOResponse)connectionObject.getResponse(reqGen);

            //acct.setCustno(new BigDecimal(3));
            //acct.setName(resGen.getName());
            //acct.setAddress1(resGen.getAddress1());
            //acct.setAddress2(resGen.getAddress2());
            //acct.setCity(resGen.getCity());
            //acct.setState(resGen.getState());
            //acct.setZip(resGen.getZipCd());
            //acct.setStatusType(resGen.getStatusType());
            //acct.setPhoneNumber(resGen.getPhone());

            //System.out.println("Account Information: " + acct.toString());

        }catch (Exception e){
            System.out.println("Error: SXe API method call failed");
        }



    }

}