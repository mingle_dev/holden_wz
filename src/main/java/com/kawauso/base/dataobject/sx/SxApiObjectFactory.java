package com.kawauso.base.dataobject.sx;

import com.infor.sxapi2.callobject.CallObject;
import com.kawauso.base.ConfigReader;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.log4j.Logger;

public class SxApiObjectFactory implements PoolableObjectFactory
{
    private static SxApiObjectFactory sxApiObjectFactory;

    private static final Logger log = Logger.getLogger(SxApiObjectFactory.class);

    private String connectionString = ConfigReader.getProperty("sxe.connectionString");
    private int companyNumber = ConfigReader.getPropertyAsInt("sxe.cono");
    private String operator = ConfigReader.getProperty("sxe.oper");
    private String password;
    private int appserverRetryConnectTimes = ConfigReader.getPropertyAsInt("sxe.appserverRetryConnectTimes");
    private int appserverRetryConnectSleepSeconds = ConfigReader.getPropertyAsInt("sxe.appserverRetryConnectSleepSeconds");

    protected SxApiObjectFactory() {
    }

    public synchronized static SxApiObjectFactory getInstance()
    {
        if(sxApiObjectFactory == null) {
            sxApiObjectFactory = new SxApiObjectFactory();
        }

        return sxApiObjectFactory;
    }

    @Override
    public Object makeObject() throws Exception
    {
        CallObject connectionObject = null;

        if(this.connectionString != null) {
            log.info("Connect to SX.API Using ConnectionString = " + this.connectionString);
        }

        try {
            String pass = "";
            if (this.password != null) {
                pass = this.password;
            }
            connectionObject = new CallObject(this.connectionString, this.companyNumber, this.operator, pass, this.appserverRetryConnectTimes, this.appserverRetryConnectSleepSeconds);

            if (connectionObject != null)
                log.debug("ConnectionUtility, instantiated SXAPI connection object: " + connectionObject.toString());
        }catch (Throwable ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

        return connectionObject;
    }

    @Override
    public void destroyObject(Object o) throws Exception {
        log.error("destroyObject: " + o);
    }

    @Override
    public boolean validateObject(Object other)
    {
        boolean isValid = false;
        CallObject obj = (CallObject)other;

        log.debug("Call made to validateObject");

        if (obj != null) {
            isValid = true;
        }

        return isValid;
    }

    @Override
    public void activateObject(Object o) throws Exception {
        log.debug("Call made to activateObject");
    }

    @Override
    public void passivateObject(Object o) throws Exception {
        log.debug("Call made to passivateObject");
    }
}
