package com.kawauso.base.dataobject.sx;

import com.infor.sxapi2.callobject.ICallObject;
import com.infor.sxapi2.paramclass.SxApiConnectionException;
import com.kawauso.base.ConfigReader;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import java.util.NoSuchElementException;

/**
 * Need to use actually use InitalizingBean instead
 * of the old school properties file
 */
public class SxApiConnectionPool extends GenericObjectPool
{
    private static SxApiConnectionPool sxApiConnectionPool;

    private static final Logger log = Logger.getLogger(SxApiConnectionPool.class);

    private PoolableObjectFactory objectFactory;
    private int maximumActive = ConfigReader.getPropertyAsInt("pool.maximumActive");
    private int maximumIdle = ConfigReader.getPropertyAsInt("pool.maximumIdle");
    private int maximumWait = ConfigReader.getPropertyAsInt("pool.maximumWait");

    protected SxApiConnectionPool()
    {
        this.objectFactory = SxApiObjectFactory.getInstance();

        setFactory(this.objectFactory);
        setMaxActive(this.maximumActive);
        setMaxIdle(this.maximumIdle);
        setWhenExhaustedAction((byte)1);

        long milliseconds = this.maximumWait * 1000;
        setMaxWait(milliseconds);
        setTestOnBorrow(false);
        setTestOnReturn(false);
    }

    public synchronized static SxApiConnectionPool getInstance()
    {
        if(sxApiConnectionPool == null) {
            sxApiConnectionPool = new SxApiConnectionPool();

            log.info("New SxApiConnectionPool Instantiated");
        }
        return sxApiConnectionPool;
    }

    public ICallObject getPooledConnection() throws SxApiConnectionException
    {
        this.objectFactory = SxApiObjectFactory.getInstance();

        ICallObject connection = null;

        try {
            connection = (ICallObject) borrowObject();
        } catch (IllegalStateException ie) {
            log.error(ie);
            throw new SxApiConnectionException(ie.getMessage());
        } catch (NoSuchElementException ne) {
            log.error(ne);
            throw new SxApiConnectionException(ne.getMessage());
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            throw new SxApiConnectionException(e.getMessage());
        }

        return connection;
    }

    public void returnPooledConnection(ICallObject connectionObject)
    {
        if (this != null)
            try {
            returnObject(connectionObject);
        } catch (Exception e) {
                e.printStackTrace();
                log.error("Exception returning to pool: " + e);
        }
    }

    /**
     * This actually does nothing and is best avoided. The intent
     * is to destroy the underlying connection but it does not
     * appear that ICallObject has a way to destroy
     */
    public void destroyPooledConnection(ICallObject connectionObject)
    {
        log.debug("Destroying pooled SXAPI connection");

        if (this != null) {
            try {
                getObjectFactory().destroyObject(connectionObject);
            } catch (Exception e) {
                //this.exceptionHandler.unexpectedExceptionHandler(e);
            }
        }
    }

    /**
     * This actually does nothing and is best avoided. The intent
     * is to destroy the underlying connection but it does not
     * appear that ICallObject has a way to destroy
     */
    public void destroyEntirePool(ICallObject connectionObject)
    {
        log.debug("Destroying pooled SXAPI connection as well as any other open connections.");

        if (this != null) {
            try {
                getObjectFactory().destroyObject(connectionObject);
                clear();
            } catch (Exception e) {
                //this.exceptionHandler.unexpectedExceptionHandler(e);
            }
        }
    }

    public final PoolableObjectFactory getObjectFactory()
    {
        return this.objectFactory;
    }

    public final void setObjectFactory(PoolableObjectFactory objectFactory)
    {
        this.objectFactory = SxApiObjectFactory.getInstance();
    }

    public static void main(String args[]){

        SxApiConnectionPool connectionPool = SxApiConnectionPool.getInstance();
        try {
            connectionPool.getPooledConnection();
        } catch (SxApiConnectionException e) {
            e.printStackTrace();
        }
    }
}
