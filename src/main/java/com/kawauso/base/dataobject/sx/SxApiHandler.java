package com.kawauso.base.dataobject.sx;

import org.apache.log4j.Logger;

public abstract class SxApiHandler
{
    protected SxApiConnectionPool pool;
    private static Logger log = Logger.getLogger( SxApiHandler.class );

    public final SxApiConnectionPool getPool()
    {
        return SxApiConnectionPool.getInstance();
    }
}