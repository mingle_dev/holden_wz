package com.kawauso.base.dataobject;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.ListCategory;
import com.kawauso.base.bean.ListItem;
import com.kawauso.base.bean.Product;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("listDao")
public class ListDaoImpl implements ListDao
{
    @Autowired
    private SessionFactory sessionfactory;

    @Override
    public List<ListCategory> getLists(String userId, Integer limit, boolean includeExtra)
    {
        List<ListCategory> items = new ArrayList<ListCategory>();
        
        if(includeExtra == true) {
            String sql = "select user_id, categoryid, name, numItems, 'shared' = case isnull(savelist_id, '0') when '0' then '0' else '1' end " +
                          "from ( " +
                          "  SELECT node.user_id, node.categoryId, node.name, COUNT(*) as 'numitems' " +
                          "  FROM savelist_categories AS node " +
                          "  LEFT JOIN savelist_items items ON (items.categoryId = node.categoryId) " +
                          "  WHERE node.user_id=:id " +
                          "        AND node.name != 'root_tree' " +
                          "  GROUP BY node.user_id, node.categoryId, node.name " +
                          ") l " +
                          "left join ( " +
                          "  SELECT savelist_id FROM savelist_shares " +
                          "  WHERE owner_id=:id " +
                          "  GROUP BY savelist_id " +
                          ") s on (s.savelist_id = l.categoryid) ";
            
            Session session = sessionfactory.getCurrentSession();

            SQLQuery query = session.createSQLQuery(sql);
            query.setString("id", userId);
            
            List<Object[]> list = query.list();
            for(Object[] obj : list) {
                ListCategory l = new ListCategory();
                l.setUserId( obj[0].toString() );
                l.setId( obj[1].toString() );
                l.setName( obj[2].toString() );
                l.setNumItems( Integer.parseInt( obj[3].toString() ) );
                l.setShared( (obj[4].toString().equals("1")) ? true : false );
                items.add(l);
            }
            
            return items;
            
        }else{
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(ListCategory.class);
            crit.add( Restrictions.eq("userId", userId) );

            if(limit != null)
                crit.setMaxResults(limit);

            return crit.list();  
        }
    }
    
    @Override
    public ListCategory getList(String listId, boolean includeExtra)
    {
        ListCategory list = null;
        
        if(includeExtra == true) {
            String sql = "SELECT DISTINCT * " +
                         "FROM (" +
                         "  SELECT sc.categoryid, sc.name, si.uuid, si.defaultqty, p.id, " +
                         "    p.cono, p.prod, p.status, p.image, p.netavail, p.description as 'defaultDescrip', " +
                         "    coalesce(si.displayOrder,9999) as 'displayOrder', upd.description as 'userDescrip' " +
                         "  from savelist_categories sc " +
                         "  join savelist_items si ON (si.categoryid = sc.categoryid) " +
                         "  join view_products p on (p.prod = si.productCode) " +
                         "  left join userProductData upd on (upd.productCode = p.prod and upd.user_id = sc.user_id) " +
                         "  where sc.categoryid='" + listId + "' " +
                         ") x " +
                         "ORDER BY x.displayOrder ASC";

            Session session = sessionfactory.getCurrentSession();

            List<Object[]> tmp = session.createSQLQuery(sql).list();
            for(Object[] obj : tmp) {
                if(list == null) {
                    list = new ListCategory();
                    list.setId(obj[0].toString());
                    list.setName(obj[1].toString());
                }

                try {
                    ListItem item = new ListItem();
                    item.setId(obj[2].toString());
                    item.setQty( Integer.parseInt(obj[3].toString()) );
                    item.setDisplayOrder( Integer.parseInt(obj[11].toString()) );

                    Product prod = new Product();
                    prod.setId( obj[4].toString() );
                    prod.setCono( Integer.parseInt(obj[5].toString()) );
                    prod.setProduct( obj[6].toString() );
                    prod.setStatus( obj[7].toString() );
                    prod.setImage( obj[8] != null ? obj[8].toString() : null );
                    prod.setNetAvail( (int) Double.parseDouble(obj[9].toString()) );
                    prod.setDescription( obj[10].toString() );
                    prod.setUserDescription( obj[12] != null ? obj[12].toString() : null );
                    item.setProduct(prod);

                    list.addListItem(item);
                }catch(Exception ex) { }
            }
        
            return list;
            
        }else{
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(ListCategory.class);
            crit.add( Restrictions.eq("id", listId) );
            
            return (ListCategory) crit.uniqueResult();
        }
    }
    
    @Override
    public String saveList(ListCategory list)
    {
        Session session = sessionfactory.getCurrentSession();
        session.save(list);
        session.flush();

        return list.getId();
    }
    
    @Override
    public String updateList(ListCategory list)
    {
        Session session = sessionfactory.getCurrentSession();
        session.update(list);
        session.flush();

        return list.getId();
    }    
    
    @Override
    public String renameList(String id, String name)
    {
        Session session = sessionfactory.getCurrentSession();

        Criteria crit = session.createCriteria(ListCategory.class);
        crit.add( Restrictions.eq("id", id) );
        
        ListCategory list = (ListCategory) crit.uniqueResult();
        if(list != null) {
            list.setName(name);
            session.update(list);
            session.flush();
        }
        
        return list.getId();
    }
    
    @Override
    public String addItem(ListItem item)
    {
        Session session = sessionfactory.getCurrentSession();
        session.save(item);
        session.flush();

        return item.getId();
    }
    
    @Override
    public String addItem(ListCategory item)
    {
        Session session = sessionfactory.getCurrentSession();
        session.save(item);
        session.flush();
        
        return item.getId();
    }

    @Override
    public boolean deleteItem(String listId, String prod)
    {
        Session session = sessionfactory.getCurrentSession();
        Query query = session.createQuery("DELETE FROM ListItem WHERE categoryid=:id and productCode=:prod");
        query.setString("id", listId);
        query.setString("prod", prod);

        int res = query.executeUpdate();

        return (res >= 0) ? true : false;
    }

    @Override
    public boolean updateItemQty(String listId, String prod, BigDecimal qty)
    {
        Session session = sessionfactory.getCurrentSession();
        Query query = session.createQuery("UPDATE ListItem SET qty=:qty WHERE categoryid=:id and productCode=:prod");
        query.setBigDecimal("qty", qty);
        query.setString("id", listId);
        query.setString("prod", prod);

        int res = query.executeUpdate();

        return (res >= 0) ? true : false;
    }

    @Override
    public int getItemCount(ListCategory cat)
    {
        Session session = sessionfactory.getCurrentSession();
        Query query = session.createQuery("SELECT COUNT(*) FROM ListItem WHERE categoryid=:id");
        query.setString("id", cat.getId());
        
        return ( (Long) query.uniqueResult() ).intValue();
    }
    
    @Override
    public String cloneList(String listId)
    {
        Session session = sessionfactory.getCurrentSession();
        String uuid = UUID.randomUUID().toString();

        //create list
        String sql = "insert into savelist_categories (categoryid, user_id, name) " +
                     "select '" + uuid + "', a.user_id, a.name + ' [CLONE]' " +
                     "from savelist_categories a " +
                     "where a.categoryid='" + listId + "'";
        
        if(session.createSQLQuery(sql).executeUpdate() == 0)
            return null;
        
        //copy items
        sql = "insert into savelist_items (uuid, categoryid, productCode, defaultqty, displayOrder) " + 
              "select lower(newid()), '" + uuid + "', productCode, defaultqty, displayOrder " +
              "from savelist_items where categoryid='" + listId + "'";

        session.createSQLQuery(sql).executeUpdate();
        
        return uuid;
    }
    
    @Override
    public boolean deleteList(String listId)
    {
        Session session = sessionfactory.getCurrentSession();

        String sql = "delete from savelist_categories where categoryid='" + listId + "'";
        if(session.createSQLQuery(sql).executeUpdate() == 0)
            return false;

        sql = "delete from savelist_items where categoryid='" + listId + "'";
        session.createSQLQuery(sql).executeUpdate();
        
        return true;
    }
    
    @Override
    public boolean isShared(String listId, BigDecimal custno)
    {
        String sql = "SELECT id FROM savelist_shares WHERE savelist_id = :listId";
        Session session = sessionfactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        query.addScalar("id", Hibernate.STRING);
        query.setString("listId", listId);
        
        List<Object[]> tmp = query.list();
        
        return !tmp.isEmpty();
    }
    
    @Override
    public List<Account> getShares(String listId)
    {
        List<Account> shares = new ArrayList<Account>();
        
        String sql = "select a.custno, a.name, a.slsrepout, a.territory, a.customer_type " +
                     "from savelist_shares s " +
                     "join view_accounts a on (a.cono = s.cono and a.custno = s.custno) " +
                     "where savelist_id = :listId";
        Session session = sessionfactory.getCurrentSession();
        
        List<Object[]> list = session.createSQLQuery(sql).setString("listId", listId).list();

        for(Object[] obj : list) {
            Account a = new Account();
            a.setCustno(new BigDecimal(obj[0].toString()));
            a.setName(obj[1].toString());
            a.setSalesRep(obj[2].toString());
            a.setWhse(obj[3].toString());
            a.setCustType(obj[4].toString());
            shares.add(a);
        }
        
        return shares;
    }
    
    @Override
    public boolean addShare(String listId, String ownerId, String accountId, int cono, BigDecimal custno)
    {
        String sql = "INSERT INTO savelist_shares (id, owner_id, savelist_id, cono, custno) " +
                     " VALUES (:id, :owner_id, :savelist_id, :cono, :custno)";
        Session session = sessionfactory.getCurrentSession();
        
        SQLQuery query = session.createSQLQuery(sql);
        query.setString("id", UUID.randomUUID().toString());
        query.setString("owner_id", ownerId);
        query.setString("savelist_id", listId);
        query.setInteger("cono", cono);
        query.setBigDecimal("custno", custno);
        
        int res = query.executeUpdate();

        return (res >= 0) ? true : false;
    }

    @Override
    public boolean removeShare(String listId)
    {
        String sql = "DELETE FROM savelist_shares WHERE savelist_id = :listId";
        Session session = sessionfactory.getCurrentSession();
        
        SQLQuery query = session.createSQLQuery(sql);
        query.setString("listId", listId);
        
        int res = query.executeUpdate();
        
        return (res >= 0) ? true : false;
    }    
    
    @Override
    public boolean removeShare(String listId, int cono, BigDecimal custno)
    {
        String sql = "DELETE FROM savelist_shares WHERE savelist_id = :listId AND cono = :cono AND custno = :custno";
        Session session = sessionfactory.getCurrentSession();
        
        SQLQuery query = session.createSQLQuery(sql);
        query.setString("listId", listId);
        query.setInteger("cono", cono);
        query.setBigDecimal("custno", custno);
        
        int res = query.executeUpdate();

        return (res >= 0) ? true : false;
    }

    @Override
    public List<ListCategory> getSharedLists(int cono, BigDecimal custno)
    {
        List<ListCategory> results = new ArrayList<ListCategory>();

        String sql = "select c.categoryid, c.name, count(*) " +
                "from view_accounts a " +
                "join savelist_shares s on (s.cono = a.cono AND s.custno = a.custno) " +
                "join savelist_categories c on (c.categoryid = s.savelist_id) " +
                "join savelist_items sl on (c.categoryid = sl.categoryid)  " +
                "where a.cono=:cono and a.custno=:custno " +
                "group by c.categoryid, c.name " ;

        Session session = sessionfactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        query.setInteger("cono", cono);
        query.setBigDecimal("custno", custno);

        List<Object[]> list = query.list();
        for(Object[] obj : list) {
            ListCategory cat = new ListCategory();
            cat.setId(obj[0].toString());
            cat.setName(obj[1].toString());
            cat.setNumItems( Integer.parseInt( obj[2].toString() ) );
            results.add(cat);
        }

        return results;
    }
}