package com.kawauso.base.dataobject;

import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.MenuCategory;
import com.kawauso.base.bean.Product;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("menuDao")
public class MenuDaoImpl implements MenuDao
{
    @Autowired
    private SessionFactory sessionfactory;

    @Override
    public List<MenuCategory> getMenu(int id)
    {
        @SuppressWarnings("unchecked")
        Session session = sessionfactory.getCurrentSession();
        
        Query query = session.getNamedQuery("getProductMenuSubTree").setParameter("id", id);

        return query.list();
    }
    
    @Override
    public List<Breadcrumb> getNavTree(int id)
    {
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        
        String sql = "SELECT savelist_categories.id, savelist_categories.name " +
                     "FROM productMenu AS node, " +
                     "     productMenu AS savelist_categories " +
                     "WHERE node.lft BETWEEN savelist_categories.lft AND savelist_categories.rght " +
                     "      AND node.id=:id AND savelist_categories.id != 1 " +
                     "ORDER BY node.lft;";
        
        Session session = sessionfactory.getCurrentSession();
        
        List<Object[]> list = session.createSQLQuery(sql)
                           .setInteger("id", id)
                           .list();
        
        for(Object[] obj : list) {
            Breadcrumb crumb = new Breadcrumb();
            crumb.setQuery("id=" + obj[0].toString());
            crumb.setName(obj[1].toString());
            
            crumbs.add(crumb);
        }
        
        return crumbs;
    }
    
    @Override
    public List<Breadcrumb> getNavTree(String productCode)
    {
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        
        String sql = "SELECT savelist_categories.id, savelist_categories.name " +
                     "FROM productMenu AS node, productMenu AS savelist_categories " +
                     "WHERE node.lft BETWEEN savelist_categories.lft AND savelist_categories.rght " +
                     "  AND node.id = (SELECT TOP 1 node_id FROM productMenu_products WHERE product='" + productCode + "') " +
                     "  AND savelist_categories.id != 1 " +
                     "ORDER BY node.lft";
        
        Session session = sessionfactory.getCurrentSession();
        
        List<Object[]> list = session.createSQLQuery(sql).list();
        for(Object[] obj : list) {
            Breadcrumb crumb = new Breadcrumb();
            crumb.setQuery("id=" + obj[0].toString());
            crumb.setName(obj[1].toString());
            crumbs.add(crumb);
        }
        
        return crumbs;
    }    
    
    @Override
    public List<Product> getMenuItems(int id)
    {
        @SuppressWarnings("unchecked")
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Product.class);
        crit.add( Restrictions.eq("categoryId", id) );

        return crit.list();
    }
}