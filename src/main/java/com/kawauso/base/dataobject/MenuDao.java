package com.kawauso.base.dataobject;

import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.MenuCategory;
import com.kawauso.base.bean.Product;
import java.util.List;

public interface MenuDao
{
    List<MenuCategory> getMenu(int id);

    List<Breadcrumb> getNavTree(int id);

    List<Breadcrumb> getNavTree(String productCode);

    List<Product> getMenuItems(int id);
}
