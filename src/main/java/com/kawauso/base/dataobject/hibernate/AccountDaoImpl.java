/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.SalesRep;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chris
 */
@Repository
public class AccountDaoImpl implements AccountDao
{
    private static final Logger log = Logger.getLogger( AccountDaoImpl.class );
    
    @Autowired
    private SessionFactory sessionfactory;
    
    @Override
    public SalesRep getSalesRep(int cono, String salesRepId)
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(SalesRep.class);
        crit.add( Restrictions.eq("cono", cono) );
        crit.add( Restrictions.eq("salesRepId", salesRepId) );

        return (SalesRep) crit.uniqueResult();
    }

    @Override
    public List<Account> getAccountsforSalesRep(int cono, String name)
    {
        String sql = "SELECT name, accountno, custno, billing_addr_city, billing_addr_state, billing_addr_zip, slsrepout, slsrepout_name, salesytd, lastsalesytd FROM view_accounts_sales_info WHERE cono=1 " +
                "AND (UPPER(slsrepout_name) LIKE '%"+name+"%' OR UPPER(slsrepout) = '"+name+"') AND statustype=1";

        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(sql);

        List<Object[]> list = query.list();
        List<Account> results = new ArrayList<>();

        for(Object[] obj : list) {

            String custname           = obj[0].toString();
            String accountno          = obj[1].toString();
            String custno             = obj[2].toString();
            String billing_addr_city  = obj[3].toString();
            String billing_addr_state = obj[4].toString();
            String billing_addr_zip   = obj[5].toString();
            String slsrepout          = obj[6].toString();
            String slsrepout_name     = obj[7].toString();
            String salesytd           = obj[8].toString();
            String lastSalesytd       = obj[9].toString();

            int custNum  = Integer.parseInt(custno);
            double sytd  = Double.parseDouble(salesytd);
            double lsytd = Double.parseDouble(lastSalesytd);

            Account account = new Account();
            account.setCustno(new BigDecimal(custNum));
            account.setName(custname);
            account.setCity(billing_addr_city);
            account.setState(billing_addr_state);
            account.setZip(billing_addr_zip);
            account.setSlsRepOut(slsrepout);
            account.setSalesYtd(new BigDecimal(sytd));
            account.setLastSalesYtd(new BigDecimal(lsytd));
            results.add(account);
        }

        return results;
    }

    @Override
    public List<Account> getAccounts(int cono, String key, String val, int limit)
    {
        StringBuilder strbuilder;

        if ( limit > 0)

            strbuilder = new StringBuilder("SELECT TOP " + limit + " name, accountno, custno, billing_addr_city, billing_addr_state, billing_addr_zip, slsrepout, slsrepout_name, salesytd, lastsalesytd FROM view_accounts_sales_info WHERE cono=1 and status=1 ");

        else

            strbuilder = new StringBuilder("SELECT name, accountno, custno, billing_addr_city, billing_addr_state, billing_addr_zip, slsrepout, slsrepout_name, salesytd, lastsalesytd FROM view_accounts_sales_info WHERE cono=1 and status=1 ");

        if (key.equals("tm"))
        {
            strbuilder.append(" AND (UPPER(slsrepout_name) LIKE '%" + val + "%' OR UPPER(slsrepout) = '" + val + "')");
        }
        else if (key.equals("custno")) {
            strbuilder.append(" AND UPPER(custno) like '" + val.toUpperCase() + "%'");
        }
        else if (key.equals("name")) {
            strbuilder.append(" AND name like '" + val + "%'");
        }
        else if (key.equals("city")) {
            strbuilder.append(" AND billing_addr_city like '" + val + "%'");
        }
        else if (key.equals("state")) {
            strbuilder.append(" AND billing_addr_state like '" + val + "%'");
        }
        else if (key.equals("zip")) {
            strbuilder.append(" AND billing_addr_zip like '" + val + "%'");
        }

        strbuilder.append(" order by accountno asc");

        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(strbuilder.toString());

        List<Object[]> list = query.list();
        List<Account> results = new ArrayList<>();

        for(Object[] obj : list) {

            String custname           = obj[0].toString();
            String accountno          = obj[1].toString();
            String custno             = obj[2].toString();
            String billing_addr_city  = obj[3].toString();
            String billing_addr_state = obj[4].toString();
            String billing_addr_zip   = obj[5].toString();
            String slsrepout          = obj[6].toString();
            String slsrepout_name     = obj[7].toString();
            String salesytd           = obj[8].toString();
            String lastSalesytd       = obj[9].toString();

            int custNum  = Integer.parseInt(custno);
            double sytd  = Double.parseDouble(salesytd);
            double lsytd = Double.parseDouble(lastSalesytd);

            Account account = new Account();
            account.setCustno(new BigDecimal(custNum));
            account.setName(custname);
            account.setCity(billing_addr_city);
            account.setState(billing_addr_state);
            account.setZip(billing_addr_zip);
            account.setSlsRepOut(slsrepout);
            account.setSalesYtd(new BigDecimal(sytd));
            account.setLastSalesYtd(new BigDecimal(lsytd));
            results.add(account);
        }

        return results;
    }


    @Override
    public List<String[]> getAccounts(int cono, String term)
    {
        String sql = "SELECT TOP 25 custno, name, billing_addr_city, billing_addr_state FROM view_accounts WHERE cono=1 " +
                     "AND (custno LIKE '"+term+"%' OR name LIKE '"+term+"%') AND status=1";

        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(sql);

        List<Object[]> list = query.list();
        List<String[]> results = new ArrayList<>();

        for(Object[] obj : list) {
            String[] t = { obj[0].toString(), obj[1].toString(), obj[2].toString(), obj[3].toString() };
            results.add(t);
        }

        return results;
    }

    @Override
    //@todo; migrate to hibernate criteria search once we figure out
    //what to do with the contacts <-> mailchimp sync....
    public List<Contact> getContacts(String custno, String term)
    {
        List<Contact> results = new ArrayList<>();

        String top = (custno == null || custno.isEmpty()) ? "TOP 25" : "";

        String sql = "SELECT " + top + "a.custno, c.first_name, c.last_name, c.email_addr, c.phone_work FROM contacts c " +
                "JOIN view_accounts a ON (a.id = c.account_id) WHERE ";

                if(custno != null && !custno.isEmpty())
                    sql += "a.custno="+custno;

                if(term != null && !term.isEmpty()) {
                    if (custno != null && !custno.isEmpty())
                        sql += " AND ";
                    sql += "(last_name LIKE '" + term + "%' OR first_name LIKE '" + term + "%' OR (c.first_name + ' ' + c.last_name) like '%" + term + "%' ) ";
                }

        sql += "ORDER BY last_name, first_name";

        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(sql);

        List<Object[]> list = query.list();

        if(list == null) return results;

        for(Object[] obj : list) {
            Contact c = new Contact();
            if(obj[0] != null && !obj[0].toString().isEmpty())  { c.setCustomerNumber( new BigDecimal(obj[0].toString())); }
            if(obj[1] != null && !obj[1].toString().isEmpty())  { c.setFirstName(obj[1].toString()); }
            if(obj[2] != null && !obj[2].toString().isEmpty())  { c.setLastName(obj[2].toString()); }
            if(obj[3] != null && !obj[3].toString().isEmpty())  { c.setEmailAddr(obj[3].toString()); }
            if(obj[4] != null && !obj[4].toString().isEmpty())  { c.setPhoneWork(obj[4].toString()); }

            results.add(c);
        }

        return results;
    }
}
