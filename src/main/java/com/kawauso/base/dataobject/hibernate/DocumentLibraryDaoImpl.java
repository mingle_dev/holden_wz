/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Document;
import com.kawauso.base.bean.ServiceBulletin;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris
 */
@Repository
public class DocumentLibraryDaoImpl implements DocumentLibraryDao
{
    private static final Logger log = Logger.getLogger( CartDaoImpl.class );
    
    @Autowired
    private SessionFactory sessionfactory;
    
    @Override
    public List<Document> find(String query)
    {
        Criteria crit = sessionfactory.openSession().createCriteria( Document.class );

        if(query != null && !query.isEmpty())
            crit.add( Restrictions.like("title", "%" + query + "%") );

        return crit.list();
    }

    @Override
    public Document findById(String id)
    {
        Session session = sessionfactory.openSession();
        return (Document) session.load(Document.class, id);
    }
            
    @Override
    public void updateCounter(String id)
    {
        String sql = "UPDATE documents SET downloads=downloads+1 WHERE id=:id";
        sessionfactory.openSession().createSQLQuery(sql).setString("id", id).executeUpdate();
    }
    
    @Override
    public List<ServiceBulletin> findServiceBulletins(int year)
    {
        Criteria crit = sessionfactory.openSession().createCriteria( ServiceBulletin.class );
        crit.add( Restrictions.eq("year", year) );
        crit.add( Restrictions.eq("status", 1));
        crit.addOrder(org.hibernate.criterion.Order.asc("smbId"));
        return crit.list();
        /*

        String sql = "SELECT * FROM pricebooktemplates where sort_order=3300";

        List aList =  sessionfactory.openSession().createSQLQuery(sql).list();


        ServiceBulletin aBulletin = new ServiceBulletin();
        aBulletin.setYear(2016);
        aBulletin.setSmbId("Commercial Equipment - BRYANT");
        aBulletin.setId("Commercial Equipment - BRYANT");
        List<ServiceBulletin> bulletinList = new ArrayList<ServiceBulletin>();
        bulletinList.add(aBulletin);
        return bulletinList;
        */

    }    
    
    @Override
    public List<ServiceBulletin> findServiceBulletins(String query)
    {
        Criteria crit = sessionfactory.openSession().createCriteria( ServiceBulletin.class );
        crit.add( Restrictions.like("keywords", "%"+query+"%") );
        crit.add( Restrictions.eq("status", 1));
        crit.addOrder(org.hibernate.criterion.Order.asc("smbId"));
        return crit.list();
    }

    @Override
    public List<ServiceBulletin> findServiceBulletins(String query, int year)
    {
        Criteria crit = sessionfactory.openSession().createCriteria( ServiceBulletin.class );
        crit.add( Restrictions.like("keywords", "%"+query+"%") );
        crit.add( Restrictions.eq("year", year));
        crit.add( Restrictions.eq("status", 1));
        crit.addOrder(org.hibernate.criterion.Order.asc("smbId"));
        return crit.list();
    }
    
    @Override
    public List findServiceBulletinYears()
    {
        String sql = "SELECT DISTINCT year FROM service_bulletins where status=1 ORDER BY year";
        
        return  sessionfactory.openSession().createSQLQuery(sql).list();
    }
    
    @Override
    public ServiceBulletin findServiceBulletin(String id)
    {


        Session session = sessionfactory.openSession();
        return (ServiceBulletin) session.load(ServiceBulletin.class, id);

        /*
        String sql = "SELECT file_size, file_data FROM pricebooktemplates where sort_order=3300";

        String filesize = null;
        java.sql.Blob filedata = null;
        List<Object[]> list = (List<Object []>) sessionfactory.openSession().createSQLQuery(sql).list();

        for (Object[] row : list) {
            filesize = (String) row[0];
            filedata = (java.sql.Blob) row[1];
        }
        ServiceBulletin aBulletin = new ServiceBulletin();
        aBulletin.setId("Commercial Equipment - BRYANT");
        aBulletin.setSmbId("Commercial Equipment - BRYANT");
        aBulletin.setYear(2016);
        aBulletin.setFileSize(Integer.parseInt(filesize));
        aBulletin.setFileData(filedata);
        return aBulletin;
        */

    }
}