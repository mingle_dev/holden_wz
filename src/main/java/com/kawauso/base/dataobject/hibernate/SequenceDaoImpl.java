package com.kawauso.base.dataobject.hibernate;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.impl.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

/**
 * Created by chris on 1/30/15.
 */
@Repository
public class SequenceDaoImpl implements SequenceDao
{
    private static final Logger log = Logger.getLogger( SequenceDaoImpl.class );
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public int getNext(String keyname)
    {
        try {
            Connection conn = ((SessionImpl)sessionFactory.openSession()).connection();
            CallableStatement cs = conn.prepareCall("{ call usp_sequence(?,?) }");
            cs.setString(1, keyname);
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.execute();

            return cs.getInt(2);

        } catch (Exception ex) {
            log.error(ex);
        }

        return 0;
    }
}
