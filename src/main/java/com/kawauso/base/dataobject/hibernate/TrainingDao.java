/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.TrainingCourse;
import com.kawauso.base.bean.TrainingEvent;
import java.util.List;

/**
 *
 * @author chris
 */
public interface TrainingDao
{
    TrainingCourse findCourse(String id) throws Exception;

    List<TrainingCourse> findUpcomingCourses() throws Exception;

    TrainingEvent findEvent(String id) throws Exception;
    
    void registerStudent(String eventId, String contactId) throws Exception;
}
