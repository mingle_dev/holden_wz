package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Location;
import com.kawauso.base.bean.LocationV18;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository("locationDao")
public class LocationDaoImpl implements LocationDao
{
    @Autowired
    private SessionFactory sessionfactory;

    @Override
    public List<Location> findAll()
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Location.class);
        crit.add( Restrictions.eq("cono", 1) );
        crit.add( Restrictions.eq("salesAllowed", 1) );
        crit.add( Restrictions.eq("user1", "web") );
        
        return crit.list();
    }

    @Override
    public List<LocationV18> getLocations(BigDecimal custno, int cono)
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(LocationV18.class);
        crit.add( Restrictions.eq("cono", cono) );
        crit.add( Restrictions.eq("custno", custno.intValue()));
        crit.add( Restrictions.eq("salesAllowed", 1) );
        crit.add( Restrictions.eq("user1", "web") );

        return crit.list();
    }

    
    @Override
    public Location getLocation(String whseId)
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Location.class);
        crit.add( Restrictions.eq("cono", 1) );
        crit.add( Restrictions.eq("whseId", whseId) );
        return (Location) crit.uniqueResult();
    }
    
    /**
     * 
     * ZipCode data found at http://www.boutell.com/zipcodes/
     * 
     * @param zipCd
     * @return 
     */
    @Override
    public Double[] getCoordinates(int zipCd)
    {
        Session session = sessionfactory.getCurrentSession();
        List<Object[]> items = session.createSQLQuery("SELECT latitude, longitude FROM zipcodes WHERE zip=:zip")
                .addScalar("latitude", Hibernate.DOUBLE)
                .addScalar("longitude", Hibernate.DOUBLE)
                .setInteger("zip", zipCd)
                .list();

        try {
            if(items != null && items.size() > 0) {
                Double[] res = new Double[2];
                res[0] = (Double) items.get(0)[0];
                res[1] = (Double) items.get(0)[1];

                return res;
            }
        }catch(Exception ex) {ex.printStackTrace();}
        
        return null;
    }
    
    @Override
    public Double[] getCoordinates(String city, String state)
    {
        if(!state.isEmpty())
            state = "'" + state + "'";
        else
            state = "'AL','GA','FL','MS','SC'";
        
        Session session = sessionfactory.getCurrentSession();
        List<Object[]> items = session.createSQLQuery("SELECT TOP 1 latitude, longitude FROM zipcodes WHERE city=:city AND state IN(" + state + ")")
                .addScalar("latitude", Hibernate.DOUBLE)
                .addScalar("longitude", Hibernate.DOUBLE)
                .setString("city", city)
                .list();
 
        try {
            if(items != null && items.size() > 0) {
                Double[] res = new Double[2];
                res[0] = (Double) items.get(0)[0];
                res[1] = (Double) items.get(0)[1];

                return res;
            }
        }catch(Exception ex) {}

        return null;
    }    
}