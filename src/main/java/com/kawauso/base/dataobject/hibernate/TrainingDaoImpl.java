/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.TrainingCourse;
import com.kawauso.base.bean.TrainingEvent;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris
 */
@Repository("trainingDao")
public class TrainingDaoImpl implements TrainingDao
{
    private static final Logger log = LoggerFactory.getLogger( TrainingDaoImpl.class );
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    public TrainingCourse findCourse(String id) throws Exception
    {
        Session session = sessionFactory.openSession();
        
        //fetch course
        TrainingCourse course = (TrainingCourse) session.load(TrainingCourse.class, id);

        //fetch events
        Query q = session.createQuery("SELECT e FROM TrainingEvent e WHERE e.course=:id AND e.startDt>=:date AND e.privateEvent=0 AND e.deleted=0");
        q.setString("id", id);
        q.setDate("date", new Date());
        
        //bind events to course
        course.setTrainingEvents( q.list() );

        return course;
    }

    @Override
    public List<TrainingCourse> findUpcomingCourses() throws Exception
    {
        Criteria c = sessionFactory.openSession().createCriteria(TrainingCourse.class, "course");
        c.createAlias("course.trainingEvents", "event");
        c.add( Restrictions.eq("status", "Active") );
        c.add( Restrictions.eq("deleted", 0) );
        c.add( Restrictions.ge("event.startDt", new Date()) );
        c.add( Restrictions.eq("event.privateEvent", 0) );
        c.add( Restrictions.eq("event.deleted", 0) );
        c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY); 
        
        return c.list();
    }

    @Override
    public TrainingEvent findEvent(String id) throws Exception
    {
        return (TrainingEvent) sessionFactory.openSession().load(TrainingEvent.class, id);
    }    
    
    @Override
    public void registerStudent(String eventId, String contactId) throws Exception
    {
            String sql = "INSERT INTO trainingEventStudents (id,trainingEvent_id,contact_id,created) " +
                         "VALUES (lower(NEWID()), ?, ?, getdate())";
        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString(1, eventId);
        query.setString(2, contactId);
        query.executeUpdate();
    }    
}
