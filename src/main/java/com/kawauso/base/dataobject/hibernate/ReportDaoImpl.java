/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.reporting.DataField;
import com.kawauso.base.bean.reporting.Report;

import java.util.List;
import java.util.Map;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The idea here is to have an ad-hoc reporting system that users can select
 * from prebuilt reports or build their own by selecting fields and limits. Instead,
 * i ran out of time and just copied queries from jaspersoft
 */
@Repository
public class ReportDaoImpl implements ReportDao
{
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Report run(String name, String type, String cono, String div)
    {
        if(name.equals("slstot") && type.equals("dly"))
            return dailySales(cono);
        if(name.equals("slstot") && type.equals("mtd"))
            return monthlySales(cono);

        if(name.equals("slsloc") && type.equals("dly"))
            return whseDailySales(cono, div);
        if(name.equals("slsloc") && type.equals("mtd"))
            return whseMonthlySales(cono, div);

        if(name.equals("slstm") && type.equals("dly"))
            return TmDailySales(cono);
        if(name.equals("slstm") && type.equals("mtd"))
            return TmMonthlySales(cono);

        if(name.equals("orders"))
            return getOrderList(cono, type, div);

        return null;
    }

    private Report dailySales(String div)
    {
        Report report = new Report();

        //fetch query from mongo
        String sql = "select upper(transtype) 'transtype', count(transtype) 'cnt', sum(totamt) 'net', SUM(margin) 'margin' " +
                "from view_order_line_sales " +
                "where cono = :cono and stagecd=3 " +
                "group by transtype";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", div);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report monthlySales(String div)
    {
        Report report = new Report();

        //fetch query from mongo
        String sql = "select upper(transtype) 'transtype', count(transtype) 'cnt', sum(totamt) 'net', SUM(margin) 'margin' " +
                "from view_order_line_sales " +
                "where cono=:cono " +
                "AND invoicedt between DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0) and DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))) " +
                "GROUP BY transtype";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", div);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report whseDailySales(String cono, String div)
    {
        Report report = new Report();

        String sql = "select count(s.cono) 'cnt', s.cono, s.whse, w.city, " +
               "       SUM(s.totamt) 'net', SUM(s.rebated_cost) 'cost', SUM(margin) 'margin', " +
               "       CASE WHEN SUM(s.totamt) <> 0 THEN (SUM(margin) / SUM(s.totamt)) * 100 ELSE 0.00 END as margin_pct " +
               "from view_order_line_sales s " +
               "join view_warehouses w on (w.cono = s.cono and w.whse = s.whse) " +
               "where s.cono=:cono " +
                ( (div != null) ? "AND w.buygroup=:div " : "") +
               "and s.stagecd=3 " + //div
               "group by s.cono, s.whse, w.city " +
               "order by s.whse asc";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);

        if(div != null) {
            cono = "1";
            query.setString("div", div);
        }

        query.setString("cono", cono);

        List list = query.list();

        report.setResults(list);

        return report;
    }

    private Report whseMonthlySales(String cono, String div)
    {
        Report report = new Report();

        String sql = "SELECT COUNT(s.cono) 'cnt', s.cono, s.whse, w.city " +
            ", SUM(s.totamt) 'net', SUM(s.rebated_cost) 'cost', SUM(margin) 'margin' " +
            ", CASE WHEN SUM(s.totamt) <> 0 THEN (SUM(margin) / SUM(s.totamt)) * 100 ELSE 0.00 END as margin_pct " +
            "FROM view_order_line_sales s " +
            "JOIN view_warehouses w on (w.cono = s.cono and w.whse = s.whse) " +
            "WHERE s.cono=:cono " +
                //div
            "   AND invoicedt between DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0) and DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))) " +
            "GROUP BY s.cono, s.whse, w.city " +
            "ORDER BY s.whse asc";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", cono);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report TmDailySales(String cono)
    {
        Report report = new Report();

        String sql = "select z.cono, slsrepout, s.name, cnt, net, cost, margin, " +
                "       (CASE WHEN net <> 0 THEN ((margin / net) * 100) ELSE 0.00 END) as margin_pct " +
                "from " +
                "( " +
                "	select cono, slsrepout, sum(cnt) as cnt, SUM(net) as net, SUM(cost) as cost, SUM(net - cost) as margin " +
                "	from ( " +
                "	  select h.cono, h.slsrepout, count(h.whse) as cnt, " +
                "			(CASE WHEN h.transtype = 'rm' then SUM(h.totordamt * -1) else SUM(h.totordamt) end) as net,  " +
                "			(CASE WHEN h.transtype = 'rm' then SUM((h.totcost * -1) - h.vendrebamt) else SUM(h.totcost - h.vendrebamt) end) as cost " +
                "	  from nxt_oeeh h " +
                "	  join warehouses w on (w.erpcono = h.cono and w.erpwhse = h.whse) " +
                "	  where h.cono = :cono and h.stagecd = 3 " +
                "	  group by cono, slsrepout, h.transtype " +
                "	) x " +
                "	group by cono, slsrepout " +
                ") z  " +
                "JOIN nxt_smsn s ON (s.cono = z.cono and s.slsrep = z.slsrepout) " +
                "ORDER BY net DESC;";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", cono);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report TmMonthlySales(String cono)
    {
        Report report = new Report();

        String sql = "SELECT z.cono, z.slsrepout, s.name, cnt, net, cost, margin " +
                ", (CASE WHEN net <> 0 THEN ((margin / net) * 100) ELSE 0.00 END) as margin_pct " +
                "FROM ( " +
                "SELECT x.cono, x.slsrepout, SUM(x.cnt) as cnt, SUM(x.net) as net,  SUM(x.cost) as cost, SUM(x.net - x.cost) as margin " +
                "FROM ( " +
                "SELECT a.cono, a.slsrep as 'slsrepout' " +
                ", CASE " +
                "WHEN MONTH(GETDATE()) = 1 and a.salesamt1##1 <> 0  THEN COUNT(a.salesamt1##1) " +
                "WHEN MONTH(GETDATE()) = 2 and a.salesamt1##2 <> 0  THEN COUNT(a.salesamt1##2) " +
                "WHEN MONTH(GETDATE()) = 3 and a.salesamt1##3 <> 0  THEN COUNT(a.salesamt1##3) " +
                "WHEN MONTH(GETDATE()) = 4 and a.salesamt1##4 <> 0  THEN COUNT(a.salesamt1##4) " +
                "WHEN MONTH(GETDATE()) = 5 and a.salesamt1##5 <> 0  THEN COUNT(a.salesamt1##5) " +
                "WHEN MONTH(GETDATE()) = 6 and a.salesamt1##6 <> 0  THEN COUNT(a.salesamt1##6) " +
                "WHEN MONTH(GETDATE()) = 7 and a.salesamt1##7 <> 0  THEN COUNT(a.salesamt1##7) " +
                "WHEN MONTH(GETDATE()) = 8 and a.salesamt1##8 <> 0  THEN COUNT(a.salesamt1##8) " +
                "WHEN MONTH(GETDATE()) = 9 and a.salesamt1##9 <> 0  THEN COUNT(a.salesamt1##9) " +
                "WHEN MONTH(GETDATE()) = 10 and a.salesamt1##10 <> 0 THEN COUNT(a.salesamt1##10) " +
                "WHEN MONTH(GETDATE()) = 11 and a.salesamt1##11 <> 0 THEN COUNT(a.salesamt1##11) " +
                "WHEN MONTH(GETDATE()) = 12 and a.salesamt1##12 <> 0 THEN COUNT(a.salesamt1##12) " +
                "END AS 'cnt' " +
                ", CASE " +
                "WHEN MONTH(GETDATE()) = 1 THEN SUM(a.salesamt1##1) " +
                "WHEN MONTH(GETDATE()) = 2 THEN SUM(a.salesamt1##2) " +
                "WHEN MONTH(GETDATE()) = 3 THEN SUM(a.salesamt1##3) " +
                "WHEN MONTH(GETDATE()) = 4 THEN SUM(a.salesamt1##4) " +
                "WHEN MONTH(GETDATE()) = 5 THEN SUM(a.salesamt1##5) " +
                "WHEN MONTH(GETDATE()) = 6 THEN SUM(a.salesamt1##6) " +
                "WHEN MONTH(GETDATE()) = 7 THEN SUM(a.salesamt1##7) " +
                "WHEN MONTH(GETDATE()) = 8 THEN SUM(a.salesamt1##8) " +
                "WHEN MONTH(GETDATE()) = 9 THEN SUM(a.salesamt1##9) " +
                "WHEN MONTH(GETDATE()) = 10 THEN SUM(a.salesamt1##10) " +
                "WHEN MONTH(GETDATE()) = 11 THEN SUM(a.salesamt1##11) " +
                "WHEN MONTH(GETDATE()) = 12 THEN SUM(a.salesamt1##12) " +
                "END AS 'net' " +
                ", CASE " +
                "WHEN MONTH(GETDATE()) = 1 THEN SUM(a.cogamt1##1) " +
                "WHEN MONTH(GETDATE()) = 2 THEN SUM(a.cogamt1##2) " +
                "WHEN MONTH(GETDATE()) = 3 THEN SUM(a.cogamt1##3) " +
                "WHEN MONTH(GETDATE()) = 4 THEN SUM(a.cogamt1##4) " +
                "WHEN MONTH(GETDATE()) = 5 THEN SUM(a.cogamt1##5) " +
                "WHEN MONTH(GETDATE()) = 6 THEN SUM(a.cogamt1##6) " +
                "WHEN MONTH(GETDATE()) = 7 THEN SUM(a.cogamt1##7) " +
                "WHEN MONTH(GETDATE()) = 8 THEN SUM(a.cogamt1##8) " +
                "WHEN MONTH(GETDATE()) = 9 THEN SUM(a.cogamt1##9) " +
                "WHEN MONTH(GETDATE()) = 10 THEN SUM(a.cogamt1##10) " +
                "WHEN MONTH(GETDATE()) = 11 THEN SUM(a.cogamt1##11) " +
                "WHEN MONTH(GETDATE()) = 12 THEN SUM(a.cogamt1##12) " +
                "END AS 'cost' " +
                "FROM nxt_zzmgdsum a " +
                "JOIN nxt_sasta b ON (a.cono = b.cono) and (a.prodcat = SUBSTRING(b.codeval, 5, 4)) " +
                "WHERE a.cono = :cono " +
                "AND a.yr = RIGHT(YEAR(GETDATE()),2) " +
                "GROUP BY a.cono, a.slsrep, a.salesamt1##1, a.salesamt1##2, a.salesamt1##3, a.salesamt1##4, a.salesamt1##5, a.salesamt1##6, a.salesamt1##7 " +
                ", a.salesamt1##8, a.salesamt1##9, a.salesamt1##10, a.salesamt1##11, a.salesamt1##12 " +
                ") x " +
                "GROUP BY x.cono, x.slsrepout " +
                "HAVING SUM(x.net) != 0 " +
                ") z " +
                "JOIN nxt_smsn s ON (s.cono = z.cono and s.slsrep = z.slsrepout) " +
                "ORDER BY net DESC;";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", cono);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report TmYtdSales(String cono)
    {
        Report report = new Report();

        String sql = "SELECT z.cono, z.slsrep, s.name, cnt, net, cost, margin " +
                ", (CASE WHEN net <> 0 THEN ((margin / net) * 100) ELSE 0.00 END) as 'margin_pct'  " +
                "FROM ( " +
                "SELECT cono, slsrep, SUM(cnt) as 'cnt', SUM(net) as 'net', SUM(cost) as 'cost', SUM(net - cost) as 'margin' " +
                "FROM ( " +
                "SELECT z.cono, z.slsrep, COUNT(z.whse) as cnt " +
                ", SUM(z.salesamt1##1 + z.salesamt1##2 + z.salesamt1##3 + z.salesamt1##4 + z.salesamt1##5 + z.salesamt1##6 + z.salesamt1##7 + z.salesamt1##8 + z.salesamt1##9 + z.salesamt1##10 + z.salesamt1##11 + z.salesamt1##12) as 'net' " +
                ", SUM(z.cogamt1##1 + z.cogamt1##2 + z.cogamt1##3 + z.cogamt1##4 + z.cogamt1##5 + z.cogamt1##6 + z.cogamt1##7 + z.cogamt1##8 + z.cogamt1##9 + z.cogamt1##10 + z.cogamt1##11 + z.cogamt1##12) as 'cost' " +
                "FROM nxt_zzmgdsum z " +
                "JOIN nxt_sasta t ON (z.cono = t.cono) and (z.prodcat = SUBSTRING(t.codeval, 5, 4)) " +
                "WHERE z.yr = RIGHT(YEAR(GETDATE()),2) " +
                "AND t.codeiden = '91' " +
                "AND t.codeval > '09' and t.codeval < '61' " +
                "AND t.codeval != '20' " +
                "GROUP BY z.cono, z.slsrep " +
                ") x " +
                "GROUP BY cono, slsrep " +
                ") z " +
                "JOIN nxt_smsn s ON (s.cono = z.cono and s.slsrep = z.slsrep) " +
                "ORDER BY net DESC;";


        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", cono);

        List list = query.list();
        report.setResults(list);

        return report;
    }

    private Report getOrderList(String cono, String key, String val)
    {
        Report report = new Report();

        String sql = "select orderno, ordersuf, custno, shiptonm, takenby, net, cost, margin, " +
                "       (CASE WHEN net <> 0 THEN ((margin / net) * 100) ELSE 0.00 END) as margin_pct " +
                "from " +
                "( " +
                "	select h.orderno, h.ordersuf, h.custno, h.shiptonm, h.takenby, " +
                "	       (CASE WHEN h.transtype = 'rm' then SUM(h.totordamt * -1) else SUM(h.totordamt) end) as net, " +
                "			   (CASE WHEN h.transtype = 'rm' then SUM((h.totcost * -1) - h.vendrebamt) else SUM(h.totcost - h.vendrebamt) end) as cost, " +
                "         (CASE WHEN h.transtype = 'rm' then SUM((h.totordamt * -1) - ((h.totcost * -1) - h.vendrebamt)) else SUM(h.totordamt - (h.totcost - h.vendrebamt)) end) as margin " +
                "	from nxt_oeeh h " +
                "	where h.cono = :cono and h.stagecd = 3 " +
                (key.equals("slsrep") ? " and h.slsrepout=:val " : " and h.whse=:val ") +
                "	group by h.orderno, h.ordersuf, h.custno, h.shiptonm, h.takenby, h.transtype " +
                ") z";

        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", cono);
        query.setString("val", val);

        List list = query.list();
        report.setResults(list);

        return report;
    }


    //---- experimental ----//
    @Override
    public Report runReport(String reportId, Map<String,Object> params)
    {
        Report report = new Report();
        
        //probably do this in mongo in the ReportService layer
        DataField f1 = new DataField();
        f1.setName("transtype");
        f1.setStrType("String");
        f1.setType(String.class);
        
        DataField f2 = new DataField();
        f2.setName("cnt");
        f2.setStrType("int");
        f2.setType(int.class);

        DataField f3 = new DataField();
        f3.setName("net");
        f3.setStrType("double");
        f3.setType(double.class);
        
        DataField f4 = new DataField();
        f4.setName("margin");
        f4.setStrType("double");
        f4.setType(double.class);
        
        DataField[] fields = new DataField[4];
        fields[0] = f1;
        fields[1] = f2;
        fields[2] = f3;
        fields[3] = f4;
        
        report.setFields(fields);
        
        //fetch query from mongo
        String sql = "select upper(transtype) 'transtype', count(transtype) 'cnt', sum(totamt) 'net', SUM(margin) 'margin' " +
                     "from insite.dbo.OrderLineSales " +
                     "where cono=:cono and stagecd=3 " +
                     "group by transtype";
        
        SQLQuery query = sessionFactory.openSession().createSQLQuery(sql);
        query.setString("cono", "1");
        
        report.setResults(query.list());

        return report;
    }
}
