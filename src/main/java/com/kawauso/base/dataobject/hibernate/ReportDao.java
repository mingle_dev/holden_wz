/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.reporting.Report;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chris
 */
public interface ReportDao
{
    Report run(String name, String type, String cono, String div);

    Report runReport(String reportId, Map<String, Object> params);
}
