package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.SpiffClaimDetails;

/**
 * Created by cbyrd on 5/13/15.
 *
 * This file can be factored out later. It will be placed in the JDBC directory
 */
public interface SpiffClaimDetailsDao {



     SpiffClaimDetails getSpiffClaimDetails(String id);



}
