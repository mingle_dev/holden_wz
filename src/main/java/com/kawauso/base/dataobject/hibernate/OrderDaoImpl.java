package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Order;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kawauso.base.bean.OrderLine;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("orderDao")
public class OrderDaoImpl implements OrderDao {

    private static final String ENTERED_DATE = "entered_date";
    private static final String INVOICE_DATE = "invoice_date";

    @Autowired
    private SessionFactory sessionfactory;

    private int searchResultCount = 0;

    @Override
    public int getSearchResultCount()
    {
        return searchResultCount;
    }

    @Override
    //not used?
    public List<Order> search(int cono, BigDecimal custno, int order, String whse, String po, String prod, String serial, Date fromDt, Date toDt, String type, int stage, int page, int limit)
    {
        String sql = "select prrowid, cono, custno, shipto, orderno, ordersuf, custpo, enterdt, invoicedt, totordamt, totinvamt, stagecd, transtype, " +
                "       whse, shipviaty, refer, shipdt, shipinstr, termstype, shiptonm, shiptoaddr_1, shiptoaddr_2, shiptocity, shiptost, shiptozip " +
                "from ( " +
                "select row_number() OVER ( ORDER BY oeeh.orderno ASC) as recno, totalRows=Count(*) OVER(), " +
                "oeeh.prrowid, oeeh.cono, oeeh.custno, oeeh.shipto, oeeh.orderno, oeeh.ordersuf, oeeh.custpo, oeeh.enterdt, oeeh.shipdt, " +
                "oeeh.invoicedt, oeeh.totordamt, oeeh.totinvamt, oeeh.transtype, oeeh.stagecd, oeeh.whse, oeeh.shipviaty, oeeh.refer, oeeh.shipinstr, " +
                "oeeh.termstype, shiptonm, shiptoaddr##1 as 'shiptoaddr_1', shiptoaddr##2 as 'shiptoaddr_2', shiptocity, shiptost, shiptozip " +
                "from nxt_oeeh ";

        if(prod != null && !prod.isEmpty())
            sql += "JOIN nxt_oeel oeel ON (oeel.cono=oeeh.cono and oeel.orderno=oeeh.orderno and oeel.ordersuf=oeeh.ordersuf) " ;

        if(serial != null && !serial.isEmpty())
            sql += "JOIN nxt_icses icses ON (icses.cono=oeeh.cono and icses.custno=oeeh.custno and icses.invno=oeeh.orderno and icses.invsuf=oeeh.ordersuf) ";

        sql += "WHERE oeeh.cono=" + cono + " AND oeeh.custno=" + custno;

        if(whse != null && !whse.isEmpty())
            sql += " AND oeeh.whse='" + whse + "'";

        if(po != null && !po.isEmpty())
            sql += " AND oeeh.custpo LIKE '%" + po + "%'";

        if(order > 0)
            sql += " AND oeeh.orderno="+order;

        if(prod != null && !prod.isEmpty())
            sql += " AND (oeel.shipprod LIKE '%" + prod + "%' AND oeel.specnstype != 'l')";

        if(serial != null && !serial.isEmpty())
            sql += " AND icses.serialno LIKE '%" + serial + "%'";

        if(type != null && !type.isEmpty())
            sql += " AND oeeh.transtype='" + type + "'";

        if(stage > 0)
            sql += " AND oeeh.stagecd=" + stage;

        sql += ") x WHERE recno between 1 and 20 ORDER BY recno";

        Session session = sessionfactory.getCurrentSession();

        return session.createSQLQuery(sql)
                .addEntity(Order.class)
                .list();
    }

    @Override
    public List<Order> search(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit)
    {
        return search(cono, custno, term, type, stage, sort, page, limit, null, null, null);
    }

    public List<Order> search(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit, String filter_typ, Date fromDt, Date toDt)
    {
        Session session = sessionfactory.getCurrentSession();

        int orderno = 0;
        try { orderno = Integer.parseInt(term); }catch(Exception ignore){}

        String sql = "select b.*, recordsFound " +
                "from ( " +
                "  select row_number() OVER (ORDER BY " + sort + " DESC) as recno, recordsFound=Count(*) OVER(), a.* " +
                "  from  ( " +
                "    select distinct oeeh.* " +
                "    from nxt_oeeh oeeh ";

        if(term != null && !term.isEmpty()) {
            sql += "    LEFT JOIN nxt_oeel oeel ON (oeel.cono=oeeh.cono and oeel.orderno=oeeh.orderno and oeel.ordersuf=oeeh.ordersuf)  " +
                    "    LEFT JOIN nxt_icets icets ON (icets.cono=oeel.cono and icets.orderno=oeel.orderno and icets.ordersuf=oeel.ordersuf and icets.line_no=oeel.line_no) ";
        }

        sql += "    WHERE oeeh.cono=:cono AND oeeh.custno=:custno ";


        if (INVOICE_DATE.equals(filter_typ) || ENTERED_DATE.equals(filter_typ))
        {
            if (fromDt == null) {
                java.util.TimeZone.getTimeZone("UTC");
                fromDt = new Date(0);
            }

            if (toDt == null) toDt = new Date();

            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
            String fromDateString = sdf.format(fromDt);
            String toDateString = sdf.format(toDt);

            if ( INVOICE_DATE.equals(filter_typ) )
                sql += " and ( invoicedt >= '" + fromDateString + "' and invoicedt <= '" + toDateString + "' )";
            else if ( ENTERED_DATE.equals(filter_typ))
                sql += " and ( enterdt >= '" + fromDateString + "' and enterdt <= '" + toDateString + "' )";
        }

        if(term != null && !term.isEmpty()) {
            sql += " AND (oeeh.custpo LIKE :term ";

            if(orderno > 0)
                sql += " OR oeeh.orderno=:orderno ";

            sql += " OR (oeel.shipprod LIKE :term AND oeel.specnstype != 'l') " +
                    " OR icets.serialno LIKE :term) ";
        }

        if(stage > 0)
            sql += " AND oeeh.stagecd=:stage ";

        if(type != null && !type.isEmpty())
            sql += " AND oeeh.transtype=:type ";

        sql += "  ) a ) b WHERE recno between :offset AND :limit ORDER BY recno";

        System.out.println(sql);

        SQLQuery q = session.createSQLQuery(sql)
                .addEntity("b", Order.class)
                .addScalar("recordsFound", Hibernate.INTEGER);

        q.setParameter("cono", cono);
        q.setParameter("custno", custno);

        int offset = 1 + (limit * (page-1));
        q.setParameter("offset", offset);
        q.setParameter("limit", (offset+limit) - 1);

        if(term != null && !term.isEmpty()) {
            q.setParameter("term", "%" + term + "%");
            if(orderno > 0)
                q.setParameter("orderno" , orderno);
        }

        if(stage > 0)
            q.setParameter("stage", stage);

        if(type != null && !type.isEmpty())
            q.setParameter("type", type);

        List<Object[]> rows = q.list();

        //all rows contain count
        searchResultCount = (!rows.isEmpty()) ? (Integer) rows.get(0)[1] : 0;

        List<Order> orders = new ArrayList<Order>();
        for (Object[] row : rows)
            orders.add( (Order) row[0] );

        return orders;
    }

    @Override
    public List<Order> getAll(int cono, BigDecimal custno)
    {
        @SuppressWarnings("unchecked")
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Order.class);
        crit.add( Restrictions.eq("coNo", cono) );
        crit.add( Restrictions.eq("custNo", custno) );

        return crit.list();
    }

    //TODO  Do the implementation
    @Override
    public List<Object[]> getSerialNumbers(Order order)
    {
        Session session = sessionfactory.getCurrentSession();

        String sql = "select line_no, serialno " +
                "from nxt_icets " +
                "where cono=:cono and ordertype='o' and orderno=:orderno and ordersuf=:ordersuf";

        SQLQuery q = session.createSQLQuery(sql);
        q.setInteger("cono", order.getCono());
        q.setInteger("orderno", order.getOrderNo());
        q.setInteger("ordersuf", order.getOrderSuf());

        List<Object[]> list = q.list();

        return list;
    }
}