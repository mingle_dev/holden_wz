/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Document;
import com.kawauso.base.bean.ServiceBulletin;
import java.util.List;

/**
 *
 * @author chris
 */
public interface DocumentLibraryDao
{
    List<Document> find(String query);

    Document findById(String id);

    void updateCounter(String id);
    
    ServiceBulletin findServiceBulletin(String id);

    List<ServiceBulletin> findServiceBulletins(int year);

    List<ServiceBulletin> findServiceBulletins(String query);

    List<ServiceBulletin> findServiceBulletins(String query, int year);

    List findServiceBulletinYears();
}
