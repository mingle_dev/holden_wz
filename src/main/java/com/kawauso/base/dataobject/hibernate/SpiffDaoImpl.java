package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffGroup;
import com.kawauso.jdbc.SpiffDataFacade;
import com.kawauso.jdbc.domain.SpiffClaimDetails;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;




import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chris
 */
@Repository
public class SpiffDaoImpl implements SpiffDao
{
    private final Logger logger = LoggerFactory.getLogger(SpiffDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    
    @Override
    public SpiffClaim findById(String id)
    {
        return (SpiffClaim) sessionFactory.openSession().load(SpiffClaim.class, id);
    }


    /**
     * NOTE: call the Spiff Data Facde for JDBC here
     * @param year
     * @param contactId
     * @return
     */
    @Override
    public List<SpiffClaim> findAll(int year, String contactId)
    {
        //date setup
//        String startDate = String.format("01/01/%d",year);
//        String endDate = String.format("12/31/%d",year);
//        Criteria crit = sessionFactory.openSession().createCriteria( SpiffClaim.class );
//        crit.add( Restrictions.eq("contactId", contactId) );
//       // crit.add( Restrictions.eq("year", year) );
//        crit.add(Restrictions.le("installed",endDate));
//        crit.add(Restrictions.ge("installed",startDate));
//        crit.add( Restrictions.eq("deleted", false) );
//
//        return crit.list();

        SpiffDataFacade f = new SpiffDataFacade();

        List<SpiffClaim> newList = new ArrayList<SpiffClaim>();
        List<SpiffClaimDetails> detailsList = f.findAll(contactId,year);

        for(SpiffClaimDetails details : detailsList){

            SpiffClaim claim = new SpiffClaim();
            claim.setId(details.getId());
            claim.setContactId(details.getContactId());
            claim.setClaimNumber(details.getClaimNumber());
            claim.setCustomerName(details.getCustomerName());
            claim.setEnteredDate(details.getEnteredDate());
            claim.setApprovedDate(details.getApprovedDate());
            claim.setPaidDate(details.getPaidDate());
            claim.setAmount(details.getAmount());
            newList.add(claim);
        }

        return newList;

    }
    
    @Override
    public List<SpiffGroup> findGroups(String program)
    {
        Session session = sessionFactory.openSession();
        
        Criteria crit = session.createCriteria( SpiffGroup.class );
        crit.add( Restrictions.eq("program", program) );
        
        List<SpiffGroup> groups = crit.list();
        for(SpiffGroup group : groups) {
            
            SQLQuery q = session.createSQLQuery("SELECT prod FROM nxt_icsp WHERE prod LIKE '"+group.getProdGroup()+"%'");
            group.setProducts(q.list());
         }

        return groups;
    }

    @Override
    public String[] validateProduct(int cono, BigDecimal custno, String prod, String serial)
    {

        /**
         * This is a revised query method to get both Coolray data
         * and othere data.
         */
        String P_CUSTNO = custno.toPlainString();

        List<Object[]> l = new ArrayList<Object[]>();
        String[] res = new String[2];
        Object[] obj = new Object[2];

        logger.info("inside the validateProduct function ");

        SpiffDataFacade data = new SpiffDataFacade();

        if ((P_CUSTNO.equals("21675")) || (P_CUSTNO.equals("107570")) || (P_CUSTNO.equals("107569"))) {

               //SpiffDataFacade data = new SpiffDataFacade();
               l = data.getClaimAndSerial(cono, prod, serial);
               logger.info("list size is " + l.size());

               obj = l.get(0);

               res[0] = (String) obj[0];
               res[1] = (String) obj[1];

               logger.info("list size is " + l.size());
               logger.info("claim no = " + res[0]);
               logger.info("order status = " + res[1]);

               return res;

        }
        else {


              l = data.getClaimAndSerial(cono,P_CUSTNO,prod,serial);


              if (l.size() == 0){

                  res = new String[2];

              }
              else{


                  obj = l.get(0);
                  res[0] = (String) obj[0];
                  res[1] = (String) obj[1];

              }


              logger.info("list size is " + l.size());
              logger.info("claim no = " + res[0]);
              logger.info("order status = " + res[1]);


              return res;
        }

    }

    public boolean save(SpiffClaim claim)
    {
        Session session = sessionFactory.openSession();
        session.save(claim);
        session.flush();

        //look to see if it actually saved...
        return true;
    }


    public static void main(String[] args){


        SpiffDaoImpl i = new SpiffDaoImpl();

//'21675','107570','107569'

       // String[] x = i.validateProduct(1,new BigDecimal("21675"),"24ANB624A003","1715E01633");

        String[] x = i.validateProduct(1,new BigDecimal("67965"),"58CVA090---1--16","1815A21513");

        //58CVA070---1--12/1715A16847



        System.out.println("claim no = " + x[0] + "....... order no = " + x[1]);

    }
}