package com.kawauso.base.dataobject.hibernate;


import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffClaimDetails;
import com.kawauso.base.bean.SpiffClaimItem;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.joda.time.DateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cbyrd on 5/13/15.
 *
 * This file wil be factored out later. It will be placed in the JDBC package
 */
@Repository
public class SpiffClaimDetailsImpl implements SpiffClaimDetailsDao {

    final static Logger logger = LoggerFactory.getLogger(SpiffClaimDetailsImpl.class);
    private Session session;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SpiffClaimDetails getSpiffClaimDetails(String id) {


        SpiffDaoImpl impl = new SpiffDaoImpl();
        SpiffClaimDetails details = new SpiffClaimDetails();
        DateTime dt = new DateTime();

        //Creating a master-detail relation with the SpiffClaimDetails object
        //We are creating tswo calls to the datrabase. One to get
        // the Spiff claim, the other to get tje claim items.
        String queryForMaster = "select " +
                "   id," +
                "   claim_no," +
                "   customer_name," +
                "   customer_address," +
                "   customer_address_city," +
                "   customer_address_state," +
                "   customer_address_zip," +
                "   installed," +
                "   entered," +
                "   approved," +
                "   paid," +
                "   claim_total," +
                "   deleted " +
                "from dbo.spiff_claims " +
                "where id = :claim_id    " +
                "and  claim_no IS NOT NULL " +
                "and  claim_no != 0 " +
                "and YEAR = " + dt.getYear();

        String queryForDetails = "select classification," +
                                 "product_group, " +
                                 "product," +
                                 "serial_number," +
                                 "from spiff_claim_items " +
                                 " where spiff_claim_id = :cid " +
                                 " and year = " + dt.getYear();


        try{



            session = sessionFactory.openSession();

            SpiffClaim claim = (SpiffClaim) session.createSQLQuery(queryForMaster).setString("claim_id",id);

            List<SpiffClaimItem> claimsItemsList = session.createSQLQuery(queryForDetails).setString("cid", id).list();


            details.setClaim(claim);
            details.setClaimItems(claimsItemsList);

            logger.info("Created spiff claims items for ID #" + id);


        }
        catch(Exception ex){



                logger.warn("Unable to process Spiff Claim Details..");
                ex.printStackTrace();


        }

        return details;
    }


}
