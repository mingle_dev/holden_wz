/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.SalesRep;

import java.util.List;

/**
 *
 * @author chris
 */
public interface AccountDao
{
    List<String[]> getAccounts(int cono, String term);

    SalesRep getSalesRep(int cono, String slsRepId);

    List<Account> getAccountsforSalesRep(int cono, String name);

    List<Account> getAccounts(int cono, String key, String val, int limit);

    List<Contact> getContacts(String custno, String term);
}
