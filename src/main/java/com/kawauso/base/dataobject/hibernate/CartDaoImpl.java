/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Cart;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.ShipVia;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris
 */
@Repository("cartDao")
public class CartDaoImpl implements CartDao
{
    private static final Logger log = Logger.getLogger( CartDaoImpl.class );
    
    @Autowired
    private SessionFactory sessionfactory;

    //@todo; might just want to return a boolean
    @Override
    public CartItem saveItem(String userId, String productCode, String note, int qty, boolean appendQty, BigDecimal price) //need to add for priceType
    {
        Session session = sessionfactory.getCurrentSession();
            
        if(qty == 0) {
            Query q = session.createQuery("DELETE CartItem WHERE userId=:userId AND productCode=:prod");
            q.setString("userId", userId);
            q.setString("prod", productCode);
            q.executeUpdate(); //returns # of records affected
        }else{
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(CartItem.class);
            crit.add( Restrictions.eq("userId", userId) );
            crit.add( Restrictions.eq("productCode", productCode) );
            
            CartItem item = (CartItem) crit.uniqueResult();

            //not found, create a new item
            if(item == null) {
                item = new CartItem();
                item.setUserId(userId);
                item.setProductCode(productCode);
                item.setNote(note);
                item.setPriceType("s"); //(s)tock, (k)it, (n)onstock
                item.setUnitPrice(price);
            }
            
            //set/update qty
            if(appendQty == true)
                item.setQty( item.getQty() + qty );
            else
                item.setQty(qty);    

            //save
            session.save(item); //returns (String) id of record
            session.flush();
        }
 
        return null;
    }
    
    @Override
    public List<CartItem> getCartItems(String userId)
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(CartItem.class);
        crit.add( Restrictions.eq("userId", userId) );
        
        return crit.list();
    }
    
    @Override
    public boolean clear(String userId)
    {
        boolean status = false;
        
        Session session = sessionfactory.getCurrentSession();
        
        try {
            Query q = session.createQuery("DELETE Cart WHERE userId=:userId");
            q.setString("userId", userId);
            q.executeUpdate();

            List<CartItem> items = getCartItems(userId);
            for(CartItem item : items)
                session.delete(item);
            status = true;
        }finally{
            session.flush();
        }
                
        return status;
    }
    
    @Override
    public List<ShipVia> getShipViaList()
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(ShipVia.class);
        crit.add( Restrictions.eq("cono", 1) );
        return crit.list();
    }
    
    @Override
    public Cart getCart(String userId)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(Cart.class);
            crit.add( Restrictions.eq("userId", userId) );

            return (Cart) crit.uniqueResult();
            
        //multiples found, clean up the garbage and start over
        }catch(NonUniqueResultException ex) {
            Query q = sessionfactory.getCurrentSession().createQuery("DELETE Cart WHERE userId=:userId");
            q.setString("userId", userId);
            q.executeUpdate();

            return null;
        }
    }
    
    @Override
    public String saveCart(Cart cart)
    {
        Session session = sessionfactory.getCurrentSession();

        String id = cart.getId();
        if(id != null)
            session.update(cart);
        else
            id = (String) session.save(cart);
        
        session.flush();    
        
        return id;
    }
}