package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Attribute;
import com.kawauso.base.bean.AttributeValue;
import com.kawauso.base.bean.Kit;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.ProductPDRecord;
import com.kawauso.base.bean.UserProduct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private SessionFactory sessionfactory;

    private int searchResultCount = 0;
    
    @Override
    public List<Product> quickLook(int cono, String term, int limit)
    {
        List<Product> products = null;
        
        String sql = "select cono, prod, category_id, status, image, netavail, description " + 
                     "from ( " + 
                      "    select top " + limit +
                      "        prod.id, prod.cono, prod.prod, prod.category_id, " + 
                      "        prod.image, prod.status, prod.netavail, prod.description, SUM(smsw.nolinebill) 'tot' " + 
                      "    from view_products prod " + 
                      "    left join nxt_smsw smsw on (smsw.cono=1 and smsw.yr>10 and smsw.prod=prod.prod) " +
                      "    where prod.cono=1  " + 
                      "        and (prod.prod like :term or prod.description like :term) " + 
                      "        and prod.status in ('A','S') " + 
                      "    group by prod.id, prod.cono, prod.prod, prod.category_id, prod.status, prod.image, prod.netavail, prod.description " + 
                      "    having SUM(smsw.nolinebill) > 0 " + 
                      "    order by tot desc " + 
                      ") res";
        
        Session session = sessionfactory.getCurrentSession();

        List<Object[]> items = session.createSQLQuery(sql).setString("term", "%" + term + "%").list();
        
        for(Object[] obj : items) {
            if(products == null)
                products = new ArrayList<Product>();
            
            Product p = new Product();
            p.setProduct(obj[1].toString());
            p.setDescription(obj[6].toString());
            products.add(p);
        }
        
        return products;
    }

    @Override
    public List<Product> getProductBySku(int cono, String term, Integer limit)
    {
        List<Product> products = new ArrayList<Product>();

        String sql = "select cono, prod, category_id, status, image, netavail, description from view_products " +
        "where status in ('A','S') " +
        " and prod like :term ";

        Session session = sessionfactory.getCurrentSession();

        List<Object[]> items = session.createSQLQuery(sql).setString("term", "%" + term + "%").list();

        for(Object[] obj : items) {
            if(products == null)
                products = new ArrayList<Product>();

            Product p = new Product();
            p.setProduct(obj[1].toString());
            p.setDescription(obj[6].toString());
            products.add(p);
        }

        searchResultCount = products == null ? 0 : products.size();

        return products;
    }

    @Override
    public List<String> getAllSku(int cono, Integer limit)
    {
        List<String> skuList = new ArrayList<String>();

        String sql = "select prod from view_products " +
                "where status in ('A','S') ";


        Session session = sessionfactory.getCurrentSession();

        List<String> items = session.createSQLQuery(sql).list();

        for(String obj : items) {
            if(skuList == null)
                skuList = new ArrayList<String>();

            skuList.add(obj);
        }

        return skuList;
    }

    @Override
    public List<Product> getAlternateProducts(String productCode)
    {

        String sql = "select prod, status, description, prodcat, extend_descrip " +
                "from view_products where prodcat = (select prodcat from view_products where prod=:productCode )" +
                " and cast(netavail as int) > 0 ";

        Session session = sessionfactory.getCurrentSession();

        List<Object[]> list =
                session.createSQLQuery(sql)
                        .setString("productCode", productCode)
                        .list();

        List<Product> items = new ArrayList<Product>();


        for (Object[] obj : list) {
            Product p = new Product();
            p.setProduct(obj[0].toString());
            p.setStatus(obj[1].toString());
            p.setDescription(obj[2].toString());
            p.setProdCat(obj[3].toString());

            if (obj[4] != null){
                //the User Description may not be populated
                p.setUserDescription(obj[4].toString());
            }else{
                //the user description is shortname
                //if is not populated use description
                p.setUserDescription(p.getDescription());
            }

            items.add(p);
        }

        return items;
    }
    @Override
    public List<Product> getAccessories(String[] product)
    {
        List<Product> items = null;

        String in = "";
        for(int i=0; i<product.length; i++) {
            in += "'" + product[i] + "'";
            if(i < product.length - 1)
                in += ", ";
        }

        String sql = "select icsec.altprod, icsp.user4 " +
                "from nxt.dbo.icsec icsec " +
                "join nxt.dbo.icsp icsp on (icsp.cono=icsec.cono and icsp.prod=icsec.altprod) " +
                "where icsec.cono=1 and icsec.rectype='o' and icsec.prod in (" + in + ") " +
                "group by icsec.altprod, icsp.user4";

        Session session = sessionfactory.getCurrentSession();

        List<Object[]> list = session.createSQLQuery(sql).list();

        if(list == null) return null;

        for(Object[] obj : list) {
            if(items == null) items = new ArrayList<Product>();
            Product p = new Product();
            p.setProduct(obj[0].toString());
            p.setDescription(obj[1].toString());
            items.add(p);
        }

        return items;
    }

    @Override
    public int getSearchResultCount()
    {
        return searchResultCount;
    }    
    
    @Override
    public void setSearchResultCount(int cnt)
    {
        searchResultCount = cnt;
    }
    
    @Override
    public List<Product> search(String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset)
    {
        List<Product> products = new ArrayList<Product>();

        boolean applyAttrFilter = false;
        
        //build attribute filter sub-query
        String attrFilter = "";
        if(attributes.size() > 0) {
            attrFilter = "SELECT pa1.product FROM ( ";
            int attrCount = 1;

            for(Attribute attr : attributes) {
                String values = "";
                for(AttributeValue av : attr.getValues()) {
                    if(av.isChecked() == true)
                        values += "'" + av.getName() +"',";
                }
                
                if(!values.isEmpty()) {
                    values += "''";
                    
                    if(attrCount > 1) {
                        attrFilter += "JOIN (SELECT product FROM productAttributes WHERE (tag='" + attr.getName() + "' AND value IN (" + values + "))";
                        attrFilter += ") pa" + attrCount + " ON (pa" + attrCount + ".product = pa1.product) ";
                    }else{
                        attrFilter += "SELECT product FROM productAttributes WHERE (tag='" + attr.getName() + "' AND value IN (" + values + "))";
                        attrFilter += ") pa" + attrCount + " ";
                    }

                    attrCount++;
                    
                    applyAttrFilter = true;
                }
            }
        }        
    
        String sql = "SELECT * " +
            "FROM " +
            "( " +
            "  SELECT row_number() OVER ( ORDER BY "+ sort + " " + dir +") AS recno, totalRows=Count(*) OVER(), " +
            "         data.id, icsp.cono, icsp.prod as 'itemName', icsp.statustype AS 'status', icsp.prodcat as 'prodCat', " +
            "         icsp.user4 AS 'description', data.imagepath, data.image, data.details, " +
            "         data.phydata, coalesce((icsw.qtyonhand - (icsw.qtycommit + icsw.qtyreservd)),0) AS 'netavail' " +
            "  FROM nxt_icsp icsp " +
            "  LEFT JOIN nxt_icsw icsw ON (icsw.cono = icsp.cono AND icsw.prod = icsp.prod AND icsw.whse=:whse ) " +
            "  INNER JOIN productData data ON (data.cono = icsp.cono AND data.prod = icsp.prod) "; //and data.image is not null) ";
                
        if(menuId != null)
            sql += " INNER JOIN dbo.productMenu_products AS menu ON (menu.product = icsp.prod AND menu.node_id=" + menuId + ") ";
        
        if(sort.equals("smsw.nolinebill"))
            sql += " LEFT OUTER JOIN nxt_smsw smsw ON (smsw.cono=1 and smsw.yr=13 AND smsw.prod=icsp.prod AND smsw.whse=:whse) ";

        if(applyAttrFilter == true)
            sql += "  JOIN ( " + attrFilter + ") pa ON (pa.product=icsp.prod) ";

        sql += "  WHERE icsp.cono=1 AND icsp.statustype IN ('A','S') ";
        
        if(term != null && !term.isEmpty())
            sql += " AND (data.prod LIKE :term OR icsp.user4 LIKE :term) ";
        
        sql += ") x " +
               "WHERE recno BETWEEN " + start + " AND " + (start+offset) + " ORDER BY recno";

        Session session = sessionfactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        
        if(term != null && !term.isEmpty())
            query.setString("term", "%" + term.replaceAll(" ", "%") + "%");
        
        List<Object[]> list = query.setString("whse", whse).list();
        
        if(list.size() > 0) {
           searchResultCount = (int) Double.parseDouble( list.get(0)[1].toString() );
        }

        for(Object[] obj : list) {
            Product product = new Product();
            product.setCono(Integer.parseInt(obj[3].toString()));
            product.setProduct(obj[4].toString());
            product.setStatus(obj[5].toString());
            product.setCategoryId(6); //cateogory is menucategroy, not prodcat
            
            product.setDescription(obj[7] == null ? "" : obj[7].toString() );
            product.setImage( obj[9] == null ? "" : obj[9].toString() );
            
            product.setDetails( obj[10] == null ? "" : obj[10].toString() );
            
            try { product.setNetAvail( (int) Double.parseDouble(obj[12].toString()) ); }catch(Exception ignore) { }
            
            products.add(product);
        }

        return products; 
    }

    @Override
    public Map<String, String> getAttributes(String product)
    {
        Map<String, String> results = new HashMap<String, String>();
        
        String sql = "SELECT tag, value FROM productAttributes WHERE cono=1 AND product=:prod";
        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("tag", Hibernate.STRING);
        query.addScalar("value", Hibernate.STRING);
        query.setString("prod", product);
        
        List<Object[]> l = query.list();
        for(Object[] o : l)
            results.put(o[0].toString(), o[1].toString());
        
        return results;
    }
    
    @Override
    public String getManufacturer(String product)
    {
        String sql = "SELECT TOP 1 value as 'manu' FROM productAttributes WHERE cono=1 AND product=:prod AND tag='Manufacturer'";
        SQLQuery query = sessionfactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("manu", Hibernate.STRING);
        query.setString("prod", product);
        
        return (String) query.uniqueResult();
    }
    
//    @Override
//    public List<Attribute> getAttributes(String term, Integer menuId)
//    {
//        List<Attribute> attributes = new ArrayList<Attribute>();
//
//        if(term != null && !term.isEmpty())
//            term = "%" + term.replaceAll(" ", "%") + "%";
//
//        String sql = "select distinct tag, value from productAttributes attr " +
//                     "join nxt_icsp icsp on (icsp.cono=attr.cono and icsp.prod=attr.product) ";
//
//        if(menuId != null)
//            sql += "join productMenu_products menu ON (menu.product = icsp.prod) ";
//
//        sql += "WHERE icsp.cono=1 AND icsp.statustype IN ('A','S') ";
//
//        if(term != null && !term.isEmpty())
//            sql += " AND (icsp.prod LIKE '" + term + "' OR icsp.user4 LIKE '" + term + "') ";
//
//        if(menuId != null)
//            sql += "     AND menu.node_id = " + menuId + " ";
//
//        sql += " AND attr.tag != 'MISC' order by tag asc, value asc";
//
//        Session session = sessionfactory.getCurrentSession();
//
//        List list = session.createSQLQuery(sql).list();
//
//        for(int i=0; i<list.size(); i++) {
//            Object[] obj = (Object[]) list.get(i);
//
//            Attribute attr = new Attribute(obj[0].toString());
//
//            if(!attributes.contains(attr)) {
//                attr.add(obj[1].toString(), false);
//                attributes.add(attr);
//            }else{
//                attr = attributes.get( attributes.indexOf(attr) );
//                attr.add(obj[1].toString(), false);
//            }
//        }
//
//        return attributes;
//    }
    
    @Override
    public List<Product> getAll()
    {
        @SuppressWarnings("unchecked")
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Product.class);
        crit.add( Restrictions.eq("cono", 1) );
        return crit.list();
    }
    
    @Override
    public Product getProduct(String productCode)
    {
        //@SuppressWarnings("unchecked")
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Product.class);
        crit.add( Restrictions.eq("cono", 1) );
        crit.add( Restrictions.eq("product", productCode));

        return (Product) crit.uniqueResult();
    }
    
    @Override
    public String updateUserProduct(UserProduct product)
    {
        Session session = sessionfactory.getCurrentSession();

        //fetch existing record
        Criteria crit = session.createCriteria(UserProduct.class);
        crit.add( Restrictions.eq("userId", product.getUserId()) );
        crit.add( Restrictions.eq("productCode", product.getProductCode()) );
        UserProduct upTmp = (UserProduct) crit.uniqueResult();
        
        //if exists, update record
        if(upTmp != null) {
            upTmp.setDescription( product.getDescription() );
            session.update(upTmp);
            session.flush();
            product.setId( upTmp.getId() );
            
        //otherwise, create new record
        }else{
            session.save(product);
            session.flush();
        }
        
        return product.getId();        
    }
    
    @Override
    public ProductPDRecord getExtendedPricing(int cono, BigDecimal custno, String whse, String productCode, BigDecimal price)
    {
        Session session = sessionfactory.getCurrentSession();
        
        String sql = "select top 1 " +
                     "  saleprice - (stndcost - rebate) as 'margin', " + 
                     "  ((saleprice - (stndcost - rebate) ) / saleprice) * 100 as 'marginPct', " +
                     "  rebate, saleprice, rebrecno, rebateamt, capsell, stndcost, contractno, rebcalcty, custrebty, listprice " +
                     "from ( " +
                     "  select " +
                     "    'margin' = case when " + price + " < capsell then saleprice - (stndcost - rebateamt) " +
                     "                    else saleprice - (stndcost - (rebateamt - ( (" + price + " - capsell) / 2))) " +
                     "               end, " +
                     "    'rebate' = case when " + price + " < capsell then rebateamt " +
                     "                    when rebateamt - ( (" + price + " - capsell) / 2 ) < 0 then 0 " +
                     "                    else rebateamt - ( (" + price + " - capsell) / 2 ) " +
                     "               end, " +
                     "    * " +
                     "   from ( " +
                     "           select " + price + " as 'saleprice', " +
                     "               pdsr.rebrecno, pdsr.rebateamt, pdsr.rebcalcty, pdsr.custrebty, " +
                     "               convert(decimal(18,2), substring(pdsr.user3,1,10)) as 'capsell', " +
                     "               pdsr.contractno, icsw.stndcost, icsw.listprice " +
                     "           from nxt_pdsr pdsr " +
                     "           join nxt_zaesrbty zaesrbty on (zaesrbty.cono = pdsr.cono and zaesrbty.rebty = pdsr.custrebty) " +
                     "           join nxt_icsw icsw on (icsw.cono=pdsr.cono and icsw.prod = pdsr.levelkey) " +
                     "           where pdsr.cono = " + cono + " " +
                     "                   and pdsr.levelkey='" + productCode + "' " +
                     "                   and ( pdsr.startdt <= getdate() and pdsr.enddt >= getdate() ) " +
                     "                   and pdsr.dropshipty = 'w' " +
                     "                   and zaesrbty.custno = " + custno + " " +
                     "                   and icsw.whse = '" + whse + "' " +
                     "   ) a " +
                     ") b " +
                     "order by margin desc";
        
        Object[] obj = (Object[]) session.createSQLQuery(sql).uniqueResult();

        if(obj != null) {
            ProductPDRecord rec = new ProductPDRecord();
            rec.setMargin( new BigDecimal(obj[0].toString()) );
            rec.setMarginPct( new BigDecimal(obj[1].toString()) );
            rec.setCalcRebateAmount( new BigDecimal(obj[2].toString()) );
            rec.setSalePrice( new BigDecimal(obj[3].toString()) );
            rec.setRebateRecord( obj[4].toString() );
            rec.setRebateAmount( new BigDecimal(obj[5].toString()) );
            rec.setCapSell( new BigDecimal(obj[6].toString()) );
            rec.setStandardCost( new BigDecimal(obj[7].toString()) );
            rec.setContractRecord( obj[8].toString() );
            rec.setRebateCalcType( obj[9].toString() );
            rec.setRebateType( obj[10].toString() );
            rec.setListPrice( new BigDecimal(obj[11].toString()) );
            return rec;
        }

        //probably not a pdsr so just go for the icsw cost
        sql = "select icsw.stndcost, icsw.listprice " +
              "  from nxt_icsw icsw " +
                " WHERE icsw.cono="+cono+" AND icsw.whse = '" + whse + "' AND icsw.prod='"+productCode+"'";
        obj = (Object[]) session.createSQLQuery(sql).uniqueResult();
        if(obj != null) {
            BigDecimal stndCost = new BigDecimal(obj[0].toString());
            BigDecimal listPrice = new BigDecimal(obj[1].toString());

            ProductPDRecord rec = new ProductPDRecord();
            rec.setMargin( price.subtract(stndCost) );
            rec.setMarginPct( (price.subtract(stndCost)).divide(price, 2, RoundingMode.CEILING).multiply(new BigDecimal(100)) );
            rec.setCalcRebateAmount( null );
            rec.setSalePrice( price );
            rec.setRebateRecord( null );
            rec.setRebateAmount( null );
            rec.setCapSell( null );
            rec.setStandardCost( stndCost );
            rec.setContractRecord( null );
            rec.setRebateCalcType( null );
            rec.setRebateType( null );
            rec.setListPrice( listPrice );
            return rec;
        }

        return null;
    }
    
    public List<Kit> getKits()
    {
        return getKits(null);
    }
    
    public List<Kit> getKits(String brand)
    {
        //@SuppressWarnings("unchecked")
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(Kit.class);
        if(brand != null)
            crit.add( Restrictions.eq("brand", brand) );

        return crit.list();   
    }
}