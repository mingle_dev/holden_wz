/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris
 */
@Repository("pricebookDao")
public class PricebookDaoImpl implements PricebookDao
{
    private static final Logger log = LoggerFactory.getLogger( PricebookDaoImpl.class );

    private final int cono = 1;
    
    @Autowired
    private SessionFactory sessionFactory;
        
    @Override
    public List<PricebookTemplate> getTemplates(String type, String[] categories)
    {
        Criteria crit = sessionFactory.openSession().createCriteria( PricebookTemplate.class );
        crit.add( Restrictions.eq("type", type) );
        
        if(categories != null)
            crit.add( Restrictions.in("category", categories));

        crit.addOrder(Order.asc("sortOrder"));
        
        return crit.list();
    }
    
    /**
     *
     * @param user
     * @param shipto
     * @param type
     * @param name
     * @param templates
     * @throws Exception
     */
    @Override
    public void run(BigDecimal custno, String shipto, String emailAddr, String type, String name, String[] templates) throws Exception
    {
        Session session = sessionFactory.openSession();
        
        String book_id = String.valueOf(UUID.randomUUID());
        shipto = (shipto.length() > 0) ? shipto : null;

        //fetch the account_id until we merge from sugarcrm
        String sql = "SELECT id from view_accounts WHERE cono=:cono AND custno=:custno AND type='Customer'";
        Object account_id = session.createSQLQuery(sql)
                                        .addScalar("id", Hibernate.STRING)
                                        .setInteger("cono", cono)
                                        .setBigDecimal("custno", custno)
                                        .uniqueResult();

        sql = "INSERT INTO pricebooks " +
              "(id,name,created,user_id,ship_to,emailaddr,makepdf,remove,status,type,account_id) " +
              "VALUES " +
              "(:id,:name,GETDATE(),:user_id,:ship_to,:emailaddr,'1','1','Ready',:type,:account_id)";

        session.createSQLQuery(sql)
                    .setString("id", book_id)
                    .setString("name", name)
                    .setString("user_id", "1") //@todo; fix when migration from sugar complete
                    .setString("ship_to", shipto)
                    .setString("emailaddr", emailAddr)
                    .setString("type", type)
                    .setString("account_id", (String) account_id)
                    .executeUpdate();

        //add templates
        for(String template_id : templates)
        {
            sql = "INSERT INTO pricebooks_pricebookTemplates " +
                  "(id, pricebook_id, template_id) VALUES (:id, :bid, :tid)";

            session.createSQLQuery(sql)
                        .setString("id", String.valueOf(UUID.randomUUID()))
                        .setString("bid", book_id)
                        .setString("tid", template_id)
                        .executeUpdate();
        }

        //set to run
        sql = "UPDATE pricebooks SET status='Queued' WHERE id=:bid";
        session.createSQLQuery(sql).setString("bid", book_id).executeUpdate();
        
        //add to new engine queue
        sql = "insert into pricebookFifoQueue (pricebook_id) values (:bid)";
        session.createSQLQuery(sql).setString("bid", book_id).executeUpdate();
    }
    
    public boolean isRetailProfileActive(int cono, double custno) throws Exception {
        Session session = sessionFactory.openSession();

        int count = ((Long) session.createQuery("select count(*) from Book").uniqueResult()).intValue();

        return (count == 1);
    }

    @Override
    public void updateImage(String id, String type, String data)
    {
        Session session = sessionFactory.openSession();

        String sql = "UPDATE productPricebookImages SET image=:image WHERE id=:id";
        int rows = session.createSQLQuery(sql)
                        .setString("id", id)
                        .setString("image", data)
                        .executeUpdate();

        if(rows == 0)
        {
            sql = "INSERT INTO productPricebookImages (id, image) VALUES (:id, :image)";
            session.createSQLQuery(sql)
                    .setString("id", id)
                    .setString("image", data)
                    .executeUpdate();
        }
    }

    @Override
    public List<String[]> getTemplateNames(int limit)
    {
        Session session = sessionFactory.openSession();

        String top = (limit > 0) ? "TOP " + limit : "";

        //String sql = "SELECT template_name FROM productPricebookItems GROUP BY template_name ORDER BY template_name";
        String sql = "select " + top + " it.template_name, im.image from ( " +
                        "  SELECT template_name " +
                        "  FROM productPricebookItems " +
                        "  GROUP BY template_name " +
                        ") it " +
                        "left join productPricebookImages im on (im.id = it.template_name) " +
                        "ORDER BY it.template_name";

        return session.createSQLQuery(sql)
                    .addScalar("template_name", Hibernate.STRING)
                    .addScalar("image", Hibernate.STRING)
                    .list();
    }

    @Override
    public List<String[]> getSheetNames(String tid)
    {
        if (tid.contains("Supersede"))
        {
            return getCategories(tid);
        }

        Session session = sessionFactory.openSession();

        String sql = "select it.sheet_name, im.image " +
                     "from ( " +
                     "  SELECT sheet_name " +
                     "  FROM productPricebookItems " +
                     "  WHERE template_name=:tid " +
                     "  GROUP BY sheet_name " +
                     ") it " +
                     "left join productPricebookImages im on (im.id = it.sheet_name) " +
                     "ORDER BY it.sheet_name";

        return session.createSQLQuery(sql)
                .addScalar("sheet_name", Hibernate.STRING)
                .addScalar("image", Hibernate.STRING)
                .setString("tid", tid)
                .list();
    }

    private List<String[]> getCategories(String tid)
    {
        Session session = sessionFactory.openSession();

        String sql = "select DISTINCT(catname), null as image, catorder from supersede_categories order by catorder" ;

        return session.createSQLQuery(sql)
                .addScalar("catname", Hibernate.STRING)
                .addScalar("image", Hibernate.STRING)
                .addScalar("catorder", Hibernate.STRING)
                .list();
    }

    @Override
    public String getActiveRetailProfileId(int cono, BigDecimal custno)
    {
        Session session = sessionFactory.openSession();

        String sql = "SELECT uuid " +
                     "FROM pricebookRetailProfiles p " +
                     "WHERE p.cono=:cono AND p.custno=:custno and p.active_profile=1";

        return (String) session.createSQLQuery(sql)
                            .addScalar("uuid", Hibernate.STRING)
                            .setInteger("cono", cono)
                            .setBigDecimal("custno", custno)
                            .uniqueResult();
    }

    @Override
    public List<Product> getProducts(String tid, String sid)
    {
        List<Product> products = new ArrayList<Product>();

        Session session = sessionFactory.openSession();

        /*
        String sql = "SELECT p.prod, p.status, p.description, p.image, p.prodcat " +
                "FROM view_products p " +
                "JOIN ( SELECT prod " +
                "  FROM productPricebookItems " +
                "  WHERE template_name=:tid and sheet_name=:sid " +
                "  GROUP BY prod " +
                ") pb ON pb.prod = p.prod " +
                "WHERE p.status in ('A','S') " +
                "GROUP BY p.prod, p.status, p.description, p.image, p.prodcat";
        */

        String sql = "select distinct p.prod, p.status, p.description, p.image, p.prodcat, i.sort_order " +
                     "from view_products p " +
                     "join productPricebookItems i on (i.prod = p.prod) " +
                     "where i.template_name=:tid and i.sheet_name=:sid and (p.status = 'A' or (UPPER(p.status) = 'S' and (cast (p.netavail as INT)) <>  0)) " +
                     "order by i.sort_order asc";

        List<Object[]> list = session.createSQLQuery(sql)
                        .setString("tid", tid)
                        .setString("sid", sid)
                        .list();

        for(Object[] obj : list) {
            Product p = new Product();
            p.setProduct( obj[0].toString() );
            try { p.setStatus( obj[1].toString() ); }catch(Exception e) {}
            try { p.setDescription( obj[2].toString() ); }catch(Exception e) {}
            try { p.setImage( obj[3].toString() ); }catch(Exception e) {}
            try { p.setProdCat(obj[4].toString()); }catch(Exception e) {}
            products.add(p);
        }

        return products;
    }

    @Override
    public List<Product> getSupersedeProducts(int cono, String oper, String tid, String sid)
    {
        List<Product> products = new ArrayList<Product>();

        Session session = sessionFactory.openSession();

        String sql;

        if (tid.contains("North Alabama"))
        {
            sql = "SELECT DISTINCT prod " +
                    ", status " +
                    ", description " +
                    ", image " +
                    ", catname " +
                    ", catorder " +
                    "FROM view_supersedes_north_alabama " +
                    "where catname=:catname" ;
        }
        else {

            sql = "SELECT DISTINCT prod " +
                    ", status " +
                    ", description " +
                    ", image " +
                    ", catname " +
                    ", catorder " +
                    "FROM view_supersedes " +
                    "where catname=:catname" ;
        }

        List<Object[]> list = session.createSQLQuery(sql)
                .setString("catname", sid)
                .list();

        for(Object[] obj : list) {
            Product p = new Product();
            p.setProduct( obj[0].toString() );
            try { p.setStatus( obj[1].toString() ); }catch(Exception e) {}
            try { p.setDescription( obj[2].toString() ); }catch(Exception e) {}
            try { p.setImage( obj[3].toString() ); }catch(Exception e) {}
            try { p.setProdCat(obj[4].toString()); }catch(Exception e) {}
            products.add(p);
        }

        return products;
    }

    public List<Product> getSupersedeProducts(String tid, String sid)
    {
        List<Product> products = new ArrayList<Product>();

        Session session = sessionFactory.openSession();

        String sql = "select distinct p.prod, p.status, p.description, p.image, p.prodcat, i.sort_order " +
                "from view_products p " +
                "join productPricebookItems i on (i.prod = p.prod) " +
                "where i.template_name=:tid and i.sheet_name=:sid and ((UPPER(p.status) = 'S' and (cast (p.netavail as INT)) <>  0)) " +
                "order by i.sort_order asc";

        List<Object[]> list = session.createSQLQuery(sql)
                .setString("tid", tid)
                .setString("sid", sid)
                .list();

        for(Object[] obj : list) {
            Product p = new Product();
            p.setProduct( obj[0].toString() );
            try { p.setStatus( obj[1].toString() ); }catch(Exception e) {}
            try { p.setDescription( obj[2].toString() ); }catch(Exception e) {}
            try { p.setImage( obj[3].toString() ); }catch(Exception e) {}
            try { p.setProdCat(obj[4].toString()); }catch(Exception e) {}
            products.add(p);
        }

        return products;
    }
    /**
     * The method name is a little misleading. This returns a list of products
     * that are from the same product categories found in the selected
     * template/sheet combination EXCLUDING the products found on the template/sheet
     */
    @Override
    public List<Product> getAllProducts(String tid, String sid)
    {
        List<Product> products = new ArrayList<Product>();

        Session session = sessionFactory.openSession();

        String sql = "SELECT vp2.prod, vp2.status, vp2.description, vp2.image " +
                "FROM productPricebookItems ppi " +
                "JOIN view_products vp on vp.prod=ppi.prod " +
                "JOIN view_products vp2 on (vp2.prodcat=vp.prodcat) "+
                "WHERE ppi.template_name=:tid " +
                "  and ppi.sheet_name=:sid " +
                "  and vp.status in ('A','S') " +
                "  and vp2.prod not in ( " +
                "    SELECT prod FROM productPricebookItems " +
                "    WHERE template_name=:tid AND sheet_name=:sid GROUP BY prod" +
                "  )" +
                "GROUP BY vp2.prod, vp2.status, vp2.description, vp2.image";

        List<Object[]> list = session.createSQLQuery(sql)
                .setString("tid", tid)
                .setString("sid", sid)
                .list();

        for(Object[] obj : list) {
            Product p = new Product();
            p.setProduct( obj[0].toString() );
            try { p.setStatus( obj[1].toString() ); }catch(Exception e) {}
            try { p.setDescription( obj[2].toString() ); }catch(Exception e) {}
            try { p.setImage( obj[3].toString() ); }catch(Exception e) {}
            try { p.setProdCat( obj[4].toString() ); }catch(Exception e) {}
            products.add(p);
        }

        return products;
    }

    @Override
    public List<RetailPricebookProfile> getRetailProfiles(int cono, BigDecimal custno) throws Exception
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String sql = "SELECT uuid,profile_name,tax_rate,create_dt,active_profile " +
                "FROM pricebookRetailProfiles " +
                "WHERE cono=:cono AND custno=:custno and deleted=0";

        List<Object[]> res = sessionFactory.openSession().createSQLQuery(sql)
                .setInteger("cono", cono)
                .setBigDecimal("custno", custno)
                .list();

        List<RetailPricebookProfile> profiles = new ArrayList<RetailPricebookProfile>();
        for(Object[] obj : res) {
            RetailPricebookProfile profile = new RetailPricebookProfile();
            profile.setId(obj[0].toString());
            profile.setName(obj[1].toString());
            profile.setTaxRate( Double.parseDouble( obj[2].toString() ) );
            profile.setCreateDate(formatter.parse(obj[3].toString()));
            profile.setActive(obj[4].toString().equals("1") ? true : false);
            profiles.add(profile);
        }

        return profiles;
    }

    @Override
    public RetailPricebookProfile getRetailProfile(String profileId) throws Exception
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String sql = "SELECT uuid,profile_name,tax_rate,create_dt,active_profile " +
                "FROM pricebookRetailProfiles " +
                "WHERE uuid=:profileId and deleted=0";

        Object[] obj = (Object[]) sessionFactory.openSession()
                .createSQLQuery(sql)
                .setString("profileId", profileId)
                .uniqueResult();

        RetailPricebookProfile profile = new RetailPricebookProfile();
        profile.setId(obj[0].toString());
        profile.setName(obj[1].toString());
        profile.setTaxRate( Double.parseDouble( obj[2].toString() ) );
        profile.setCreateDate(formatter.parse(obj[3].toString()));
        profile.setActive(obj[4].toString().equals("1") ? true : false);

        return profile;
    }

    @Override
    public List<RetailPricebookItemGroup> getRetailItemGroups(int cono, String itemGroupId, String[] rootCategories) throws Exception
    {
        List<RetailPricebookItemGroup> groups = null;

        if(itemGroupId != null && !itemGroupId.isEmpty()) {
            String sql =
                    "SELECT pg.gm_pct, pg.labor_amt, pg.misc_amt, pg.comm_rate, ig.category_id, c.category, ig.filter_group, ig.description " +
                            "FROM pricebookRetailProfileGroups pg " +
                            "RIGHT JOIN pricebookItemGroups ig ON (ig.item_group = pg.item_group AND pg.profile_id=:itemGroupId) " +
                            "INNER JOIN pricebookCategories c ON (ig.category_id = c.uuid) " +
                            "WHERE ig.cono=:cono AND ig.deleted=0 AND " +
                            "  ig.category_id IN (" +
                            "      SELECT DISTINCT cg.addon_category_id " +
                            "      FROM pricebookCategoryGroups cg " +
                            "      WHERE " + buildInClause("cg.category_id", rootCategories) +
                            "  ) " +
                            "GROUP BY pg.gm_pct, pg.labor_amt, pg.misc_amt, pg.comm_rate, ig.category_id, c.category, ig.filter_group, ig.description " +
                            "ORDER BY ig.category_id, ig.filter_group";

            List<Object[]> objArr = sessionFactory.openSession().createSQLQuery(sql)
                    .addScalar("gm_pct", Hibernate.DOUBLE)
                    .addScalar("labor_amt", Hibernate.DOUBLE)
                    .addScalar("misc_amt", Hibernate.DOUBLE)
                    .addScalar("comm_rate", Hibernate.DOUBLE)
                    .addScalar("category_id", Hibernate.STRING)
                    .addScalar("category", Hibernate.STRING)
                    .addScalar("filter_group", Hibernate.STRING)
                    .addScalar("description", Hibernate.STRING)
                    .setInteger("cono", cono)
                    .setString("itemGroupId", itemGroupId)
                    .list();

            groups = new ArrayList<RetailPricebookItemGroup>();
            for(Object[] obj : objArr) {
                RetailPricebookItemGroup g = new RetailPricebookItemGroup();
                g.setGmPct(obj[0] != null ? Double.parseDouble(obj[0].toString()) : 0.00);
                g.setLaborAmt(obj[1] != null ? Double.parseDouble(obj[1].toString()) : 0.00);
                g.setMiscAmt(obj[2] != null ? Double.parseDouble(obj[2].toString()) : 0.00);
                g.setCommissionRate(obj[3] != null ? Double.parseDouble(obj[3].toString()) : 0.00);
                g.setCategoryId(obj[4] != null ? obj[4].toString() : "");
                g.setCategoryName(obj[5] != null ? obj[5].toString() : "");
                g.setName(obj[6] != null ? obj[6].toString() : "");
                g.setDescription(obj[7] != null ? obj[7].toString() : "");

                groups.add(g);
            }


        }else{
            String sql = "SELECT 0.00 as gm_pct, 0.00 as labor_amt, 0.00 as misc_amt, 0.00 as comm_rate, category_id, category, filter_group, description " +
                    "FROM ( SELECT ig.category_id, c.category, ig.filter_group, ig.description " +
                    "       FROM pricebookItemGroups ig " +
                    "       INNER JOIN pricebookCategories c ON (ig.category_id = c.uuid) " +
                    "       WHERE ig.cono=:cono AND ig.deleted=0 AND " +
                    "       ig.category_id IN (" +
                    "              SELECT DISTINCT cg.addon_category_id " +
                    "              FROM pricebookCategoryGroups cg " +
                    "              WHERE " + buildInClause("cg.category_id", rootCategories) +
                    "       ) " +
                    "       GROUP BY ig.category_id, c.category, ig.filter_group, ig.description " +
                    "      ) z " +
                    "ORDER BY category_id, filter_group";

            List<Object[]> objArr = sessionFactory.openSession().createSQLQuery(sql)
                    .addScalar("gm_pct", Hibernate.DOUBLE)
                    .addScalar("labor_amt", Hibernate.DOUBLE)
                    .addScalar("misc_amt", Hibernate.DOUBLE)
                    .addScalar("comm_rate", Hibernate.DOUBLE)
                    .addScalar("category_id", Hibernate.STRING)
                    .addScalar("category", Hibernate.STRING)
                    .addScalar("filter_group", Hibernate.STRING)
                    .addScalar("description", Hibernate.STRING)
                    .setInteger("cono", cono)
                    .list();

            groups = new ArrayList<RetailPricebookItemGroup>();
            for(Object[] obj : objArr) {
                RetailPricebookItemGroup g = new RetailPricebookItemGroup();
                g.setGmPct(obj[0] != null ? Double.parseDouble(obj[0].toString()) : 0.00);
                g.setLaborAmt(obj[1] != null ? Double.parseDouble(obj[1].toString()) : 0.00);
                g.setMiscAmt(obj[2] != null ? Double.parseDouble(obj[2].toString()) : 0.00);
                g.setCommissionRate(obj[3] != null ? Double.parseDouble(obj[3].toString()) : 0.00);
                g.setCategoryId(obj[4] != null ? obj[4].toString() : "");
                g.setCategoryName(obj[5] != null ? obj[5].toString() : "");
                g.setName(obj[6] != null ? obj[6].toString() : "");
                g.setDescription(obj[7] != null ? obj[7].toString() : "");

                groups.add(g);
            }
        }

        return groups;
    }

    @Override
    public void disableRetailProfiles(int cono, BigDecimal custno) throws Exception
    {
        String sql = "UPDATE pricebookRetailProfiles SET active_profile=0 WHERE cono=:cono AND custno=:custno";
        sessionFactory.openSession().createSQLQuery(sql)
                .setInteger("cono", cono)
                .setBigDecimal("custno", custno)
                .executeUpdate();
    }

    @Override
    public void enableRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception
    {
        this.disableRetailProfiles(cono, custno);

        String sql = "UPDATE pricebookRetailProfiles SET active_profile=1 WHERE uuid=:profileId";
        sessionFactory.openSession().createSQLQuery(sql)
                .setString("profileId", profileId)
                .executeUpdate();
    }

    @Override
    public void deleteRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception
    {
        String sql = "UPDATE pricebookRetailProfiles " +
                "SET active_profile=0, deleted=1, deleted_date=GETDATE() " +
                "WHERE cono=:cono AND custno=:custno AND uuid=:profileId";
        sessionFactory.openSession().createSQLQuery(sql)
                .setInteger("cono", cono)
                .setBigDecimal("custno", custno)
                .setString("profileId", profileId)
                .executeUpdate();
    }

    @Override
    public void cloneRetailPricebookProfile(String profileId) throws Exception
    {
        String cloneId = String.valueOf(UUID.randomUUID());

        String sql = "INSERT INTO pricebookRetailProfiles (uuid,cono,custno,profile_name,tax_rate,active_profile,create_dt) " +
                "SELECT :cloneId,cono,custno, profile_name + ' (copy)', tax_rate, 0, GETDATE() " +
                "  FROM pricebookRetailProfiles " +
                "  WHERE uuid=:profileId";
        int res = sessionFactory.openSession().createSQLQuery(sql)
                .setString("cloneId", cloneId)
                .setString("profileId", profileId)
                .executeUpdate();

        if(res > 0) {
            sql = "INSERT INTO pricebookRetailProfileGroups (uuid,profile_id,item_group,gm_pct,labor_amt,misc_amt,comm_rate) " +
                    "SELECT NEWID(),:cloneId,item_group,gm_pct,labor_amt,misc_amt,comm_rate " +
                    "  FROM pricebookRetailProfileGroups " +
                    "  WHERE profile_id=:profileId";
            sessionFactory.openSession().createSQLQuery(sql)
                    .setString("cloneId", cloneId)
                    .setString("profileId", profileId)
                    .executeUpdate();
        }
    }

    @Override
    public void createRetailProfile(int cono, BigDecimal custno, String profileName, double taxRate, Map<String, String[]> map) throws Exception
    {
        String profileId = String.valueOf(UUID.randomUUID());

        String sql = "INSERT INTO pricebookRetailProfiles " +
                "(uuid,cono,custno,profile_name,tax_rate,create_dt) " +
                "VALUES " +
                "(:uuid,:cono,:custno,:profile_name,:tax_rate,GETDATE())";

        sessionFactory.openSession().createSQLQuery(sql)
                .setString("uuid", profileId)
                .setInteger("cono", cono)
                .setBigDecimal("custno", custno)
                .setString("profile_name", profileName)
                .setDouble("tax_rate", taxRate)
                .executeUpdate();

        for(int i=1; i<=map.size(); i++)
        {
            String[] e = map.get(String.valueOf(i));

            double gm = 0, labor = 0, misc = 0, comm =0;
            try { gm = Double.parseDouble(e[2]); }catch(Exception ex){}
            try { labor = Double.parseDouble(e[3]); }catch(Exception ex){}
            try { misc = Double.parseDouble(e[4]); }catch(Exception ex){}
            try { comm = Double.parseDouble(e[5]); }catch(Exception ex){}

            sql = "SELECT g.item_group FROM pricebookItemGroups g " +
                    //"WHERE g.cono='1' AND g.filter_group="+e[0]+" AND g.deleted='0'";
                    "WHERE g.cono='1' AND g.filter_group=:group AND g.deleted='0'";

            List list = sessionFactory.openSession().createSQLQuery(sql)
                    .setString("group", e[0])
                    .list();

            for(int j=0; j<list.size(); j++) {
                String item_group = list.get(j).toString();
                this.insertRetailProfileGroup(profileId, item_group, gm, labor, misc, comm);
            }
        }
    }

    public void updateRetailProfile(String profileId, BigDecimal custno, String profileName, double taxRate, HashMap<String, String[]> map) throws Exception
    {
        String sql = "UPDATE pricebookRetailProfiles " +
                "SET profile_name=:profile_name, tax_rate=:tax_rate, create_dt=GETDATE() " +
                "WHERE uuid=:profile_id";

        sessionFactory.openSession().createSQLQuery(sql)
                .setString("profile_name", profileName)
                .setDouble("tax_rate", taxRate)
                .setString("profile_id", profileId)
                .executeUpdate();

        for(int i=1; i<=map.size(); i++)
        {
            String[] e = map.get(String.valueOf(i));

            double gm = 0, labor = 0, misc = 0, comm = 0;
            try { gm = Double.parseDouble(e[2]); }catch(Exception ex){}
            try { labor = Double.parseDouble(e[3]); }catch(Exception ex){}
            try { misc = Double.parseDouble(e[4]); }catch(Exception ex){}
            try { comm = Double.parseDouble(e[5]); }catch(Exception ex){}

            sql = "SELECT g.item_group FROM pricebookItemGroups g " +
                    "WHERE g.cono='1' AND g.filter_group=:group AND g.deleted='0'";

            List list = sessionFactory.openSession().createSQLQuery(sql)
                    .setString("group", e[0])
                    .list();

            for(int j=0; j<list.size(); j++) {
                String item_group = list.get(j).toString();
                int val = updateRetailProfileGroup(profileId, item_group, gm, labor, misc, comm);

                if(val == 0)
                    insertRetailProfileGroup(profileId, item_group, gm, labor, misc, comm);
            }
        }
    }

    /**
     *
     * @param profile_id
     * @param item_group
     * @param gm
     * @param labor
     * @param misc
     * @param comm
     * @throws Exception
     */
    private void insertRetailProfileGroup(String profile_id, String item_group, double gm, double labor, double misc, double comm) throws Exception
    {
        String sql = "INSERT INTO pricebookRetailProfileGroups " +
                "(uuid,profile_id,item_group,gm_pct,labor_amt,misc_amt,comm_rate) " +
                "VALUES " +
                "(NEWID(),:profile_id,:item_group,:gm,:labor,:misc,:comm)";

        sessionFactory.openSession().createSQLQuery(sql)
                .setString("profile_id", profile_id)
                .setString("item_group", item_group)
                .setDouble("gm", gm)
                .setDouble("labor", labor)
                .setDouble("misc", misc)
                .setDouble("comm", comm)
                .executeUpdate();
    }

    /**
     *
     * @param profile_id
     * @param item_group
     * @param gm
     * @param labor
     * @param misc
     * @param comm
     * @return
     * @throws Exception
     */
    private int updateRetailProfileGroup(String profile_id, String item_group, double gm, double labor, double misc, double comm) throws Exception
    {
        String sql = "UPDATE pricebookRetailProfileGroups " +
                "SET gm_pct=:gm,labor_amt=:labor,misc_amt=:misc,comm_rate=:comm " +
                "WHERE profile_id=:profile_id AND item_group=:group";

        return sessionFactory.openSession().createSQLQuery(sql)
                .setDouble("gm", gm)
                .setDouble("labor", labor)
                .setDouble("misc", misc)
                .setDouble("comm", comm)
                .setString("profile_id", profile_id)
                .setString("group", item_group)
                .executeUpdate();
    }

    /**
     *
     * @param column
     * @param params
     * @return
     */
    private String buildInClause(String column, String[] params)
    {
        if(params.length == 0)
            return column + "=''";

        String c = "";
        for(int i=1; i<=params.length; i++) {
            c += "'" + params[i-1] + "'";
            if(i<params.length)
                c += ",";
        }
        return column + " IN (" + c + ")";
    }
}