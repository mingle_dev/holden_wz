package com.kawauso.base.dataobject;

import com.kawauso.base.Utilities.Encryption;
import com.kawauso.base.bean.Contact;
import com.kawauso.base.bean.User;
import com.kawauso.base.bean.UserFavorite;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository("userDao")
public class UserDaoImpl implements UserDao
{
    private static final String SECURITYSALT = "DYhG93b0qyJfIxfs2guVoUubWwvniR7G0FgaC9mi";
    private static final Logger log = Logger.getLogger( UserDaoImpl.class );
    
    @Autowired
    private SessionFactory sessionfactory;

    @Override
    public void saveUser(User user, boolean encyrptPassword)
    {
        try {
            if(encyrptPassword == true)
                user.setPassword( Encryption.SHA1( SECURITYSALT + user.getPassword() ) );

            //setup default security
            user.setSecurityOrderInquiry(true);
            user.setSecurityOrderCreate(true);
            user.setSecurityBasePricing(true);

            Session session = sessionfactory.getCurrentSession();
            session.saveOrUpdate(user);
            session.flush();

//            //@todo; remove once old site is shutdown and bluedart logic migrated to helix
//            String sql = "update contacts " +
//                    "set contacts.account_id = x.id " +
//                    "from contacts " +
//                    "inner join " +
//                    "(" +
//                    "  select a.id, a.custno " +
//                    "  from contacts c " +
//                    "  join view_accounts a on (a.cono=1 and a.custno = c.custno) " +
//                    "  where c.id='"+ user.getContactId() +"' " +
//                    ") x on contacts.custno = x.custno";
//            session.createSQLQuery(sql).executeUpdate();
//            session.flush();

//            //@todo; remove once old site is shutdown and bluedart logic migrated to helix
//            sql = "INSERT INTO userSecuritySettings (user_id,orderInquiry,orderCreate,accountInquiry,productPricing,userAdmin) " +
//                  "VALUES ('"+user.getId()+"',1,1,0,1,0)";
//            SQLQuery query = session.createSQLQuery(sql);
//            query.executeUpdate();
//            session.flush();

        }catch(Exception ex) {
            log.error(ex);
        }
    }

    @Override
    public User getUser(String username, String password)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(User.class);
            crit.add( Restrictions.eq("username", username) );
            crit.add(Restrictions.eq("password", Encryption.SHA1(SECURITYSALT + password)));

            return (User) crit.uniqueResult();        
        }catch(Exception ex) {
            log.error(ex);
            return null;
        }
    }

    @Override
    public User getUser(String username, String password, int cono)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(User.class);
            crit.add( Restrictions.eq("username", username) );
            crit.add(Restrictions.eq("password", Encryption.SHA1(SECURITYSALT + password)));
            crit.add(Restrictions.eq("cono", cono));

            return (User) crit.uniqueResult();
        }catch(Exception ex) {
            log.error(ex);
            return null;
        }
    }
    
    @Override
    public User getUser(String userId)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(User.class);
            crit.add( Restrictions.eq("id", userId) );
            return (User) crit.uniqueResult();        
        }catch(Exception ex) {
            log.error(ex);
            return null;
        }
    }  

    @Override
    public User getUserFromToken(String token)
    {
        try {
            String sql = "SELECT * FROM users " +
                         "WHERE CONVERT(VARCHAR(4000), HASHBYTES('SHA1', CONCAT(username,password)), 2) = '" + token + "'";

            Query query = sessionfactory.getCurrentSession().createSQLQuery(sql)
                    .addEntity(User.class);

            return (User) query.uniqueResult();

        }catch(Exception ex) {
            log.error(ex);
            return null;
        }

    }

    @Override
    public boolean validUserName(String username)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(User.class);
            crit.add( Restrictions.eq("username", username) );
            return (crit.list().isEmpty()) ? true : false;
            
        }catch(Exception ex) {
            return false;
        }
    }

    @Override
    public String getContactId(BigDecimal custno, String cono, String firstName, String lastName)
    {
        String result = null;
        
        try {
            String sql = "select id from accounts a where a.cono=:cono and a.accountno=:custno";
            Query query = sessionfactory.getCurrentSession().createSQLQuery(sql)
                                .addScalar("id", StringType.class.newInstance())
                                .setParameter("cono", cono)
                                .setParameter("custno", custno);            
            String accountId = (String) query.uniqueResult();
            
            sql = "select id from contacts c where (c.custno=:custno OR c.account_id=:acctid) AND c.first_name=:first AND c.last_name=:last";
            query = sessionfactory.getCurrentSession().createSQLQuery(sql)
                            .addScalar("id", StringType.class.newInstance())
                            .setParameter("custno", custno)
                            .setParameter("acctid", accountId)
                            .setParameter("first", firstName.trim())
                            .setParameter("last", lastName.trim());

            result = (String) query.uniqueResult();
        }catch(Exception ex) {
            log.error(ex);
        }

        return result;
    }
    
    @Override
    public void saveContact(Contact contact)
    {
        Session session = sessionfactory.getCurrentSession();
        session.saveOrUpdate(contact);
        session.flush();
    }   
    
    @Override
    public Contact getContact(String contactId)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(Contact.class);
            crit.add( Restrictions.eq("id", contactId) );
            Contact contact = (Contact) crit.uniqueResult();
            contact.setCustomerNumber( getCustomerNumber(contact.getAccountId()) );

            return contact;
            
        }catch(Exception ex) {
            log.error(ex);
            return null;
        }
    }
    
    @Override
    public String getUserGroup(String groupId)
    {
        try {
            String sql = "select name FROM groups where id=:id";
            Query query = sessionfactory.getCurrentSession().createSQLQuery(sql)
                                .addScalar("name", StringType.class.newInstance())
                                .setParameter("id", groupId);
            return (String) query.uniqueResult();
        }catch(Exception ex) {
            log.error(ex);
            return null;
        }
    }
    
    /**
     * 
     * @param accountId
     * @return 
     */
    private BigDecimal getCustomerNumber(String accountId)
    {
        BigDecimal result = null;
                
        String sql = "select accountno from accounts WHERE id=:id";
        try {
            Query query = sessionfactory.getCurrentSession().createSQLQuery(sql)
                                .addScalar("accountno", BigDecimalType.class.newInstance())
                                .setParameter("id", accountId);
            result = (BigDecimal) query.uniqueResult();

        }catch(Exception ex) {
            log.error(ex);
        }

        return result;
    }

    @Override
    public List<UserFavorite> getFavorites(String userId)
    {
        Criteria crit = sessionfactory.getCurrentSession().createCriteria(UserFavorite.class);
        crit.add( Restrictions.eq("userId", userId) );

        return crit.list();
    }

    @Override
    public UserFavorite getFavorite(String id)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(UserFavorite.class);
            crit.add( Restrictions.eq("id", id) );
            return (UserFavorite) crit.uniqueResult();
        }catch(Exception ex) {
            return null;
        }
    }

    @Override
    public UserFavorite getFavorite(String userId, String url)
    {
        try {
            Criteria crit = sessionfactory.getCurrentSession().createCriteria(UserFavorite.class);
            crit.add( Restrictions.eq("userId", userId) );
            crit.add( Restrictions.eq("url", url) );

            return (UserFavorite) crit.uniqueResult();
        }catch(Exception ex) {
            return null;
        }
    }

    @Override
    public void addFavorite(String userId, String url, String name, String linkIcon)
    {
        Session session = sessionfactory.getCurrentSession();
        UserFavorite f = new UserFavorite();
        f.setUserId(userId);
        f.setUrl(url);
        f.setName(name);
        f.setLinkIcon(linkIcon);

        session.save(f);
        session.flush();
    }

    @Override
    public void removeFavorite(String id)
    {
        Session session = sessionfactory.getCurrentSession();
        UserFavorite fav = (UserFavorite) session.load(UserFavorite.class, id);
        session.delete(fav);
        session.flush();
    }
}