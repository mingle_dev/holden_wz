package com.kawauso.base.interfaces;

import com.kawauso.base.bean.Order;
import java.util.Date;
import java.util.List;


/**
 * Created by twall on 9/15/15.
 */
public interface OrderCriteria {

    List<Order> meetCriteria(List<Order> orders, Date fromDt, Date toDt);
    //System.out.println("trying to push code using mercurial");
    public interface Criteria {
        public List<Order> meetCriteria(List<Order> persons, Date fromDt, Date toDt);
    }
}
