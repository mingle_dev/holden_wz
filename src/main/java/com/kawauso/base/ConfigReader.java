package com.kawauso.base;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.Properties;

public class ConfigReader
{
    private static final String configFileName = "sitesettings.properties";  // filename of the properties file
    private static final Properties properties;
    private static final Logger log = Logger.getLogger( ConfigReader.class );
    private static final String APP_REGION_BETA = "beta";

    static {
        InputStream inputStream = null;
        properties = new Properties();
        
        try {
            inputStream = getResourceAsStream(configFileName);
            properties.load(inputStream);

            String region = getProperty("wz.app.region");
            System.out.println("APP REGION: "+ region);
            if ( APP_REGION_BETA.equalsIgnoreCase(region) ){
                String tmp = configFileName.replace("sitesettings", "sitesettings_"+APP_REGION_BETA);
                inputStream = getResourceAsStream(tmp);
                properties.load(inputStream);
            }
            
        } catch(Exception ex) {
            log.error(ex);
        } finally {
            try {
                inputStream.close();
            }
            catch(Exception e) {}
        }
    }

    public static String getProperty(String key)
    {
        return properties.getProperty(key);
    }
    
    public static int getPropertyAsInt(String key)
    {
        try {
            return Integer.parseInt( properties.getProperty(key) );
        }catch(Exception ex) {
            
        }
        
        return 0;
    }

    private static InputStream getResourceAsStream(String resource)
    {
        String stripped = resource.startsWith("/") ? resource.substring(1) : resource;
        InputStream stream = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (classLoader != null) {
            stream = classLoader.getResourceAsStream( stripped );
        }

        return stream;
    }    
}
