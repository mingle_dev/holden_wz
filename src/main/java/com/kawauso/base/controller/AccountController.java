/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.google.gson.Gson;
import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.bean.*;
import com.kawauso.base.service.AccountService;
import com.kawauso.base.service.LocationService;
import com.kawauso.base.utils.impl.EnteredDateOrderCriteria;
import com.kawauso.base.utils.impl.InvoiceDateOrderCriteria;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class AccountController
{
    private static final Logger log = Logger.getLogger( AccountController.class );
    private static final String ENTERED_DATE = "entered_date";
    private static final String INVOICE_DATE = "invoice_date";

    @Autowired
    AccountService accountService;

    @Autowired
    LocationService locationService;

    //private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    @RequestMapping(value = "/account/balances", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView balances(@RequestParam(value = "period", required = false) String p, HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        //not authorized
        if(userSession.getUser().isSecurityAccount() != true) {

            BigDecimal custno = userSession.getCustomerNumber();
            int cono = userSession.getCono();
            String customerName = userSession.getCustomerName();
            String companyName = userSession.getAccount().getName();
            String distributorName = (ConfigReader.getProperty("sxe.distr.name." + cono));
            String firstName = userSession.getContact().getFirstName();
            String lastName = userSession.getContact().getLastName();
            String fullName = (firstName + " " + lastName);
            String emailAddress = userSession.getContact().getEmailAddr();

            model.put("custno", custno);
            model.put("distributorName", distributorName);
            model.put("customerName", customerName);
            model.put("companyName", companyName);
            model.put("firstName", firstName);
            model.put("lastName", lastName);
            model.put("contactEmail", emailAddress);

            String bodyMessage = distributorName + "\n" +
                                companyName + "\n" +
                                custno + "\n" +
                                fullName + "\n";
            try {
                bodyMessage = java.net.URLEncoder.encode(bodyMessage, "UTF-8");
                bodyMessage = bodyMessage.replace("+", "%20");
                System.out.println(bodyMessage);
            }catch (Exception ex){
                ex.printStackTrace();
            }

            model.put("emailBodyMessage", bodyMessage);
            model.put("emailReplyAddress", emailAddress);
            model.put("billpaySendToEmail", ConfigReader.getProperty("billpay.sendTo.email"));

            return new ModelAndView("/account/unauthorized", model);
        }

        int period = -1;
        try { period = Integer.parseInt(p); } catch(Exception ignore) {}

        model.put("period", period);

        //display period detail
        if(period >= 0 && period <= 5) {

            //breadcrumbs
            List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
            crumbs.add(new Breadcrumb("Account Information","/account/balances.htm",""));
            crumbs.add(new Breadcrumb("Period Details","",""));
            model.put("navTree", crumbs);

            try {
                List<Order> orders = accountService.getPeriodOrders(userSession.getCono(), OPER, userSession.getCustomerNumber(), period);
                model.put("orders", orders);
                return new ModelAndView("/account/period", model);

            } catch (Exception ex) {
                model.put("message", "An error occurred while retrieving your information");
                model.put("error", ex.toString());
                return new ModelAndView("/error/service", model);
            }
        }

        String cono = String.valueOf(userSession.getCono());
        //generate billtrust invoice gateway url
        try {

            String qryStr = ConfigReader.getProperty("billtrust.single.sign.on.url");
            String clientguid_property=ConfigReader.getProperty("billtrust.client.guid.property");
            String clientguid=clientguid_property + "." + cono;
            String clientguid_value=ConfigReader.getProperty(clientguid);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
            Date now = new Date();
            String btData = "AN="+ cono+ "_" + userSession.getCustomerNumber().intValue() + ";EA=" + userSession.getContact().getEmailAddr().trim() + ";PB=Y;EN=Y;EC=Y;TS=" + sdfDate.format(now);
            String data = encrypt(btData, cono);
            qryStr += "BTDATA=\"" + data + "\"&CG=\""+ clientguid_value + "\"&ETYPE=1";
            model.put("billtrustURL", qryStr);
        } catch (Exception ex) {
            model.put("billtrustException", "Unable to generate BillPay URL. Please contact Support");
            log.error(ex);
        }

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Account Information","",""));
        model.put("navTree", crumbs);

        //display info
        try {
            Account acct = accountService.getAccountInfo(userSession.getCono(), OPER, userSession.getCustomerNumber());
            model.put("account", acct);
            return new ModelAndView("/account/balances", model);

        } catch (Exception ex) {
            model.put("message", "An error occurred while retrieving your information");
            model.put("error", ex.toString());
            return new ModelAndView("/error/service", model);
        }
    }

    /**
     * Returns list of orders
     *
     * @todo validate security and setup paging
     */
    @RequestMapping(value = "/account/filters", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView orders(@RequestParam(value = "term", required = false) String term,
                               @RequestParam(value = "type", required = false) String type,
                               @RequestParam(value = "stage", required = false) String stage,
                               @RequestParam(value = "sort", required = false) String sort,
                               @RequestParam(value = "page", required = false) Integer page,
                               @RequestParam(value = "limit", required = false) Integer limit,
                               @RequestParam(value = "criteria", required = false) String criteria,
                               HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("criteria", criteria);
        return (new ModelAndView("/account/filters", model));
    }
    /**
     * Returns list of orders
     *
     * @todo validate security and setup paging
     */
    @RequestMapping(value = "/account/orders", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView orders(@RequestParam(value = "term", required = false) String term,
                               @RequestParam(value = "type", required = false) String type,
                               @RequestParam(value = "stage", required = false) String stage,
                               @RequestParam(value = "sort", required = false) String sort,
                               @RequestParam(value = "page", required = false) Integer page,
                               @RequestParam(value = "limit", required = false) Integer limit,
                               @RequestParam(value = "criteria", required = false) String criteria,
                               @RequestParam(value = "from_dt", required = false) String from_dt,
                               @RequestParam(value = "to_dt", required = false) String to_dt,
                               HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        //not authorized
        if(userSession.getUser().isSecurityOrderInquiry() != true)
            return new ModelAndView("/unauthorized", model);

        int stageNo = 0;
        try { stageNo = Integer.parseInt(stage); } catch(Exception ignore) {}

        //these have to be something
        sort = (sort != null) ? sort : "Entered";
        page = (page != null) ? page : 1;
        limit = (limit != null) ? limit : 20;

        model.put("term", term);
        model.put("type", type);
        model.put("stage", stage);
        model.put("sort", sort);
        model.put("page", page);
        model.put("limit", limit);
        model.put("criteria", criteria);
        model.put("from_dt", from_dt);
        model.put("to_dt", to_dt);


        try {
            String erpSort = "enterdt";
            //translate sort to ERP column
            if(sort.equals("Invoice")) erpSort = "orderno";
            if(sort.equals("PO")) erpSort = "custpo";
            if(sort.equals("Type")) erpSort = "transtype";
            if(sort.equals("Status")) erpSort = "stagecd";
            if(sort.equals("Entered")) erpSort = "enterdt";
            if(sort.equals("Invoiced")) erpSort = "invoicedt";
            if(sort.equals("Amount")) erpSort = "totinvamt";

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date fromDt = null, toDt = null;

            if (from_dt != null && from_dt != "") fromDt = format.parse(from_dt);
            if (to_dt != null && to_dt != "") toDt = format.parse(to_dt);

            List<Order> orders = accountService.getOrders(userSession.getCono(), userSession.getCustomerNumber(), term, type, stageNo, erpSort, page, limit, criteria, fromDt, toDt);

            model.put("orders", orders);

            if(!orders.isEmpty())
                model.put("numPages", (accountService.getSearchResultCount()) / limit + 1);

        } catch (Exception ex) {
            model.put("message", "An error occurred while retrieving your information");
            model.put("error", ex.toString());

            log.error(ex);

            return new ModelAndView("/error/service", model);
        }

        //return login form view
        return new ModelAndView("/account/orders", model);
    }

    /**
     * Returns list of orders
     *
     * @todo validate security and setup paging
     */
    @RequestMapping(value = "/account/filters", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView filter(@RequestParam(value = "term", required = false) String term,
                               @RequestParam(value = "type", required = false) String type,
                               @RequestParam(value = "stage", required = false) String stage,
                               @RequestParam(value = "sort", required = false) String sort,
                               @RequestParam(value = "page", required = false) Integer page,
                               @RequestParam(value = "limit", required = false) Integer limit,
                               @RequestParam(value = "criteria", required = false) String criteria,
                               @RequestParam(value = "from_dt", required = false) String from_dt,
                               @RequestParam(value = "to_dt", required = false) String to_dt,
                               HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        //not authorized
        if(userSession.getUser().isSecurityOrderInquiry() != true)
            return new ModelAndView("/unauthorized", model);

        int stageNo = 0;
        try { stageNo = Integer.parseInt(stage); } catch(Exception ignore) {}

        //these have to be something
        sort = (sort != null) ? sort : "Entered";
        page = (page != null) ? page : 1;
        limit = (limit != null) ? limit : 20;

        model.put("term", term);
        model.put("type", type);
        model.put("stage", stage);
        model.put("sort", sort);
        model.put("page", page);
        model.put("limit", limit);
        model.put("criteria", criteria);
        model.put("from_dt", from_dt);
        model.put("to_dt", to_dt);

        try {
            String erpSort = "enterdt";

            //translate sort to ERP column
            if(sort.equals("Invoice")) erpSort = "orderno";
            if(sort.equals("PO")) erpSort = "custpo";
            if(sort.equals("Type")) erpSort = "transtype";
            if(sort.equals("Status")) erpSort = "stagecd";
            if(sort.equals("Entered")) erpSort = "enterdt";
            if(sort.equals("Invoiced")) erpSort = "invoicedt";
            if(sort.equals("Amount")) erpSort = "totinvamt";

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date fromDt = null, toDt = null;

            if (from_dt != null && from_dt != "") fromDt = format.parse(from_dt);
            if (to_dt != null && to_dt != "") toDt = format.parse(to_dt);

            List<Order> orders = accountService.getOrders(1, userSession.getCustomerNumber(), term, type, stageNo, erpSort, page, limit, criteria, fromDt, toDt);

            //List<Order> filteredOrders;

            //if (criteria != null &&  !criteria.equals("")) {
            //    filteredOrders = filter(orders, criteria, from_dt, to_dt);
            //}else{
            //    filteredOrders = orders;
            //}

            model.put("orders", orders);

            if(!orders.isEmpty())
                model.put("numPages", (accountService.getSearchResultCount()) / limit + 1);

        } catch (Exception ex) {
            model.put("message", "An error occurred while retrieving your information");
            model.put("error", ex.toString());

            log.error(ex);

            return new ModelAndView("/error/service", model);
        }

        //return login form view
        return new ModelAndView("/account/orders", model);
    }
    /**
     * Returns order detail
     *
     * @todo validate security and order is owned by custno
     */
    @RequestMapping(value = "/account/orders/detail", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView detail(@RequestParam(value = "orderno", required = false) String orderno,
                               @RequestParam(value = "ordersuf", required = false) String ordersuf,
                               HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        //not authorized
        if(userSession.getUser().isSecurityOrderInquiry() != true)
            return new ModelAndView("/unauthorized", model);

        try {
            Order order = null;

            //<c:if test="${session.type eq 'EMPLOYEE' || session.type eq 'ADMINISTRATOR'}">

            System.out.println(userSession.getType());

            if(userSession.getType() == Session.Role.ADMINISTRATOR || userSession.getType() == Session.Role.EMPLOYEE)
                order = accountService.getOrder(userSession.getCono(), OPER, Integer.parseInt(orderno), Integer.parseInt(ordersuf));
            else
                order = accountService.getOrder(userSession.getCono(), OPER, userSession.getCustomerNumber(), Integer.parseInt(orderno), Integer.parseInt(ordersuf));
            model.put("order", order);

        } catch (Exception ex) {
            log.error(ex);
            model.put("message", "An error occurred while retrieving your information");
            model.put("error", ex.toString());
        }

        return new ModelAndView("/account/orderDetail", model);
    }

    /**
     * BillTrust URL encryption
     */
    private String encrypt(String input, String cono) throws Exception
    {
        //String ks = "C5B26AEFD8988E0BDDF48577";
        //String is = "C5B26AEF";

        String key_propertyname=ConfigReader.getProperty("billtrust.key.property");
        String iv_propertyname=ConfigReader.getProperty("billtrust.iv.property");

        String ks=ConfigReader.getProperty(key_propertyname + "." + cono);
        String is=ConfigReader.getProperty(iv_propertyname + "." + cono);

        SecretKey key = new SecretKeySpec(ks.getBytes(), "DESede");
        IvParameterSpec iv = new IvParameterSpec(is.getBytes());

        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

        byte[] encrypted = cipher.doFinal(input.getBytes("UTF-8"));
        byte[] encoded = Base64.encodeBase64(encrypted);

        return new String(encoded);
    }

    @RequestMapping(value = "/account/get-shipto", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String getShipTo(@RequestParam(value = "custno", required = false) String custno,
                                          @RequestParam(value = "shipto", required = false) String shipto,
                                          HttpSession session)
    {
        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        try {
            ShipTo s = accountService.getShipTo(userSession.getCono(), OPER, new BigDecimal(custno), shipto);

            Gson g = new Gson();
            String res = g.toJson(s);
            return res;

        }catch(Exception ex) {
            log.error(ex);
        }

        return null;
    }

    @RequestMapping(value = "/account/getall-shipto", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String getShipToList(@RequestParam(value = "custno", required = false) String custno,
                                              HttpSession session)
    {
        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        try {
            List<ShipTo> s = accountService.getShipToList(userSession.getCono(), OPER, new BigDecimal(custno), false);

            Gson g = new Gson();
            String res = g.toJson(s);

            return res;

        }catch(Exception ex) {
            log.error(ex);
        }

        return null;
    }

    /**
     * Returns order detail
     *
     * @todo validate security and order is owned by custno
     */
    @RequestMapping(value = "/account/orders/get-doc", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView getDoc(HttpServletRequest request, HttpServletResponse response, HttpSession session)
    {

        String mrPathString = "http://webdocs.mingledorffs.com/Viewer/RetrieveFileForUserName";
        String docType  = request.getParameter("doctype");
        String orderno  = request.getParameter("orderno");
        String ordersuf = request.getParameter("ordersuf");
        Session userSession = (Session) session.getAttribute("session");
        String docExtension = null;

        //doc type can be D1130 for the pick list or D1175 for the invoice
        try {

            if(docType.equalsIgnoreCase("d1175")){

                docExtension = "_inv.pdf";
            }
            else if(docType.equalsIgnoreCase("d1130")){


                docExtension = "_pod.pdf";

            }

            String docName = String.format(mrPathString + "/%s/,%s,%s/our/guest", docType,orderno, ordersuf);
            response.setContentType("application/pdf");

            response.setHeader("Content-disposition", "attachment; filename=" + orderno + "-" + ordersuf + docExtension);
            URL url = new URL(docName);
            InputStream is = url.openStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buf = new byte[4096];
            int n;

            while ((n = is.read(buf)) >= 0)
                os.write(buf, 0, n);
            os.close();
            is.close();

            ServletOutputStream sos2 = response.getOutputStream();
            sos2.write(os.toByteArray());
            sos2.flush();


        } catch (Exception e) {

            log.error("processRequest(); custno=" + userSession.getCustomerNumber() +
                    " unable to locate document for order: " + orderno + "-" + ordersuf);

            String msg = "We were unable to locate this document in our storage system.";
            try {
                response.sendRedirect(request.getHeader("Referer") + "&message=" + msg);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        //no redirect
        return null;

    }

    /**
     * Fetches maxrecall document name from the provided url
     *
     * @param url
     * @return String
     * @throws Exception
     */
    private String getDocName(String url) throws Exception
    {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream() ) );
        StringBuilder response = new StringBuilder();

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);
        in.close();

        return response.toString().trim();
    }

    @RequestMapping(value = "/account/admin-search", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView accountSearch(@RequestParam(value = "key", required = false) String key,
                                      @RequestParam(value = "val", required = false) String val,
                                      @RequestParam(value = "limit", required = false) String limitStr,
                                      @RequestParam(value = "page", required = false) Integer page,
                                      HttpSession session)
    {

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        if(key != null && !key.isEmpty() && val != null && !val.isEmpty()) {

            int limit = 20;
            try { limit = Integer.parseInt(limitStr); }catch(Exception ex) {}

            page = (page != null) ? page : 1;
            model.put("page", page);

            if(key.equals("contact")) {

                List<Contact> contacts = accountService.getContacts("", val);
                model.put("contacts", contacts);

            }
            else {
                //List<Account> accounts = accountService.getAccounts(CONO, OPER, key, val, limit);
                List<Account> accounts = accountService.getAccounts(userSession.getCono(), key, val, limit);

                model.put("accounts", accounts);

                if(!accounts.isEmpty())
                {
                    model.put("numPages", (accountService.getSearchResultCount())/limit + 1);
                }
            }
        }
        //update model to include user select search key
        model = setUserSearchKey(model, key, val);

        //populate the search value for user search string
        model = setUserSearchValue(model, val);

        return new ModelAndView("/account/search", model);
    }

    @RequestMapping(value = "/account/quicklist", method = RequestMethod.GET)
    public @ResponseBody String quickList(String term)
    {
        if(term == null || term.isEmpty())
            return "[]";

        Gson gson = new Gson();

        try {
            List<String[]> list = accountService.getAccounts(1, term);
            List<String> strArr = new ArrayList<>();

            String data = "[";
            for(int i=0; i<list.size(); i++)
            {
                String[] s = list.get(i);
                data += "{\"custno\":\"" + s[0] + "\", \"name\":\"" + s[1] + "\", \"city\":\"" + s[2] + "\", \"state\":\"" + s[3] + "\"}";

                if(i < list.size()-1)
                    data += ", ";
            }
            data += "]";

            return data;
        }catch(Exception ex){
            System.out.println(ex);
        }

        return null;
    }

    @RequestMapping(value = "/account/admin-details", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView accountDetails(@RequestParam(value = "key", required = false) String key,
                                       @RequestParam(value = "val", required = false) String val,
                                       @RequestParam(value = "custno", required = true) String custno,
                                       HttpSession session)
    {

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Account Search", "/account/admin-search", "key="+key+"&val="+val));
        crumbs.add(new Breadcrumb("Account Details - " + custno,"",""));
        model.put("navTree", crumbs);

        try {
            Account account               = accountService.getAccountInfo(userSession.getCono(), OPER, new BigDecimal(custno));
            SalesRep salesRep             = accountService.getSalesRep(userSession.getCono(), account.getSlsRepOut());
            Map<String, Object> orderInfo = accountService.getOrderingInfo(userSession.getCono(), OPER, new BigDecimal(custno));

            model.put("account",  account);
            model.put("acctXtra", orderInfo);
            model.put("contacts", accountService.getContacts(custno, ""));
            model.put("salesRep", salesRep);

            String whseId = (String)orderInfo.get("dfltWhse");
            model.put("currentLocation", locationService.getLocation(whseId));

            return new ModelAndView("/account/admin-details", model);

        } catch (Exception ex) {
            model.put("message", "An error occurred while retrieving your information");
            model.put("error", ex.toString());
            return new ModelAndView("/error/service", model);
        }
    }

    @RequestMapping(value = "/account/get-contacts-json", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String getContactsAsJson(@RequestParam(value = "custno", required = false) String custno,
                                                  @RequestParam(value = "term", required =  false) String term,
                                                  HttpSession httpSession)
    {
        Gson gson = new Gson();
        List<Contact> c = accountService.getContacts(custno, term);
        return gson.toJson(c);
    }

    private Map<String, Object> setUserSearchKey(Map<String, Object> model, String key, String value)
    {
        if ( value == null )
        {
            //if there is no search value assume user didn't one
            //usually occurs on page load so set default to Account Name
            key = "name";
        }

        String select = "sel" + key;
        model.put(select, "selected");
        return model;
    }

    private Map<String, Object> setUserSearchValue(Map<String, Object> model, String value)
    {
        model.put("val", value);
        return model;
    }

    private List<Order> filter (List<Order> orders, String criteria, String from_dt, String to_dt)
    {
        List<Order> filteredOrders = new ArrayList<Order>();

        if (criteria == null || from_dt == null || to_dt == null)
        {
            return orders;
        }
        try {

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date fromDt = format.parse(from_dt);
            Date toDt = format.parse(to_dt);

            if (ENTERED_DATE.equals(criteria)) {
                EnteredDateOrderCriteria filter = new EnteredDateOrderCriteria();
                filteredOrders = filter.meetCriteria(orders, fromDt, toDt);
            } else if (INVOICE_DATE.equals(criteria)) {
                InvoiceDateOrderCriteria filter = new InvoiceDateOrderCriteria();
                filteredOrders = filter.meetCriteria(orders, fromDt, toDt);
            }
        } catch (ParseException ex) {

            ex.printStackTrace();
            return orders;
        }

        return filteredOrders;
    }

}