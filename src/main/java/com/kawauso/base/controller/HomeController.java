package com.kawauso.base.controller;


import com.google.gson.Gson;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.Utilities.Encryption;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.JsonResult;
import com.kawauso.base.bean.Session;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kawauso.base.bean.UserFavorite;
import com.kawauso.base.dataobject.sx.AdminDao;
import com.kawauso.base.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController
{
    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request, HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //search for favorites if logged in
        Session userSession = (Session) session.getAttribute("session");
        if(userSession != null && userSession.getUserId() != null) {
            List<UserFavorite> favorites = userService.getFavorites(userSession.getUserId());
            if(!favorites.isEmpty())
                model.put("favorites", favorites);
        }else {
            //if the token is set, try to log user in
            if (request.getParameter("tok") != null && !request.getParameter("tok").isEmpty()) {
                System.out.println("token is set");
                return new ModelAndView("redirect:/user/token.htm?tok=" + request.getParameter("tok"));
            }

            model.put("ignoreWidthFix", true);
        }

        //return view
        return new ModelAndView("index", model);
    }        

    @RequestMapping("/stats")
    public ModelAndView getStats()
    {
        Map<String, Object> model = new HashMap<String, Object>();

        AdminDao admin = new AdminDao();
        model.put("poolstats", admin.getPoolStats());

        return new ModelAndView("/home/stats", model);
    }

    @RequestMapping("/test")
    //@SecurityAnnotation(requireLogin = true)
    public @ResponseBody String test(String t, HttpSession session)
    {
        JsonResult res = new JsonResult();

        res.setStatus(false);
        res.setMessage("You must first be logged on");
        return new Gson().toJson(res);
    }
    
    @RequestMapping("/about")
    public String about()
    {
        return "/home/about";
    } 
    
    @RequestMapping("/terms")
    public String terms()
    {
        return "/home/terms";
    }
    
    @RequestMapping("/privacy")
    public String privacy()
    {
        return "/home/privacy";
    }

    @RequestMapping("/resources")
    public ModelAndView resources(@RequestParam(value = "f", required = false) String function)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Resources","",""));
        model.put("navTree", crumbs);        
        
        if(function == null)
            return new ModelAndView("/resources", model);
        
        return new ModelAndView("/error/workinprogress", model);
    }    
}