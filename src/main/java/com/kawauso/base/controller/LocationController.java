package com.kawauso.base.controller;


import com.google.gson.Gson;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.Location;
import com.kawauso.base.bean.LocationV18;
import com.kawauso.base.bean.Session;
import com.kawauso.base.service.LocationService;
import com.kawauso.base.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

@Controller
public class LocationController
{
    @Autowired
    private UserService userService;

    @Autowired
    private LocationService locationService;    

    @RequestMapping("/locations")
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView index(@RequestParam(value = "locSearchKey", required = false) String searchKey, 
                              @RequestParam(value = "locSearchVal", required = false) String searchVal,
                              HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        BigDecimal custno = null;

        if (httpSession != null){
            Session session = (Session)httpSession.getAttribute("session");
            custno = session.getCustomerNumber();
        }
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Locations","",""));
        model.put("navTree", crumbs);        
        
        List<LocationV18> locations = locationService.getLocations(custno, 1);
        
        //default placeholder
        model.put("locPlaceHolder", "Zip Code");
        
        if(searchKey != null && !searchKey.isEmpty() && searchVal != null && !searchVal.isEmpty()) {
            if(searchKey.equals("all")) {
                model.put("locations", locations);
                return new ModelAndView("/location/index", model);
            }
                
            Double srcLat = null;
            Double srcLong = null;
                
            if(searchKey.equals("zip")) {
                try {
                    Double[] coords = locationService.getCoordinates( Integer.parseInt(searchVal) );
                    srcLat = coords[0];
                    srcLong = coords[1];
                }catch(Exception ex) {}
                
                model.put("locPlaceHolder", "Zip Code");
                
            }else if(searchKey.equals("loc")) {
                try {
                    //try to split up city and state
                    String[] tmp = searchVal.split(",");
                    String city = searchVal;
                    String state = "";
                    
                    if(tmp.length > 1) {
                        city = tmp[0].trim();
                        state = tmp[1].trim();
                    }
                       
                    Double[] coords = locationService.getCoordinates( city, state );
                    
                    srcLat = coords[0];
                    srcLong = coords[1];
                }catch(Exception ex) {}
                
                model.put("locPlaceHolder", "City, State");
                
            }else if(searchKey.equals("geo")) {
                try {
                    String[] coords = searchVal.split(",");
                    srcLat = Double.parseDouble(coords[0].trim());
                    srcLong = Double.parseDouble(coords[1].trim());
                }catch(Exception ignore) {}
                
                model.put("locPlaceHolder", "Latitude, Longitude");
            }
            
            //search for nearest locations if we have a source
            if(srcLat != null && srcLong != null)
            {
                for(LocationV18 location : locations) {
                    try {
                        double distance = calcDistance( srcLat, srcLong, location.getLatitude(), location.getLongitude() );
                        location.setDistance(distance);
                    }catch(Exception ignore) {
                    }
                }

                //sort results based on distance
                Collections.sort(locations, new Comparator<LocationV18>() {
                    @Override
                    public int compare(LocationV18 loc1, LocationV18 loc2) {
                        return (int) loc1.getDistance() - (int) loc2.getDistance();
                    }
                });

                model.put("locations", locations.subList(0, (locations.size() > 5) ? 5 : locations.size() ) );
            }
            
        }
        
        model.put("locSearchKey", searchKey);
        model.put("locSearchVal", searchVal);
        
        //return view
        return new ModelAndView("/location/index", model);
    }  
    
    @RequestMapping(value = "/locations/detail")
    public ModelAndView getLocation(String code)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        Location location = locationService.getLocation(code);
        model.put("location", location);
        
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Locations", "locations", ""));
        crumbs.add(new Breadcrumb("Location Details", "", ""));
        model.put("navTree", crumbs);
        
        return new ModelAndView("/location/detail", model);
    }
    
    /**
     * @todo; lots of moving parts with this, might scrap ajax and just use a success/error page
     * 
     * @param code
     * @param session
     * @return 
     */
    @RequestMapping(value = "/locations/set", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String setLocation(String code, HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");
        userSession.setWarehouse(code);

        if(userSession.getWarehouse().equals(code)) {
            Gson gson = new Gson();
            String data = gson.toJson(locationService.getLocation(code));
            return data;
        }else{
            return null; //need to send error
        }
    }
    
    @RequestMapping(value = "/locations/set", method = RequestMethod.GET)
    public String setLocation_g(String code, String prod, HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");
        userSession.setWarehouse(code);

        if(prod != null && !prod.isEmpty()) {
            return "redirect:/product/detail.htm?prod=" + prod;
        }else if(userSession.getWarehouse().equals(code)) {
            return "redirect:/locations/detail.htm?code=" + code;
        }else{
            return "/error/service.htm";
        }
    }    
    
    @RequestMapping(value = "/locations/list", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String getLocations()
    {
        List<Location> locations = locationService.findAll();
        
        Gson gson = new Gson();
        
        return gson.toJson(locations);
    } 
    
    /**
     * Calculate the distance between two geographic point
     * 
     * http://en.wikipedia.org/wiki/Haversine_formula
     * 
     * 
     * @return long The distance
     */
    private double calcDistance(double fromLat, double fromLong, double toLat, double toLong) {
        int R = 6371; //earth's approximate radius km; en.wikipedia.org/wiki/Earth_radius
        
        fromLat = Math.toRadians(fromLat);
        fromLong = Math.toRadians(fromLong);
        toLat = Math.toRadians(toLat);
        toLong = Math.toRadians(toLong);
        
        double a = haversin(toLat - fromLat) + Math.cos(fromLat) * Math.cos(toLat) * haversin(toLong - fromLong);
        double c = 2 * Math.asin( Math.sqrt( a ) );

        return R * c;
    }
    
    private double haversin(double t) {
        return (1 - Math.cos(t)) / 2;
    }
}

