/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.kawauso.base.ConfigReader;
import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.ProductPDRecord;
import com.kawauso.base.bean.Session;
import com.kawauso.base.bean.quote.*;
import com.kawauso.base.service.ProductService;
import com.kawauso.base.service.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author chris.weaver
 *
 * references:
 *   http://stackoverflow.com/questions/15738918/splitting-a-csv-file-with-quotes-as-text-delimiter-using-string-split/15739087#15739087
 */
@Controller
public class QuoteController
{
    @Autowired
    private QuoteService quoteService;

    @Autowired
    private ProductService productService;

    private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    private static final String WHSE = ConfigReader.getProperty("sxe.whse");

    @RequestMapping(value = "/quote/index", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(value = "view", required = false) String view,
                              HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");

        List<Quote> quotes = quoteService.findByOwnerId(session.getUserId());

        model.put("quotes", quotes);

        return new ModelAndView("/quote/index", model);
    }

    @RequestMapping(value = "/quote/s1", method = RequestMethod.GET)
    public ModelAndView s1(@RequestParam(value = "qid", required = false) String qid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index.htm",""));
        crumbs.add(new Breadcrumb("Header Information","",""));
        model.put("navTree", crumbs);

        Quote quote = quoteService.findById(qid);

        if(quote != null) {
            model.put("quote", quote);
            crumbs.add(new Breadcrumb("Edit Quote","",""));
        }else {
            model.put("quote", new Quote(UUID.randomUUID().toString()));
            crumbs.add(new Breadcrumb("New Quote","",""));
        }

        model.put("navTree", crumbs);

        return new ModelAndView("/quote/builder_1", model);
    }

    @RequestMapping(value = "/quote/review", method = RequestMethod.GET)
    public ModelAndView review(@RequestParam(value = "qid", required = true) String qid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index.htm",""));
        crumbs.add(new Breadcrumb("Quote Review","",""));
        model.put("navTree", crumbs);

        Quote quote = quoteService.findById(qid);

        if(quote == null) {
            return new ModelAndView("/message", model);
        }

        model.put("quote", quote);

        return new ModelAndView("/quote/review", model);
    }

    @RequestMapping(value = "/quote/s1", method = RequestMethod.POST)
    public String s1(@ModelAttribute("quote") Quote quote, BindingResult result, ModelMap model, HttpSession session)
    {
        if(quote == null)
            return "/message";

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index.htm",""));
        crumbs.add(new Breadcrumb("Header Information","",""));
        model.put("navTree", crumbs);

        //validate field data
        Errors errors = new BeanPropertyBindingResult(quote, "quote");
        String[] reqFields = { "quoteName", "customerNumber", "contactName", "emailAddress" };

        for(int i=0; i<reqFields.length; i++)
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, reqFields[i], "field.required");

        if(errors.getErrorCount() > 0) {
            model.put("formError", true);
            result.addAllErrors(errors);
            return "/quote/builder_1";
        }

        Date date = new Date();
        quote.setCreateDate(date);
        quote.setStatus(Quote.Status.DRAFT);

        if(quote.getQuoteId() == null || quote.getQuoteId().isEmpty())
        {
            SimpleDateFormat format = new SimpleDateFormat("yyMMddhhmmss"); //new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            quote.setQuoteId( format.format(date) );
        }

        //save the quote
        quote = quoteService.save(quote);

        return "redirect:/quote/review.htm?qid=" + quote.getId();
    }

    @RequestMapping(value = "/quote/s2", method = RequestMethod.GET)
    public ModelAndView s2(@RequestParam(value = "qid", required = false) String qid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote q = quoteService.findById(qid);
        if(q == null) {
            model.put("message", "Quote not found");
            return new ModelAndView("/message", model);
        }
        model.put("quote", q);

        return new ModelAndView("/quote/builder_2", model);
    }

    @RequestMapping(value = "/quote/s3_template", method = RequestMethod.GET)
    public ModelAndView s3_template(@RequestParam(value = "qid", required = false) String qid,
                                    @RequestParam(value = "menuid", required = false) String menuid) {
        Map<String, Object> model = new HashMap<String, Object>();

        List<QBMenu> menu = null;

        Quote quote = quoteService.findById(qid);
        if(quote == null)
            return new ModelAndView("/message", model);

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index",""));
        crumbs.add(new Breadcrumb("Quote Details","/quote/review","qid="+qid));

        if (menuid == null || menuid.isEmpty()) {
            menu = quoteService.getMenu(null);
            model.put("menu", menu);
            crumbs.add(new Breadcrumb("Templates","",""));

        } else {
            QBMenu nav = quoteService.getNav(ELUtilities.base64Decode(menuid));
            if(nav != null) {
                crumbs.add(new Breadcrumb("Templates", "/quote/s3_template", "qid=" + qid));

                if(nav.getAncestors() != null) {
                    for(String s : nav.getAncestors()) {
                        crumbs.add(new Breadcrumb(s, "/quote/s3_template", "qid="+qid+"&menuid="+ELUtilities.base64Encode(s)));
                    }
                }
                crumbs.add(new Breadcrumb(nav.getId(), "/quote/s3_template", ""));
            }
            model.put("nav", nav);

            List<QBTemplate> templates = quoteService.getTemplates(ELUtilities.base64Decode(menuid));
            for(QBTemplate qb : templates)
            {
                try {
                    List<QBProduct> qbc = new ArrayList<>();
                    List<QBProduct> qba = new ArrayList<>();

                    //List<Product> qbpc = new ArrayList<>();

                    for (String c : qb.getComponents()) {
                        try {
                            Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, c);
                            QBProduct q = new QBProduct(c);
                            q.setQty(p.getAvail());
                            q.setNetAvail(new BigDecimal(p.getNetAvail()));
                            q.setBasePrice(p.getPrice());
                            q.setSellPrice(p.getPrice());
                            q.setDescription(p.getDescription());
                            qbc.add(q);

                            //qbpc.add(p);

                        } catch (Exception ex) {
                            QBProduct q = new QBProduct(c);
                            qbc.add(q);
                            System.out.println(c + ": " + ex);
                        }
                    }

                    if(qb.getAccessories() != null) {
                        for (String c : qb.getAccessories()) {
                            try {
                                Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, c);
                                QBProduct q = new QBProduct(c);
                                q.setQty(p.getAvail());
                                q.setNetAvail(new BigDecimal(p.getNetAvail()));
                                q.setBasePrice(p.getPrice());
                                q.setSellPrice(p.getPrice());
                                q.setDescription(p.getDescription());
                                qba.add(q);
                            } catch (Exception ex) {
                                QBProduct q = new QBProduct(c);
                                qba.add(q);
                                System.out.println(c + ": " + ex);
                            }
                        }
                    }

                    //model.put("qbpc", qbpc);

                    qb.setProdComponents(qbc);
                    qb.setProdAccessories(qba);
                }catch(Exception e) {
                    System.out.println(e);
                }
            }

            model.put("templates", templates);
        }

        model.put("navTree", crumbs);

        return new ModelAndView("/quote/s3_template_menu", model);
    }

    @RequestMapping(value = "/quote/template-convert", method = RequestMethod.GET)
    public ModelAndView systemFromTemplate(@RequestParam(value = "qid", required = false) String qid,
                                           @RequestParam(value = "tid", required = false) String tid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote quote = quoteService.findById(qid);

        if(quote == null)
            return new ModelAndView("/message", model);

        QBTemplate template = quoteService.getTemplate(tid);
        if(template == null)
            return new ModelAndView("/message", model);

        MarkFor system = new MarkFor();
        system.setDescription(template.getSystemName());
        system.setStatus(MarkFor.Status.NEW);

        for(String c : template.getComponents()) {
            Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, c);
            QBProduct qb = new QBProduct(c);
            qb.setQty(BigDecimal.ONE);
            qb.setBasePrice(p.getPrice().setScale(2, RoundingMode.CEILING));
            qb.setSellPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
            qb.setExtPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
            qb.setDescription(p.getDescription());

            try {
                ProductPDRecord pdRec = productService.getExtendedPricing(1, new BigDecimal(quote.getCustomerNumber()), WHSE, p.getProduct(), p.getPrice());
                qb.setCost(pdRec.getStandardCost().setScale(2, RoundingMode.CEILING));
                if(pdRec.getMarginPct().compareTo(BigDecimal.ONE) < 1)
                    qb.setMarginPct(pdRec.getMarginPct().multiply(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
                else
                    qb.setMarginPct(pdRec.getMarginPct().setScale(2, RoundingMode.CEILING));
            }catch(Exception ex) {
                System.out.println(ex);
                System.out.println(quote.getCustomerNumber() + " " + p.getProduct() + " " + p.getPrice());
            }
            system.addProduct(qb);
        }

        if(template.getAccessories() != null) {
            for (String a : template.getAccessories()) {
                Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, a);
                QBProduct qb = new QBProduct(a);
                qb.setBasePrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                qb.setSellPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                qb.setExtPrice(BigDecimal.ZERO);
                qb.setDescription(p.getDescription());
                qb.setQty(BigDecimal.ZERO);

                try {
                    ProductPDRecord pdRec = productService.getExtendedPricing(1, new BigDecimal(quote.getCustomerNumber()), WHSE, p.getProduct(), p.getPrice());
                    qb.setCost(pdRec.getStandardCost().setScale(2, RoundingMode.CEILING));
                    if(pdRec.getMarginPct().compareTo(BigDecimal.ONE) < 1)
                        qb.setMarginPct(pdRec.getMarginPct().multiply(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
                    else
                        qb.setMarginPct(pdRec.getMarginPct().setScale(2, RoundingMode.CEILING));
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                system.addAccessory(qb);
            }
        }
        quote.addSystem(system);

        //save system to quote in db
        quoteService.upsertSystem(qid, system);

        //send to system editor
        return new ModelAndView("redirect:/quote/s3_edit.htm?qid="+qid+"&sid="+system.getId());
    }

    @RequestMapping(value = "/quote/template-convert-mult", method = RequestMethod.GET)
    public ModelAndView systemsFromTemplate(@RequestParam(value = "qid", required = false) String qid,
                                           @RequestParam(value = "tid", required = false) String[] templates)
    {
        if(templates == null || templates.length == 0)
            return new ModelAndView("redirect:/quote/review.htm?qid="+qid);

        Map<String, Object> model = new HashMap<String, Object>();

        for(String tid : templates) {
            Quote quote = quoteService.findById(qid);

            if(quote == null)
                return new ModelAndView("/message", model);

            QBTemplate template = quoteService.getTemplate(tid);
            if(template == null)
                return new ModelAndView("/message", model);

            MarkFor system = new MarkFor();
            system.setDescription(template.getSystemName());
            system.setStatus(MarkFor.Status.NEW);

            for(String c : template.getComponents()) {
                Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, c);
                QBProduct qb = new QBProduct(c);
                qb.setQty(BigDecimal.ONE);
                qb.setBasePrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                qb.setSellPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                qb.setExtPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                qb.setDescription(p.getDescription());

                try {
                    ProductPDRecord pdRec = productService.getExtendedPricing(1, new BigDecimal(quote.getCustomerNumber()), WHSE, p.getProduct(), p.getPrice());
                    qb.setCost(pdRec.getStandardCost().setScale(2, RoundingMode.CEILING));
                    if(pdRec.getMarginPct().compareTo(BigDecimal.ONE) < 1)
                        qb.setMarginPct(pdRec.getMarginPct().multiply(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
                    else
                        qb.setMarginPct(pdRec.getMarginPct().setScale(2, RoundingMode.CEILING));
                }catch(Exception ex) {
                    System.out.println(ex);
                    System.out.println(quote.getCustomerNumber() + " " + p.getProduct() + " " + p.getPrice());
                }
                system.addProduct(qb);
            }

            if(template.getAccessories() != null) {
                for (String a : template.getAccessories()) {
                    Product p = productService.getProduct(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), "21", a);
                    QBProduct qb = new QBProduct(a);
                    qb.setBasePrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                    qb.setSellPrice(p.getPrice().setScale(2, RoundingMode.CEILING));
                    qb.setExtPrice(BigDecimal.ZERO);
                    qb.setDescription(p.getDescription());
                    qb.setQty(BigDecimal.ZERO);

                    try {
                        ProductPDRecord pdRec = productService.getExtendedPricing(1, new BigDecimal(quote.getCustomerNumber()), WHSE, p.getProduct(), p.getPrice());
                        qb.setCost(pdRec.getStandardCost().setScale(2, RoundingMode.CEILING));
                        if(pdRec.getMarginPct().compareTo(BigDecimal.ONE) < 1)
                            qb.setMarginPct(pdRec.getMarginPct().multiply(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
                        else
                            qb.setMarginPct(pdRec.getMarginPct().setScale(2, RoundingMode.CEILING));
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    system.addAccessory(qb);
                }
            }
            quote.addSystem(system);

            //save system to quote in db
            quoteService.upsertSystem(qid, system);
        }

        //send to multi system editor
        return new ModelAndView("redirect:/quote/s3_edit_all.htm?qid="+qid);
    }

    @RequestMapping(value = "/quote/s3_history", method = RequestMethod.GET)
    public ModelAndView s3_history()
    {
        Map<String, Object> model = new HashMap<String, Object>();

        System.out.println("buildQuote()");

        model.put("quote", new Quote(UUID.randomUUID().toString()));

        return new ModelAndView("/quote/builder_2", model);
    }


    @RequestMapping(value = "/quote/s3_blank", method = RequestMethod.GET)
    public ModelAndView s3_blank(@RequestParam(value = "qid", required = true) String qid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        System.out.println("buildQuote()");

        model.put("system", new MarkFor());

        //return new ModelAndView("/quote/builder_3_blank", model);
        return new ModelAndView("/quote/system_edit", model);
    }

    @RequestMapping(value = "/quote/s3_edit", method = RequestMethod.GET)
    public ModelAndView s3_edit(@RequestParam(value = "qid", required = true) String qid,
                                @RequestParam(value = "sid", required = true) String sid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index.htm",""));
        crumbs.add(new Breadcrumb("Quote Details","/quote/review.htm","qid="+qid));
        crumbs.add(new Breadcrumb("Edit System","",""));
        model.put("navTree", crumbs);

        Quote quote = quoteService.findById(qid);

        if(quote == null) {
            return new ModelAndView("/message", model);
        }

        List<MarkFor> systems = quote.getSystems();
        int idx = systems.indexOf(new MarkFor(sid));

        //not found
        if (idx < 0) {
            return new ModelAndView("/message", model);
        }

        MarkFor system = systems.get(idx);

        //fetch accessories based on components
        List<String> products = new ArrayList<String>();
        for(QBProduct p : system.getProducts())
            products.add(p.getSku());
        List<Product> accessories = productService.getAccessories(products.toArray(new String[0]));

        //bind vars to model
        model.put("quote", quote);
        model.put("system", system);
        model.put("accessories", accessories);

        return new ModelAndView("/quote/system_edit", model);
    }

    @RequestMapping(value = "/quote/s3_edit_all", method = RequestMethod.GET)
    public ModelAndView s3_edit_all(@RequestParam(value = "qid", required = true) String qid)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("QuoteBuilder","/quote/index.htm",""));
        crumbs.add(new Breadcrumb("Quote Details","/quote/review.htm","qid="+qid));
        crumbs.add(new Breadcrumb("Edit All","",""));
        model.put("navTree", crumbs);

        Quote quote = quoteService.findById(qid);

        if(quote == null) {
            return new ModelAndView("/message", model);
        }

        model.put("quote", quote);

        return new ModelAndView("/quote/system_edit_all", model);
    }

    @RequestMapping(value = "/quote/publish", method = RequestMethod.GET)
    public ModelAndView publishQuote(@RequestParam(value = "qid", required = true) String qid,
                                     @RequestParam(value = "level", required = true) String level)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote quote = quoteService.findById(qid);

        if(quote == null) {
            return new ModelAndView("/message", model);
        }

        if(quote.getStatus() == Quote.Status.DRAFT) {
            Quote.PriceDetail pd = Quote.PriceDetail.QUOTE;
            if(level != null && !level.isEmpty()) {
                if(level.equals("SKU"))
                    pd = Quote.PriceDetail.SKU;
                else if(level.equals("SYSTEM"))
                    pd = Quote.PriceDetail.SYSTEM;
            }
            quote.setPriceDetail(pd);

            quote.setStatus(Quote.Status.PUBLISHED);
            quoteService.save(quote);

            //@todo; email all contacts quote in pdf as well as link to online version

            model.put("status", "success");
            model.put("message", "Your quote has been published successfully. Add other Accounts?");
            model.put("button_yes", "/quote/account-add.htm?qid="+quote.getId());
            model.put("button_no", "/quote/index.htm");

            return new ModelAndView("/message", model);
        }

        model.put("status", "error");
        model.put("message", "An error occurred while converting your quote.");
        model.put("button_continue", "/quote/index.htm");
        return new ModelAndView("/message", model);
    }

    @RequestMapping(value = "/quote/account-add", method = RequestMethod.GET)
    public ModelAndView addAccountToQuote(@RequestParam(value = "qid", required = true) String qid,
                                          @RequestParam(value = "custno", required = false) String custno)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote q = new Quote(qid);
        model.put("quote", q);

        return new ModelAndView("/quote/quote_share", model);
    }

    @RequestMapping(value = "/quote/account-add", method = RequestMethod.POST)
    public String addAccountToQuotePost(@ModelAttribute("quote") Quote quote, BindingResult result, ModelMap model, HttpSession session)
    {
        //validate field data
        Errors errors = new BeanPropertyBindingResult(quote, "quote");
        String[] reqFields = { "id", "customerNumber", "customerName", "contactName", "emailAddress" };

        for(int i=0; i<reqFields.length; i++)
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, reqFields[i], "field.required");

        if(errors.getErrorCount() > 0) {
            model.put("formError", true);
            result.addAllErrors(errors);
            return "/quote/quote_share";
        }

        //save account/contact to quote
        QBShare share = new QBShare();
        share.setId(UUID.randomUUID().toString());
        share.setCustomerNumber(quote.getCustomerNumber());
        share.setCustomerName(quote.getCustomerName());
        share.setContactName(quote.getContactName());
        share.setEmailAddress(quote.getEmailAddress());
        Quote q = quoteService.findById(quote.getId());
        q.addShare(share);
        quoteService.save(q);

        //@todo; email contact quote in pdf as well as link to online version

        model.put("status", "success");
        model.put("message", "Quote shared successfully. Add another Account?");
        model.put("button_yes", "/quote/account-add.htm?qid="+quote.getId());
        model.put("button_no", "/quote/index.htm");

        return "/message";
    }




    @RequestMapping(value = "/quote/clone", method = RequestMethod.GET)
    public ModelAndView cloneQuote(@RequestParam(value = "qid", required = true) String qid,
                                   @RequestParam(value = "custno", required = false) String custno)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote quote = quoteService.findById(qid);

        if(quote == null) {
            model.put("status", "error");
            model.put("message", "Unable to locate that quote in our system.");
            model.put("button_continue", "/quote/index.htm");
            return new ModelAndView("/message", model);
        }

        if(quote.getStatus() != Quote.Status.PUBLISHED) {
            model.put("status", "error");
            model.put("message", "You cannot clone non-published quotes.");
            model.put("button_continue", "/quote/index.htm");
            return new ModelAndView("/message", model);
        }

        if(custno != null && !custno.isEmpty()) {
            //@todo; look to see if customer # exists

            quote.setId(UUID.randomUUID().toString());
            quote.setCustomerNumber(custno);
            quote.setShipTo("");
            quote.setContactName("");
            quote.setEmailAddress("");
            quote.setAddrStreet("");
            quote.setAddrCity("");
            quote.setAddrState("");
            quote.setAddrZip("");
            quoteService.save(quote);

            return new ModelAndView("redirect:/quote/s1.htm?qid="+quote.getId(), model);
        }

        //fetch all quotes that share the same quoteId
        List<Quote> quotes = quoteService.findByQuoteId(quote.getQuoteId());
        model.put("quotes", quotes);

        return new ModelAndView("/quote/quote_share", model);
    }

    @RequestMapping(value = "/quote/system-save-all", method = RequestMethod.POST)
    public String saveSystems(@ModelAttribute("quote") Quote quote,
                              BindingResult result, ModelMap model, HttpSession session)
    {
        System.out.println("size="+quote.getSystems().size());

        quoteService.save(quote);

        return "redirect:/quote/review.htm?qid=" + quote.getId();
    }

    @RequestMapping(value = "/quote/system-save", method = RequestMethod.POST)
    public String saveSystem(@RequestParam(value = "qid", required = true) String qid,
                             @RequestParam(value = "submit", required = false) String submit,
                             @ModelAttribute("system") MarkFor system, BindingResult result, ModelMap model, HttpSession session)
    {
        //validate field data
        Errors errors = new BeanPropertyBindingResult(system, "system");
        String[] reqFields = { "systemName" };

        for(int i=0; i<reqFields.length; i++)
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, reqFields[i], "field.required");

        if(errors.getErrorCount() > 0) {
            model.put("formError", true);
            result.addAllErrors(errors);
            return "/quote/system_edit";
        }

        if(submit != null && !submit.isEmpty()) {
            if(submit.equals("Cancel")) {
                return "redirect:/quote/review.htm?qid=" + qid;
            }else if(submit.equals("Delete")) {
                quoteService.deleteSystem(qid, system);
            }else{
                system.setStatus(MarkFor.Status.SAVED);
                quoteService.upsertSystem(qid, system);
            }

            //if (submit.equals("Add Accessories"))
            //    return "redirect:/quote/acc.htm?qid="+qid+"&sid="+system.getId();

                //return "redirect:/quote/review.htm?qid=" + qid;
        }

        //default behavior; save system and return to review screen
        //quoteService.upsertSystem(qid, system);

        return "redirect:/quote/review.htm?qid=" + qid;

        //return "redirect:/quote/s2.htm?qid=" + qid;
        //return "redirect:/quote/acc.htm?qid="+qid+"&sid="+system.getId();
    }

    @RequestMapping(value = "/quote/acc", method = RequestMethod.GET)
    public ModelAndView accessoriesView(@RequestParam(value = "qid", required = true) String qid,
                                  @RequestParam(value = "sid", required = true) String sid,
                                  //@RequestParam(value = "tmpl", required = false) String tmpl,
                                  HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Quote quote = quoteService.findById(qid);
        int systemIdx = quote.getSystems().indexOf(new MarkFor(sid));

        //quote/system not found
        if(systemIdx < 0)
            return new ModelAndView("/message", model);

        MarkFor system = quote.getSystems().get(systemIdx);

        //fetch all accessories found for components
        List<QBProduct> products = system.getProducts();
        List<Product> accessories = new ArrayList<Product>();
        for(QBProduct p : products) {
            List<Product> acc = productService.getAccessories(CONO, OPER, new BigDecimal(quote.getCustomerNumber()), quote.getShipTo(), WHSE, p.getSku());
            if(acc != null)
                accessories.addAll(acc);
        }
        model.put("accessories", accessories);

        model.put("qid", qid);
        model.put("sid", sid);

        model.put("system", system);

        return new ModelAndView("/quote/accessories", model);

        //if(tmpl != null && !tmpl.isEmpty()) {
        //    if (tmpl.equals("review"))
        //        return "redirect:/quote/review.htm?qid=" + qid;
        //}

        //return "redirect:/quote/s2.htm?qid=" + qid;
    }

    @RequestMapping(value = "/quote/acc-save", method = RequestMethod.POST)
    public String saveSystem(@RequestParam(value = "qid", required = true) String qid,
                             @RequestParam(value = "sid", required = true) String sid,
                             @RequestParam(value = "submit", required = false) String submit,
                             @ModelAttribute("quote") MarkFor system, BindingResult result, ModelMap model, HttpSession session)
    {
        System.out.println("acc -> save : " + submit);

        if(submit != null && !submit.isEmpty() && submit.equals("Cancel"))
            return "redirect:/quote/s3_edit.htm?qid="+qid+"&sid="+system.getId();

        //add accessories to system


        //save system
        quoteService.upsertSystem(qid, system);

        //return to review screen
        return "redirect:/quote/s3_edit.htm?qid="+qid+"&sid="+system.getId();
    }


    /*
     * ----------------------------
     */






    @RequestMapping(value = "/quote/find", method = RequestMethod.GET)
    public ModelAndView find(HttpServletRequest request, HttpServletResponse response)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        quoteService.find();

        System.out.println("System -> save");

        return new ModelAndView("/quote/index", model);
    }

    @RequestMapping(value = "/quote/create", method = RequestMethod.GET)
    public ModelAndView create(@RequestParam(value = "entry", required = false) String entry,
                               @RequestParam(value = "level", required = false) String level)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        int lvl = 0;
        try { lvl = Integer.parseInt( level ); }catch(Exception ignore) {}
        
        model.put("level", lvl);
        
        if(lvl == 1) {
            List<String> level1 = new ArrayList<String>();
            level1.add("Residential G/E Vert. Splits");
            level1.add("Residential G/E Horz. Splits");
            level1.add("Residential Heat Pump Splits");
            level1.add("Res Gas Packs");
            level1.add("Res Heat Pumps");
            level1.add("Res Hybrid Packs");
            level1.add("Elec/Elec Rooftops");
            level1.add("Gas/Elec Rooftops");
            level1.add("Heat Pump Rooftops");
            level1.add("Gas Unit Heaters");
            level1.add("'M' Series Mini Splits");
            level1.add("'P' Series Mini Splits");
            model.put("levelItems", level1);
        }else if(lvl == 2) {        
            List<String> level2 = new ArrayList<String>();
            level2.add("1.5 Ton Systems");
            level2.add("2.0 Ton Systems");
            level2.add("2.5 Ton Systems");
            level2.add("3.0 Ton Systems");
            level2.add("3.5 Ton Systems");
            level2.add("4.0 Ton Systems");
            level2.add("5.0 Ton Systems");        
            model.put("levelItems", level2);
        }else if(lvl == 3) {
            List<String> level3 = new ArrayList<String>();
            level3.add("113***018");
            level3.add("116***018");
            level3.add("126***018");
            level3.add("24***318");
            level3.add("24***618");        
            model.put("levelItems", level3);            
        }

        return new ModelAndView("/quote/create", model);
    }

    @RequestMapping(value = "/quote/add", method = RequestMethod.GET)
    public ModelAndView add()
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //new quote
        Quote quote = new Quote();
        quote.setQuoteName("Test Quote 2");

        quote.setAddrCity("Atlanta");

        //build a system and add to quote
        MarkFor system = new MarkFor();
        system.addProduct( new QBProduct("50TC-A09A2A5-0A0A0") );
        quote.addSystem(system);

        //save quote
        String quoteId = quoteService.add(quote);

        System.out.println("quoteid: " + quoteId);

        return new ModelAndView("/quote/create", model);
    }

    @RequestMapping(value = "/quote/update", method = RequestMethod.GET)
    public ModelAndView update(@RequestParam(value = "id", required = true) String id)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        /*
        Quote q = new Quote();

        q.setQuoteName("Test quote");

        quoteService.add(q);
        */

        Quote q = quoteService.findById(id);


        System.out.println( q.getQuoteName() );


        q.setLastName("last name");

        List<MarkFor> systems = new ArrayList<MarkFor>();

        MarkFor s = new MarkFor();
        s.setSystemName("system name 2");

        List<QBProduct> products = new ArrayList<QBProduct>();
        QBProduct p = new QBProduct();
        p.setSku("prod_sku");
        p.setDescription("prod description");
        products.add(p);

        s.setProducts(products);


        systems.add(s);

        q.setSystems(systems);

        quoteService.save(q);


        return new ModelAndView("/quote/create", model);
    }

    @RequestMapping(value = "/quote/admin-menu", method = RequestMethod.GET)
    public ModelAndView admin()
    {
//        List<String[]> menu = new ArrayList<String[]>();
//
//        String[] r1 = { "Carrier", "Gas/Electric Split Upflow", "1.5 Ton 13 SEER, Std Upflow G/E 80%, 230-1" };
//        menu.add( r1 );
//
//        String[] r2 = { "Carrier", "Gas/Electric Split Upflow", "2.0 Ton 13 SEER, Std Upflow G/E 80%, 230-1" };
//        menu.add( r2 );
//
//        String[] r3 = { "Carrier", "Heat Pump System", "1.5 Ton 13 SEER, 5 kw, 230-1" };
//        menu.add( r3 );
//
//        String[] r4 = { "Carrier", "Heat Pump System", "2.0 Ton 13 SEER, 5 kw, 230-1" };
//        menu.add( r4 );
//
//        String[] r5 = { "Carrier", "Gas/Electric Rooftop", "48TC, 3.0 Ton, 208-3" };
//        menu.add( r5 );
//
//        String[] r6 = { "Carrier", "Gas/Electric Rooftop", "48TC, 3.0 Ton, 460-3" };
//        menu.add( r6 );
//
//        for(String[] s : menu)
//        {
//            for(int i=0; i<s.length; i++)
//            {
//                QBMenu m = new QBMenu();
//
//                //set id
//                m.setId(s[i]);
//
//                //set parent
//                if(i == 0) m.setParent(null); else m.setParent(s[i-1]);
//
//                //set ancestors
//                List<String> ancestors = new ArrayList<String>();
//                if(i > 0)
//                    for(int j=0; j<i; j++)
//                        ancestors.add( s[j] );
//                m.setAncestors(ancestors);
//
//                //save to db
//                quoteService.addMenuItem(m);
//            }
//        }

        Map<String, Object> model = new HashMap<String, Object>();
        return new ModelAndView("/quote/admin-menu", model);
    }

    @RequestMapping(value = "/quote/admin-menu", method = RequestMethod.POST)
    public ModelAndView uploadFileHandler(@RequestParam("file") MultipartFile file)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(!file.getOriginalFilename().endsWith(".csv"))
        {
            model.put("message", "Error. Only '.csv' files allowed");
            return new ModelAndView("/quote/admin-menu", model);
        }

        if (!file.isEmpty()) {
            try {
                //clear collections
                quoteService.truncateCollection("qb_menu");
                quoteService.truncateCollection("qb_template");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));

                //loop through each line
                String line;
                int lineCount = 1;
                //int menuEndPos = 0;

                String prevLineType = "";

                QBTemplate template = new QBTemplate();

                while ((line = bufferedReader.readLine()) != null) {
                    //split csv line into an array
                    String[] splitted = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

                    if(splitted[0].equalsIgnoreCase("x")) {
                        //do nothing?
                    }else if(splitted[0].equalsIgnoreCase("C")) {
                        //add menu item
                        QBMenu m = new QBMenu();
                        m.setId(splitted[1]);
                        quoteService.addMenuItem(m);

                        //last line was an accessory so we are on a new system
                        //save off last template and create a new one
                        if(prevLineType.equalsIgnoreCase("x")) {
                            quoteService.addTemplate(template);
                            template = new QBTemplate();
                            template.setMenuId(splitted[1]);
                            template.setSystemName(splitted[2].replace("\"", ""));
                        }
//
                        template.addComponent(splitted[3]);
                    }else if(splitted[0].equalsIgnoreCase("A")) {
                        template.addAccessory(splitted[3]);
                    }
//
                    prevLineType = splitted[0];
                    lineCount++;
                }

                //add final template
                quoteService.addTemplate(template);

                model.put("message", "Upload Succeeded");
            } catch (Exception e) {
                System.out.println(e);
                model.put("message", "Upload Failed. Error=" + e.getMessage());
            }
        }else {
            model.put("message", "No file found");
        }

        return new ModelAndView("/quote/admin-menu", model);
    }

    @RequestMapping(value = "/quote/admin-menu-v2", method = RequestMethod.POST)
    public ModelAndView uploadFileHandler_v2(@RequestParam("file") MultipartFile file)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(!file.getOriginalFilename().endsWith(".csv"))
        {
            model.put("message", "Error. Only '.csv' files allowed");
            return new ModelAndView("/quote/admin-menu", model);
        }

        if (!file.isEmpty()) {
            try {
                //clear collections
                quoteService.truncateCollection("qb_menu");
                quoteService.truncateCollection("qb_template");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));

                //loop through each line
                String line;
                int lineCount = 1;
                //int menuEndPos = 0;

                String prevLineType = "";

                QBTemplate template = new QBTemplate();

                while ((line = bufferedReader.readLine()) != null)
                {
                    //split csv line into an array
                    String[] splitted = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

                    System.out.println("length: " + splitted[3]);

                    if(splitted[0].equalsIgnoreCase("C"))
                    {
                        //add menu item
                        QBMenu m = new QBMenu();
                        m.setId(splitted[1]);
                        quoteService.addMenuItem(m);

                        //last line was an accessory so we are on a new system
                        //save off last template and create a new one
                        if(prevLineType.equalsIgnoreCase("A")) {
                            quoteService.addTemplate(template);
                            template = new QBTemplate();
                            template.setMenuId(splitted[1]);
                            template.setSystemName(splitted[2]);
                        }
//
                        template.addComponent(splitted[3]);
                    }else if(splitted[0].equalsIgnoreCase("A")) {
                        template.addAccessory(splitted[3]);
                    }
//
                    prevLineType = splitted[0];
                    lineCount++;
                }

                //add final template
                quoteService.addTemplate(template);

                model.put("message", "Upload Succeeded");
            } catch (Exception e) {
                System.out.println(e);
                model.put("message", "Upload Failed. Error=" + e.getMessage());
            }
        }else {
            model.put("message", "No file found");
        }

        return new ModelAndView("/quote/admin-menu", model);
    }

    @RequestMapping(value = "/quote/admin-menu-v1", method = RequestMethod.POST)
    public ModelAndView uploadFileHandler_v1(@RequestParam("file") MultipartFile file)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(!file.getOriginalFilename().endsWith(".csv"))
        {
            model.put("message", "Error. Only '.csv' files allowed");
            return new ModelAndView("/quote/admin-menu", model);
        }

        if (!file.isEmpty()) {
            try {
                //clear collections
                quoteService.truncateCollection("qb_menu");
                quoteService.truncateCollection("qb_template");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));

                //loop through each line
                String line;
                int lineCount = 1;
                int menuEndPos = 0;
                while ((line = bufferedReader.readLine()) != null)
                {
                    //split csv line into an array
                    String[] splitted = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

                    if(lineCount == 1) {
                        System.out.println("head: " + line);
                        for(int i=0; i<splitted.length; i++)
                        {
                            if(splitted[i].equals("menu") && i > menuEndPos)
                                menuEndPos = i;
                        }
                    }else {
                        //split array into menu / product parts
                        String[] menuItems = Arrays.copyOfRange(splitted, 0, menuEndPos + 1);
                        String[] prodItems = Arrays.copyOfRange(splitted, menuEndPos+1, splitted.length);

                        buildMenuItem(menuItems);
                        buildTemplate(splitted[menuEndPos], prodItems);
                    }
                    lineCount++;
                }

                model.put("message", "Upload Succeeded");
            } catch (Exception e) {
                model.put("message", "Upload Failed. Error=" + e.getMessage());
            }
        }else {
            model.put("message", "No file found");
        }

        return new ModelAndView("/quote/admin-menu", model);
    }

    private void buildMenuItem(String[] arr) {
        for(int i=0; i<arr.length; i++)
        {
            QBMenu m = new QBMenu();

            //set id
            m.setId( arr[i].replace("\"", "") );

            //set parent
            if(i == 0) m.setParent(null); else m.setParent( arr[i-1].replace("\"", "") );

            //set ancestors
            List<String> ancestors = new ArrayList<String>();
            if(i > 0)
                for(int j=0; j<i; j++)
                    ancestors.add( arr[j].replace("\"", "") );
            m.setAncestors(ancestors);

            //save to db
            quoteService.addMenuItem(m);
        }
    }

    private void buildTemplate(String menu, String[] arr)
    {
        QBTemplate template = new QBTemplate();
        template.setId( UUID.randomUUID().toString() );
        template.setMenuId( menu.replace("\"", "")) ;
        template.setComponents( Arrays.asList(arr) );

        quoteService.addTemplate(template);
    }
}