/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author chris.weaver
 */
public class BaseController
{
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ModelAndView handleException(MissingServletRequestParameterException ex)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        model.put("messageTitle", "Missing Parameter");
        model.put("messageNum", "400");
        model.put("message", ex.getMessage());
        model.put("stackTrace", ex.getStackTrace());

        model.put("linkText", "Back");
        model.put("linkUrl", "javascript:history.back();");

        return new ModelAndView("/error/service", model);
    }
    
    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception ex)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        model.put("messageTitle", "Unknown Server Error");
        model.put("messageNum", "501");
        model.put("message", ex.getMessage());
        model.put("stackTrace", ex.getStackTrace());
        
        model.put("linkText", "Back");
        model.put("linkUrl", "/");        
        
        return new ModelAndView("/error/service", model);
    }
}
