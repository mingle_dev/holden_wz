/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author chris.weaver
 */
@Controller
public class ErrorController
{
      public String handle404()
    {
    	return "/error/404";
    }
}
