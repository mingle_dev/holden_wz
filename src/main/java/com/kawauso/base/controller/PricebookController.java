/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.bean.*;
import com.kawauso.base.form.CheckboxList;
import com.kawauso.base.service.PricebookService;
import com.kawauso.base.service.UserService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author chris
 */
@Controller
public class PricebookController
{
    //private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    @Autowired
    private PricebookService pricebookService;
    
    @Autowired
    UserService userService;
    
    @RequestMapping(value = "/pricebook/index", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView index()
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Pricebooks","",""));
        model.put("navTree", crumbs); 
        
        return new ModelAndView("/pricebook/index", model);
    }
    
    @RequestMapping(value = "/pricebook/dealer", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView dealer(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        Session userSession = (Session) session.getAttribute("session");
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Pricebooks","/pricebook/index",""));
        crumbs.add(new Breadcrumb("Dealer Pricebooks","",""));
        model.put("navTree", crumbs);  
        
        //page display type
        model.put("type", "Dealer");
        
        //fetch categories user is tied to
        User user = userService.getUser(userSession.getUserId());

        if(user != null && user.getPricebookCateoryies() != null && !user.getPricebookCateoryies().isEmpty()) {
            String[] categories = user.getPricebookCateoryies().split(",");
            
            //fetch templates
            List<PricebookTemplate> templates = pricebookService.getTemplates("Dealer", categories);
            
            Map checkboxItems = new LinkedHashMap();
            for(PricebookTemplate t : templates) {
                System.out.println(t.getName());
                
                checkboxItems.put(t.getId(), t.getName());
            }
        
            model.put("checkboxesMap", checkboxItems);
            model.put("checkboxList", new CheckboxList());
        }

        return new ModelAndView("/pricebook/schedule", model);
    }
    
    @RequestMapping(value = "/pricebook/retail", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView retail(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        Session userSession = (Session) session.getAttribute("session");
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Pricebooks","/pricebook/index",""));
        crumbs.add(new Breadcrumb("Retail Pricebooks","",""));
        model.put("navTree", crumbs);  
        
        //page display type
        model.put("type", "Retail");
        
        //fetch categories user is tied to
        User user = userService.getUser(userSession.getUserId());

        String profileId = pricebookService.getActiveRetailProfileId(userSession.getCono(), userSession.getCustomerNumber());

        if(profileId == null) {
            model.put("status", "error");
            model.put("message", "No active profiles found. If you are an admin, click continue, otherwise click cancel.");
            model.put("button_cancel", "/pricebook/index.htm");
            model.put("button_continue", "/pricebook/admin/index.htm");
            return new ModelAndView("/message", model);
        }

        if(user != null && user.getPricebookCateoryies() != null && !user.getPricebookCateoryies().isEmpty()) {
            String[] categories = user.getPricebookCateoryies().split(",");
            
            //fetch templates
            List<PricebookTemplate> templates = pricebookService.getTemplates("Retail", categories);
            
            Map checkboxItems = new LinkedHashMap();
            for(PricebookTemplate t : templates)
                checkboxItems.put(t.getId(), t.getName());
        
            model.put("checkboxesMap", checkboxItems);
            model.put("checkboxList", new CheckboxList());
        }
        return new ModelAndView("/pricebook/schedule", model);
    }

    
    
    @RequestMapping(value = "/pricebook/schedule", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView schedule(@RequestParam(value = "type", required = true) String type,
                                 @ModelAttribute CheckboxList checkboxList,
                                 HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(type.isEmpty() || checkboxList.getCheckboxItems() == null || checkboxList.getCheckboxItems().isEmpty()) {
            model.put("status", "error");
            model.put("message", "There was a problem submitting your book. Please make sure you have selected at least one template.");
            model.put("button_continue", "/pricebook/index.htm");
            return new ModelAndView("/message", model);
        }
        
        String[] templates = checkboxList.getCheckboxItems().toArray(new String[0]);
        
        Session userSession = (Session) session.getAttribute("session");
        
        try {
            BigDecimal custno = userSession.getCustomerNumber();
            String shipto = "";
            String emailAddr = userSession.getContact().getEmailAddr();

            pricebookService.run(custno, shipto, emailAddr, type, "Created From Weatherzone", templates);

            model.put("status", "success");
            model.put("message", "Your book has been submitted for processing.");
            model.put("button_continue", "/pricebook/index.htm");
        }catch(Exception ex) {
            model.put("status", "error");
            model.put("message", "There was a problem submitting your book. If the problem persists, please contact support.");
            model.put("button_continue", "/pricebook/index.htm");
        }
        
        return new ModelAndView("/message", model);
    }

    @RequestMapping(value = "/pricebook/upload-image", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView updateImage(@RequestParam(value = "id", required = false) String id)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        System.out.println("get");

        return new ModelAndView("/pricebook/uploadImage", model);
    }

    @RequestMapping(value = "/pricebook/upload-image", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public String updateImage_POST(@RequestParam(value = "id", required = true) String id,
                                   @RequestParam(value = "type", required = true) String type,
                                   @RequestParam("file") MultipartFile file)
    {
        if (!file.isEmpty()) {
            System.out.println(file.getContentType());

            try {
                InputStream in = file.getInputStream();
                BufferedImage bufferedImage = ImageIO.read(in);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ImageIO.write(bufferedImage, "PNG", out);
                byte[] bytes = out.toByteArray();

                Base64 encoder = new Base64();
                byte[] base64bytes = encoder.encode(bytes);

                pricebookService.updateImage(ELUtilities.base64Decode(id), type, new String(base64bytes));

            }catch(Exception ex) {
                System.out.println(ex);
            }
        }

        return "redirect:/pricebook/browse.htm";
    }

    @RequestMapping(value = "/pricebook/browse", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView browse(@RequestParam(value = "tid", required = false) String tid,
                               @RequestParam(value = "sid", required = false) String sid,
                               HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        BigDecimal custno = null;
        String whse = null;
        String shipto = "";

        if(session != null) {
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;
            whse = (session.getWarehouse() != null) ? session.getWarehouse() : null;
        }

        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Product Browser","/pricebook/browse",""));

        if(tid == null || tid.isEmpty()) {
            model.put("categories", pricebookService.getTemplateNames(0));
            model.put("navTree", crumbs);
            return new ModelAndView("/pricebook/category", model);
        }

        if(sid == null || sid.isEmpty()) {
            System.out.println("show by tid");
            model.put("tid", tid);
            model.put("categories", pricebookService.getSheetNames(ELUtilities.base64Decode(tid)));

            crumbs.add(new Breadcrumb(ELUtilities.base64Decode(tid),"/pricebook/browse",""));
            model.put("navTree", crumbs);

            return new ModelAndView("/pricebook/category", model);
        }

        if ((ELUtilities.base64Decode(tid)).contains("Supersede"))
        {
            model.put("products", pricebookService.getSupersedeItems(
                                    session.getCono(),
                                    OPER,
                                    custno,
                                    shipto,
                                    whse,
                                    ELUtilities.base64Decode(tid),
                                    ELUtilities.base64Decode(sid)));

        }else {


            model.put("products", pricebookService.getItems(
                                    session.getCono(),
                                    OPER,
                                    custno,
                                    shipto,
                                    whse,
                                    ELUtilities.base64Decode(tid),
                                    ELUtilities.base64Decode(sid)));
        }
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(tid),"/pricebook/browse","tid="+tid));
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(sid),"/pricebook/browse",""));
        model.put("navTree", crumbs);

        model.put("viewAllButton", true);

        return new ModelAndView("/pricebook/list", model);
    }


    @RequestMapping(value = "/pricebook/browse-all", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView browseAll(@RequestParam(value = "tid", required = false) String tid,
                               @RequestParam(value = "sid", required = false) String sid,
                               HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        BigDecimal custno = null;
        String whse = null;
        String shipto = "";

        if(session != null) {
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;
            whse = (session.getWarehouse() != null) ? session.getWarehouse() : null;
        }

        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Product Browser","/pricebook/browse",""));
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(tid),"/pricebook/browse","tid="+tid));
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(sid),"/pricebook/browse","tid="+tid+"&sid="+sid));
        crumbs.add(new Breadcrumb("Other Products","",""));
        model.put("navTree", crumbs);

        List<Product> prodList = pricebookService.getAllItems(
                session.getCono(),
                OPER,
                custno,
                shipto,
                whse,
                ELUtilities.base64Decode(tid),
                ELUtilities.base64Decode(sid));

        model.put("products", prodList);

        return new ModelAndView("/pricebook/list", model);
    }

    @RequestMapping(value = "/pricebook/admin/index", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView adminIndex(HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");

        //test for retail pb admin security

        BigDecimal custno = null;
        if(session != null)
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;

        try {
            List<RetailPricebookProfile> profiles = pricebookService.getRetailProfiles(session.getCono(), custno);

            for(RetailPricebookProfile p : profiles)
                System.out.println(p.getName() + " " + p.isActive());

            model.put("profiles", profiles);
        }catch(Exception ex) {
            System.out.println(ex);
            //log error and send to user
        }

        return new ModelAndView("/pricebook/retailAdmin", model);
    }

    @RequestMapping(value = "/pricebook/admin/edit", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView adminEdit(@RequestParam(value = "id", required = false) String id,
                                  HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");

        User user = userService.getUser(session.getUserId());

        //test for retail pb admin security

        BigDecimal custno = null;
        if(session != null)
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;

        try {
            RetailPricebookProfile profile = pricebookService.getRetailProfile(id);
            model.put("profile", profile);

            String[] categories = user.getPricebookCateoryies().split(",");
            List<RetailPricebookItemGroup> itemGroups = pricebookService.getRetailItemGroups(session.getCono(), id, categories);
            model.put("itemGroups", itemGroups);

        }catch(Exception ex) {
            System.out.println(ex);
            //log error and send to user
        }

        model.put("mode", "edit");

        return new ModelAndView("/pricebook/retailAdminEdit", model);
    }

    @RequestMapping(value = "/pricebook/admin/new", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView adminNew(HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");
        User user = userService.getUser(session.getUserId());

        //test for retail pb admin security

        try {
            String[] categories = user.getPricebookCateoryies().split(",");
            List<RetailPricebookItemGroup> itemGroups = pricebookService.getRetailItemGroups(session.getCono(), null, categories);
            model.put("itemGroups", itemGroups);

        }catch(Exception ex) {
            System.out.println(ex);
            //log error and send to user
        }

        model.put("mode", "new");

        return new ModelAndView("/pricebook/retailAdminEdit", model);
    }

    @RequestMapping(value = "/pricebook/admin/save", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public String adminSave(HttpServletRequest httpRequest, HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        BigDecimal custno = session.getCustomerNumber();

        String profileName = httpRequest.getParameter("profile_name");
        if(profileName == null || profileName.isEmpty())
            profileName = "Un-named profile";

        double taxRate = 0.00;
        try {
            taxRate = Double.parseDouble(httpRequest.getParameter("tax_rate"));
        }catch(Exception ex){}

        //Pattern to match request parameters for grouped items
        Pattern itemPattern = Pattern.compile("formItem\\[(\\d+)\\].*");

        //store itemGroups by index
        HashMap<String,String[]> map = new HashMap<String,String[]>();

        Enumeration paramNames = httpRequest.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();

            Matcher itemMatcher = itemPattern.matcher(paramName);
            if (itemMatcher.matches()) {
                String index = itemMatcher.group(1); //don't worry about converting to int

                String key = paramName.substring(paramName.lastIndexOf(".")+1);
                String value = httpRequest.getParameter(paramName);

                int pos = -1;
                if(key.equals("groupName"))
                    pos = 0;
                else if(key.equals("description"))
                    pos = 1;
                else if(key.equals("gmPct"))
                    pos = 2;
                else if(key.equals("laborAmt"))
                    pos = 3;
                else if(key.equals("miscAmt"))
                    pos = 4;
                else if(key.equals("commissionRate"))
                    pos = 5;

                //ignore param entry
                if(pos == -1) continue;

                if(!map.containsKey(index)) {
                    String[] entry = new String[6];
                    entry[pos] = value;
                    map.put(index, entry);
                }else{
                    String[] entry = map.get(index);
                    entry[pos] = value;
                }
            }
        }

        if(map.isEmpty()) {
            return "redirect:/error.htm";
        }

        if(httpRequest.getParameter("mode").equals("edit")) {
            String profileId = httpRequest.getParameter("id");
            if(profileId == null || profileId.isEmpty()){
                return "redirect:/error.htm";
            }

            //@todo; this is very slow!!!
            try {
                pricebookService.updateRetailProfile(profileId, custno, profileName, taxRate, map);
            } catch (Exception ex) {
                return "redirect:/error.htm";
            }
        }else {
            try {
                pricebookService.createRetailProfile(session.getCono(), custno, profileName, taxRate, map);
            } catch (Exception ex) {
                return "redirect:/error.htm";
            }
        }
        return "redirect:/pricebook/admin/index.htm";
    }

    @RequestMapping(value = "/pricebook/admin/activate", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public String adminActivate(@RequestParam(value = "id", required = false) String id, HttpSession httpSession)
    {
        //@todo; this seems dangerous; should use user.custno so that employees cannot change customer pricing levels
        //tied to session.custno (employees can change)
        Session session = (Session) httpSession.getAttribute("session");

        try {
            pricebookService.enableRetailProfile(session.getCono(), session.getCustomerNumber(), id);
        } catch (Exception ex) {
            //log error
            //send to error page
        }

        return "redirect:/pricebook/admin/index.htm";
    }

    @RequestMapping(value = "/pricebook/admin/deactivate", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public String adminDeactivate(@RequestParam(value = "id", required = false) String id, HttpSession httpSession)
    {
        //@todo; this seems dangerous; should use user.custno so that employees cannot change customer pricing levels
        //tied to session.custno (employees can change)
        Session session = (Session) httpSession.getAttribute("session");

        try {
            pricebookService.disableRetailProfiles(session.getCono(), session.getCustomerNumber());
        } catch (Exception ex) {
            //log error
            //send to error page
        }

        return "redirect:/pricebook/admin/index.htm";
    }

    @RequestMapping(value = "/pricebook/admin/delete", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public String adminDelete(@RequestParam(value = "id", required = false) String id, HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        try {
            pricebookService.deleteRetailProfile(session.getCono(), session.getCustomerNumber(), id);
        } catch (Exception ex) {

        }

        return "redirect:/pricebook/admin/index.htm";
    }

    @RequestMapping(value = "/pricebook/admin/clone", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public String adminClone(@RequestParam(value = "id", required = false) String id, HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        try {
            pricebookService.cloneRetailPricebookProfile(id);
        } catch (Exception ex) {

        }

        return "redirect:/pricebook/admin/index.htm";
    }

    @RequestMapping(value = "/pricebook/admin/download", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ResponseEntity<byte[]> adminDownload(@RequestParam(value = "id", required = false) String id, HttpSession httpSession)
    {
        ResponseEntity<byte[]> response = null;

        return response;
    }
}