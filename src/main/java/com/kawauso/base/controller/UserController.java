package com.kawauso.base.controller;

import com.google.gson.Gson;
import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.Utilities.Encryption;
import com.kawauso.base.bean.*;
import com.kawauso.base.form.ProfileForm;
import com.kawauso.base.form.RegisterForm;
import com.kawauso.base.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.net.URI;
import java.util.*;


/**
 *
 * @author chris.weaver
 */
@Controller
public class UserController extends BaseController
{
    @Autowired
    private UserService userService;

    @Autowired
    private ListService listService;
    
    @Autowired
    private ProductService productService;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private AccountService accountService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private BluedartService bluedartService;

    //private static int CONO;

    //ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");
    private static final Logger log = LoggerFactory.getLogger( UserController.class );
    private static final BigDecimal DUMMY_ACCOUNT = new BigDecimal(3);
//    @EJB
//    private WZUserSessionBean wzUserSessionBean;
    
    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response)
    {
        Session session = (Session) request.getSession().getAttribute("session");

        //already logged in
        if(session.getType() != null && session.getType().equals("User") && session.getUserId() != null && !session.getUserId().isEmpty()) {
            return new ModelAndView("redirect:/index.htm");
        }

        //set the default value for the distributor's company number
        int cono = Integer.parseInt(ConfigReader.getProperty("sxe.cono"));

        //get the login user's distributor's company number from request
        String value = request.getParameter("cono");

        if (value != null && !value.equals("")){

            cono = Integer.parseInt(value);

        }

        Map<String, String> model = new HashMap<String, String>();
        
        if(request.getParameter("p_username") == null || request.getParameter("p_username").isEmpty() ||
           request.getParameter("p_password") == null || request.getParameter("p_password").isEmpty())
        {
            model.put("message", "Username/Password are required fields.");
            return new ModelAndView("/user/login", model);
        }
        
        //fetch user data
        User user = userService.getUser(request.getParameter("p_username"), request.getParameter("p_password"), cono);

        //user lookup failed
        if(user == null) {
            model.put("message", "Unknown Username/Password. Please try again.");
            return new ModelAndView("/user/login", model);
        }

        //bind user to session
        session.setUser(user);

        //@todo; we might want to sync the user/contact/acct data once and a while
        //to the session since the user can technically be logged on forever and
        //information could change (user/contact/acct) before they log back in again
        //making the data out of sync - do it once a day? - how to track? in session?
        session.setUserId( user.getId() );

        //session.setUsername( user.getUsername() );
        session.setWarehouse( user.getWarehouse() );

        session.setCono( user.getCono() );
//        session.setSecurityAccount( user.isSecurityAccount() );
//        session.setSecurityBasePricing( user.isSecurityBasePricing() );
//        session.setSecurityOrderCreate( user.isSecurityOrderCreate() );
//        session.setSecurityOrderInquiry( user.isSecurityOrderInquiry() );
//        session.setSecurityPBAdmin( user.isSecurityPBAdmin() );
//        //session.setSecurityRetailPricing( user.isSecurityRetailPricing() );
//        session.setSecurityUsers( user.isSecurityUsers() );

        //session.setJobAccount(user.getShipTo());

        //session.setAvailType( user.getAvailType() );

        //session.setType( userService.getUserGroup( user.getGroupId() ) );
        //TODO consider moving getting the group and associating it with user to a method
        String group = userService.getUserGroup( user.getGroupId() );
        if(group.equals("Customers"))
            session.setType( Session.Role.CUSTOMER );
        else if(group.equals("Employees"))
            session.setType( Session.Role.EMPLOYEE );
        else if(group.equals("Distributors"))
            session.setType( Session.Role.DISTRIBUTOR );
        else if(group.equals("Vendors"))
            session.setType( Session.Role.VENDOR );
        else if(group.equals("Administrators"))
            session.setType( Session.Role.ADMINISTRATOR );

        if (user.getUsername().equals("twall") || user.getUsername().equals("ccaldwell")){
            session.setAccess(Session.Function.SPIFFS);
        }

        try {
            //contact
            Contact contact = userService.getContact(user.getContactId());
            session.setContact(contact);

            //account
            Account account = accountService.getAccountInfo(user.getCono(), OPER, contact.getCustomerNumber());
            session.setAccount(account);

            session.setCustomerNumber( account.getCustno() ); //reset session
            session.setCustomerName( account.getName() ); //reset session
            session.setShipTo( user.getShipTo() ); //reset

            //session.setSlsRepId( account.getSalesRep() );

            //session.setTerritory( account.getSalesTerritory() );

            //Map<String, Object> orderInfo = accountService.getOrderingInfo(CONO, OPER, contact.getCustomerNumber());
            //session.setTerritory( (String) orderInfo.get("dfltWhse") );
        }catch(Exception ex) {
            //TODO login(param1, param2) line 155 UserController should display login(username,password) versus login(username, username)
            log.error("login('"+ request.getParameter("p_username") + "', '" + request.getParameter("p_username") + "') - " + ex);
            log.error("new log entry - login('"+ request.getParameter("p_username") + "', '" + request.getParameter("p_password") + "') - " + ex);
            ex.printStackTrace();
            model.put("message", "Unable to locate account information. Please contact support.");
            return new ModelAndView("/user/login", model);
        }
        
        //slsrep details (name, email, etc); might want to tie to Account
        try {
            SalesRep slsrep = accountService.getSalesRep(session.getCono(), session.getAccount().getSlsRepOut());
            session.setSlsRepName( slsrep.getName() );
        }catch(Exception ignore) {
            log.error(ignore.toString());
        }
        
        //misc
        session.setLastSeen(new Date());
        session.setRemoteAddress(request.getHeader("X-FORWARDED-FOR"));

        //TODO consider creating method to handle the authentication
        //try to get the site referer otherwise send to main page
        String token = "";
        try {
            String SECURITYSALT = "DYhG93b0qyJfIxfs2guVoUubWwvniR7G0FgaC9mi";
            String pass = Encryption.SHA1(SECURITYSALT + request.getParameter("p_password"));
            token = Encryption.SHA1(request.getParameter("p_username") + pass);
        } catch (Exception e) {
            log.error(e.toString());
        }

        String referer = "/index.htm?tok="+token;
        try {
            URI refererURI = new URI(request.getHeader("referer"));
            String path = refererURI.getPath().substring( request.getServletContext().getContextPath().length());

            if(!path.contains("login") && !path.equals("/user/profile.htm")) {
                if(refererURI.getQuery() != null)
                    referer = path + "?" + refererURI.getQuery();
                else
                    referer = path;
            }

        }catch(Exception ignore) {
            log.error(ignore.toString());
        }

        //return view
        return new ModelAndView("redirect:" + referer);
    }

//    //@todo; this shares a lot of code with the other login method; need to merge shared code
//    @RequestMapping(value = "/user/token", method = RequestMethod.GET)
//    public ModelAndView login(@RequestParam(value = "tok", required = true) String tok,
//                              HttpServletRequest request,
//                              HttpServletResponse response)
//    {
//        System.out.println("login with token");
//
//        Session session = (Session) request.getSession().getAttribute("session");
//
//        //already logged in
//        if(session.getType() != null && session.getType().equals("User") && session.getUserId() != null && !session.getUserId().isEmpty()) {
//            return new ModelAndView("redirect:/index.htm");
//        }
//
//        Map<String, String> model = new HashMap<String, String>();
//
//        if(tok == null || tok.isEmpty())
//            return new ModelAndView("redirect:/user/login.htm");
//
//        //fetch user data
//        User user = userService.getUserFromToken(tok);
//
//        //user lookup failed
//        if(user == null) {
//            model.put("message", "Unknown Username/Password. Pleas try again.");
//            return new ModelAndView("/user/login", model);
//        }
//
//        //@todo; we might want to sync the user/contact/acct data once and a while
//        //to the session since the user can technically be logged on forever and
//        //information could change (user/contact/acct) before they log back in again
//        //making the data out of sync - do it once a day? - how to track? in session?
//        session.setUserId( user.getId() );
//        session.setUsername( user.getUsername() );
//        session.setWarehouse( user.getWarehouse() );
//        session.setSecurityAccount( user.isSecurityAccount() );
//        session.setSecurityBasePricing( user.isSecurityBasePricing() );
//        session.setSecurityOrderCreate( user.isSecurityOrderCreate() );
//        session.setSecurityOrderInquiry( user.isSecurityOrderInquiry() );
//        session.setSecurityPBAdmin( user.isSecurityPBAdmin() );
//        //session.setSecurityRetailPricing( user.isSecurityRetailPricing() );
//        session.setSecurityUsers( user.isSecurityUsers() );
//
//        session.setJobAccount( user.getShipTo() );
//
//        //session.setType( userService.getUserGroup( user.getGroupId() ) );
//        String group = userService.getUserGroup( user.getGroupId() );
//        if(group.equals("Customers"))
//            session.setType( Session.Role.CUSTOMER );
//        else if(group.equals("Employees"))
//            session.setType( Session.Role.EMPLOYEE );
//        else if(group.equals("Distributors"))
//            session.setType( Session.Role.DISTRIBUTOR );
//        else if(group.equals("Vendors"))
//            session.setType( Session.Role.VENDOR );
//        else if(group.equals("Administrators"))
//            session.setType( Session.Role.ADMINISTRATOR );
//
//        //contact
//        Contact contact = userService.getContact(user.getContactId());
//        session.setFullName( contact.getFirstName() + " " + contact.getLastName() );
//        session.setEmailAddr( contact.getEmailAddr() );
//        session.setCustomerNumber( contact.getCustomerNumber() ); //arsc
//
//        //account
//        try {
//            Account account = accountService.getAccountInfo(CONO, OPER, contact.getCustomerNumber());
//            session.setCustomerName( account.getName() );
//            session.setSlsRepId( account.getSalesRep() );
//
//            Map<String, Object> orderInfo = accountService.getOrderingInfo(CONO, OPER, contact.getCustomerNumber());
//            session.setTerritory( (String) orderInfo.get("dfltWhse") );
//        }catch(Exception ignore) {
//            log.error(ignore);
//        }
//
//        //slsrep
//        try {
//            SalesRep slsrep = accountService.getSalesRep(CONO, session.getSlsRepId());
//            session.setSlsRepName( slsrep.getName() );
//        }catch(Exception ignore) {
//            log.error(ignore);
//        }
//
//        //misc
//        session.setLastSeen(new Date());
//        session.setRemoteAddress(request.getHeader("X-FORWARDED-FOR"));
//
//        //sessionService.set(session);
//
//        String referer = "/index.htm";
//        /*
//        try {
//            URI refererURI = new URI(request.getHeader("referer"));
//            String path = refererURI.getPath().substring( request.getServletContext().getContextPath().length());
//
//            System.out.println(refererURI + " " + path);
//
//            if(!path.contains("login") && !path.equals("/user/profile.htm")) {
//                if(refererURI.getQuery() != null)
//                    referer = path + "?" + refererURI.getQuery();
//                else
//                    referer = path;
//            }
//
//        }catch(Exception ignore) {
//            log.error(ignore);
//        }
//        */
//
//        //return view
//        return new ModelAndView("redirect:" + referer);
//    }

    @RequestMapping(value = "/user/login", method = RequestMethod.GET)
    public ModelAndView login()
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //return login form view
        return new ModelAndView("/user/login", model);
    }   
    
    @RequestMapping(value = "/user/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
    {
//        //expire cookies
//        Cookie[] cookies = request.getCookies();
//        if (cookies != null) {
//            for (int i = 0; i < cookies.length; i++) {
//                cookies[i].setValue("");
//                cookies[i].setPath("/web");
//                cookies[i].setMaxAge(0);
//                response.addCookie(cookies[i]);
//            }        
//        }
        
        Session session = (Session) request.getSession().getAttribute("session");

        session = null;

//        //invalidate login data
//        //@todo; a better way to do this would be to include what fields to
//        //preserve, and invalidate the rest; that way, if a new field is later
//        //added, it will get cleared by default
//        session.setType( Session.Role.ANONYMOUS );
//        session.setUserId(null);
//        session.setCustomerName(null);
//        session.setCustomerNumber(null);
//        session.setEmailAddr(null);
//        session.setFullName(null);
//        session.setLocation(null);
//        session.setSecurityAccount(false);
//        session.setSecurityBasePricing(false);
//        session.setSecurityOrderCreate(false);
//        session.setSecurityOrderCreate(false);
//        session.setSecurityOrderInquiry(false);
//        session.setSecurityPBAdmin(false);
//        session.setSecurityRetailPricing(false);
//        session.setSecurityUsers(false);
//        session.setTerritory(null);
//
        //expire db session
        //sessionService.expire(session);
        //sessionService.set(session);
        
        //expire tomcat session
        //request.getSession().removeAttribute("session");
        //request.getSession().setAttribute("session", session);

        request.getSession().removeAttribute("session");

        //return view
        return new ModelAndView("redirect:/index.htm");
    }

    /**
     *
     * NEW feature change password functionality implementing POST request
     */
    @RequestMapping(value = "/user/password",
            method = RequestMethod.POST)
    public ModelAndView password(@RequestParam("username") String username,
                                 @RequestParam("password") String password,
                                 @RequestParam("new_password") String new_password,
                                 HttpServletRequest request){


        Map<String, Object> model = new ModelMap();

        User user = userService.getUser(username, password);

        if (user == null)
        {
            user = new User();
            user.setUsername(username);
            model.put("user", user);
            model.put("status", -1);
            model.put("message", "Unknown Username/Password. Please try again.");
            return new ModelAndView("/user/password", model);

        }

        user.setPassword(new_password);
        userService.saveUser(user, true);

        model.put("user", user);
        model.put("message", "User password has been successfully updated. Please log in with the new user password.");

        //remove the user's session
        Session session = (Session) request.getSession().getAttribute("session");

        session = null;

        request.getSession().removeAttribute("session");

        //return view
        return new ModelAndView("redirect:/index.htm");

    }


    /**
     *
     * NEW feature change password functionality implementing GET request
     */
    @RequestMapping(value = "/user/password", method = RequestMethod.GET)
    public ModelAndView password(ModelMap model, HttpSession httpSession)
    {
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Change Password", "/user/password", ""));
        model.put("navTree", crumbs);

        Session session = (Session) httpSession.getAttribute("session");
        User user = userService.getUser(session.getUserId());

        model.put("user", user);

        //return login form view
        return new ModelAndView("/user/password", model);
    }

    @RequestMapping(value = "/user/profile", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView profile(ModelMap model, HttpSession httpSession)
    {
        //fetch session data
        Session session = (Session) httpSession.getAttribute("session");
        
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("User Profile","",""));
        model.put("navTree", crumbs);          
        
        User user = userService.getUser(session.getUserId());
        Contact contact = userService.getContact( user.getContactId() );
        model.put("user", user);
        model.put("contact", contact);

        //get locations
        model.put("locations", locationService.findAll());
        
        //shipto's
        model.put("shipToList", accountService.getShipToList(session.getCono(), OPER, session.getCustomerNumber(), false));
        
        model.put("profileForm", new ProfileForm());
        
        return new ModelAndView("/user/profile", model);
    }
    
    @RequestMapping(value = "/user/profile", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView profile(@ModelAttribute("profileForm") ProfileForm form, BindingResult result, ModelMap model, HttpSession session)
    {
        //fetch session data
        Session userSession = (Session) session.getAttribute("session");
        
        //update user
        User user = userService.getUser(userSession.getUserId());
        user.setWarehouse( form.getWarehouse() );
        user.setShipTo(form.getJobAccount());
        user.setShipVia(form.getShipVia());
        user.setPageLimit(form.getPageLimit());
        user.setResetWhse(form.isResetWhse());
        user.setShipNotifyType(form.getShipNotifyType());
        user.setAvailType( form.getAvailType() );
        userService.saveUser(user, false);
        
        //update contact
        Contact contact = userService.getContact( user.getContactId() );
        contact.setEmailAddr( form.getEmailAddr() );
        contact.setPhoneWork( form.getOfficePhone() );
        userService.saveContact(contact);
        
        //finally, update session to match
        userSession.setWarehouse( user.getWarehouse() );
        userSession.setShipTo( user.getShipTo() );

        model.put("message", "Your profile has been updated.");

        return profile(model, session);
    }


    /**
     * Saved List Index View
     * 
     * Gets all lists for a specific user based on id
     */
    @RequestMapping(value = "/user/lists/index", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView lists(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session userSession = (Session) session.getAttribute("session");

        //get all lists for user
        List<ListCategory> lists = listService.getLists(userSession.getUserId(), null, true);
        model.put("lists", lists);
        
        //return login form view
        return new ModelAndView("/user/listIndex", model);
    }
    
    /**
     * Saved List Details
     */    
    @RequestMapping(value = "/user/lists/detail", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listDetail(@RequestParam(value = "id", required = false) String id,
                                   HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session userSession = (Session) session.getAttribute("session");
        
        //fetch list by id
        if(id == null || id.isEmpty()) {
            model.put("status", "error");
            model.put("message", "Unknown saved list");
            model.put("button_continue", "/user/lists/index.htm");
            return new ModelAndView("/message", model);
        }
        
        //fetch list and set pricing/availability
        ListCategory list = listService.getList(id, true);
        for(ListItem item : list.getListItems()) {
            Product p = productService.applyPricing(userSession.getCono(), OPER, userSession.getCustomerNumber(), userSession.getShipTo(), userSession.getWarehouse(), item.getProduct());
            item.setProduct(p);
        }
        
        if(list == null)
        {
            model.put("status", "error");
            model.put("message", "Unable to retrieve list details.");
            model.put("button_continue", "/user/lists/index.htm");
            return new ModelAndView("/message", model);
        }
        
        model.put("list", list);
        
        return new ModelAndView("/user/listDetail", model);
    }
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/edit", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listEdit(@RequestParam(value = "id", required = false) String id,
                                 HttpSession session)
    {
        ModelAndView mv = this.listDetail(id, session);

        Map<String, Object> model = mv.getModel();
        
        if(model.containsKey("error"))
            return mv;
        
        mv.setViewName("/user/listEdit");
        
        return mv;
    }
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/add-item", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listAddItem(@RequestParam(value = "prod", required = false) String prod,
                                    HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        Session userSession = (Session) session.getAttribute("session");
        
        if(userSession == null)
            return null; //need to return error screen        
        
        //get all lists for user (really could do this using lists() method
        List<ListCategory> lists = listService.getLists(userSession.getUserId(), null, true);
        model.put("lists", lists);

        model.put("productCode", prod);
        
        return new ModelAndView("/user/listAddItem", model);
    }

    /**
     * Saved List Editor
     */
    @RequestMapping(value = "/user/lists/remove-item", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listRemoveItem(@RequestParam(value = "listId", required = false) String listId,
                                       @RequestParam(value = "prod", required = false) String prod,
                                       @RequestParam(value = "qty", required = false) BigDecimal qty,
                                       HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session userSession = (Session) session.getAttribute("session");

        if(userSession == null)
            return null; //need to return error screen

        listService.deleteItem(listId, ELUtilities.base64Decode(prod));

        return new ModelAndView("redirect:edit.htm?id="+listId, model);
    }

    /**
     * Saved List Editor
     */
    @RequestMapping(value = "/user/lists/update-item-qty", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public  @ResponseBody String  listUpdateItemQty(@RequestParam(value = "listId", required = false) String listId,
                                       @RequestParam(value = "prod", required = false) String prod,
                                       @RequestParam(value = "qty", required = false) BigDecimal qty,
                                       HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session userSession = (Session) session.getAttribute("session");

        if(userSession == null)
            return null; //need to return error screen

        boolean status = listService.updateItemQty(listId, ELUtilities.base64Decode(prod), qty);
        System.out.println(status);

        JsonResult res = new JsonResult();
        res.setStatus(true);
        res.setMessage("Your item has been updated");

        return new Gson().toJson(res);
    }

    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/add-item", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true) //not sure how to handle this one...
    public @ResponseBody String listAddItem(@RequestParam(value = "list_id", required = false) String id,
                                     @RequestParam(value = "new_listname", required = false) String name,
                                     @RequestParam(value = "prod", required = false) String prod,
                                     @RequestParam(value = "qty", required = false) String qty,
                                     HttpSession session)
    {
        JsonResult res = new JsonResult();
        
        try {
            Session userSession = (Session) session.getAttribute("session");

            if(id != null && !id.isEmpty()) {
                ListCategory list = new ListCategory();
                list.setId(id);
                int displayOrder = listService.getItemCount(list);

                ListItem item = new ListItem();
                item.setProductCode(ELUtilities.base64Decode(prod));
                try { item.setQty( Integer.parseInt(qty) ); }catch(Exception ignore){}
                item.setDisplayOrder(displayOrder + 1);

                //link list category to item
                item.setListCategory(list);

                listService.addItem(item);


                if(item.getId() != null) {
                    res.setStatus(true);
                    res.setMessage("Your item has been saved");
                }else{
                    res.setStatus(false);
                    res.setMessage("Unable to save item");
                }
            }else if(name != null && !name.isEmpty()) {
                ListCategory list = new ListCategory();
                list.setName(name);
                list.setUserId(userSession.getUserId());

                ListItem item = new ListItem();
                item.setProductCode(ELUtilities.base64Decode(prod));
                try { item.setQty( Integer.parseInt(qty) ); }catch(Exception ignore){}
                item.setDisplayOrder(1);

                //setup hibernate associations
                list.addListItem(item);
                item.setListCategory(list);

                //save
                listService.saveList(list);

                if(list.getId() != null) {
                    res.setStatus(true);
                    res.setMessage("Your item has been saved");
                }else{
                    res.setStatus(false);
                    res.setMessage("Unable to save item");
                }  

            }else{
                res.setStatus(false);
                res.setMessage("Unable to save item");
            }
        }catch(Exception ex) {
                res.setStatus(false);
                res.setMessage("Fatal error occurred. Unable to complete transaction.");
        }
        return new Gson().toJson(res);
    }
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/new-from-cart", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listAddCart(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        Session userSession = (Session) session.getAttribute("session");

        return new ModelAndView("/user/listAddCart", model);
    }    
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/new-from-cart", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String listAddCart(@RequestParam(value = "listName", required = false) String listName,
                                            HttpSession session)
    {
        JsonResult res = new JsonResult();
        
        Session userSession = (Session) session.getAttribute("session");

        ListCategory list = new ListCategory();
        list.setUserId( userSession.getUserId() );
        list.setName(listName);
        
        int cnt = 1;
        List<CartItem> items = cartService.getCartItems(userSession.getUserId());
        for(CartItem item : items) {
            ListItem li = new ListItem();
            li.setProductCode(item.getProductCode());
            li.setQty( item.getQty() );
            li.setDisplayOrder(cnt++);
            li.setListCategory(list); //tie item to category (parent)
            
            list.addListItem(li); //add item to category
        }
        
        listService.saveList(list);

        if(list.getId() != null) {
            res.setStatus(true);
            res.setMessage("List created successfully");
        }else{
            res.setStatus(false);
            res.setMessage("Unable to create new list");
        }

        return new Gson().toJson(res);
    }    
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/lists/rename", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String listRename(@RequestParam(value = "id", required = false) String id,
                                           @RequestParam(value = "name", required = false) String name,
                                           HttpSession session)
    {
        JsonResult res = new JsonResult();
        
        try {
            Session userSession = (Session) session.getAttribute("session");

            if(id != null && !id.isEmpty()) {
                if( listService.renameList(id, name) != null) {
                    res.setStatus(true);
                    res.setMessage("Your list has been renamed");
                }else{
                    res.setStatus(true);
                    res.setMessage("DB Error. Unable to rename list.");
                }
            }else{
                res.setStatus(false);
                res.setMessage("Unable to rename list");
            }
        }catch(Exception ex) {
            res.setStatus(false);
            res.setMessage("Fatal error occurred. Unable to complete transaction.");
            log.error(ex.toString());
        }
        return new Gson().toJson(res);
    }    
    
    /**
     * Save List Share for employees and admins
     */
    @RequestMapping(value = "/user/lists/admin-share", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView adminShare(@RequestParam(value = "id", required = true) String id,
                                   @RequestParam(value = "custno", required = false) BigDecimal custno,
                                   @RequestParam(value = "action", required = false) String action)
    {
        Map<String, Object> model = new HashMap<String, Object>();
                
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession httpSession = attr.getRequest().getSession(false);
        Session session = (Session) httpSession.getAttribute("session");  

        if(action != null) {
            if(action.equalsIgnoreCase("share")) {
                
                String accountId = bluedartService.getAccountId(session.getCono(), custno);
                listService.addShare(id, session.getUserId(), accountId, session.getCono(), custno);
            }else if(action.equalsIgnoreCase("unshare")) {
                listService.removeShare(id, session.getCono(), custno);
            }
        }

        model.put("list", listService.getList(id, false));
        model.put("shares", listService.getShares(id));

        return new ModelAndView("/user/listShare", model);
    }
    
    /**
     * Saved List Share for customers
     */    
    @RequestMapping(value = "/user/lists/share", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listShare(@RequestParam(value = "id", required = true) String id,
                                  @RequestParam(value = "q", required = false) String confirm)
    {
        Map<String, Object> model = new HashMap<String, Object>();
                
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession httpSession = attr.getRequest().getSession(false);
        Session session = (Session) httpSession.getAttribute("session");  

        //get the list
        ListCategory list = listService.getList(id, false);

        //get bluedart account id for legacy support
        String accountId = bluedartService.getAccountId(session.getCono(), session.getCustomerNumber());
        
        boolean status = false;
        boolean isShared = listService.isShared(list.getId(), session.getCustomerNumber());

        if(isShared) { //remove share(s)
            
            if(confirm != null && !confirm.isEmpty()) {
                if(confirm.equals("yes")) {
                    status = listService.removeShare(list.getId());
                    model.put("status", "success");
                    model.put("message", "Shares successfully removed from this list.");
                }else{
                    model.put("status", "success");
                    model.put("message", "Share delete request has been cancelled.");
                }
            
                model.put("button_continue", "/user/lists/index.htm");
            
            }else{
                model.put("message", "Thist list is already shared. Do you want to remove this share?"); 
                model.put("button_yes", "/user/lists/share.htm?id=" + id + "&q=yes");
                model.put("button_no", "/user/lists/share.htm?id=" + id + "&q=no");
            }
        
        }else{ //add share(s)
            
            status = listService.addShare(list.getId(), list.getUserId(), accountId, session.getCono(), session.getCustomerNumber());
            
            model.put("status", "success");
            model.put("message", "Your list has been successfully shared.");
            model.put("button_continue", "/user/lists/index.htm"); 
        }

        return new ModelAndView("/message", model);
    }    
    
    /**
     * Saved List Share
     */
    @RequestMapping(value = "/user/lists/shares", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listShares(@RequestParam(value = "id", required = false) String id,
                                   HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");

        List<ListCategory> shares = listService.getSharedLists(session.getCono(), session.getCustomerNumber());
        model.put("shares", shares);

        return new ModelAndView("/user/listShares", model);
    }
    
    /**
     * Saved List Clone
     */  
    @RequestMapping(value = "/user/lists/clone", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listClone(@RequestParam(value = "id", required = true) String id)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        String newList = listService.cloneList(id);
        
        if( newList != null) {
            model.put("status", "success");
            model.put("message", "Your list has been successfully cloned.");
            model.put("button_continue", "/user/lists/detail.htm?id=" + newList);
        }else{
            model.put("status", "error");
            model.put("message", "There was an error cloning your list.");
            model.put("button_continue", "/user/lists/index.htm");
        }
        
        return new ModelAndView("/message", model);
    }    
    
/**
     * Saved List Clone
     */  
    @RequestMapping(value = "/user/lists/delete", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView listDelete(@RequestParam(value = "id", required = true) String id,
                                   @RequestParam(value = "q", required = false) String confirm)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(confirm != null && !confirm.isEmpty()) {
            if(confirm.equals("yes")) {
                ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
                HttpSession session = attr.getRequest().getSession(false);
                Session userSession = (Session) session.getAttribute("session");        

                ListCategory  list = listService.getList(id, false);

                if(!list.getUserId().equals(userSession.getUserId())) {
                    model.put("status", "error");
                    model.put("message", "You can not delete a list that does not belong to you.");
                }else{
                    if(listService.deleteList(id) == true) {
                        model.put("status", "success");
                        model.put("message", "Your list has been successfully deleted.");
                    }else{
                        model.put("status", "error");
                        model.put("message", "There was an error deleting your list.");
                    }
                }
            }else{
                model.put("status", "success");
                model.put("message", "Delete request has been cancelled.");
            }
            
            model.put("button_continue", "/user/lists/index.htm");
            
        }else{
            model.put("message", "Are you sure you want to delete this list?"); 
            model.put("button_yes", "/user/lists/delete.htm?id=" + id + "&q=yes");
            model.put("button_no", "/user/lists/delete.htm?id=" + id + "&q=no");
        }
        
        return new ModelAndView("/message", model);
    }    
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/product/update-item", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public @ResponseBody String updateProductData(@RequestParam(value = "prod", required = false) String prod,
                                                  @RequestParam(value = "descrip", required = false) String descrip,
                                                  HttpSession session)
    {
        JsonResult res = new JsonResult();
        
        Session userSession = (Session) session.getAttribute("session");

        UserProduct product = new UserProduct();
        product.setUserId(userSession.getUserId());
        product.setProductCode(ELUtilities.base64Decode(prod));
        product.setDescription(descrip);

        productService.updateUserProduct(product);
        
        if(product.getId() != null) {
            res.setStatus(true);
            res.setMessage("Your item has been updated");
        }else{
            res.setStatus(false);
            res.setMessage("Unable to update item");
        }

        return new Gson().toJson(res);
    }    
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/register", method = RequestMethod.GET)
    public ModelAndView register(@RequestParam(value = "cono", required = false) String cono)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Sign In", "/user/login", ""));
        crumbs.add(new Breadcrumb("User Registration", "", ""));
        model.put("navTree", crumbs);

        if (cono != null && !cono.equals("")) {
            model.put("cono", cono);
        }

        model.put("registerForm", new RegisterForm());
        
        return new ModelAndView("/user/register", model);
    }
    
    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("registerForm") RegisterForm form, BindingResult result, ModelMap model, HttpSession session)
    {
        //validate field data
        Errors errors = new BeanPropertyBindingResult(form, "registerForm");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "field.required");
        if(form.getLastName() != null && form.getLastName().trim().isEmpty() && errors.getFieldError("firstName") == null)
            errors.rejectValue("firstName", "field.required");
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddr", "field.required");        

        //username validation
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "field.required");  
        if(errors.getFieldError("username") == null && userService.validUserName(form.getUsername()) == false)
            errors.rejectValue("username", "field.username.invalid");
        
        //password validation & comparison
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password1", "field.required"); 
        if(errors.getFieldError("password1") == null && form.getPassword1().length() < 8)
            errors.rejectValue("password1", "field.passwd.spec");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password2", "field.required");        
        if(errors.getFieldError("password2") == null && !form.getPassword1().equals(form.getPassword2()))
            errors.rejectValue("password2", "field.passwd.match");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyName", "field.required");
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accountNo", "field.required");
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "invoice1", "field.required");
        if(form.getInvoice2() != null && form.getInvoice2().trim().isEmpty() && errors.getFieldError("invoice1") == null)
            errors.rejectValue("invoice1", "field.required");

        int cono = Integer.parseInt(form.getCono());

        //validate invoice #'s against account #
        BigDecimal custno = BigDecimal.ZERO;
        try {
            custno = new BigDecimal(form.getAccountNo());
            if (!custno.equals(DUMMY_ACCOUNT)) {
                String[] inv = form.getInvoice1().split("-");
                if (accountService.getOrder(cono, OPER, custno, Integer.parseInt(inv[0]), 0) == null)
                    throw new Exception("inv1 no match");

                inv = form.getInvoice2().split("-");
                if (accountService.getOrder(cono, OPER, custno, Integer.parseInt(inv[0]), 0) == null)
                    throw new Exception("inv2 no match");
            }
        }catch(Exception ex) {

            errors.rejectValue("invoice1", "field.inv.match");
        }
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tos", "field.tos");
 
        if(errors.getErrorCount() > 0) {
            model.put("formError", true);
            result.addAllErrors(errors);
            return "/user/register";
        }
        
        /* 
         * at this point, we know that the data supplied is valid
         * setup user, log them in and send to profile page
         */
        //search for existing contact_id in contacts by first/last name
        String contactId = userService.getContactId(custno, String.valueOf(cono), form.getFirstName(), form.getLastName());

        //create contact, tie to account
        if(contactId == null || contactId.equals("")) {

            String acctId = bluedartService.getAccountId(cono, custno);

            Contact contact = new Contact();
            contact.setAccountId(acctId);
            //contact.setCustomerNumber(custno);
            contact.setFirstName(form.getFirstName());
            contact.setLastName(form.getLastName());
            contact.setPhoneWork(form.getPhoneNo());
            contact.setEmailAddr(form.getEmailAddr());

            userService.saveContact(contact);

            contactId = contact.getId();
        }

        //create user, tie to contact
        User user = new User();
        user.setContactId(contactId);
        user.setCono(cono);
        user.setUsername(form.getUsername());
        user.setPassword(form.getPassword1());
        user.setGroupId("4d6f98a3-9690-4ec7-99e5-5940c0a80a21"); //customers
        user.setAvailType("local");

        Map<String, Object> orderInfo = accountService.getOrderingInfo(cono, OPER, custno);
        user.setWarehouse( (String) orderInfo.get("dfltWhse") );
        
        userService.saveUser(user, true);
        
        //@todo; for now redirect to login; would like to log user in here
        //and send to profile page for further information gathering
        return "redirect:/user/login.htm";
    }
    
    /**
     * Saved List Editor
     */    
    @RequestMapping(value = "/user/recovery", method = RequestMethod.GET)
    public ModelAndView recovery(@RequestParam(value = "forgotpassword", required = false) String forgotpassword,
                                    HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        String url;
        
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Sign In", "/user/login", ""));
        
        if(forgotpassword != null) {
            crumbs.add(new Breadcrumb("Reset Password", "", ""));
            url = "/user/forgotPass";
        }else{
            crumbs.add(new Breadcrumb("Forgot Username", "", ""));
            url = "/user/forgotUser";
        }
        
        model.put("navTree", crumbs);
        
        return new ModelAndView(url, model);
    }


    @RequestMapping(value = "/user/grant-acct", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView grantAcctAccess(@RequestParam(value = "token", required = true) String token, HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //fetch session data
        Session userSession = (Session) session.getAttribute("session");

        if(!token.equals("193975")) {

            BigDecimal custno = userSession.getCustomerNumber();
            int cono = userSession.getCono();
            String customerName = userSession.getCustomerName();
            String companyName = userSession.getAccount().getName();
            String distributorName = ConfigReader.getProperty("sxe.distr.name." + cono);
            String firstName = userSession.getContact().getFirstName();
            String lastName = userSession.getContact().getLastName();
            String emailAddress = userSession.getContact().getEmailAddr();

            model.put("custno", custno);
            model.put("distributorName", distributorName);
            model.put("customerName", customerName);
            model.put("companyName", companyName);
            model.put("firstName", firstName);
            model.put("lastName", lastName);
            model.put("contactEmail", emailAddress);

            String bodyMessage = distributorName + "\n" +
                        companyName + "\n" +
                        custno + "\n" +
                        firstName + " " + lastName + "\n";

            try {
                bodyMessage = java.net.URLEncoder.encode(bodyMessage, "UTF-8");
                bodyMessage = bodyMessage.replace("+", bodyMessage);
                System.out.println(bodyMessage);
            }catch (Exception ex){
                ex.printStackTrace();
            }

            model.put("emailBodyMessage", bodyMessage);
            model.put("emailReplyAddress", emailAddress);
            model.put("billpaySendToEmail", ConfigReader.getProperty("billpay.sendTo.email"));

            model.put("message", "Invalid Token.");
            return new ModelAndView("/account/unauthorized", model);
        }

        User user = userService.getUser(userSession.getUserId());
        
        user.setSecurityAccount(true);
        
        userService.saveUser(user, false);

        //TODO: Uncomment to enable session checking  CEB
      //  userSession.setSecurityAccount(user.isSecurityAccount());


        return new ModelAndView("redirect:/account/balances.htm");
    }

    @RequestMapping(value = "/user/favorite-maint", method = RequestMethod.GET)
    public ModelAndView maintFavoriteView(@RequestParam(value = "id", required = true) String id,
                                          HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        model.put("favorite", userService.getFavorite(id));

        System.out.println("uid: " + userSession.getUserId());
        System.out.println("fid: " + id);

        return new ModelAndView("/user/favorite-edit", model);
    }

    @RequestMapping(value = "/user/favorite-maint", method = RequestMethod.POST)
    public ModelAndView maintFavorite(@RequestParam(value = "id", required = true) String id,
                                      @RequestParam(value = "url", required = true) String url,
                                      @RequestParam(value = "name", required = false) String name,
                                      @RequestParam(value = "func", required = false) String func,
                                      HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        if(func.equals("Delete")) {
            userService.removeFavorite(id);

            model.put("message", "Favorite successfully removed.");
            model.put("button_continue", "/index.htm");
        }else {
            //userService.updateFavorite(...);

            model.put("message", "Favorite successfully updated.");
            model.put("button_continue", ELUtilities.base64Decode(url));
        }
        return new ModelAndView("/message", model);
    }

    @RequestMapping(value = "/user/favorite-add", method = RequestMethod.GET)
    public ModelAndView addFavoriteView(@RequestParam(value = "url", required = true) String url,
                                        @RequestParam(value = "name", required = false) String name,
                                        HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("url", url);
        model.put("name", name);

        return new ModelAndView("/user/favorite-add", model);
    }

    @RequestMapping(value = "/user/favorite-add", method = RequestMethod.POST)
    public ModelAndView addFavorite(@RequestParam(value = "url", required = true) String url,
                                    @RequestParam(value = "name", required = false) String name,
                                    @RequestParam(value = "link_icon", required = false) String linkIcon,
                                    HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        url = ELUtilities.base64Decode(url).replace("/web", "");

        userService.addFavorite(userSession.getUserId(), url, name, linkIcon);

        model.put("message", "Favorite successfully saved.");
        model.put("button_continue", url);

        return new ModelAndView("/message", model);
    }

    @RequestMapping(value = "/user/custno-assign", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView assignCustno(@RequestParam(value = "custno", required = true) String custno,
                        HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        Map<String, Object> model = new HashMap<String, Object>();

        userSession.setCustomerNumber( new BigDecimal(custno) );
        userSession.setShipTo("");

        model.put("message", "Customer # has been assigned successfully");
        model.put("button_continue", "/index.htm");

        return new ModelAndView("/message", model);
    }

    /**
     * getUser retrieves user information for datasource
     * @param username
     * @param password
     * @return
     */
    private User getUser (String username, String password)
    {
        User user = null;

        if (username == null || username.isEmpty() || password == null || password.isEmpty())
        {
            return user;
        }
        //fetch user data
        user = userService.getUser(username, password);

        return user;

    }

    /**
     * validateString returns boolean value... true if string is null or empty
     * @param aString
     * @return
     */
    private boolean isEmpty (String aString)
    {
        return (aString == null || aString.isEmpty());
    }
}