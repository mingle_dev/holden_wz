package com.kawauso.base.controller;

import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.bean.*;
import com.kawauso.base.form.SpiffEnrollForm;
import com.kawauso.base.service.SpiffService;
import com.kawauso.base.service.UserService;
import com.kawauso.jdbc.SpiffDataFacade;
import com.kawauso.jdbc.domain.ClaimItems;
import com.kawauso.jdbc.domain.SpiffClaimDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * this is a test
 * @author chris
 */
@Controller
public class SpiffController
{
    @Autowired
    UserService userService;
    
    @Autowired
    SpiffService spiffService;


    //private static final int YEAR = new DateTime().getYear();
    private final int YEAR = 2017;
    //private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private final Logger logger = LoggerFactory.getLogger(SpiffController.class);


    class SpiffClaimLocal{

        String   claimNbr;
        String   orderNo;
        String   message;
        Boolean  isValid;

        public String getClaimNbr() {
            return claimNbr;
        }

        public void setClaimNbr(String claimNbr) {

            this.claimNbr = claimNbr;

        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Boolean getIsValid() {
            return isValid;
        }

        public void setIsValid(Boolean isValid) {
            this.isValid = isValid;
        }
    }


    @RequestMapping(value = "/spiffs/index", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView index(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session userSession = (Session) session.getAttribute("session");

        User user = userService.getUser( userSession.getUserId() );

        //verify user is enrolled for spiff program
        if(user.getSpiffType() == 0) {
        //if(1 == 1) {
            SpiffEnrollForm form = new SpiffEnrollForm();
            form.setConditions("I certify that I am a full-time salesperson and or serviceperson " +
                    "for my dealership and I am entitled to the salesperson's spiffs " +
                    "offered by Mingledorff's. I understand that my submission is " +
                    "evaluated by an administrator to confirm my " +
                    "information and may reject my request for any reason.");
            model.put("spiffEnrollForm", form);
            return new ModelAndView("/spiffs/enroll", model);
        }

        String username = user.getUsername();
        String spiff_years = ConfigReader.getProperty("spiff.years.user." + username);

        List<SpiffClaim> claims;
        if (spiff_years != null && spiff_years.equalsIgnoreCase("ALL")) { claims = spiffService.findAll( -999, user.getContactId()); }
        else { claims = spiffService.findAll( YEAR, user.getContactId()); }
        model.put("claims", claims);
        
        BigDecimal totClaimAmt = BigDecimal.ZERO;
        for(SpiffClaim claim : claims) {
            if(claim.getAmount() != null)
                totClaimAmt = totClaimAmt.add(new BigDecimal(claim.getAmount()));
        }

        model.put("spiffType", user.getSpiffType());

        model.put("totClaimAmt", totClaimAmt);
        model.put("numClaims", (claims != null) ? claims.size() : 0);
        
        return new ModelAndView("/spiffs/index", model);
    }
    
    @RequestMapping(value = "/spiffs/enroll", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView enroll()
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        return new ModelAndView("/message", model);
    }
    
    @RequestMapping(value = "/spiffs/add", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView addView(@RequestParam(value = "type", required = true) String type)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        if(!type.equalsIgnoreCase("C") && !type.equalsIgnoreCase("B"))
        {
            model.put("status", "error");
            model.put("message", "Unknown Spiff Claim Type");
            model.put("button_continue", "/spiffs/index.htm");

            return new ModelAndView("/message", model);
        }

        //this keeps everything flowing for values on the view
        Map<String, String> map = new HashMap<>();
        map.put("claim_type", type);
        model.put("mapData", map);

        //merge map data
        model.putAll( buildCategories(type) );
        
        return new ModelAndView("/spiffs/add", model);
    }

    @RequestMapping(value = "/spiffs/add", method = RequestMethod.POST)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView addClaim(HttpServletRequest request, HttpServletResponse response, HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //merge map data
        model.putAll( buildCategories(request.getParameter("claim_type")) );

        //tie request key/val params back to view
        Map<String, String> data = new HashMap<>();
        Enumeration keys = request.getParameterNames();
        while(keys.hasMoreElements())
        {
            String k = (String) keys.nextElement();
            data.put(k, request.getParameter(k));
        }
        model.put("mapData", data);

        //fetch contactId
        Session userSession = (Session) session.getAttribute("session");
        String contactId = userSession.getUser().getContactId();

        if(contactId == null || contactId.isEmpty())
        {
            model.put("status", "error");
            model.put("message", "Oh Dear, We are unable to determine who you are! " +
                      "It looks like there is a problem with your account. Please contact our technical support for assistance.");
            model.put("button_continue", "/spiffs/index.htm");
            return new ModelAndView("/message", model);
        }

        //set default to true
        boolean valid = true;

        BigDecimal claimTotal = null;

        //bryant claim
        if( request.getParameter("claim_type").equalsIgnoreCase("B") ) {
            BigDecimal systemSpiffAmt = this.validateBryant(request);

            //get E, F amounts
            BigDecimal accTotal = new BigDecimal(0);
            if(request.getParameter("spiff_category_E").equalsIgnoreCase("GAPAA") || request.getParameter("spiff_category_E").equalsIgnoreCase("GAPAB"))
                accTotal = accTotal.add(new BigDecimal(20));
            if(request.getParameter("spiff_category_F").equalsIgnoreCase("HUMBB"))
                accTotal = accTotal.add(new BigDecimal(15));

            //if system not found and no accessories included
            if(systemSpiffAmt == null && accTotal.equals(BigDecimal.ZERO)) {
                model.put("message", "Current configuration is not an approved system");
                valid = false;
                return new ModelAndView("/spiffs/add", model);
            }

            //set total claim amount
            claimTotal = (systemSpiffAmt == null) ? accTotal : systemSpiffAmt.add(accTotal);

        //carrier
        } else if( request.getParameter("claim_type").equalsIgnoreCase("C") ) {
            BigDecimal systemSpiffAmt = this.validateCarrier(request);

            //get E, F amounts
            BigDecimal accTotal = new BigDecimal(0);
            if(request.getParameter("spiff_category_E").equalsIgnoreCase("GAPAA") || request.getParameter("spiff_category_E").equalsIgnoreCase("GAPAB"))
                accTotal = accTotal.add(new BigDecimal(20));
            if(request.getParameter("spiff_category_F").equalsIgnoreCase("HUMCC"))
                accTotal = accTotal.add(new BigDecimal(15));

               //if system not found and no accessories included
            if(systemSpiffAmt == null && accTotal.equals(BigDecimal.ZERO)) {
                model.put("message", "Current configuration is not an approved system");
                valid = false;
                return new ModelAndView("/spiffs/add", model);
            }

            //set total claim amount
            claimTotal = (systemSpiffAmt == null) ? accTotal : systemSpiffAmt.add(accTotal);

        //unknown claim type
        } else {
            model.put("status", "error");
            model.put("message", "Unknown Spiff Claim Type");
            model.put("button_continue", "/spiffs/index.htm");
            return new ModelAndView("/message", model);
        }


        Map<String,Boolean> validCategoryMap = new HashMap();

        //spiff claim number of units in error
        int count = 0;

        // If it matches any of the customer numbers validate it as true
        //validate prod, serial, order # exist and unclaimed
        String[] cat = {"A","B","C","D","E","F"};
        for(String c : cat) {

            String prod = request.getParameter("spiff_product_"+c);
            String serial = request.getParameter("spiff_serial_"+c);
            String currentSpiffMsg = "spiff_message_" + c;


            if(prod != null) {


                String[] res = new String[2];
                res = spiffService.validateProduct(userSession.getCono(),userSession.getCustomerNumber(),prod,serial);
                String claimNbrOut  = (String) res[0];
                String orderNbrOut  = (String) res[1];


                //The next block is for invalid serial numbers
                if ((claimNbrOut == null) && (orderNbrOut == null)){

                    data.put(currentSpiffMsg, "Unknown Model/Serial Combination");
                    valid = false;
                    count++;
                }
                else if ((claimNbrOut != null) && (orderNbrOut  != null)){

                    data.put(currentSpiffMsg,"Serial # already claimed on " + claimNbrOut);
                    valid = false;
                    count++;
                }

                //According to business process, if the order number exists, after
                //the product is validate, then return the invoice number
                //Make the claim valid by setting the flag to true. Process the claim.
                //Further reasearch on how the DB is queried shows that new claims won't have
                //a claim number (duh) but will have a valid order/invoice number.
                else if ((orderNbrOut != null) && (claimNbrOut == null)) {

                    data.put(currentSpiffMsg,"Invoice # = " + orderNbrOut);
                    //valid = false;

                    //valid is already set to true by default. This is just an
                    //assurance that it is really set to true for this condition.
                    valid = true;

                }
                else{

                     data.put(currentSpiffMsg,"Unknown Error.");
                     valid = false;
                     count++;
                }
                //End of block for invalid serial number
            }
        }


        //if valid = true; create claim entry using data var
        if(count == 0) { //no errors
            boolean success = addClaim(YEAR, contactId, data, claimTotal);
            if(success == false) {
                return new ModelAndView("/spiffs/add", model);
            }

            model.put("status", "success");
            model.put("message", "Spiff Claim Created Successfully!");
            model.put("button_continue", "/spiffs/index.htm");
            return new ModelAndView("/message", model);
        }

        //there was an error somewhere, return to add screen with data
        return new ModelAndView("/spiffs/add", model);
    }

    @RequestMapping(value = "/spiffs/detail", method = RequestMethod.GET)
    public ModelAndView getClaims(@RequestParam(value = "id", required = true) String id){


        SpiffDataFacade data = new SpiffDataFacade();
        SpiffClaimDetails details = new SpiffClaimDetails();
        ModelAndView claimModel= new ModelAndView("spiffs/detail");

        try{

           details = data.getSpiffClaimDetails(id);
           claimModel.addObject("claim",details);


           /** If someone ever looks at this snippet of code, please forgive the way I did this. I don't like violating the MVC like this.
            *  The model is contains a list of objects. It was supposed to be able to use a JSTL forEach loop
            *  to get the contents of the list. It is not working. Looked at documentation and plenty of examples -- still wont work
            *  This is a kludgy solution to the problem until I have time to figure things out.
            *
            **/

            List<ClaimItems> items = details.getClaimItems();
            StringBuilder sb = new StringBuilder();


            if (items.size() > 0){

                ;
                sb.append("<br\\><br\\><table class=\"claim-details-tab\" width=\"100%\" border=\"0\" cellPadding = \"0\" cellSpacing=\"0\" style> \n");
                sb.append("<th style = \"text-align:center; border-top-left-radius: 5px; background-color:#eaeaea; font-wieght: bold; position:relative; top: 50%; transform: translateY(-50%); color:#555\">Classification</th><th style = \"text-align:center;background-color:#eaeaea; font-wieght: bold; position:relative; top: 50%; transform: translateY(-50%);color:#555\">Product</th><th style = \"text-align:center; border-top-right-radius: 5px;background-color:#eaeaea; font-wieght: bold;position:relative; top: 50%; transform: translateY(-50%);color:#555\">Serial Number</th> \n");

                for(ClaimItems item : items) {
                    String s = String.format("<tr><td class=\"claim-id-cell\" style=\"border-left: solid 1px #eaeaea; \">%s</td><td class=\"claim-id-cell\">%s</td><td class=\"claim-id-cell\" style=\"border-right: solid 1px #eaeaea; \">%s</td></tr> \n",
                            item.getClassification(),
                           // item.getProductGroup(),
                            item.getProduct(),
                            item.getSerialNumber());
                    sb.append(s);
                }
                sb.append("</table> \n");

            }

            logger.info("table = " + sb.toString());
            claimModel.addObject("details", sb.toString());



        }
        catch(Exception ex){

            logger.error("Error occured in the getClaims() method ..." , ex);

        }

        return claimModel;


    }

    /**
     *
     */
    private boolean addClaim(int year, String contactId, Map<String, String> data, BigDecimal claimTotal)
    {
        SpiffClaim claim = new SpiffClaim();
        claim.setContactId(contactId);
        claim.setYear(year);
        claim.setClaimNumber(spiffService.getNextClaimNo());
        claim.setCustomerName(data.get("fullName"));
        claim.setAddrStreet(data.get("addrStreet"));
        claim.setAddrCity(data.get("addrCity"));
        claim.setAddrState(data.get("addrState"));
        claim.setAddrZip(data.get("addrZip"));

        claim.setEnteredDate(new Date());

        try {
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            claim.setInstallDate(df.parse(data.get("instDate")));
        }catch(Exception ignore) {
            data.put("instDate_message", "Missing or invalid Installation date");
            return false;
        }

        claim.setAmount(claimTotal.doubleValue());

        String[] cat = {"A","B","C","D","E","F"};
        for(String c : cat) {
            String prod = data.get("spiff_product_"+c);
            if(prod != null) {
                SpiffClaimItem ci = new SpiffClaimItem();
                ci.setClaim(claim);
                ci.setClassification(c);
                ci.setProductGroup( data.get("spiff_category_"+c) );
                ci.setProduct(prod);
                ci.setSerialNumber( data.get("spiff_serial_"+c) );
                ci.setOrderNo( data.get("spiff_order_"+c) );

                claim.addItem(ci);
            }
        }

        spiffService.save(claim);

        return true;
    }

    /**
     *
     */
    private Map<String, Object> buildCategories(String type)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //create categories and pre-load
        Map<String, List<SpiffGroup>> categories = new HashMap<String, List<SpiffGroup>>();
        categories.put("A", new ArrayList<SpiffGroup>());
        categories.put("B", new ArrayList<SpiffGroup>());
        categories.put("C", new ArrayList<SpiffGroup>());
        categories.put("D", new ArrayList<SpiffGroup>());
        categories.put("E", new ArrayList<SpiffGroup>());
        categories.put("F", new ArrayList<SpiffGroup>());

        List<SpiffGroup> groups = spiffService.findGroups(type);

        StringBuilder json = new StringBuilder();

        for(SpiffGroup group : groups) {
            json.append("\"").append(group.getProdGroup()).append("\" : [");
            for(String prod : group.getProducts()) {
                json.append("\"").append(prod).append("\",");
            }
            json.deleteCharAt( json.length() - 1);
            json.append("],");

            //add group to category
            try { categories.get( group.getClassification() ).add( group ); }catch(Exception ignore) { }
        }
        json.deleteCharAt( json.length() - 1);

        model.put("jsonData", "{ " + json.toString() + " }");
        model.put("categories", categories);

        return model;
    }

    /**
     *
     */
    private BigDecimal validateCarrier(HttpServletRequest request)
    {
        BigDecimal amount = null;

        //first check to make sure the groups make up a valid system
        String cat_a = request.getParameter("spiff_category_A");
        String cat_b = request.getParameter("spiff_category_B");
        String cat_c = request.getParameter("spiff_category_C");
        String cat_d = request.getParameter("spiff_category_D");

        //outdoor, indoor, controller
        if(cat_a != null && !cat_a.equals(""))
        {
            //greenspeed
            if(cat_a.equalsIgnoreCase("25VNA0") == true) {
                if(cat_b.equalsIgnoreCase("58CVA") == true || cat_b.equalsIgnoreCase("59TN6") == true || cat_b.equalsIgnoreCase("59MN7") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXCC") == true)
                        amount = new BigDecimal(120.00);
                }
            }
            //Infinity VS
            if(cat_a.equalsIgnoreCase("25VNA8") == true || cat_a.equalsIgnoreCase("24VNA9") == true) {
                if(cat_b.equalsIgnoreCase("58CVA") == true || cat_b.equalsIgnoreCase("59TN6") == true || cat_b.equalsIgnoreCase("59MN7") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXCC") == true)
                        amount = new BigDecimal(100.00);
                }
            }
            //Infinity 2-speed
            if(cat_a.equalsIgnoreCase("25HNB6") == true || cat_a.equalsIgnoreCase("24ANB7") == true) {
                if(cat_b.equalsIgnoreCase("58CVA") == true || cat_b.equalsIgnoreCase("59TN6") == true || cat_b.equalsIgnoreCase("59MN7") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXCC") == true)
                        amount = new BigDecimal(85.00);
                }
            }
            //Infinity 1-speed
            if(cat_a.equalsIgnoreCase("25HNB5") == true || cat_a.equalsIgnoreCase("24ANB6") == true) {
                if(cat_b.equalsIgnoreCase("58CVA") == true || cat_b.equalsIgnoreCase("59TN6") == true || cat_b.equalsIgnoreCase("59MN7") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXCC") == true)
                        amount = new BigDecimal(75.00);
                }
            }
            //Infinity out / Performance in
            if(cat_a.equalsIgnoreCase("25HNB5") == true || cat_a.equalsIgnoreCase("25HNB6") == true || cat_a.equalsIgnoreCase("24ANB6") == true || cat_a.equalsIgnoreCase("24ANB7") == true) {
                if(cat_b.equalsIgnoreCase("FV4") == true || cat_b.equalsIgnoreCase("58PHA") == true || cat_b.equalsIgnoreCase("58CVA") == true || cat_b.equalsIgnoreCase("59TP") == true) {
                    if(cat_d.equalsIgnoreCase("TP-WEM01") == true)
                        amount = new BigDecimal(65.00);
                }
            }

        //package units
        } else if(cat_c != null && !cat_c.equals("")) {
            if(cat_c.equalsIgnoreCase("48VG") == true || cat_c.equalsIgnoreCase("50VR") == true) {
                if(cat_d.equalsIgnoreCase("TP-WEM01") == true)
                    amount = new BigDecimal(60.00);
            }
            if(cat_c.equalsIgnoreCase("GC") == true || cat_c.equalsIgnoreCase("GZ") == true) {
                    amount = new BigDecimal(75.00);
            }
        }

        return amount;
    }

    /**
     *
     */
    private BigDecimal validateBryant(HttpServletRequest request)
    {
        BigDecimal amount = null;

        //first check to make sure the groups make up a valid system
        String cat_a = request.getParameter("spiff_category_A");
        String cat_b = request.getParameter("spiff_category_B");
        String cat_c = request.getParameter("spiff_category_C");
        String cat_d = request.getParameter("spiff_category_D");

        //outdoor, indoor, controller
        if(cat_a != null && !cat_a.equals(""))
        {
            //evolution extreme
            if(cat_a.equalsIgnoreCase("280A") == true) {
                if(cat_b.equalsIgnoreCase("315A") == true || cat_b.equalsIgnoreCase("987M") == true || cat_b.equalsIgnoreCase("986T") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXBB") == true)
                        amount = new BigDecimal(120.00);
                }
            //evolution vs
            } else if(cat_a.equalsIgnoreCase("288BNV") == true || cat_a.equalsIgnoreCase("189BNV") == true) {
                if(cat_b.equalsIgnoreCase("315A") == true || cat_b.equalsIgnoreCase("987M") == true || cat_b.equalsIgnoreCase("986T") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXBB") == true)
                        amount = new BigDecimal(100.00);
                }
            //evolution
            } else if(cat_a.equalsIgnoreCase("286B") == true || cat_a.equalsIgnoreCase("187B") == true) {
                //2-speed
                if(cat_b.equalsIgnoreCase("315A") == true || cat_b.equalsIgnoreCase("987M") == true || cat_b.equalsIgnoreCase("986T") == true || cat_b.equalsIgnoreCase("FE4A") == true) {
                    if(cat_d.equalsIgnoreCase("SYSTXBB") == true)
                        amount = new BigDecimal(85.00);
                }
                //with performance in
                if(cat_b.equalsIgnoreCase("313A") == true || cat_b.equalsIgnoreCase("315A") == true || cat_b.equalsIgnoreCase("925S") == true || cat_b.equalsIgnoreCase("FV4C") == true) {
                    if(cat_d.equalsIgnoreCase("T6-WEM01") == true)
                        amount = new BigDecimal(75.00);
                }
            //prefered 2-speed
            } else if(cat_a.equalsIgnoreCase("226A") == true || cat_a.equalsIgnoreCase("127A") == true) {
                if(cat_b.equalsIgnoreCase("313A") == true || cat_b.equalsIgnoreCase("315A") == true || cat_b.equalsIgnoreCase("925S") == true || cat_b.equalsIgnoreCase("FV4C") == true) {
                    if(cat_d.equalsIgnoreCase("T6-WEM01") == true)
                        amount = new BigDecimal(65.00);
                }
            }

        //package unit
        } else if(cat_c != null && !cat_c.equals("")) {
            if( cat_c.equalsIgnoreCase("577E") == true ||
                    cat_c.equalsIgnoreCase("607C") == true ||
                    cat_c.equalsIgnoreCase("607E") == true)   //added by CEB on 5/21/2015
            {
                if(cat_d.equalsIgnoreCase("T6-WEM01") == true)
                    amount = new BigDecimal(60.00);
            }
        }

        return amount;
    }

}