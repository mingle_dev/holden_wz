/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.Utilities.Emailer;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.JobApplication;
import com.kawauso.base.bean.Position;
import com.kawauso.base.bean.Session.Role;
import com.kawauso.base.form.RegisterForm;
import com.kawauso.base.service.CareerService;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CareerController
{
    @Autowired
    private CareerService careerService;
    
    private static final Logger log = Logger.getLogger( CareerController.class );
    
    @RequestMapping(value = "/career/index", method = RequestMethod.GET)
    public ModelAndView index(ModelMap model, HttpSession session)
    {
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Career Opportunities", "", ""));
        model.put("navTree", crumbs);   

        return new ModelAndView("/career/index", model);
    }
    
    @RequestMapping(value = "/career/detail", method = RequestMethod.GET)
    public ModelAndView detail(@RequestParam(value = "id", required = true) String id, 
                               ModelMap model, 
                               HttpSession session)
    {
        Position p = null;
        try {
            p = careerService.find(id);
            model.put("position", p);
        }catch(Exception ex) {
            //@todo; log error
            System.out.println(ex);
        }

        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Career Opportunities", "/career/index", ""));
        crumbs.add(new Breadcrumb(p.getName(), "", ""));
        model.put("navTree", crumbs);          
        
        return new ModelAndView("/career/detail", model);
    }    
    
    @RequestMapping(value = "/career/add", method = RequestMethod.GET)
    public ModelAndView add()
    {   
        Map<String, Object> model = new HashMap<String, Object>();

        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Human Resources", "/user/login", ""));
        crumbs.add(new Breadcrumb("Career Opportunities", "", ""));
        model.put("navTree", crumbs);
        
        model.put("positionForm", new Position());
        
        return new ModelAndView("/career/add", model);        
    }
    
    @RequestMapping(value = "/career/add", method = RequestMethod.POST)
    public String add(@ModelAttribute("PositionForm") Position form, BindingResult result, ModelMap model, HttpSession session)
    {
        careerService.add(form);
        
        model.put("availPositions", careerService.list());
        
        //@todo; redirect to index
        return "/career/index";
    }
    
    @RequestMapping(value = "/career/apply", method = RequestMethod.GET)
    public ModelAndView apply(@RequestParam(value = "id", required = true) String id, HttpSession session)
    {   
        //clean up anything
        session.removeAttribute("jobApplication");
        
        Map<String, Object> model = new HashMap<String, Object>();

        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Career Opportunities", "/career/index", ""));
        crumbs.add(new Breadcrumb("Online Application", "", ""));
        model.put("navTree", crumbs);
        
        //fetch job info
        Position p = careerService.find(id);
        model.put("position", p);
        
        //setup application form
        model.put("applicationForm", new JobApplication(id));
        
        return new ModelAndView("/career/applyTerms", model);        
    }
    
    @RequestMapping(value = "/career/apply", method = RequestMethod.POST)
    public String apply(@ModelAttribute("applicationForm") JobApplication form, BindingResult result, ModelMap model, HttpSession session)
    {
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Career Opportunities", "/career/index", ""));
        crumbs.add(new Breadcrumb("Online Application", "", ""));
        model.put("navTree", crumbs);        
        
        //validate field data
        Errors errors = new BeanPropertyBindingResult(form, "applicationForm");    
        
        //fetch app from session and merge form data with it
        JobApplication app = (JobApplication) session.getAttribute("jobApplication");
        if(app == null)
            app = form;
 
        try {
            Method[] methods = app.getClass().getDeclaredMethods();
            for(Method method : methods) {
                
                String methodName = method.getName();
                
                if(methodName.startsWith("get"))
                {
                    Method formMethod = form.getClass().getMethod( methodName );
                    Method appMethod = null;
                    
                    if(method.getReturnType().getSimpleName().equals("String"))
                        appMethod = app.getClass().getMethod( methodName.replaceFirst("get", "set"), String.class);
                    else if(method.getReturnType().getSimpleName().equals("int"))
                        appMethod = app.getClass().getMethod( methodName.replaceFirst("get", "set"), int.class);
                    else if(method.getReturnType().getSimpleName().equals("boolean"))
                        appMethod = app.getClass().getMethod( methodName.replaceFirst("get", "set"), boolean.class);
                    else if(method.getReturnType().getSimpleName().equals("Date"))
                        appMethod = app.getClass().getMethod( methodName.replaceFirst("get", "set"), Date.class);
                    else if(method.getReturnType().getSimpleName().equals("List"))
                        appMethod = app.getClass().getMethod( methodName.replaceFirst("get", "set"), List.class);
                    
                    if(appMethod != null) {
                        Object formObj = formMethod.invoke(form);
                        if(formObj != null)
                            appMethod.invoke(app, formObj);
                    }
                }
            }
            
        }catch(Exception ex) {
            System.out.println("damn: " + ex);
        }

        //update saved session application data and save back to model
        session.setAttribute("jobApplication", app);
        model.put("application", app);
        
        //fetch job info
        Position p = careerService.find(app.getJobId());
        model.put("position", p);        
        
        //load correct page
        if(form.getStep() == 1)
            return "/career/applyPersonal";
        
        else if(form.getStep() == 2) {
            //validate required fields; first letter is uppercase to match "get" methods
            String[] reqFields = { "firstName", "lastName", "streetAddr", "city", "state", "zip", "homePhone" };
            
            for(int i=0; i<reqFields.length; i++)
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, reqFields[i], "field.required"); 
            
            if(errors.getErrorCount() > 0) {
                model.put("formError", true);
                result.addAllErrors(errors);
                return "/career/applyPersonal";
            }
            
            return "/career/applyQuestions";
            
        }else if(form.getStep() == 3)
            return "/career/applyEducation";       
        else if(form.getStep() == 4)
            return "/career/applyExperience";
        else if(form.getStep() == 5)
            return "/career/applyResume";              
        else if(form.getStep() == 6)
            return "/career/applyReview";   
        else if(form.getStep() == 7) {

            if(form.isCertifyFacts() == false)
                errors.rejectValue("certifyFacts", "field.checkbox.agree");

            if(errors.getErrorCount() > 0) {
                model.put("formError", true);
                result.addAllErrors(errors);
                return "/career/applyReview";
            }

            String appId = careerService.addApplicant(app);
            
            if(!app.getEmailAddr().isEmpty()) {
                try {
                    //send email to applicant (if emailaddr provided)
                    Emailer appEmail = new Emailer(ConfigReader.getProperty("email.smtpserver"));
                    int smtpPort = Integer.valueOf(ConfigReader.getProperty("email.smtpPort"));
                    appEmail.setSmtpPort(smtpPort);
                    appEmail.setFromAddress("noreply@mingledorffs.com");
                    appEmail.setFromName("Mingledorff's Careers");
                    appEmail.addToAddress(app.getEmailAddr());
                    appEmail.setSubject("Thank you for your interest");
                    appEmail.setMessage("We appreciate your interest in Mingledorff’s, Inc. and acknowledge receipt of your resume. In the event that we wish to arrange a formal interview, we will contact you. Again, thank you for your interest in employment at Mingledorff’s, Inc.  This is an automated response. Please do not reply to this email.");
                    appEmail.sendMessage();
                }catch(Exception ex) {
                    //add error to view
                    log.error(ex);
                }
            }
            
            //send application link to HR
            try {
                //send email to applicant (if emailaddr provided)
                Emailer appEmail = new Emailer(ConfigReader.getProperty("email.smtpserver"));
                int smtpPort = Integer.valueOf(ConfigReader.getProperty("email.smtpPort"));
                appEmail.setSmtpPort(smtpPort);
                appEmail.setFromAddress("noreply@mingledorffs.com");
                appEmail.setFromName("Mingledorff's Portal");
                appEmail.addToAddress("jpowell@mingledorffs.com");
                appEmail.setSubject("Job Application Submitted Online");
                appEmail.setMessage("http://portal.mingledorffs.com/web/career/view-app.htm?id=" + appId); //@todo; fix url to be dynamic
                appEmail.sendMessage();
            }catch(Exception ex) {
                //add error to view
                log.error(ex);
            }
            
            //send application link to me @todo; remove this once it works
            try {
                //send email to applicant (if emailaddr provided)
                Emailer appEmail = new Emailer(ConfigReader.getProperty("email.smtpserver"));
                int smtpPort = Integer.valueOf(ConfigReader.getProperty("email.smtpPort"));
                appEmail.setSmtpPort(smtpPort);
                appEmail.setFromAddress("noreply@mingledorffs.com");
                appEmail.setFromName("Mingledorff's Portal");
                appEmail.addToAddress("twall@mingledorffs.com");
                appEmail.setSubject("Job Application Submitted Online");
                appEmail.setMessage("http://portal.mingledorffs.com/web/career/view-app.htm?id=" + appId); //@todo; fix url to be dynamic
                appEmail.sendMessage();
            }catch(Exception ex) {
                //add error to view
                log.error(ex);
            }            
            
            return "/career/applyConfirm";       
        }else
            return "/career/applyTerms";
    }
    
    @RequestMapping(value = "/career/view-app", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true, allowedRole = { Role.EMPLOYEE, Role.ADMINISTRATOR })
    public ModelAndView viewApplication(@RequestParam(value = "id", required = true) String id)
    {
        ModelMap model = new ModelMap();
        
        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Career Opportunities", "", ""));
        model.put("navTree", crumbs);   

        JobApplication app = careerService.getApplicant(id);
        model.put("application", app);
        model.put("position", careerService.find(app.getJobId()));

        return new ModelAndView("/career/appViewer", model);
    }    
}