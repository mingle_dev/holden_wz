//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.kawauso.base.controller;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.Document;
import com.kawauso.base.bean.ServiceBulletin;
import com.kawauso.base.bean.TrainingEvent;
import com.kawauso.base.service.TechServicesService;
import com.kawauso.jdbc.ServiceBulletinFacade;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TechServiceController {

    private final Logger logger = LoggerFactory.getLogger(TechServiceController.class);

    @Autowired
    TechServicesService tsService;

    Map<String, String> dsb_write_access = new HashMap<String, String>();

    public TechServiceController() {
    }

    @RequestMapping({"/techserv/bulletins"})
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView bulletins(@RequestParam(value = "username", required = false) String username,
                                  @RequestParam(value = "year", required = false) String year,
                                  @RequestParam(value = "query", required = false) String query) {

        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Resources", "/resources", ""));
        crumbs.add(new Breadcrumb("Service Bulletins", "", ""));
        model.put("navTree", crumbs);

        List years = this.tsService.findServiceBulletinYears();
        model.put("smbYears", years);

       if(query != null && !query.isEmpty()) {
            List sbYear1 = this.tsService.findServiceBulletins(query);
            model.put("documents", sbYear1);
        } else {
            int sbYear = 2016;

            try {
                if (year != null && !year.equals("")) {
                    sbYear = Integer.parseInt(year);
                }else{
                    Integer obj = (Integer)years.get(years.size() - 1);
                    sbYear = obj.intValue();
                }
            } catch (Exception var7) {
                ;
            }

            List docList = this.tsService.findServiceBulletins(sbYear);
            model.put("documents", docList);
        }

        //fetch user write access
        String dsb_user = null;

        if (dsb_write_access == null || dsb_write_access.isEmpty())
        {
            //get users from properties file
            String filename = ConfigReader.getProperty("dsb.access_list_filename");
            if (filename == null || filename.isEmpty()){
                filename = "acl.properties";
            }
            String result = "";
            java.util.Properties properties = new java.util.Properties();

            try {
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
                if (inputStream != null){
                    properties.load(inputStream);
                }
                //get the users from the property
                String prop = (String)properties.get("dsb.write_access.users");
                java.util.StringTokenizer st = new java.util.StringTokenizer(prop, ";");

                //store in hash map
                while (st.hasMoreElements()){
                    String user = st.nextToken();
                    dsb_write_access.put(user, user);
                }

            }catch (java.io.FileNotFoundException ex) {

                ex.printStackTrace();

            } catch (java.io.IOException io){

                io.printStackTrace();
            }
        }

        if ( username != null && !username.isEmpty()) {

            String decodedUser = com.kawauso.base.Utilities.ELUtilities.base64Decode(username);
            dsb_user = dsb_write_access.get(decodedUser);

            if (dsb_user != null) {
                model.put("dsbwriteaccess", true);
            }

        } else {

            model.put("dsbwriteaccess", false);

        }

        return new ModelAndView("/techserv/bulletins", model);
    }

    @RequestMapping(value = {"/techserv/bulletin-download"}, method = {RequestMethod.GET})
    @SecurityAnnotation(requireLogin = true)
    public ResponseEntity<byte[]> bulletinDownload(@RequestParam(value = "id", required = true) String id) {
        if(id != null && id.length() != 0) {
            try {
                ServiceBulletin bulletin = this.tsService.findServiceBulletin(id);
                byte[] buffer = bulletin.getFileData().getBytes(1L, (int)bulletin.getFileData().length());
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.parseMediaType("application/pdf"));
                String filename = bulletin.getSmbId() + ".pdf";
                headers.setContentDispositionFormData(filename, filename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                ResponseEntity response = new ResponseEntity(buffer, headers, HttpStatus.OK);
                return response;
            } catch (Exception var7) {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/techserv/library"}, method = {RequestMethod.GET})
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView library(@RequestParam(
            value = "query",
            required = false
    ) String query) {
        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Resources", "/resources", ""));
        crumbs.add(new Breadcrumb("Document Library", "", ""));
        model.put("navTree", crumbs);
        List docList = this.tsService.findLibraryDocuments(query);
        model.put("documents", docList);
        return new ModelAndView("/techserv/library", model);
    }

    @RequestMapping(value = {"/techserv/library-download"}, method = {RequestMethod.GET})
    @SecurityAnnotation(requireLogin = true)
    public ResponseEntity<byte[]> download(@RequestParam(value = "id", required = true) String id) {
        if(id != null && id.length() != 0) {
            this.tsService.updateLibraryDocmentCounter(id);
            Document document = this.tsService.findLibraryDocument(id);

            try {
                JSch e = new JSch();
                Session session = e.getSession("root", "192.168.10.33", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("narWhal_669");
                session.connect();
                Channel channel = session.openChannel("sftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp)channel;
                InputStream inStream = sftpChannel.get(document.getFilePath() + document.getId());
                ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
                byte[] data = new byte[16384];

                int nRead;
                while((nRead = inStream.read(data, 0, data.length)) != -1) {
                    byteArray.write(data, 0, nRead);
                }

                sftpChannel.exit();
                session.disconnect();
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.parseMediaType("application/pdf"));
                String filename = document.getFileName();
                headers.setContentDispositionFormData(filename, filename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                ResponseEntity response = new ResponseEntity(byteArray.toByteArray(), headers, HttpStatus.OK);
                return response;
            } catch (Exception var14) {
                System.out.println(var14);
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/techserv/manage"}, method = {RequestMethod.GET})
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView manageLibrary(@RequestParam(value = "query", required = false) String query,
                                      @RequestParam(value = "year", required = false) String year,
                                      @RequestParam(value = "username", required = false) String username) {

        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Service Bulletins", "/techserv/bulletins", ""));
        crumbs.add(new Breadcrumb("Manage Service Bulletins", "", ""));
        model.put("navTree", crumbs);
        List years = this.tsService.findServiceBulletinYears();
        model.put("smbYears", years);

        if(query != null && !query.isEmpty()) {
            List sbYear1 = this.tsService.findServiceBulletins(query);
            model.put("documents", sbYear1);
        } else {
            int sbYear = 2016;

            try {
                if (year != null && !year.equals("")) {
                    sbYear = Integer.parseInt(year);
                }else{
                    Integer obj = (Integer)years.get(years.size() - 1);
                    sbYear = obj.intValue();
                }
            } catch (Exception var8) {
                ;
            }
            List docList = this.tsService.findServiceBulletins(sbYear);
            model.put("documents", docList);
        }

        return new ModelAndView("/techserv/manage", model);
    }

    @RequestMapping(value = {"/techserv/manage"}, method = {RequestMethod.POST})
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView manageLibrary(@RequestParam(value = "query", required = false) String query,
                                      @RequestParam(value = "year", required = false) String year) {

        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Service Bulletins", "/techserv/bulletins", ""));
        crumbs.add(new Breadcrumb("Manage Service Bulletins", "", ""));
        model.put("navTree", crumbs);
        List years = this.tsService.findServiceBulletinYears();
        model.put("smbYears", years);

        if(query != null && !query.isEmpty()) {
            List sbYear1 = this.tsService.findServiceBulletins(query);
            model.put("documents", sbYear1);
        } else {
            int sbYear = 2016;

            try {
                if (year != null && !year.equals("")) {
                    sbYear = Integer.parseInt(year);
                }else{
                    Integer obj = (Integer)years.get(years.size() - 1);
                    sbYear = obj.intValue();
                }
            } catch (Exception var8) {
                ;
            }
            List docList = this.tsService.findServiceBulletins(sbYear);
            model.put("documents", docList);
        }

        return new ModelAndView("/techserv/manage", model);
    }

    @RequestMapping(value = {"/techserv/addbulletin"}, method = {RequestMethod.GET})
    public ModelAndView addBulletin() {

        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Manage Service Bulletins", "/techserv/manage", ""));
        crumbs.add(new Breadcrumb("Add Service Bulletin", "", ""));
        model.put("navTree", crumbs);
        return new ModelAndView("/techserv/addbulletin", model);

    }

    @RequestMapping(
            value = {"/techserv/addbulletin"},
            method = {RequestMethod.POST}
    )
    public ModelAndView addBulletin(@RequestParam(value = "smbid", required = false)
                                        String smbId, @RequestParam(value = "subject", required = false)
                                        String description, @RequestParam(value = "year", required = false)
                                        String year, @RequestParam(value = "keywords", required = false)
                                        String keywords, @RequestParam(value = "uploadedfile", required = false) CommonsMultipartFile uploadedFile)
    {
        Boolean itWorked = Boolean.valueOf(false);
        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Manage Service Bulletins", "/techserv/manage", ""));
        crumbs.add(new Breadcrumb("Add A Service Bulletin", "", ""));
        model.put("navTree", crumbs);
        ServiceBulletinFacade facade = new ServiceBulletinFacade();
        Object bytes = null;
        Integer message;
        if(uploadedFile != null && uploadedFile.getSize() > 0L) {
            this.logger.info("Uploading service bulletin id " + smbId);
            this.logger.info("Description: " + description);
            this.logger.info("Year : " + year);
            this.logger.info("Keywords : " + keywords);

            try {
                Long bytesLoaded = Long.valueOf(uploadedFile.getSize());
                message = Integer.valueOf(bytesLoaded.intValue());
                itWorked = facade.insert("1", smbId, description, Integer.valueOf(year), keywords, message, uploadedFile);
            } catch (Exception var14) {
                var14.printStackTrace();
            }
        }

        String bytesLoaded1 = "Service bulletin " + smbId + " has been successfully uploaded. Total bytes loaded: " + uploadedFile.getSize();
        model.put("bytes_uploaded", bytesLoaded1);
        message = null;
        byte status = 0;
        String message1;
        if(itWorked.booleanValue()) {
            message1 = bytesLoaded1;
            status = 1;
        } else {
            message1 = "Service bulletin upload " + smbId + " failed.  Please review service bulletin information entered and try again.";
        }

        model.put("message", message1);
        model.put("status", Integer.valueOf(status));
        return new ModelAndView("/techserv/addbulletin", model);
    }

    @RequestMapping(value = {"/techserv/deletebulletin"}, method = {RequestMethod.GET})
    public ModelAndView deleteBulletin(@RequestParam(value = "query", required = false) String query,
                                       @RequestParam(value = "year", required = false) String year,
                                       @RequestParam(value = "userid", required = false) String userId,
                                       @RequestParam("id") String id,
                                       @RequestParam("smbid") String smbid) {
        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Service Bulletins", "/techserv/bulletins", ""));
        crumbs.add(new Breadcrumb("Manage Service Bulletins", "", ""));
        model.put("navTree", crumbs);
        Boolean itWorked = Boolean.valueOf(false);
        ServiceBulletinFacade facade = new ServiceBulletinFacade();
        String message = null;
        byte status = 0;

        try {
            itWorked = facade.delete(id);
            if(itWorked.booleanValue()) {
                message = "Successfully deleted service bulletin " + smbid;
                status = 1;
            } else {
                message = "Failed to delete service bulletin " + smbid;
            }

            model.put("status", Integer.valueOf(status));
            model.put("message", message);
        } catch (Exception var15) {
            var15.printStackTrace();
        }

        model.put("smbYears", this.tsService.findServiceBulletinYears());
        if(query != null && !query.isEmpty()) {
            List sbYear1 = this.tsService.findServiceBulletins(query);
            model.put("documents", sbYear1);
        } else {
            int sbYear = 2016;

            try {
                sbYear = Integer.parseInt(year);
            } catch (Exception var14) {
                ;
            }

            List docList = this.tsService.findServiceBulletins(sbYear);
            model.put("documents", docList);
        }

        return new ModelAndView("/techserv/manage", model);
    }

    @RequestMapping(value = {"/techserv/training"}, method = {RequestMethod.GET})
    public ModelAndView training() {
        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Resources", "/resources", ""));
        crumbs.add(new Breadcrumb("Training", "", ""));
        model.put("navTree", crumbs);

        try {
            model.put("courses", this.tsService.findTrainingUpcomingCourses());
        } catch (Exception var4) {
            System.out.println(var4);
        }

        return new ModelAndView("/techserv/training", model);
    }

    @RequestMapping(value = {"/techserv/training/course"}, method = {RequestMethod.GET})
    public ModelAndView trainingCourse(@RequestParam(value = "id", required = true) String id) {
        HashMap model = new HashMap();
        ArrayList crumbs = new ArrayList();
        crumbs.add(new Breadcrumb("Resources", "/resources", ""));
        crumbs.add(new Breadcrumb("Training", "/techserv/training", ""));
        crumbs.add(new Breadcrumb("Course Information", "", ""));
        model.put("navTree", crumbs);

        try {
            model.put("course", this.tsService.findTrainingCourse(id));
        } catch (Exception var5) {
            ;
        }

        return new ModelAndView("/techserv/training_course", model);
    }

    @RequestMapping(value = {"/techserv/training/event"}, method = {RequestMethod.GET})
    public ModelAndView trainingEvent(@RequestParam(value = "id", required = true) String id) {
        HashMap model = new HashMap();
        String courseId = "";

        try {
            TrainingEvent crumbs = this.tsService.findTrainingEvent(id);
            courseId = crumbs.getCourse().getId();
            model.put("event", crumbs);
        } catch (Exception var5) {
            ;
        }

        ArrayList crumbs1 = new ArrayList();
        crumbs1.add(new Breadcrumb("Resources", "/resources", ""));
        crumbs1.add(new Breadcrumb("Training", "/techserv/training", ""));
        crumbs1.add(new Breadcrumb("Course Information", "/techserv/training/course", "id=" + courseId));
        crumbs1.add(new Breadcrumb("Event Details", "", ""));
        model.put("navTree", crumbs1);
        return new ModelAndView("/techserv/training_event", model);
    }
}
