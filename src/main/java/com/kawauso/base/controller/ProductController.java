/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.google.gson.Gson;
import com.kawauso.base.ConfigReader;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.Utilities.ELUtilities;
import com.kawauso.base.bean.*;
import com.kawauso.base.bean.Session.Role;
import com.kawauso.base.service.AccountService;
import com.kawauso.base.service.MenuService;
import com.kawauso.base.service.ProductService;
import com.kawauso.ext.epic.EPICPortType;
import com.kawauso.ext.epic.EPICService;
import com.kawauso.ext.epic.GetModelDataReply;
import com.kawauso.ext.epic.ListModelsReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.Holder;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chris.weaver
 */
@Controller
public class ProductController
{
    @Autowired
    private MenuService menuService;
    
    @Autowired
    private ProductService productService;

    @Autowired
    private AccountService accountService;

    private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    @RequestMapping("/product/index")
    public ModelAndView index()
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //default breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Product Search","",""));
        model.put("navTree", crumbs);

        return new ModelAndView("/product/index", model);
    }  
    
    @RequestMapping(value = "/product/search", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam(value = "term", required = false) String term,
                               @RequestParam(value = "id", required = false) String id,
                               @RequestParam(value = "attr", required = false) String[] attr,
                               @RequestParam(value = "sort", required = false) String sort, 
                               @RequestParam(value = "page", required = false) Integer page, 
                               @RequestParam(value = "limit", required = false) Integer limit,
                               @RequestParam(value = "searchtyp", required = false) String searchtyp,
                               HttpServletRequest request, HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        BigDecimal custno = null;
        String shipto = "";
        String whse = null;

        if(userSession != null) {
            custno = (userSession.getCustomerNumber() != null) ? userSession.getCustomerNumber() : null;
            whse = (userSession.getWarehouse() != null) ? userSession.getWarehouse() : null;
            shipto = (userSession.getShipTo() != null) ? userSession.getShipTo() : "x";
        }

        Map<String, Object> model = new HashMap<String, Object>();
        
        Integer menuId = null;
        try { menuId = Integer.parseInt(id); }catch(Exception ignore) {}

        long startTm = System.currentTimeMillis();

        //default breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Product Search","",""));
        model.put("navTree", crumbs);         
        
        if(menuId != null) {
            model.put("menuId", id);

            //build breadcrumbs
            crumbs = menuService.getNavTree(menuId);
            for(Breadcrumb crumb : crumbs)
                crumb.setUrl("/product/search");
            model.put("navTree", crumbs);

            crumbs.add(0, new Breadcrumb("Product Browse", "/product/index.htm", ""));

            //fetch category items
            List<MenuCategory> categories = menuService.getMenu(menuId);
            if(categories.size() > 0) {
                model.put("categories", categories);
                return new ModelAndView("/product/category", model);
            }
        }

        if ( searchtyp != null && searchtyp.equals("searchsku")){

            //perform sku# search
            model = _searchSku(custno, shipto, whse, term, menuId, attr, sort, page, limit, model, userSession, searchtyp);

       }else {
            //return list view
            //@todo; this is temporary; will merge once completed
            model = _search(custno, shipto, whse, term, menuId, attr, sort, page, limit, model, userSession);
        }
        //advanced layout to show margin/claim data to employees & admins
        if(request.getParameter("layout") != null && (userSession.getType() == Role.EMPLOYEE || userSession.getType() == Role.ADMINISTRATOR )) {
            for(Product prod : (List<Product>) model.get("products")) {
                ProductPDRecord pdRec = productService.getExtendedPricing(1, custno, whse, prod.getProduct(), prod.getPrice());
                prod.setPdRecord(pdRec);
            }

            return new ModelAndView("/product/listAdv", model);
        }
        
        return new ModelAndView("/product/list", model);
    }

    @RequestMapping(value = "/product/get-prod-json", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String jsonGetProduct(@RequestParam(value = "custno", required = false) BigDecimal custno,
                                               @RequestParam(value = "shipTo", required = false) String shipTo,
                                               @RequestParam(value = "whse", required = false) String whse,
                                               @RequestParam(value = "prod", required = false) String prod,
                                               HttpSession httpSession)
    {
        //if whse not set, get whse from customer account
        if(whse == null || whse.isEmpty()) {
            Map<String, Object> orderInfo = accountService.getOrderingInfo(CONO, OPER, custno);
            whse = orderInfo.get("dfltWhse").toString();
        }

        Product product = productService.getProduct(CONO, OPER, custno, shipTo, whse, prod);

        ProductPDRecord pdRec = productService.getExtendedPricing(CONO, custno, whse, prod, product.getPrice());
        product.setPdRecord(pdRec);

        Gson gson = new Gson();
        return gson.toJson(product);
    }

    @RequestMapping(value = "/product/calc-claim", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String calcClaim(@RequestParam(value = "customerNumber", required = false) BigDecimal custno,
                                          @RequestParam(value = "shipTo", required = false) String shipto,
                                          @RequestParam(value = "warehouse", required = false) String whse,
                                          @RequestParam(value = "productCode", required = false) String productCode,
                                          @RequestParam(value = "target", required = false) BigDecimal target,
                                          @RequestParam(value = "targetType", required = false) int targetType,
                                          HttpSession session)
    {
        //not target passed in, consider this a reset and pass default auto price data
        if(target == null) {
            BigDecimal price =  productService.getProduct(CONO, OPER, custno, shipto, whse, productCode).getPrice();
            ProductPDRecord pdRec = productService.getExtendedPricing(1, custno, whse, productCode, price);
            Gson gson = new Gson();
            return gson.toJson(pdRec);
        }

        //if whse not set, get whse from customer account
        if(whse == null || whse.isEmpty()) {
            Map<String, Object> orderInfo = accountService.getOrderingInfo(CONO, OPER, custno);
            whse = orderInfo.get("dfltWhse").toString();
        }

        //calc sale price from gm %
        if(targetType == 1) {
            BigDecimal price =  productService.getProduct(CONO, OPER, custno, shipto, whse, productCode).getPrice();
            BigDecimal basePrice = price;
            ProductPDRecord pdRec = productService.getExtendedPricing(1, custno, whse, productCode, price);

            if(pdRec.getRebateRecord() == null) {
                BigDecimal sellPrice = pdRec.getStandardCost().divide(BigDecimal.ONE.subtract(target.divide(BigDecimal.valueOf(100))), 2, BigDecimal.ROUND_HALF_UP);

                pdRec.setSalePrice(sellPrice);

                Gson gson = new Gson();
                return gson.toJson(pdRec);
            }

            BigDecimal marginPct = pdRec.getMarginPct().setScale(2, RoundingMode.CEILING);
            BigDecimal low_x = BigDecimal.ONE;
            BigDecimal high_x = BigDecimal.ONE;

            //margin % == target margin %
            if(marginPct.compareTo(target) == 0) {
                Gson gson = new Gson();
                return gson.toJson(pdRec);
            }

            //current margin % > target margin %
            if(marginPct.compareTo(target) > 0)
                high_x = new BigDecimal("10");
            else
                low_x = new BigDecimal("0.01");
            
            BigDecimal mid_x = ( low_x.add(high_x) ).divide( new BigDecimal("2") );

            //if we enter here, the margin % is off the target
            //max 20 iterations - will get damn close
            int count = 0;
            while(!marginPct.equals(target) && count++ < 20)
            {
                pdRec = productService.getExtendedPricing(1, custno, whse, productCode, basePrice.divide(mid_x, 2, BigDecimal.ROUND_HALF_UP));
                marginPct = pdRec.getMarginPct().setScale(2, RoundingMode.CEILING);

                //margin % == target margin %
                if(marginPct.compareTo(target) == 0) {
                    Gson gson = new Gson();
                    return gson.toJson(pdRec);
                }

                if(marginPct.compareTo(target) < 0)
                    high_x = mid_x;
                else
                    low_x = mid_x;

                mid_x = (low_x.add(high_x)).divide(new BigDecimal("2"));
            }
            
            Gson gson = new Gson();
            return gson.toJson(pdRec);

        //get gm % from sale price
        }else{
            ProductPDRecord pdRec = productService.getExtendedPricing(1, custno, whse, productCode, target);
            Gson gson = new Gson();
            return gson.toJson(pdRec);
        }    
    }    

    @RequestMapping(value = "/product/sale", method = RequestMethod.GET)
    public ModelAndView sale(HttpSession session)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Products","product/index.htm",""));
        crumbs.add(new Breadcrumb("Sale Items","",""));
        model.put("navTree", crumbs);          
        
        return new ModelAndView("/product/sale", model);
    }    
    
    @RequestMapping(value = "/product/detail", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView detail(@RequestParam(value = "prod", required = true) String prod,
                               HttpSession session)
    {
        Session userSession = (Session) session.getAttribute("session");

        BigDecimal custno = null;
        String shipto = "";
        String whse = null;

        if(userSession != null) {
            custno = (userSession.getCustomerNumber() != null) ? userSession.getCustomerNumber() : null;
            whse = (userSession.getWarehouse() != null) ? userSession.getWarehouse() : null;
            shipto = (userSession.getShipTo() != null) ? userSession.getShipTo() : "";
        }
        
        Map<String, Object> model = new HashMap<String, Object>();
        
        prod = ELUtilities.base64Decode(prod);

        Product product = productService.getProduct(CONO, OPER, custno, shipto, whse, prod);

        //check whether product is available
        if (product == null){



        }

        //user doesn't have base pricing security
        //@todo; bug #0000621
        if(userSession.getUser().isSecurityBasePricing() == false)
            product.setPrice(null);
        
        //details
        if(product.getDetails() != null && !product.getDetails().isEmpty())
            product.setDetails( product.getDetails().replaceAll("\n", "<br/>") );
        
        //set product in model
        model.put("product", product);
        
        //build breadcrumbs
        List<Breadcrumb> crumbs = menuService.getNavTree(prod);
        for(Breadcrumb crumb : crumbs)
            crumb.setUrl("/product/search");
        model.put("navTree", crumbs);
        
        //return login form view
        return new ModelAndView("/product/detail", model);
    }

    @RequestMapping(value = "/product/alternates", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView browseAlternatives(@RequestParam(value = "tid", required = false) String tid,
                                           @RequestParam(value = "sid", required = false) String sid,
                                           @RequestParam(value = "prod", required=true) String prod,
                                           HttpSession httpSession)
    {
        Session session = (Session) httpSession.getAttribute("session");

        BigDecimal custno = null;
        String whse = null;
        String shipto = "";

        if(session != null) {
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;
            whse = (session.getWarehouse() != null) ? session.getWarehouse() : null;
        }

        //get the product description
        Product product = productService.getProduct(CONO, OPER, custno, shipto, whse, ELUtilities.base64Decode(prod));

        Map<String, Object> model = new HashMap<String, Object>();

        List<Product> products = productService.getAlternateProducts(
                CONO,
                OPER,
                custno,
                shipto,
                whse,
                product.getProduct());

        //process alternative products
        model.put("products", products);

        //build breadcrumbs
        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Product Browser", "/pricebook/browse", ""));
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(tid), "/pricebook/browse", "tid=" + tid));
        crumbs.add(new Breadcrumb(ELUtilities.base64Decode(sid), "/pricebook/browse", "tid=" + tid + "&sid=" + sid));
        crumbs.add(new Breadcrumb(product.getDescription(), "/product/detail", "prod=" + prod));
        crumbs.add(new Breadcrumb("Related Products", "", ""));
        model.put("navTree", crumbs);

        model.put("viewAllButton", true);

        return new ModelAndView("/product/alternates", model);

    }
    /**
     * Fetches 10 items that match search term and return JSON. Primary usage
     * is for AJAX requests but can be accessed by any POST request
     * 
     * @param term
     * @return 
     */
    @RequestMapping(value = "/product/quick", method = RequestMethod.POST)
    public @ResponseBody String quick(String term)
    {
        if(term == null || term.isEmpty())
            return "[]";
        
        Gson gson = new Gson();
        
        try {
            List<Product> list = productService.quickLook(1, term, 10);

            String data = gson.toJson(list);

            return data;
        }catch(Exception ex){ }
        
        return null;
    }

    @RequestMapping(value = "/product/quicklist", method = RequestMethod.GET)
    public @ResponseBody String quickList(String term)
    {
        if(term == null || term.isEmpty())
            return "[]";

        Gson gson = new Gson();

        try {
            List<Product> list = productService.quickLook(1, term, 10);

            String data = gson.toJson(list);

            return data;
        }catch(Exception ex){ }

        return null;
    }

    @RequestMapping(value = "/product/get-acc-json", method = RequestMethod.GET)
    public @ResponseBody String getAccesoriesAsJson(String[] products)
    {
        System.out.println("products: " + products);

        if(products == null || products.length == 0)
            return "[]";

        for(String s : products)
            System.out.println("p: " + s);

        Gson gson = new Gson();

        try {
            List<Product> list = productService.getAccessories(products);
            return gson.toJson(list);
        }catch(Exception ex){ }

        return null;
    }

    private Map<String, Object> _searchSku(BigDecimal custno, String shipto, String whse, String term, Integer menuId, String[] attr,
                                      String sort, Integer page, Integer limit, Map<String, Object>  model, Session userSession, String searchtype) {

        //fallback values
        if(page == null) page = 1;
        if(limit == null) limit = 20;
        String sortKey = null; //"icsp.prod";
        String sortDir = "asc";

        String[] sortTmp = null;
        try { sortTmp = sort.split(":"); }catch(Exception ignore) { }

        if(sortTmp != null && sortTmp.length == 2) {
            if(sortTmp[0].equals("prod")) {
                sortKey = "name";
            }else if(sortTmp[0].equals("descrip")) {
                sortKey = "description";
            }

            if(sortTmp[1].equals("desc"))
                sortDir = "desc";
        }else{
            model.put("sortKey", "Relevance");
        }

        model.put("sortDir", sortDir);

        //fetch all attributes based on search term or menuId
        List<Attribute> attributes = productService.getAttributes(term, menuId);

        //flag checked attributes; this is nasty but it is very very small
        String attrStr = "";

        //set 'checked' flag based on user input
        if(attr != null) {
            for(Attribute a : attributes) {
                for(int i=0; i<attr.length; i++) {
                    String[] kv = attr[i].split(":");
                    if(a.getName().equals(kv[0]) ) {
                        int pos = a.getValues().indexOf( new AttributeValue(kv[1], false));
                        if(pos >= 0) {
                            AttributeValue av = a.getValues().get(pos);
                            av.setChecked(true);
                            attrStr += "&attr=" + a.getName() + ":" + av.getName();
                        }
                    }
                }
            }
        }

        //add attributes back to view
        model.put("attributes", attributes);
        model.put("attrStr", attrStr);
        model.put("term", term);
        model.put("page", page);
        model.put("limit", limit);
        model.put("sku-mode", searchtype);

        //fetch products based on search terms and attributes
        List<Product> products = productService.getProductsBySku(CONO, term, limit);

        if(products != null) {
            if (userSession != null && userSession.getUser() != null && userSession.getUser().isSecurityBasePricing() == true) {
                products = productService.applyPricing(CONO, OPER, custno, shipto, whse, products);
            } else {

            }

            //apply netavail
            productService.applyNetAvail(CONO, OPER, products);
        }

        model.put("products", products);
        model.put("searchCount", productService.getSearchResultCount());
        model.put("numPages", (productService.getSearchResultCount()) / limit + 1);

        return model;

    }

    private Map<String, Object> _search(BigDecimal custno, String shipto, String whse, String term, Integer menuId, String[] attr,
                                        String sort, Integer page, Integer limit, Map<String, Object>  model, Session userSession)
    {     
        //fallback values
        if(page == null) page = 1;
        if(limit == null) limit = 20;
        String sortKey = null; //"icsp.prod";
        String sortDir = "asc";
        
        String[] sortTmp = null;
        try { sortTmp = sort.split(":"); }catch(Exception ignore) { }
        
        if(sortTmp != null && sortTmp.length == 2) {
            if(sortTmp[0].equals("prod")) {
                sortKey = "name";
            }else if(sortTmp[0].equals("descrip")) {
                sortKey = "description";
            }
            
            if(sortTmp[1].equals("desc"))
                sortDir = "desc";
        }else{
            model.put("sortKey", "Relevance");
        }

        model.put("sortDir", sortDir);
        
        //fetch all attributes based on search term or menuId
        List<Attribute> attributes = productService.getAttributes(term, menuId);

        //flag checked attributes; this is nasty but it is very very small
        String attrStr = "";
        
        //set 'checked' flag based on user input
        if(attr != null) {
            for(Attribute a : attributes) {
                for(int i=0; i<attr.length; i++) {
                    String[] kv = attr[i].split(":");
                    if(a.getName().equals(kv[0]) ) {
                        int pos = a.getValues().indexOf( new AttributeValue(kv[1], false));
                        if(pos >= 0) {
                            AttributeValue av = a.getValues().get(pos);
                            av.setChecked(true);
                            attrStr += "&attr=" + a.getName() + ":" + av.getName();
                       }
                    }
                }
            }
        }
        
        //add attributes back to view
        model.put("attributes", attributes);
        model.put("attrStr", attrStr);
        model.put("term", term);
        model.put("page", page);
        model.put("limit", limit);

        //fetch products based on search terms and attributes
        List<Product> products = productService.search(CONO, OPER, custno, shipto, term, attributes, whse, menuId, sortKey, sortDir, ((page-1) * limit) + 1, limit);

        if(products != null) {

            if (userSession != null && userSession.getUser() != null && userSession.getUser().isSecurityBasePricing() == true) {
                products = productService.applyPricing(CONO, OPER, custno, shipto, whse, products);
            } else {

            }

            //apply netavail
            productService.applyNetAvail(CONO, OPER, products);
        }

        model.put("products", products);
        model.put("searchCount", productService.getSearchResultCount());
        model.put("numPages", (productService.getSearchResultCount()) / limit + 1);

        return model;
    }
    
    @RequestMapping(value = "/product/avail", method = RequestMethod.GET)
    public ModelAndView availability(@RequestParam(value = "prod", required = false) String prod,
                                     HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        prod = ELUtilities.base64Decode(prod);

        if (httpSession != null){

            Session session = (Session) httpSession.getAttribute("session");
            BigDecimal custno = session.getCustomerNumber();

            if ( custno != null ){

                model.put("availability", productService.getAvailability(CONO, custno, OPER, prod, false) );

            }else {

                model.put("availability", productService.getAvailability(CONO, OPER, prod, false) );
            }

        } else {


            model.put("availability", productService.getAvailability(CONO, OPER, prod, false) );

        }
        return new ModelAndView("/product/avail", model);
    }

    @RequestMapping(value = "/product/accessories", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView accessories(@RequestParam(value = "prod", required = false) String prod,
                                    HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Session session = (Session) httpSession.getAttribute("session");

        BigDecimal custno = null;
        String shipto = "";
        String whse = null;

        if(session != null) {
            custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;
            whse = (session.getWarehouse() != null) ? session.getWarehouse() : null;
            shipto = (session.getShipTo() != null) ? session.getShipTo() : "";
        }

        prod = ELUtilities.base64Decode(prod);
        
        try { 
            List<Product> accessories = productService.getAccessories(CONO, OPER, custno, shipto, whse, prod);

            model.put("accessories", accessories);
            
        }catch(Exception ex) {
            //log.error(ex);
        }
        
        return new ModelAndView("/product/accessories", model);
    }     
    
    /*
     * This is experimental and as such is a mess. Carrier could decide to shutdown this
     * service at anytime so until a final decision has been made, this will remain a "proof of concept"
     */
    @RequestMapping(value = "/product/components", method = RequestMethod.GET)
    @SecurityAnnotation(requireLogin = true)
    public ModelAndView components(@RequestParam(value = "prod", required = false) String prod,
                                   @RequestParam(value = "sub", required = false) String sub,
                                   @RequestParam(value = "group", required = false) String group,
                                   HttpSession httpSession)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //build breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Products", "product/index", ""));
        crumbs.add(new Breadcrumb("Details", "product/detail", "prod="+prod));
        crumbs.add(new Breadcrumb("Component List", "", ""));
        model.put("navTree", crumbs);

        //@todo;webservice call moved to Influx for common code calls

        if(sub != null && !sub.isEmpty()) {
            model.put("prod", sub);
            prod = ELUtilities.base64Decode(sub);
        }else{
            model.put("prod", prod);
            prod = ELUtilities.base64Decode(prod);

            try {
                prod = prod.substring(0,8);
                Holder modelNumber = new Holder(prod);// new Holder(prod); //new Holder("24ABA318A0030010");

                Holder<String> description = new Holder();
                Holder<List<com.kawauso.ext.epic.ListModelsReply.PackageLine>> packageLine = new Holder();

                List<Product> products = new ArrayList<Product>();

                //@todo; great option to use memcached or similar (maybe even mongodb?)
                EPICService epic = new EPICService();
                EPICPortType port = epic.getEPICPort();

                port.listModels("BR,CA,PY", "Internal", modelNumber, description, null, packageLine);

                if(packageLine.value.size() > 1) {
                    List<String[]> models = new ArrayList<String[]>();

                    for(ListModelsReply.PackageLine l : packageLine.value) {
                        String[] m = {l.getSalesPackage(), l.getPackageVolts()};
                        models.add(m);
                    }
                    model.put("models", models);

                    return new ModelAndView("/product/components", model);
                }

            }catch(Exception ex) {
                System.out.println(ex);
            }

        }

        /* @todo; since our #'s vary from carrier's, we need to do some pre-work
         * before fetching the bomLine data.
         *
         * 1 - call "ListModels" with the first 5 characters of the SKU
         * 2 - compare SKU to results and find the closest matching item(s)
         * 2.1 - if more than 1 item found, return list to user for selection
         * 2.2 - if only 1 found, run "GetModelData" against the found item
         */
        List<Product> products = new ArrayList<Product>();

        try {
            Holder modelNumber = new Holder(prod);// new Holder(prod); //new Holder("24ABA318A0030010");

            Holder<List<GetModelDataReply.ModelGroup>> modelGroup = new Holder();
            Holder<List<GetModelDataReply.BomLine>> bomLine = new Holder();
            Holder<List<GetModelDataReply.AddOn>> addOn = new Holder();
            Holder<List<GetModelDataReply.Document>> document = new Holder();

            //@todo; great option to use memcached or similar (maybe even mongodb?)
            EPICService epic = new EPICService();
            EPICPortType port = epic.getEPICPort();
            port.getModelData("BR,CA,PY", "Internal", modelNumber, "Y", null, null, null, null, null, modelGroup, bomLine, addOn, document);

            model.put("groups", modelGroup.value);

            //groups available, none selected
            if(group == null && modelGroup.value.size() > 0) {
                model.put("groupSelect", false);
                return new ModelAndView("/product/components", model);
            }

            Session session = (Session) httpSession.getAttribute("session");

            BigDecimal custno = null;
            String shipto = "";
            String whse = null;

            if(session != null) {
                custno = (session.getCustomerNumber() != null) ? session.getCustomerNumber() : null;
                whse = (session.getWarehouse() != null) ? session.getWarehouse() : null;
                shipto = (session.getShipTo() != null) ? session.getShipTo() : "";
            }

            //iterate items
            for(GetModelDataReply.BomLine b : bomLine.value) {

                //display items if group selected or no groups exist
                if(modelGroup.value.isEmpty() || (modelGroup.value.size() > 0 && b.getGroupCode().equals(group))) {
                    Product p = null;
                    try {
                        if(custno != null && whse != null)
                            p = productService.getProduct(CONO, OPER, custno, shipto, whse, b.getPartNumber());

                    }catch(Exception ex) {
                        System.out.println(ex);
                    }

                    if(p == null) {
                        p = new Product();
                        p.setProduct(b.getPartNumber());
                        p.setDescription(b.getItemDescription());
                    }

                    products.add(p);
                }
            }

        }catch(Exception ex) {
            System.out.println(ex);
        }

        model.put("products", products);

        return new ModelAndView("/product/components", model);
    }
}