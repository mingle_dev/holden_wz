/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

/**
 *
 * @author chris.weaver
 */
public class JobApplicationEmployer
{
    private String name, city, state, zip, phone;
    private String positionHeld, reasonLeave, employDates, payRate, supervisor, duties;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the positionHeld
     */
    public String getPositionHeld() {
        return positionHeld;
    }

    /**
     * @param positionHeld the positionHeld to set
     */
    public void setPositionHeld(String positionHeld) {
        this.positionHeld = positionHeld;
    }

    /**
     * @return the reasonLeave
     */
    public String getReasonLeave() {
        return reasonLeave;
    }

    /**
     * @param reasonLeave the reasonLeave to set
     */
    public void setReasonLeave(String reasonLeave) {
        this.reasonLeave = reasonLeave;
    }

    /**
     * @return the employDates
     */
    public String getEmployDates() {
        return employDates;
    }

    /**
     * @param employDates the employDates to set
     */
    public void setEmployDates(String employDates) {
        this.employDates = employDates;
    }

    /**
     * @return the payRate
     */
    public String getPayRate() {
        return payRate;
    }

    /**
     * @param payRate the payRate to set
     */
    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    /**
     * @return the supervisor
     */
    public String getSupervisor() {
        return supervisor;
    }

    /**
     * @param supervisor the supervisor to set
     */
    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    /**
     * @return the duties
     */
    public String getDuties() {
        return duties;
    }

    /**
     * @param duties the duties to set
     */
    public void setDuties(String duties) {
        this.duties = duties;
    }
}
