package com.kawauso.base.bean;


import com.kawauso.base.interfaces.WZUserSession;

import javax.ejb.SessionBean;
import javax.ejb.Stateless;

/**
 * Created by cbyrd on 4/16/15.
 */

//@Stateless
public class WZUserSessionBean implements WZUserSession {


    String userId;
    String token;


    public void setUserId(String userId){

        this.userId = userId;

    }

    public String getUserId(){

        return userId;
    }

    public void setToken(String token){

        this.token = token;
    }

    public String getToken(){

        return token;

    }



}
