/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris.weaver
 */
@Entity
@Table(name = "cart_heads")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Cart implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")
    private String id;
    
    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "shipto_id")
    private String shipToId;

    @Column(name = "shipto_name")
    private String shipToName;
    
    @Column(name = "shipto_addr1")
    private String shipToAddr1;

    @Column(name = "shipto_addr2")
    private String shipToAddr2;
    
    @Column(name = "shipto_city")
    private String shipToCity;
    
    @Column(name = "shipto_state")
    private String shipToState;
    
    @Column(name = "shipto_zip")
    private String shipToZip;
    
    @Column(name = "shipwhse_id")
    private String shipWhseId;
    
    @Column(name = "jobaccount")
    private String jobId;
    
    @Column(name = "custpo")
    private String custPo;
    
    @Column(name = "shipdate")
    //@Temporal(javax.persistence.TemporalType.DATE)
    //private Date shipDate;
    private String shipDate;
    
    @Column(name = "shipvia")
    private String shipVia;
    
    @Column(name = "ordertype")
    private String orderType;
    
    @Column(name = "disposition")
    private String disposition;
    
    @Column(name = "reference")
    private String reference;
    
    @Column(name = "instructions")
    private String instructions;
    
    @Column(name = "notes")
    private String notes;

    public Cart() {}
    
    public Cart(Cart other)
    {
        this.id = other.id;
        setFields(other);
    }    
    
    public final void setFields(Cart other)
    {
        this.shipToId = other.shipToId;
        this.shipToName = other.shipToName;
        this.shipToAddr1 = other.shipToAddr1;
        this.shipToAddr2 = other.shipToAddr2;
        this.shipToCity = other.shipToCity;
        this.shipToState = other.shipToState;
        this.shipToZip = other.shipToZip;
        this.shipWhseId = other.shipWhseId;
        this.jobId = other.jobId;
        this.custPo = other.custPo;
        this.shipDate = other.shipDate;
        this.shipVia = other.shipVia;
        this.orderType = other.orderType;
        this.disposition = other.disposition;
        this.reference = other.reference;
        this.instructions = other.instructions;
        this.notes = other.notes;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the shipToId
     */
    public String getShipToId() {
        return shipToId;
    }

    /**
     * @param shipToId the shipToId to set
     */
    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    /**
     * @return the shipToAddr1
     */
    public String getShipToAddr1() {
        return shipToAddr1;
    }

    /**
     * @param shipToAddr1 the shipToAddr1 to set
     */
    public void setShipToAddr1(String shipToAddr1) {
        this.shipToAddr1 = shipToAddr1;
    }

    /**
     * @return the shipToAddr2
     */
    public String getShipToAddr2() {
        return shipToAddr2;
    }

    /**
     * @param shipToAddr2 the shipToAddr2 to set
     */
    public void setShipToAddr2(String shipToAddr2) {
        this.shipToAddr2 = shipToAddr2;
    }

    /**
     * @return the shipToCity
     */
    public String getShipToCity() {
        return shipToCity;
    }

    /**
     * @param shipToCity the shipToCity to set
     */
    public void setShipToCity(String shipToCity) {
        this.shipToCity = shipToCity;
    }

    /**
     * @return the shipToState
     */
    public String getShipToState() {
        return shipToState;
    }

    /**
     * @param shipToState the shipToState to set
     */
    public void setShipToState(String shipToState) {
        this.shipToState = shipToState;
    }

    /**
     * @return the shipToZip
     */
    public String getShipToZip() {
        return shipToZip;
    }

    /**
     * @param shipToZip the shipToZip to set
     */
    public void setShipToZip(String shipToZip) {
        this.shipToZip = shipToZip;
    }

    /**
     * @return the shipWhseId
     */
    public String getShipWhseId() {
        return shipWhseId;
    }

    /**
     * @param shipWhseId the shipWhseId to set
     */
    public void setShipWhseId(String shipWhseId) {
        this.shipWhseId = shipWhseId;
    }

    /**
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    /**
     * @return the custPo
     */
    public String getCustPo() {
        return custPo;
    }

    /**
     * @param custPo the custPo to set
     */
    public void setCustPo(String custPo) {
        this.custPo = custPo;
    }

    /**
     * @return the shipDate
     */
    public String getShipDate() {
        return shipDate;
    }

    /**
     * @param shipDate the shipDate to set
     */
    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    /**
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * @param shipVia the shipVia to set
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the disposition
     */
    public String getDisposition() {
        return disposition;
    }

    /**
     * @param disposition the disposition to set
     */
    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * @param instructions the instructions to set
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the shipToName
     */
    public String getShipToName() {
        return shipToName;
    }

    /**
     * @param shipToName the shipToName to set
     */
    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }
}
