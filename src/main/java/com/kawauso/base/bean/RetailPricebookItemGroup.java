package com.kawauso.base.bean;

/**
 * Created by chris on 12/22/14.
 */
public class RetailPricebookItemGroup
{
    private String id, name, description;
    private String categoryId, categoryName;
    private double gmPct, commissionRate;
    private double laborAmt, miscAmt;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the gmPct
     */
    public double getGmPct() {
        return gmPct;
    }

    /**
     * @param gmPct the gmPct to set
     */
    public void setGmPct(double gmPct) {
        this.gmPct = gmPct;
    }

    /**
     * @return the laborAmt
     */
    public double getLaborAmt() {
        return laborAmt;
    }

    /**
     * @param laborAmt the laborAmt to set
     */
    public void setLaborAmt(double laborAmt) {
        this.laborAmt = laborAmt;
    }

    /**
     * @return the miscAmt
     */
    public double getMiscAmt() {
        return miscAmt;
    }

    /**
     * @param miscAmt the miscAmt to set
     */
    public void setMiscAmt(double miscAmt) {
        this.miscAmt = miscAmt;
    }

    /**
     * @return the commissionRate
     */
    public double getCommissionRate() {
        return commissionRate;
    }

    /**
     * @param commissionRate the commissionRate to set
     */
    public void setCommissionRate(double commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     * @return the categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}