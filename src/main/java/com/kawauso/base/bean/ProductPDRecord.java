/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.math.BigDecimal;

/**
 *
 * @author chris.weaver
 */
public class ProductPDRecord
{
    private int cono;
    private String productCode;
    private BigDecimal margin, marginPct, salePrice, standardCost, capSell;
    private BigDecimal rebateAmount, calcRebateAmount, listPrice;
    private String contractRecord, rebateRecord, rebateType, rebateCalcType;

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the margin
     */
    public BigDecimal getMargin() {
        return margin;
    }

    /**
     * @param margin the margin to set
     */
    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    /**
     * @return the marginPct
     */
    public BigDecimal getMarginPct() {
        return marginPct;
    }

    /**
     * @param marginPct the marginPct to set
     */
    public void setMarginPct(BigDecimal marginPct) {
        this.marginPct = marginPct;
    }

    /**
     * @return the salePrice
     */
    public BigDecimal getSalePrice() {
        return salePrice;
    }

    /**
     * @param salePrice the salePrice to set
     */
    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * @return the standardCost
     */
    public BigDecimal getStandardCost() {
        return standardCost;
    }

    /**
     * @param standardCost the standardCost to set
     */
    public void setStandardCost(BigDecimal standardCost) {
        this.standardCost = standardCost;
    }

    /**
     * @return the capSell
     */
    public BigDecimal getCapSell() {
        return capSell;
    }

    /**
     * @param capSell the capSell to set
     */
    public void setCapSell(BigDecimal capSell) {
        this.capSell = capSell;
    }

    /**
     * @return the rebateAmount
     */
    public BigDecimal getRebateAmount() {
        return rebateAmount;
    }

    /**
     * @param rebateAmount the rebateAmount to set
     */
    public void setRebateAmount(BigDecimal rebateAmount) {
        this.rebateAmount = rebateAmount;
    }

    /**
     * @return the calcRebateAmount
     */
    public BigDecimal getCalcRebateAmount() {
        return calcRebateAmount;
    }

    /**
     * @param calcRebateAmount the calcRebateAmount to set
     */
    public void setCalcRebateAmount(BigDecimal calcRebateAmount) {
        this.calcRebateAmount = calcRebateAmount;
    }

    /**
     * @return the rebateRecord
     */
    public String getRebateRecord() {
        return rebateRecord;
    }

    /**
     * @param rebateRecord the rebateRecord to set
     */
    public void setRebateRecord(String rebateRecord) {
        this.rebateRecord = rebateRecord;
    }

    /**
     * @return the contractRecord
     */
    public String getContractRecord() {
        return contractRecord;
    }

    /**
     * @param contractRecord the contractRecord to set
     */
    public void setContractRecord(String contractRecord) {
        this.contractRecord = contractRecord;
    }

    /**
     * @return the rebateType
     */
    public String getRebateType() {
        return rebateType;
    }

    /**
     * @param rebateType the rebateType to set
     */
    public void setRebateType(String rebateType) {
        this.rebateType = rebateType;
    }

    /**
     * @return the rebateCalcType
     */
    public String getRebateCalcType() {
        return rebateCalcType;
    }

    /**
     * @param rebateCalcType the rebateCalcType to set
     */
    public void setRebateCalcType(String rebateCalcType) {
        this.rebateCalcType = rebateCalcType;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the listPrice
     */
    public BigDecimal getListPrice() {
        return listPrice;
    }

    /**
     * @param listPrice the listPrice to set
     */
    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }
}
