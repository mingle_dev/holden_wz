package com.kawauso.base.bean;


import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Document
public class Session
{
    @Id
    private String id;

    public enum Role {
        ANONYMOUS, CUSTOMER, VENDOR, EMPLOYEE, ADMINISTRATOR, DISTRIBUTOR
    }

    public enum Function {
        SPIFFS
    }

    private String userId;

    private User user;

    private Contact contact;

    private Account account;

    private Session.Role type;

    public Function getAccess() {
        return access;
    }

    public void setAccess(Function access) {
        this.access = access;
    }

    private Session.Function access;

    private Date lastSeen;
    private Date firstSeen;
    private String remoteAddress;

    private BigDecimal customerNumber;
    private String shipTo;
    private String customerName;
    private String warehouse;
    private String warehouseName;
    private int cono;

    private String slsRepName;

    public Session() {}

    //clone
    public Session(Session other)
    {
        this.id = other.id;
        this.userId = other.userId;
        this.type = other.type;
        this.lastSeen = other.lastSeen;
        this.firstSeen = other.firstSeen;
        this.remoteAddress = other.remoteAddress;
        this.customerNumber = other.customerNumber;
        this.customerName = other.customerName;
        this.warehouse = other.warehouse;
        this.warehouseName = other.warehouseName;
        this.access = other.access;
        this.cono = other.cono;
    }

    public int getCono() {
        return cono;
    }

    public void setCono(int cono) {
        this.cono = cono;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getSlsRepName() {
        return slsRepName;
    }

    public void setSlsRepName(String slsRepName) {
        this.slsRepName = slsRepName;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the type
     */
    public Session.Role getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Session.Role type) {
        this.type = type;
    }

    /**
     * @return the lastSeen
     */
    public Date getLastSeen() {
        return lastSeen;
    }

    /**
     * @param lastSeen the lastSeen to set
     */
    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    /**
     * @return the firstSeen
     */
    public Date getFirstSeen() {
        return firstSeen;
    }

    /**
     * @param firstSeen the firstSeen to set
     */
    public void setFirstSeen(Date firstSeen) {
        this.firstSeen = firstSeen;
    }

    /**
     * @return the remoteAddress
     */
    public String getRemoteAddress() {
        return remoteAddress;
    }

    /**
     * @param remoteAddress the remoteAddress to set
     */
    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the customerNumber
     */
    public BigDecimal getCustomerNumber() {
        return customerNumber;
    }

    /**
     * @param customerNumber the customerNumber to set
     */
    public void setCustomerNumber(BigDecimal customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse the warehouse to set
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the warehouseName
     */
    public String getWarehouseName() {
        return warehouseName;
    }

    /**
     * @param warehouseName the warehouseName to set
     */
    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Override
    public String toString()
    {
        return "[id=" + id + "; userId=" + userId + "; custNo=" + customerNumber + "]";
    }
}