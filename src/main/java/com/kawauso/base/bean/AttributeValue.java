/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

/**
 *
 * @author chris.weaver
 */
public class AttributeValue
{
    private String name;
    private boolean checked;

    public AttributeValue() {}

    public AttributeValue(String name, boolean checked) {
        this.name = name;
        this.checked = checked;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        AttributeValue val = (AttributeValue) obj;
        return (this.name.equals(val.name)) ? true : false;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the checked
     */
    public boolean isChecked() {
        return checked;
    }

    /**
     * @param checked the checked to set
     */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}