/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Formula;

/**
 *
 * @author chris.weaver
 * 
 * @todo; not all parameters marked transient will remain transient. This was done
 * just to get the class working quickly for both hibernate and sxapi
 */
@Entity
@Table(name = "nxt_oeeh")
public class Order implements Serializable
{
    @Id
    @Column(name = "id", insertable = false, updatable = false)
    private String id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private List<OrderLine> orderLines;
    
    @Column(name = "cono")
    private int cono;

    @Column(name = "custno", insertable = false, updatable = false)
    private BigDecimal custNo;        
    
    @Column(name = "orderno")
    private int orderNo;
    
    @Column(name = "ordersuf")
    private int orderSuf;
    
    @Column(name = "transtype")
    private String transType;
    
    @Column(name = "shipto")
    private String shipTo;
    
    @Column(name = "custpo")
    private String custPo;  
    
//    @Formula("row_number() OVER ( ORDER BY oeeh.orderno ASC)")
//    private int rowNum;
//    
//    @Formula("Count(*) OVER()")
//    private int rowCount;
    
    /** more **/
    
    @Column(name = "enterdt")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date enterDt;
    
    @Column(name = "invoicedt")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date invoiceDt;

    @Column(name = "reqshipdt")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date reqShipDt;

    @Transient
    private Date dueDt;
    
    @Column(name = "totordamt")
    private BigDecimal totalOrderAmt;
              
    @Column(name = "stagecd")
    private int stageCode;

    @Column(name = "whse")
    private String whse;
    
    @Column(name = "shipviaty")
    private String shipVia;
    
    @Column(name = "refer")
    private String reference;
    
    @Column(name = "shipinstr")
    private String instructions;
    
    @Column(name = "termstype")
    private String termsDesc;
    
    @Transient private String soldToName;
    @Transient private String soldToAddr1;
    @Transient private String soldToAddr2;
    @Transient private String soldToCity;
    @Transient private String soldToState;
    @Transient private String soldToZip;
    
    @Column(name = "shiptonm")
    private String shipToName;
    
    @Transient
    private String shipToAddr1;
    
    @Transient
    private String shipToAddr2;
    
    @Transient
    private String shipToCity;
    
    @Transient
    private String shipToState;
    
    @Transient
    private String shipToZip;
    
    @Transient
    private Date shipDt;
    
    @Column(name = "totinvord")
    private BigDecimal subTotal;
    
    @Column(name = "taxamt_1")
    private BigDecimal taxamt_1;
    
    //@Formula("taxamt_1 + 10") //<-- nullpointer?
    @Transient
    private BigDecimal taxAmount;
    
    @Column(name = "totinvamt")
    private BigDecimal invAmount;
    
    //@OneToMany(fetch = FetchType.LAZY)
    //@JoinTable(name = "nxt_oeel", 
    //           joinColumns = {@JoinColumn(name="cono"), @JoinColumn(name="orderno"), @JoinColumn(name="ordersuf")},
    //           inverseJoinColumns = {@JoinColumn(name="cono"), @JoinColumn(name="orderno"), @JoinColumn(name="ordersuf")}
    //          )
    //@Transient
    //private Set<OrderLine> orderLines;

    //, joinColumns = {@JoinColumn(name="orderno")})
    
//    /**
//     * @return the orderNo
//     */
//    public int getOrderNo() {
//        return orderNo;
//    }
//
//    /**
//     * @param orderNo the orderNo to set
//     */
//    public void setOrderNo(int orderNo) {
//        this.orderNo = orderNo;
//    }
//
//    /**
//     * @return the orderSuf
//     */
//    public int getOrderSuf() {
//        return orderSuf;
//    }
//
//    /**
//     * @param orderSuf the orderSuf to set
//     */
//    public void setOrderSuf(int orderSuf) {
//        this.orderSuf = orderSuf;
//    }

    /**
     * @return the transType
     */
    public String getTransType() {
        return transType;
    }
    
    public String getTransTypeText() {
        if(transType.equalsIgnoreCase("SO"))
            return "Stock Order";        
        else if(transType.equalsIgnoreCase("QU"))
            return "Quote";
        else if(transType.equalsIgnoreCase("DO"))
            return "Direct Order";
        else if(transType.equalsIgnoreCase("RA"))
            return "Return";        
        else
            return transType;
    }

    /**
     * @param transType the transType to set
     */
    public void setTransType(String transType) {
        this.transType = transType;
    }

    /**
     * @return the custNo
     */
    public BigDecimal getCustNo() {
        return custNo;
    }

    /**
     * @param custNo the custNo to set
     */
    public void setCustNo(BigDecimal custNo) {
        this.custNo = custNo;
    }

    /**
     * @return the shipTo
     */
    public String getShipTo() {
        return shipTo;
    }

    /**
     * @param shipTo the shipTo to set
     */
    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    /**
     * @return the custPo
     */
    public String getCustPo() {
        return custPo;
    }

    /**
     * @param custPo the custPo to set
     */
    public void setCustPo(String custPo) {
        this.custPo = custPo;
    }

    /**
     * @return the enterDt
     */
    public Date getEnterDt() {
        return enterDt;
    }

    /**
     * @param enterDt the enterDt to set
     */
    public void setEnterDt(Date enterDt) {
        this.enterDt = enterDt;
    }

    /**
     * @return the invoiceDt
     */
    public Date getInvoiceDt() {
        return invoiceDt;
    }

    /**
     * @param invoiceDt the invoiceDt to set
     */
    public void setInvoiceDt(Date invoiceDt) {
        this.invoiceDt = invoiceDt;
    }

    /**
     * @return the totalOrderAmt
     */
    public BigDecimal getTotalOrderAmt() {
        return totalOrderAmt;
    }

    /**
     * @param totalOrderAmt the totalOrderAmt to set
     */
    public void setTotalOrderAmt(BigDecimal totalOrderAmt) {
        this.totalOrderAmt = totalOrderAmt;
    }

    /**
     * @return the stageCode
     */
    public int getStageCode() {
        return stageCode;
    }

    public String getStageText() {
        switch(stageCode) {
            case 0: return "Entered";
            case 1: return "Ordered";
            case 2: return "Picked";
            case 3: return "Shipped";
            case 4: return "Invoiced";
            case 5: return "Paid";
            case 9: return "Cancelled";
            default: return "Unknown";
        }
    }
    
    /**
     * @param stageCode the stageCode to set
     */
    public void setStageCode(int stageCode) {
        this.stageCode = stageCode;
    }

//    /**
//     * @return the cono
//     */
//    public int getCoNo() {
//        return coNo;
//    }
//
//    /**
//     * @param cono the cono to set
//     */
//    public void setCono(int coNo) {
//        this.coNo = coNo;
//    }

    /**
     * @return the whse
     */
    public String getWhse() {
        return whse;
    }

    /**
     * @param whse the whse to set
     */
    public void setWhse(String whse) {
        this.whse = whse;
    }

    /**
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * @param shipVia the shipVia to set
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * @param instructions the instructions to set
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     * @return the termsDesc
     */
    public String getTermsDesc() {
        return termsDesc;
    }

    /**
     * @param termsDesc the termsDesc to set
     */
    public void setTermsDesc(String termsDesc) {
        this.termsDesc = termsDesc;
    }

    /**
     * @return the shipDt
     */
    public Date getShipDt() {
        return shipDt;
    }

    /**
     * @param shipDt the shipDt to set
     */
    public void setShipDt(Date shipDt) {
        this.shipDt = shipDt;
    }

    /**
     * @return the subTotal
     */
    public BigDecimal getSubTotal() {
        return subTotal;
    }

    /**
     * @param subTotal the subTotal to set
     */
    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * @return the taxAmount
     */
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    /**
     * @param taxAmount the taxAmount to set
     */
    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * @return the invAmount
     */
    public BigDecimal getInvAmount() {
        return invAmount;
    }

    /**
     * @param invAmount the invAmount to set
     */
    public void setInvAmount(BigDecimal invAmount) {
        this.invAmount = invAmount;
    }

    /**
     * @return the soldToName
     */
    public String getSoldToName() {
        return soldToName;
    }

    /**
     * @param soldToName the soldToName to set
     */
    public void setSoldToName(String soldToName) {
        this.soldToName = soldToName;
    }

    /**
     * @return the soldToAddr1
     */
    public String getSoldToAddr1() {
        return soldToAddr1;
    }

    /**
     * @param soldToAddr1 the soldToAddr1 to set
     */
    public void setSoldToAddr1(String soldToAddr1) {
        this.soldToAddr1 = soldToAddr1;
    }

    /**
     * @return the soldToAddr2
     */
    public String getSoldToAddr2() {
        return soldToAddr2;
    }

    /**
     * @param soldToAddr2 the soldToAddr2 to set
     */
    public void setSoldToAddr2(String soldToAddr2) {
        this.soldToAddr2 = soldToAddr2;
    }

    /**
     * @return the soldToCity
     */
    public String getSoldToCity() {
        return soldToCity;
    }

    /**
     * @param soldToCity the soldToCity to set
     */
    public void setSoldToCity(String soldToCity) {
        this.soldToCity = soldToCity;
    }

    /**
     * @return the soldToState
     */
    public String getSoldToState() {
        return soldToState;
    }

    /**
     * @param soldToState the soldToState to set
     */
    public void setSoldToState(String soldToState) {
        this.soldToState = soldToState;
    }

    /**
     * @return the soldToZip
     */
    public String getSoldToZip() {
        return soldToZip;
    }

    /**
     * @param soldToZip the soldToZip to set
     */
    public void setSoldToZip(String soldToZip) {
        this.soldToZip = soldToZip;
    }

    /**
     * @return the shipToName
     */
    public String getShipToName() {
        return shipToName;
    }

    /**
     * @param shipToName the shipToName to set
     */
    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    /**
     * @return the shipToAddr1
     */
    public String getShipToAddr1() {
        return shipToAddr1;
    }

    /**
     * @param shipToAddr1 the shipToAddr1 to set
     */
    public void setShipToAddr1(String shipToAddr1) {
        this.shipToAddr1 = shipToAddr1;
    }

    /**
     * @return the shipToAddr2
     */
    public String getShipToAddr2() {
        return shipToAddr2;
    }

    /**
     * @param shipToAddr2 the shipToAddr2 to set
     */
    public void setShipToAddr2(String shipToAddr2) {
        this.shipToAddr2 = shipToAddr2;
    }

    /**
     * @return the shipToCity
     */
    public String getShipToCity() {
        return shipToCity;
    }

    /**
     * @param shipToCity the shipToCity to set
     */
    public void setShipToCity(String shipToCity) {
        this.shipToCity = shipToCity;
    }

    /**
     * @return the shipToState
     */
    public String getShipToState() {
        return shipToState;
    }

    /**
     * @param shipToState the shipToState to set
     */
    public void setShipToState(String shipToState) {
        this.shipToState = shipToState;
    }

    /**
     * @return the shipToZip
     */
    public String getShipToZip() {
        return shipToZip;
    }

    /**
     * @param shipToZip the shipToZip to set
     */
    public void setShipToZip(String shipToZip) {
        this.shipToZip = shipToZip;
    }

    /**
     * @return the orderLines
     */
    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    /**
     * @param orderLines the orderLines to set
     */
    public void setOrderLines(List<OrderLine> orderLines) {
        this.setOrderLines(orderLines);
    }
    
    public void addOrderLine(OrderLine orderLine) {
        if(orderLines == null)
            orderLines = new ArrayList<OrderLine>();
        orderLines.add(orderLine);
    }

    /**
     * @return the dueDt
     */
    public Date getDueDt() {
        return dueDt;
    }

    /**
     * @param dueDt the dueDt to set
     */
    public void setDueDt(Date dueDt) {
        this.dueDt = dueDt;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the orderNo
     */
    public int getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the orderSuf
     */
    public int getOrderSuf() {
        return orderSuf;
    }

    /**
     * @param orderSuf the orderSuf to set
     */
    public void setOrderSuf(int orderSuf) {
        this.orderSuf = orderSuf;
    }

    /**
     * @return the taxamt_1
     */
    public BigDecimal getTaxamt_1() {
        return taxamt_1;
    }

    /**
     * @param taxamt_1 the taxamt_1 to set
     */
    public void setTaxamt_1(BigDecimal taxamt_1) {
        this.taxamt_1 = taxamt_1;
    }

    public Date getReqShipDt() {
        return reqShipDt;
    }

    public void setReqShipDt(Date reqShipDt) {
        this.reqShipDt = reqShipDt;
    }
}