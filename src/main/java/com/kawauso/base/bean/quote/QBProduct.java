package com.kawauso.base.bean.quote;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Created by chris on 11/24/14.
 */
@Document
public class QBProduct
{
    @Id
    private String id;

    private String sku;
    private String description;

    private boolean nonStock;

    @Transient
    private BigDecimal netAvail;

    private BigDecimal qty;
    private BigDecimal cost;
    private BigDecimal basePrice;
    private BigDecimal sellPrice;
    private BigDecimal extPrice;
    private BigDecimal marginPct;

    public QBProduct() {
        this.id = UUID.randomUUID().toString();
    }

    public QBProduct(String sku) {
        this.id = UUID.randomUUID().toString();
        this.sku = sku;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getMarginPct() {
        return marginPct;
    }

    public void setMarginPct(BigDecimal marginPct) {
        this.marginPct = marginPct;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getExtPrice() {
        return extPrice;
    }

    public void setExtPrice(BigDecimal extPrice) {
        this.extPrice = extPrice;
    }

    public boolean isNonStock() {
        return nonStock;
    }

    public void setNonStock(boolean nonStock) {
        this.nonStock = nonStock;
    }

    public BigDecimal getNetAvail() {
        return netAvail;
    }

    public void setNetAvail(BigDecimal netAvail) {
        this.netAvail = netAvail;
    }
}
