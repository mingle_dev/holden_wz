package com.kawauso.base.bean.quote;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by chris on 11/24/14.
 */
@Document
public class Quote
{
    @Id
    private String id;

    private String quoteId;
    private String quoteName;

    private String ownerContactId;

    private String noteInternal;
    private String noteExternal;

    @Transient
    private int nextStep;

    private String visibility;

    private String firstName;
    private String middleName;
    private String lastName;
    private String homePhone;
    private String cellPhone;
    private String workPhone;

    private String addrStreet;

    private String customerNumber;
    private String shipTo;
    private String customerName;

    private String contactName;
    private String emailAddress;

    //@Transient
    private String addrCity;
    private String addrState;
    private String addrZip;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PriceDetail getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(PriceDetail priceDetail) {
        this.priceDetail = priceDetail;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<QBShare> getShares() {
        return shares;
    }

    public void setShares(List<QBShare> shares) {
        this.shares = shares;
    }

    public void addShare(QBShare share) {
        if(shares == null)
            shares = new ArrayList<QBShare>();

        shares.add(share);
    }

    public String getNoteInternal() {
        return noteInternal;
    }

    public void setNoteInternal(String noteInternal) {
        this.noteInternal = noteInternal;
    }

    public String getNoteExternal() {
        return noteExternal;
    }

    public void setNoteExternal(String noteExternal) {
        this.noteExternal = noteExternal;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public enum Status { DRAFT, PUBLISHED, CONVERTED }

    private Status status;

    public enum PriceDetail { SKU, SYSTEM, QUOTE }

    private PriceDetail priceDetail;

    private Date createDate;
    private Date expireDate;

    private List<MarkFor> systems;

    private List<QBShare> shares;

    public Quote() {}

    public Quote(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteName() {
        return quoteName;
    }

    public void setQuoteName(String quoteName) {
        this.quoteName = quoteName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public List<MarkFor> getSystems() {
        return systems;
    }

    public void setSystems(List<MarkFor> systems) {
        this.systems = systems;
    }

    public void addSystem(MarkFor system) {
        if(systems == null)
            systems = new ArrayList<MarkFor>();

        systems.add(system);
    }

    public String toString()
    {
        return "[ id: " + id + ", " + quoteName + " ]";
    }

    public String getAddrStreet() {
        return addrStreet;
    }

    public void setAddrStreet(String addrStreet) {
        this.addrStreet = addrStreet;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddrCity() {
        return addrCity;
    }

    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

    public String getAddrState() {
        return addrState;
    }

    public void setAddrState(String addrState) {
        this.addrState = addrState;
    }

    public String getAddrZip() {
        return addrZip;
    }

    public void setAddrZip(String addrZip) {
        this.addrZip = addrZip;
    }

    public int getNextStep() {
        return nextStep;
    }

    public void setNextStep(int nextStep) {
        this.nextStep = nextStep;
    }

    public String getOwnerContactId() {
        return ownerContactId;
    }

    public void setOwnerContactId(String ownerContactId) {
        this.ownerContactId = ownerContactId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }
}