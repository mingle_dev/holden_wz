package com.kawauso.base.bean.quote;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by chris on 11/24/14.
 */
@Document
public class MarkFor
{
    @Id
    private String id;

    private String systemName;
    private String description;
    private String notes;

    private List<QBProduct> products;
    private List<QBProduct> accessories;
    private List<QBProduct> addons;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public enum Status { NEW, SAVED }

    private Status status;

    public MarkFor() {
        this.id = UUID.randomUUID().toString();
    }

    public MarkFor(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public List<QBProduct> getProducts() {
        return products;
    }

    public void setProducts(List<QBProduct> products) {
        this.products = products;
    }

    public void addProduct(QBProduct product) {
        if(products == null)
            products = new ArrayList<QBProduct>();

        products.add(product);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof MarkFor) {
            MarkFor otherMarkFor = (MarkFor) other;

            if(otherMarkFor.getId().equals(this.id))
                return true;
        }

        return false;
    }

    public List<QBProduct> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<QBProduct> accessories) {
        this.accessories = accessories;
    }

    public List<QBProduct> getAddOns() {
        return addons;
    }

    public void setAddOns(List<QBProduct> addons) {
        this.addons = addons;
    }

    public void addAccessory(QBProduct product) {
        if(accessories == null)
            accessories = new ArrayList<QBProduct>();

        accessories.add(product);
    }

    public void addAddOn(QBProduct product) {
        if(addons == null)
            addons = new ArrayList<QBProduct>();

        addons.add(product);
    }
}