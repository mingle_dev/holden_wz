package com.kawauso.base.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class User implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "spiffType")
    private int spiffType;

    @Column(name = "erpcono")
    private int cono;
    //@Column(name = "full_name")
    //private String fullName;
    
    //@Column(name = "accountno")
    //private String customerNumber;
    
    //@Column(name = "company_name")
    //private String customerName;
    
    @Column(name = "currwhse_id")
    private String warehouse;
    
    //for now, don't use relationships
    @Column(name = "contact_id")
    private String contactId;
    
    //for now, don't use relationships
    @Column(name = "group_id")
    private String groupId;
    
    @Column(name = "resetWhse")
    private boolean resetWhse;
    
    @Column(name = "pageLimit")
    private int pageLimit;
    
    @Column(name = "shipvia")
    private String shipVia;
    
    @Column(name = "shipNotifyType")
    private String shipNotifyType;
    
    @Column(name = "shipto")
    private String shipTo;
    
    //@Column(name = "territory")
    //private String territory;
    
    //@Column(name = "email_addr")
    //private String emailAddr;
    
    //@Column(name = "group_name")
    //private String type;
    
    @Column(name = "orderInquiry")
    private boolean securityOrderInquiry;
    
    @Column(name = "orderCreate")
    private boolean securityOrderCreate;
    
    @Column(name = "accountInquiry")
    private boolean securityAccount;
    
    @Column(name = "productPricing")
    private boolean securityBasePricing;

    @Column(name = "userAdmin")
    private boolean securityUsers;    
    
    @Column(name = "pbType")
    private String pricebookCateoryies;
    
    //securityRetailPricing,

    @Column(name = "availType")
    private String availType;

    @Column(name = "pbAdmin")
    private boolean securityPBAdmin;

    public int getCono() {
        return cono;
    }

    public void setCono(int cono) {
        this.cono = cono;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse the warehouse to set
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the securityOrderInquiry
     */
    public boolean isSecurityOrderInquiry() {
        return securityOrderInquiry;
    }

    /**
     * @param securityOrderInquiry the securityOrderInquiry to set
     */
    public void setSecurityOrderInquiry(boolean securityOrderInquiry) {
        this.securityOrderInquiry = securityOrderInquiry;
    }

    /**
     * @return the securityOrderCreate
     */
    public boolean isSecurityOrderCreate() {
        return securityOrderCreate;
    }

    /**
     * @param securityOrderCreate the securityOrderCreate to set
     */
    public void setSecurityOrderCreate(boolean securityOrderCreate) {
        this.securityOrderCreate = securityOrderCreate;
    }

    /**
     * @return the securityAccount
     */
    public boolean isSecurityAccount() {
        return securityAccount;
    }

    /**
     * @param securityAccount the securityAccount to set
     */
    public void setSecurityAccount(boolean securityAccount) {
        this.securityAccount = securityAccount;
    }

    /**
     * @return the securityBasePricing
     */
    public boolean isSecurityBasePricing() {
        return securityBasePricing;
    }

    /**
     * @param securityBasePricing the securityBasePricing to set
     */
    public void setSecurityBasePricing(boolean securityBasePricing) {
        this.securityBasePricing = securityBasePricing;
    }

    /**
     * @return the securityUsers
     */
    public boolean isSecurityUsers() {
        return securityUsers;
    }

    /**
     * @param securityUsers the securityUsers to set
     */
    public void setSecurityUsers(boolean securityUsers) {
        this.securityUsers = securityUsers;
    }

    /**
     * @return the securityPBAdmin
     */
    public boolean isSecurityPBAdmin() {
        return securityPBAdmin;
    }

    /**
     * @param securityPBAdmin the securityPBAdmin to set
     */
    public void setSecurityPBAdmin(boolean securityPBAdmin) {
        this.securityPBAdmin = securityPBAdmin;
    }

    /**
     * @return the contactId
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the resetWhse
     */
    public boolean isResetWhse() {
        return resetWhse;
    }

    /**
     * @param resetWhse the resetWhse to set
     */
    public void setResetWhse(boolean resetWhse) {
        this.resetWhse = resetWhse;
    }

    /**
     * @return the pageLimit
     */
    public int getPageLimit() {
        return pageLimit;
    }

    /**
     * @param pageLimit the pageLimit to set
     */
    public void setPageLimit(int pageLimit) {
        this.pageLimit = pageLimit;
    }

    /**
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * @param shipVia the shipVia to set
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * @return the shipNotifyType
     */
    public String getShipNotifyType() {
        return shipNotifyType;
    }

    /**
     * @param shipNotifyType the shipNotifyType to set
     */
    public void setShipNotifyType(String shipNotifyType) {
        this.shipNotifyType = shipNotifyType;
    }

    /**
     * @return the shipTo
     */
    public String getShipTo() {
        return shipTo;
    }

    /**
     * @param shipTo the shipTo to set
     */
    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    /**
     * @return the pricebookCateoryies
     */
    public String getPricebookCateoryies() {
        return pricebookCateoryies;
    }

    /**
     * @param pricebookCateoryies the pricebookCateoryies to set
     */
    public void setPricebookCateoryies(String pricebookCateoryies) {
        this.pricebookCateoryies = pricebookCateoryies;
    }

    /**
     * @return the spiffType
     */
    public int getSpiffType() {
        return spiffType;
    }

    /**
     * @param spiffType the spiffType to set
     */
    public void setSpiffType(int spiffType) {
        this.spiffType = spiffType;
    }

    public String getAvailType() {
        return availType;
    }

    public void setAvailType(String availType) {
        this.availType = availType;
    }
}
