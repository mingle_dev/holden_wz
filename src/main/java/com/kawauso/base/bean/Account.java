package com.kawauso.base.bean;

import java.math.BigDecimal;

public class Account {
    private int cono;
    private String name, custType, termsType, termsDescrip;
    private BigDecimal custno;
    private String address1, address2, city, state, zip, statusType;
    private BigDecimal codBal, credLimit, downPayamt;
    private BigDecimal futBal, futInvBal, lastPayamt, lastStmtBal, ordBal;
    private BigDecimal prStmtBal, returnsytd, salesYtd, servChgYtd;
    private BigDecimal lastSalesYtd;
    private BigDecimal servChgBal, unCashBal, unearnedYtd, totalBal;

    private BigDecimal period1Bal, period2Bal, period3Bal, period4Bal, period5Bal;
    private String period1Text, period2Text, period3Text, period4Text, period5Text;

    private String phoneNumber;
    private String salesRep;
    private String whse;

    private String customerPoNo;
    private String defaultShipTo;
    private String salesTerritory;
    private String slsRepOut;
    private boolean shipToRequiredFlag;
    private boolean poRequiredFlag;

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the custType
     */
    public String getCustType() {
        return custType;
    }

    /**
     * @param custType the custType to set
     */
    public void setCustType(String custType) {
        this.custType = custType;
    }

    /**
     * @return the termsType
     */
    public String getTermsType() {
        return termsType;
    }

    /**
     * @param termsType the termsType to set
     */
    public void setTermsType(String termsType) {
        this.termsType = termsType;
    }

    /**
     * @return the termsDescrip
     */
    public String getTermsDescrip() {
        return termsDescrip;
    }

    /**
     * @param termsDescrip the termsDescrip to set
     */
    public void setTermsDescrip(String termsDescrip) {
        this.termsDescrip = termsDescrip;
    }

    /**
     * @return the custno
     */
    public BigDecimal getCustno() {
        return custno;
    }

    /**
     * @param custno the custno to set
     */
    public void setCustno(BigDecimal custno) {
        this.custno = custno;
    }

    /**
     * @return the codBal
     */
    public BigDecimal getCodBal() {
        return codBal;
    }

    /**
     * @param codBal the codBal to set
     */
    public void setCodBal(BigDecimal codBal) {
        this.codBal = codBal;
    }

    /**
     * @return the credLimit
     */
    public BigDecimal getCredLimit() {
        return credLimit;
    }

    /**
     * @param credLimit the credLimit to set
     */
    public void setCredLimit(BigDecimal credLimit) {
        this.credLimit = credLimit;
    }

    /**
     * @return the downPayamt
     */
    public BigDecimal getDownPayamt() {
        return downPayamt;
    }

    /**
     * @param downPayamt the downPayamt to set
     */
    public void setDownPayamt(BigDecimal downPayamt) {
        this.downPayamt = downPayamt;
    }

    /**
     * @return the futBal
     */
    public BigDecimal getFutBal() {
        return futBal;
    }

    /**
     * @param futBal the futBal to set
     */
    public void setFutBal(BigDecimal futBal) {
        this.futBal = futBal;
    }

    /**
     * @return the futInvBal
     */
    public BigDecimal getFutInvBal() {
        return futInvBal;
    }

    /**
     * @param futInvBal the futInvBal to set
     */
    public void setFutInvBal(BigDecimal futInvBal) {
        this.futInvBal = futInvBal;
    }

    /**
     * @return the lastPayamt
     */
    public BigDecimal getLastPayamt() {
        return lastPayamt;
    }

    /**
     * @param lastPayamt the lastPayamt to set
     */
    public void setLastPayamt(BigDecimal lastPayamt) {
        this.lastPayamt = lastPayamt;
    }

    /**
     * @return the lastStmtBal
     */
    public BigDecimal getLastStmtBal() {
        return lastStmtBal;
    }

    /**
     * @param lastStmtBal the lastStmtBal to set
     */
    public void setLastStmtBal(BigDecimal lastStmtBal) {
        this.lastStmtBal = lastStmtBal;
    }

    /**
     * @return the ordBal
     */
    public BigDecimal getOrdBal() {
        return ordBal;
    }

    /**
     * @param ordBal the ordBal to set
     */
    public void setOrdBal(BigDecimal ordBal) {
        this.ordBal = ordBal;
    }

    /**
     * @return the prStmtBal
     */
    public BigDecimal getPrStmtBal() {
        return prStmtBal;
    }

    /**
     * @param prStmtBal the prStmtBal to set
     */
    public void setPrStmtBal(BigDecimal prStmtBal) {
        this.prStmtBal = prStmtBal;
    }

    /**
     * @return the returnsytd
     */
    public BigDecimal getReturnsytd() {
        return returnsytd;
    }

    /**
     * @param returnsytd the returnsytd to set
     */
    public void setReturnsytd(BigDecimal returnsytd) {
        this.returnsytd = returnsytd;
    }

    /**
     * @return the salesYtd
     */
    public BigDecimal getSalesYtd() {
        return salesYtd;
    }

    /**
     * @param salesYtd the salesYtd to set
     */
    public void setSalesYtd(BigDecimal salesYtd) {
        this.salesYtd = salesYtd;
    }

    /**
     * @return the servChgYtd
     */
    public BigDecimal getServChgYtd() {
        return servChgYtd;
    }

    /**
     * @param servChgYtd the servChgYtd to set
     */
    public void setServChgYtd(BigDecimal servChgYtd) {
        this.servChgYtd = servChgYtd;
    }

    /**
     * @return the servChgBal
     */
    public BigDecimal getServChgBal() {
        return servChgBal;
    }

    /**
     * @param servChgBal the servChgBal to set
     */
    public void setServChgBal(BigDecimal servChgBal) {
        this.servChgBal = servChgBal;
    }

    /**
     * @return the unCashBal
     */
    public BigDecimal getUnCashBal() {
        return unCashBal;
    }

    /**
     * @param unCashBal the unCashBal to set
     */
    public void setUnCashBal(BigDecimal unCashBal) {
        this.unCashBal = unCashBal;
    }

    /**
     * @return the unearnedYtd
     */
    public BigDecimal getUnearnedYtd() {
        return unearnedYtd;
    }

    /**
     * @param unearnedYtd the unearnedYtd to set
     */
    public void setUnearnedYtd(BigDecimal unearnedYtd) {
        this.unearnedYtd = unearnedYtd;
    }

    /**
     * @return the period1Bal
     */
    public BigDecimal getPeriod1Bal() {
        return period1Bal;
    }

    /**
     * @param period1Bal the period1Bal to set
     */
    public void setPeriod1Bal(BigDecimal period1Bal) {
        this.period1Bal = period1Bal;
    }

    /**
     * @return the period2Bal
     */
    public BigDecimal getPeriod2Bal() {
        return period2Bal;
    }

    /**
     * @param period2Bal the period2Bal to set
     */
    public void setPeriod2Bal(BigDecimal period2Bal) {
        this.period2Bal = period2Bal;
    }

    /**
     * @return the period3Bal
     */
    public BigDecimal getPeriod3Bal() {
        return period3Bal;
    }

    /**
     * @param period3Bal the period3Bal to set
     */
    public void setPeriod3Bal(BigDecimal period3Bal) {
        this.period3Bal = period3Bal;
    }

    /**
     * @return the period4Bal
     */
    public BigDecimal getPeriod4Bal() {
        return period4Bal;
    }

    /**
     * @param period4Bal the period4Bal to set
     */
    public void setPeriod4Bal(BigDecimal period4Bal) {
        this.period4Bal = period4Bal;
    }

    /**
     * @return the period5Bal
     */
    public BigDecimal getPeriod5Bal() {
        return period5Bal;
    }

    /**
     * @param period5Bal the period5Bal to set
     */
    public void setPeriod5Bal(BigDecimal period5Bal) {
        this.period5Bal = period5Bal;
    }

    /**
     * @return the period1Text
     */
    public String getPeriod1Text() {
        return period1Text;
    }

    /**
     * @param period1Text the period1Text to set
     */
    public void setPeriod1Text(String period1Text) {
        this.period1Text = period1Text;
    }

    /**
     * @return the period2Text
     */
    public String getPeriod2Text() {
        return period2Text;
    }

    /**
     * @param period2Text the period2Text to set
     */
    public void setPeriod2Text(String period2Text) {
        this.period2Text = period2Text;
    }

    /**
     * @return the period3Text
     */
    public String getPeriod3Text() {
        return period3Text;
    }

    /**
     * @param period3Text the period3Text to set
     */
    public void setPeriod3Text(String period3Text) {
        this.period3Text = period3Text;
    }

    /**
     * @return the period4Text
     */
    public String getPeriod4Text() {
        return period4Text;
    }

    /**
     * @param period4Text the period4Text to set
     */
    public void setPeriod4Text(String period4Text) {
        this.period4Text = period4Text;
    }

    /**
     * @return the period5Text
     */
    public String getPeriod5Text() {
        return period5Text;
    }

    /**
     * @param period5Text the period5Text to set
     */
    public void setPeriod5Text(String period5Text) {
        this.period5Text = period5Text;
    }

    /**
     * @return the totalBal
     */
    public BigDecimal getTotalBal() {
        return totalBal;
    }

    /**
     * @param totalBal the totalBal to set
     */
    public void setTotalBal(BigDecimal totalBal) {
        this.totalBal = totalBal;
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the statusType
     */
    public String getStatusType() {
        return statusType;
    }

    /**
     * @param statusType the statusType to set
     */
    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    /**
     * @return the salesRep
     */
    public String getSalesRep() {
        return salesRep;
    }

    /**
     * @param salesRep the salesRep to set
     */
    public void setSalesRep(String salesRep) {
        this.salesRep = salesRep;
    }

    /**
     * @return the whse
     */
    public String getWhse() {
        return whse;
    }

    /**
     * @param whse the whse to set
     */
    public void setWhse(String whse) {
        this.whse = whse;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public BigDecimal getLastSalesYtd() {
        return lastSalesYtd;
    }

    public void setLastSalesYtd(BigDecimal lastSalesYtd) {
        this.lastSalesYtd = lastSalesYtd;
    }

    public String getDefaultShipTo() {
        return defaultShipTo;
    }

    public void setDefaultShipTo(String defaultShipTo) {
        this.defaultShipTo = defaultShipTo;
    }

    public String getSalesTerritory() {
        return salesTerritory;
    }

    public void setSalesTerritory(String salesTerritory) {
        this.salesTerritory = salesTerritory;
    }

    public String getSlsRepOut() {
        return slsRepOut;
    }

    public void setSlsRepOut(String slsRepOut) {
        this.slsRepOut = slsRepOut;
    }

    public boolean isShipToRequiredFlag() {
        return shipToRequiredFlag;
    }

    public void setShipToRequiredFlag(boolean shipToRequiredFlag) {
        this.shipToRequiredFlag = shipToRequiredFlag;
    }

    public boolean isPoRequiredFlag() {
        return poRequiredFlag;
    }

    public void setPoRequiredFlag(boolean poRequiredFlag) {
        this.poRequiredFlag = poRequiredFlag;
    }

    public String getCustomerPoNo() {
        return customerPoNo;
    }

    public void setCustomerPoNo(String customerPoNo) {
        this.customerPoNo = customerPoNo;
    }
}