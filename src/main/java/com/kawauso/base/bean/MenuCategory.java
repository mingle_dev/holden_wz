/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "getProductMenuSubTree",
	query = "{CALL usp_getProductMenuSubTree(:id)}",
	resultClass = MenuCategory.class
	)
})
@Entity
@Table(name = "productMenu")
public class MenuCategory implements Serializable
{
    @Id
    @GeneratedValue
    @Column(name = "id")
    private String id;
    //private int id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "image")
    private String image;
    
    @Column(name = "children")
    private int children;
    
    @Column(name = "products")
    private int numProducts;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the children
     */
    public int getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(int children) {
        this.children = children;
    }

    /**
     * @return the numProducts
     */
    public int getNumProducts() {
        return numProducts;
    }

    /**
     * @param numProducts the numProducts to set
     */
    public void setNumProducts(int numProducts) {
        this.numProducts = numProducts;
    }
}
