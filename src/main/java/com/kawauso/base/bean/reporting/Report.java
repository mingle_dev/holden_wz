/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean.reporting;

import java.util.List;

/**
 *
 * @author chris
 */
public class Report
{
    private DataField[] fields;
    private List results;

    /**
     * @return the fields
     */
    public DataField[] getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(DataField[] fields) {
        this.fields = fields;
    }

    /**
     * @return the results
     */
    public List getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List results) {
        this.results = results;
    }
}
