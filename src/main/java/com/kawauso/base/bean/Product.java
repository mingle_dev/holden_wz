/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "view_products")
public class Product implements Serializable
{
    @Id
    @GeneratedValue
    @Column(name = "id")    
    private String id;
    
    @Column(name = "cono")
    private int cono;
    
    @Column(name = "prod")
    private String product;

    @Column(name = "status")
    private String status;
    
    @Column(name = "category_id")
    private int categoryId;
    
    @Column(name = "image")
    private String image;
    
    @Column(name = "netavail")
    private int netAvail;
    
    @Column(name = "description")
    private String description;

    @Column(name = "details")
    private String details;

    @Column(name = "prodcat")
    private String prodCat;

    @Transient
    private String userDescription;
    
    @Transient
    private BigDecimal price;

    //the warehouse used to calculate the price
    @Transient
    private String priceWhse;    
    
    @Transient
    private BigDecimal avail;
    
    @Transient
    private String vendor;
    
    @Transient
    private ProductPDRecord pdRecord;
    
    @Transient
    private String type;
    
    //@Formula("netavail * 0.2")
    //private int weight;
    
    public Product() {}
    
    public Product(int cono, String product) {
        this.cono = cono;
        this.product = product;
    }
    
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the netAvail
     */
    public int getNetAvail() {
        return netAvail;
    }

    /**
     * @param netAvail the netAvail to set
     */
    public void setNetAvail(int netAvail) {
        this.netAvail = netAvail;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the avail
     */
    public BigDecimal getAvail() {
        return avail;
    }

    /**
     * @param avail the avail to set
     */
    public void setAvail(BigDecimal avail) {
        this.avail = avail;
    }

    /**
     * @return the details
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(String details) {
        this.details = details;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(other == null) return false;
        if(other == this) return true;
        if(!(other instanceof Product)) return false;
        
        Product otherProduct = (Product) other;

        return product.equals(otherProduct.product);

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.cono;
        hash = 11 * hash + (this.product != null ? this.product.hashCode() : 0);
        return hash;
    }

    /**
     * @return the userDescription
     */
    public String getUserDescription() {
        return userDescription;
    }

    /**
     * @param userDescription the userDescription to set
     */
    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    /**
     * @return the priceWhse
     */
    public String getPriceWhse() {
        return priceWhse;
    }

    /**
     * @param priceWhse the priceWhse to set
     */
    public void setPriceWhse(String priceWhse) {
        this.priceWhse = priceWhse;
    }

    /**
     * @return the vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the pdRecord
     */
    public ProductPDRecord getPdRecord() {
        return pdRecord;
    }

    /**
     * @param pdRecord the pdRecord to set
     */
    public void setPdRecord(ProductPDRecord pdRecord) {
        this.pdRecord = pdRecord;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getProdCat() {
        return prodCat;
    }

    public void setProdCat(String prodCat) {
        this.prodCat = prodCat;
    }
}