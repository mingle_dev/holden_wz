/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author chris.weaver
 */
@Document
public class JobApplication
{
    @Id
    private String id;
    
    private String jobId;
    
    private int step;
    
    private String firstName, middleName, lastName;
    private String homePhone, cellPhone, workPhone, emailAddr;
    private String streetAddr, city, state, zip;
    
    private String position;
    
    private Date beginDate;
    private String desiredWage;
    
    private boolean eligible, overtime, recall, validLicense, discharged;
    private boolean contactEmployer, felony, prevEmploy, relatives;
    private String relativeName;
    
    private String eduHighSchool, eduHighState;
    private boolean eduHighDiploma;
    private String eduUgradSchool, eduUgradState, eduUgradField, eduUgradDegree;
    private String eduGradSchool, eduGradState, eduGradField, eduGradDegree;
    private String eduTradeSchool, eduTradeState, eduTradeField, eduTradeDegree;
    
    private String certifications, skills, refer, hear;
    
    private boolean certifyFacts;
    
    private List<JobApplicationEmployer> previousJobs;

    public JobApplication() {
    }
    
    public JobApplication(String jobId) {
        this.jobId = jobId;
    }
    
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the homePhone
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * @param homePhone the homePhone to set
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * @return the workPhone
     */
    public String getWorkPhone() {
        return workPhone;
    }

    /**
     * @param workPhone the workPhone to set
     */
    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * @param emailAddr the emailAddr to set
     */
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    /**
     * @return the streetAddr
     */
    public String getStreetAddr() {
        return streetAddr;
    }

    /**
     * @param streetAddr the streetAddr to set
     */
    public void setStreetAddr(String streetAddr) {
        this.streetAddr = streetAddr;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the beginDate
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * @return the desiredWage
     */
    public String getDesiredWage() {
        return desiredWage;
    }

    /**
     * @param desiredWage the desiredWage to set
     */
    public void setDesiredWage(String desiredWage) {
        this.desiredWage = desiredWage;
    }

    /**
     * @return the eligible
     */
    public boolean isEligible() {
        return eligible;
    }

    /**
     * @param eligible the eligible to set
     */
    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }

    /**
     * @return the overtime
     */
    public boolean isOvertime() {
        return overtime;
    }

    /**
     * @param overtime the overtime to set
     */
    public void setOvertime(boolean overtime) {
        this.overtime = overtime;
    }

    /**
     * @return the recall
     */
    public boolean isRecall() {
        return recall;
    }

    /**
     * @param recall the recall to set
     */
    public void setRecall(boolean recall) {
        this.recall = recall;
    }

    /**
     * @return the validLicense
     */
    public boolean isValidLicense() {
        return validLicense;
    }

    /**
     * @param validLicense the validLicense to set
     */
    public void setValidLicense(boolean validLicense) {
        this.validLicense = validLicense;
    }

    /**
     * @return the discharged
     */
    public boolean isDischarged() {
        return discharged;
    }

    /**
     * @param discharged the discharged to set
     */
    public void setDischarged(boolean discharged) {
        this.discharged = discharged;
    }

    /**
     * @return the contactEmployer
     */
    public boolean isContactEmployer() {
        return contactEmployer;
    }

    /**
     * @param contactEmployer the contactEmployer to set
     */
    public void setContactEmployer(boolean contactEmployer) {
        this.contactEmployer = contactEmployer;
    }

    /**
     * @return the felony
     */
    public boolean isFelony() {
        return felony;
    }

    /**
     * @param felony the felony to set
     */
    public void setFelony(boolean felony) {
        this.felony = felony;
    }

    /**
     * @return the prevEmploy
     */
    public boolean isPrevEmploy() {
        return prevEmploy;
    }

    /**
     * @param prevEmploy the prevEmploy to set
     */
    public void setPrevEmploy(boolean prevEmploy) {
        this.prevEmploy = prevEmploy;
    }

    /**
     * @return the relatives
     */
    public boolean isRelatives() {
        return relatives;
    }

    /**
     * @param relatives the relatives to set
     */
    public void setRelatives(boolean relatives) {
        this.relatives = relatives;
    }

    /**
     * @return the relativeName
     */
    public String getRelativeName() {
        return relativeName;
    }

    /**
     * @param relativeName the relativeName to set
     */
    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    /**
     * @return the eduHighSchool
     */
    public String getEduHighSchool() {
        return eduHighSchool;
    }

    /**
     * @param eduHighSchool the eduHighSchool to set
     */
    public void setEduHighSchool(String eduHighSchool) {
        this.eduHighSchool = eduHighSchool;
    }

    /**
     * @return the eduHighState
     */
    public String getEduHighState() {
        return eduHighState;
    }

    /**
     * @param eduHighState the eduHighState to set
     */
    public void setEduHighState(String eduHighState) {
        this.eduHighState = eduHighState;
    }

    /**
     * @return the eduUgradSchool
     */
    public String getEduUgradSchool() {
        return eduUgradSchool;
    }

    /**
     * @param eduUgradSchool the eduUgradSchool to set
     */
    public void setEduUgradSchool(String eduUgradSchool) {
        this.eduUgradSchool = eduUgradSchool;
    }

    /**
     * @return the eduUgradState
     */
    public String getEduUgradState() {
        return eduUgradState;
    }

    /**
     * @param eduUgradState the eduUgradState to set
     */
    public void setEduUgradState(String eduUgradState) {
        this.eduUgradState = eduUgradState;
    }

    /**
     * @return the eduUgradField
     */
    public String getEduUgradField() {
        return eduUgradField;
    }

    /**
     * @param eduUgradField the eduUgradField to set
     */
    public void setEduUgradField(String eduUgradField) {
        this.eduUgradField = eduUgradField;
    }

    /**
     * @return the eduUgradDegree
     */
    public String getEduUgradDegree() {
        return eduUgradDegree;
    }

    /**
     * @param eduUgradDegree the eduUgradDegree to set
     */
    public void setEduUgradDegree(String eduUgradDegree) {
        this.eduUgradDegree = eduUgradDegree;
    }

    /**
     * @return the eduGradSchool
     */
    public String getEduGradSchool() {
        return eduGradSchool;
    }

    /**
     * @param eduGradSchool the eduGradSchool to set
     */
    public void setEduGradSchool(String eduGradSchool) {
        this.eduGradSchool = eduGradSchool;
    }

    /**
     * @return the eduGradState
     */
    public String getEduGradState() {
        return eduGradState;
    }

    /**
     * @param eduGradState the eduGradState to set
     */
    public void setEduGradState(String eduGradState) {
        this.eduGradState = eduGradState;
    }

    /**
     * @return the eduGradField
     */
    public String getEduGradField() {
        return eduGradField;
    }

    /**
     * @param eduGradField the eduGradField to set
     */
    public void setEduGradField(String eduGradField) {
        this.eduGradField = eduGradField;
    }

    /**
     * @return the eduGradDegree
     */
    public String getEduGradDegree() {
        return eduGradDegree;
    }

    /**
     * @param eduGradDegree the eduGradDegree to set
     */
    public void setEduGradDegree(String eduGradDegree) {
        this.eduGradDegree = eduGradDegree;
    }

    /**
     * @return the eduTradeSchool
     */
    public String getEduTradeSchool() {
        return eduTradeSchool;
    }

    /**
     * @param eduTradeSchool the eduTradeSchool to set
     */
    public void setEduTradeSchool(String eduTradeSchool) {
        this.eduTradeSchool = eduTradeSchool;
    }

    /**
     * @return the eduTradeState
     */
    public String getEduTradeState() {
        return eduTradeState;
    }

    /**
     * @param eduTradeState the eduTradeState to set
     */
    public void setEduTradeState(String eduTradeState) {
        this.eduTradeState = eduTradeState;
    }

    /**
     * @return the eduTradeField
     */
    public String getEduTradeField() {
        return eduTradeField;
    }

    /**
     * @param eduTradeField the eduTradeField to set
     */
    public void setEduTradeField(String eduTradeField) {
        this.eduTradeField = eduTradeField;
    }

    /**
     * @return the eduTradeDegree
     */
    public String getEduTradeDegree() {
        return eduTradeDegree;
    }

    /**
     * @param eduTradeDegree the eduTradeDegree to set
     */
    public void setEduTradeDegree(String eduTradeDegree) {
        this.eduTradeDegree = eduTradeDegree;
    }

    /**
     * @return the certifications
     */
    public String getCertifications() {
        return certifications;
    }

    /**
     * @param certifications the certifications to set
     */
    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    /**
     * @return the skills
     */
    public String getSkills() {
        return skills;
    }

    /**
     * @param skills the skills to set
     */
    public void setSkills(String skills) {
        this.skills = skills;
    }

    /**
     * @return the refer
     */
    public String getRefer() {
        return refer;
    }

    /**
     * @param refer the refer to set
     */
    public void setRefer(String refer) {
        this.refer = refer;
    }

    /**
     * @return the hear
     */
    public String getHear() {
        return hear;
    }

    /**
     * @param hear the hear to set
     */
    public void setHear(String hear) {
        this.hear = hear;
    }

    /**
     * @return the certifyFacts
     */
    public boolean isCertifyFacts() {
        return certifyFacts;
    }

    /**
     * @param certifyFacts the certifyFacts to set
     */
    public void setCertifyFacts(boolean certifyFacts) {
        this.certifyFacts = certifyFacts;
    }

    /**
     * @return the previousJobs
     */
    public List<JobApplicationEmployer> getPreviousJobs() {
        return previousJobs;
    }

    /**
     * @param previousJobs the previousJobs to set
     */
    public void setPreviousJobs(List<JobApplicationEmployer> previousJobs) {
        this.previousJobs = previousJobs;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the cellPhone
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * @param cellPhone the cellPhone to set
     */
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the eduHighDiploma
     */
    public boolean isEduHighDiploma() {
        return eduHighDiploma;
    }

    /**
     * @param eduHighDiploma the eduHighDiploma to set
     */
    public void setEduHighDiploma(boolean eduHighDiploma) {
        this.eduHighDiploma = eduHighDiploma;
    }

    /**
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
}
