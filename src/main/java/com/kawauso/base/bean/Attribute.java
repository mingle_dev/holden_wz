/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chris.weaver
 */
public class Attribute
{
    private String name;
    private List<AttributeValue> values = null;

    public Attribute() {
        values = new ArrayList<AttributeValue>();
    }

    public Attribute(String name) {
        this.name = name;
        this.values = new ArrayList<AttributeValue>();
    }

    public String getName() {
        return name;
    }


    /**
     *
     * @param name
     * @param checked
     * @return
     */
    public boolean add(String name, boolean checked) {
        AttributeValue v = new AttributeValue(name,checked);
        if(values.contains(v))
             return false;

        return values.add(v);
    }

    /**
     *
     * @param name
     * @return
     */
    public boolean mark(String name)
    {
        //not the most efficient way to search an array but the
        //max size is around 10 so don't expect to be an issue
        for(AttributeValue v : values) {
            if(v.getName().equals(name)) {
                v.setChecked(true);
                return true;
            }
        }
       return false;
    }

    public List<AttributeValue> getValues()
    {
        return values;
    }

    @Override
    public boolean equals(Object other) {
        return this.name.equals(((Attribute) other).name);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
}