/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "savelist_items")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class ListItem implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "uuid")    
    private String id;
    
    //@Column(name = "categoryid")
    //private String categoryId;
    
    @Column(name = "productCode")
    private String productCode;
    
    @Column(name = "defaultqty")
    private int qty;

    @Column(name = "displayOrder")
    private int displayOrder;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryid", nullable = false)
    private ListCategory listCategory;
    
    @Transient
    private Product product;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

//    /**
//     * @return the categoryId
//     */
//    public String getCategoryId() {
//        return categoryId;
//    }
//
//    /**
//     * @param categoryId the categoryId to set
//     */
//    public void setCategoryId(String categoryId) {
//        this.categoryId = categoryId;
//    }

    /**
     * @return the product
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param product the product to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the qty
     */
    public int getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * @return the listCategory
     */
    public ListCategory getListCategory() {
        return listCategory;
    }

    /**
     * @param listCategory the listCategory to set
     */
    public void setListCategory(ListCategory listCategory) {
        this.listCategory = listCategory;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the displayOrder
     */
    public int getDisplayOrder() {
        return displayOrder;
    }

    /**
     * @param displayOrder the displayOrder to set
     */
    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }
}