/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author chris.weaver
 */
@Entity
@Table(name = "nxt_oeel")
public class OrderLine implements Serializable
{
    @Id
    @Column(name = "id", insertable = false, updatable = false)
    private String id;

    @Column(name = "line_no", insertable = false, updatable = false)
    private int lineNumber;    
    
    @Column(name = "shipprod", insertable = false, updatable = false)
    private String shipProd;
    
    @Column(name = "qtyord", insertable = false, updatable = false)
    private BigDecimal qtyOrder;
    
    @Column(name = "qtyship", insertable = false, updatable = false)
    private BigDecimal qtyShip;    
    
    //@Column(name = "description", insertable = false, updatable = false)
    @Transient
    private String description; //<-- this comes from icsp
    
    @Column(name = "price", insertable = false, updatable = false)
    private BigDecimal price;  
    
    @Column(name = "netamt", insertable = false, updatable = false)
    private BigDecimal netAmount;      
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "oeeh_id", nullable = false)
    private Order order;

    @Transient
    private List<String> serialNumbers;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the shipProd
     */
    public String getShipProd() {
        return shipProd;
    }

    /**
     * @param shipProd the shipProd to set
     */
    public void setShipProd(String shipProd) {
        this.shipProd = shipProd;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the qtyOrder
     */
    public BigDecimal getQtyOrder() {
        return qtyOrder;
    }

    /**
     * @param qtyOrder the qtyOrder to set
     */
    public void setQtyOrder(BigDecimal qtyOrder) {
        this.qtyOrder = qtyOrder;
    }

    /**
     * @return the qtyShip
     */
    public BigDecimal getQtyShip() {
        return qtyShip;
    }

    /**
     * @param qtyShip the qtyShip to set
     */
    public void setQtyShip(BigDecimal qtyShip) {
        this.qtyShip = qtyShip;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the netAmount
     */
    public BigDecimal getNetAmount() {
        return netAmount;
    }

    /**
     * @param netAmount the netAmount to set
     */
    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    /**
     * @return the lineNumber
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * @param lineNumber the lineNumber to set
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public List<String> getSerialNumbers() {
        return serialNumbers;
    }

    public void setSerialNumbers(List<String> serialNumbers) {
        this.serialNumbers = serialNumbers;
    }

    public void addSerialNumber(String serial) {
        if(this.serialNumbers == null)
            this.serialNumbers = new ArrayList<String>();

        this.serialNumbers.add(serial);
    }
}
