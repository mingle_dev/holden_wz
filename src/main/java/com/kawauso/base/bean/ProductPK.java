/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author chris.weaver
 */
@Embeddable
public class ProductPK implements Serializable
{
    @Column(name = "cono")
    private int cono;
    
    @Column(name = "prod")
    private String productCode;

    public ProductPK() {}
    
    public ProductPK(int cono, String productCode) {
        this.cono = cono;
        this.productCode = productCode;
    }
    
    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

}
