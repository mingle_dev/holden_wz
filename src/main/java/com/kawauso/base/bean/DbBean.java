/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.math.BigDecimal;

/**
 *
 * @author chris.weaver
 */
public class DbBean
{
    private Integer recordNumber, recordsFound;

    /**
     * @return the recordNumber
     */
    public Integer getRecordNumber() {
        return recordNumber;
    }

    /**
     * @param recordNumber the recordNumber to set
     */
    public void setRecordNumber(Integer recordNumber) {
        this.recordNumber = recordNumber;
    }

    /**
     * @return the recordsFound
     */
    public Integer getRecordsFound() {
        return recordsFound;
    }

    /**
     * @param recordsFound the recordsFound to set
     */
    public void setRecordsFound(Integer recordsFound) {
        this.recordsFound = recordsFound;
    }

}
