/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 *
 * @author chris.weaver
 */
@Entity
@Table(name = "userFavorite")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class UserFavorite
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")
    private String id;
    
    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "url")
    private String url;
    
    @Column(name = "name")
    private String name;

    @Column(name = "link_ico")
    private String linkIcon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkIcon() {
        return linkIcon;
    }

    public void setLinkIcon(String linkIcon) {
        this.linkIcon = linkIcon;
    }
}
