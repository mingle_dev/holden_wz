/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "view_warehouses")
public class Location implements Serializable
{
    @Id
    @GeneratedValue
    @Column(name = "prrowid", insertable = false, updatable = false)    
    private String id;
    
    @Column(name = "salesfl", insertable = false, updatable = false)
    private int salesAllowed;
    
    @Column(name = "user1", insertable = false, updatable = false)
    private String user1;    
    
    @Column(name = "cono", insertable = false, updatable = false)
    private int cono;
    
    @Column(name = "whse", insertable = false, updatable = false)
    private String whseId;
    
    @Column(name = "city", insertable = false, updatable = false)
    private String whseName;

    @Column(name = "phoneno", insertable = false, updatable = false)
    private String phoneNo;

    @Column(name = "addr_1", insertable = false, updatable = false)
    private String addr;
    
    @Column(name = "state", insertable = false, updatable = false)
    private String state;    
    
    @Column(name = "zipcd", insertable = false, updatable = false)
    private String zipCd;    
    
    @Column(name = "store_emailaddr", insertable = false, updatable = false)
    private String emailAddr;    
    
    @Column(name = "longitude", insertable = false, updatable = false)
    private Double longitude;
    
    @Column(name = "latitude", insertable = false, updatable = false)
    private Double latitude;
    
    @Transient
    private double distance;
    
    public Location() {}
    
    public Location(int cono, String whseId) {
        this.cono = cono;
        this.whseId = whseId;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @return the whseId
     */
    public String getWhseId() {
        return whseId;
    }

    /**
     * @return the whseName
     */
    public String getWhseName() {
        return whseName;
    }

    /**
     * @return the phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * @return the salesAllowed
     */
    public int getSalesAllowed() {
        return salesAllowed;
    }

    /**
     * @return the addr
     */
    public String getAddr() {
        return addr;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @return the zipCd
     */
    public String getZipCd() {
        return zipCd;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(other == null) return false;
        if(other == this) return true;
        if(!(other instanceof Location)) return false;
        
        Location that = (Location) other;

        return this.cono == that.cono && this.whseId.equals(that.whseId);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.cono;
        hash = 17 * hash + (this.whseId != null ? this.whseId.hashCode() : 0);
        return hash;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    /**
     * @return the user1
     */
    public String getUser1() {
        return user1;
    }

    /**
     * @param user1 the user1 to set
     */
    public void setUser1(String user1) {
        this.user1 = user1;
    }
}