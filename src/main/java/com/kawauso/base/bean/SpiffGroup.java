/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kawauso.base.bean;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author chris.weaver
 */
@Entity
@Table(name = "spiff_prodgroups")
public class SpiffGroup
{
    @Id
    @GeneratedValue
    @Column(name = "id")    
    private String id;
    
    @Column(name = "program")
    private String program;
    
    @Column(name = "class")
    private String classification;
    
    @Column(name = "prod_group")
    private String prodGroup;
    
    @Transient
    private List<String> products;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the program
     */
    public String getProgram() {
        return program;
    }

    /**
     * @param program the program to set
     */
    public void setProgram(String program) {
        this.program = program;
    }

    /**
     * @return the classType
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classType the classType to set
     */
    public void setClassification(String classification) {
        this.classification = classification;
    }

    /**
     * @return the prodGroup
     */
    public String getProdGroup() {
        return prodGroup;
    }

    /**
     * @param prodGroup the prodGroup to set
     */
    public void setProdGroup(String prodGroup) {
        this.prodGroup = prodGroup;
    }

    /**
     * @return the products
     */
    public List<String> getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(List<String> products) {
        this.products = products;
    }
}
