/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

/**
 *
 * @author chris.weaver
 */
public class JsonResult<T>
{
    private boolean status;
    private String message;
    
    private T dataSet;

    /*
    List<Product> l = new ArrayList<Product>();
    Product p = new Product();
    p.setProduct(prod);
    l.add(p);

    Product p2 = new Product();
    p2.setProduct(prod);
    l.add(p2);        

    res.setDataSet(l);
    */    
    
    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the dataSet
     */
    public T getDataSet() {
        return dataSet;
    }

    /**
     * @param dataSet the dataSet to set
     */
    public void setDataSet(T dataSet) {
        this.dataSet = dataSet;
    }
}