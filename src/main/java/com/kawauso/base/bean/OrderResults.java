/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

/**
 *
 * @author chris.weaver
 */
public class OrderResults
{
    private String errorMsg, ackMsg, ackData;
    private int ackErrorNum;
    private Order orderDetails;
    
    /**
     * @return the ackMsg
     */
    public String getAckMsg() {
        return ackMsg;
    }

    /**
     * @param ackMsg the ackMsg to set
     */
    public void setAckMsg(String ackMsg) {
        this.ackMsg = ackMsg;
    }

    /**
     * @return the ackData
     */
    public String getAckData() {
        return ackData;
    }

    /**
     * @param ackData the ackData to set
     */
    public void setAckData(String ackData) {
        this.ackData = ackData;
    }

    /**
     * @return the ackErrorNum
     */
    public int getAckErrorNum() {
        return ackErrorNum;
    }

    /**
     * @param ackErrorNum the ackErrorNum to set
     */
    public void setAckErrorNum(int ackErrorNum) {
        this.ackErrorNum = ackErrorNum;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the orderDetails
     */
    public Order getOrderDetails() {
        return orderDetails;
    }

    /**
     * @param orderDetails the orderDetails to set
     */
    public void setOrderDetails(Order orderDetails) {
        this.orderDetails = orderDetails;
    }
}
