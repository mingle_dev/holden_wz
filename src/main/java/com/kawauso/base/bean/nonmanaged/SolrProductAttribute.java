/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean.nonmanaged;

/**
 *
 * @author chris
 */
public class SolrProductAttribute
{
    private String id;
        
    private String k;
    private String v;
    
    public SolrProductAttribute() {}
    
    public SolrProductAttribute(String k, String v)
    {
        this.k = k;
        this.v = v;
    }

    /**
     * @return the k
     */
    public String getK() {
        return k;
    }

    /**
     * @param k the k to set
     */
    public void setK(String k) {
        this.k = k;
    }

    /**
     * @return the v
     */
    public String getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(String v) {
        this.v = v;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
