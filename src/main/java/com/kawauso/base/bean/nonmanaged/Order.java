/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean.nonmanaged;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author chris.weaver
 */
public class Order
{
    private String query;
    
    private int recordNumber;
    
    private int recordsFound;

    private int cono;
    
    private int orderno;
    
    private int ordersuf;    
    
    private int custNo;        
    
    private String transType;
    
    private String shipTo;
    
    private String custPo;  
    
    private Date enterDt;
    
    private Date invoiceDt;

    private Date dueDt;
    
    private BigDecimal totalOrderAmt;
    
    private int stageCode;

    private String whse;

    private String shipVia;

    private String reference;

    private String instructions;

    private String termsDesc;
    
    private String soldToName;
    private String soldToAddr1;
    private String soldToAddr2;
    private String soldToCity;
    private String soldToState;
    private String soldToZip;
    
    private String shipToName;
    
    private String shipToAddr1;
    
    private String shipToAddr2;
    
    private String shipToCity;
    
    private String shipToState;
    
    private String shipToZip;
    
    private Date shipDt;
    
    private BigDecimal subTotal;
    
    private BigDecimal taxAmount;
    
    private BigDecimal invAmount;    

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the recordNumber
     */
    public int getRecordNumber() {
        return recordNumber;
    }

    /**
     * @param recordNumber the recordNumber to set
     */
    public void setRecordNumber(int recordNumber) {
        this.recordNumber = recordNumber;
    }

    /**
     * @return the recordsFound
     */
    public int getRecordsFound() {
        return recordsFound;
    }

    /**
     * @param recordsFound the recordsFound to set
     */
    public void setRecordsFound(int recordsFound) {
        this.recordsFound = recordsFound;
    }

    /**
     * @return the custNo
     */
    public int getCustNo() {
        return custNo;
    }

    /**
     * @param custNo the custNo to set
     */
    public void setCustNo(int custNo) {
        this.custNo = custNo;
    }

    /**
     * @return the transType
     */
    public String getTransType() {
        return transType;
    }

    /**
     * @param transType the transType to set
     */
    public void setTransType(String transType) {
        this.transType = transType;
    }

    /**
     * @return the shipTo
     */
    public String getShipTo() {
        return shipTo;
    }

    /**
     * @param shipTo the shipTo to set
     */
    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    /**
     * @return the custPo
     */
    public String getCustPo() {
        return custPo;
    }

    /**
     * @param custPo the custPo to set
     */
    public void setCustPo(String custPo) {
        this.custPo = custPo;
    }

    /**
     * @return the enterDt
     */
    public Date getEnterDt() {
        return enterDt;
    }

    /**
     * @param enterDt the enterDt to set
     */
    public void setEnterDt(Date enterDt) {
        this.enterDt = enterDt;
    }

    /**
     * @return the invoiceDt
     */
    public Date getInvoiceDt() {
        return invoiceDt;
    }

    /**
     * @param invoiceDt the invoiceDt to set
     */
    public void setInvoiceDt(Date invoiceDt) {
        this.invoiceDt = invoiceDt;
    }

    /**
     * @return the dueDt
     */
    public Date getDueDt() {
        return dueDt;
    }

    /**
     * @param dueDt the dueDt to set
     */
    public void setDueDt(Date dueDt) {
        this.dueDt = dueDt;
    }

    /**
     * @return the totalOrderAmt
     */
    public BigDecimal getTotalOrderAmt() {
        return totalOrderAmt;
    }

    /**
     * @param totalOrderAmt the totalOrderAmt to set
     */
    public void setTotalOrderAmt(BigDecimal totalOrderAmt) {
        this.totalOrderAmt = totalOrderAmt;
    }

    /**
     * @return the stageCode
     */
    public int getStageCode() {
        return stageCode;
    }

    /**
     * @param stageCode the stageCode to set
     */
    public void setStageCode(int stageCode) {
        this.stageCode = stageCode;
    }

    /**
     * @return the whse
     */
    public String getWhse() {
        return whse;
    }

    /**
     * @param whse the whse to set
     */
    public void setWhse(String whse) {
        this.whse = whse;
    }

    /**
     * @return the shipVia
     */
    public String getShipVia() {
        return shipVia;
    }

    /**
     * @param shipVia the shipVia to set
     */
    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * @param instructions the instructions to set
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     * @return the termsDesc
     */
    public String getTermsDesc() {
        return termsDesc;
    }

    /**
     * @param termsDesc the termsDesc to set
     */
    public void setTermsDesc(String termsDesc) {
        this.termsDesc = termsDesc;
    }

    /**
     * @return the soldToName
     */
    public String getSoldToName() {
        return soldToName;
    }

    /**
     * @param soldToName the soldToName to set
     */
    public void setSoldToName(String soldToName) {
        this.soldToName = soldToName;
    }

    /**
     * @return the soldToAddr1
     */
    public String getSoldToAddr1() {
        return soldToAddr1;
    }

    /**
     * @param soldToAddr1 the soldToAddr1 to set
     */
    public void setSoldToAddr1(String soldToAddr1) {
        this.soldToAddr1 = soldToAddr1;
    }

    /**
     * @return the soldToAddr2
     */
    public String getSoldToAddr2() {
        return soldToAddr2;
    }

    /**
     * @param soldToAddr2 the soldToAddr2 to set
     */
    public void setSoldToAddr2(String soldToAddr2) {
        this.soldToAddr2 = soldToAddr2;
    }

    /**
     * @return the soldToCity
     */
    public String getSoldToCity() {
        return soldToCity;
    }

    /**
     * @param soldToCity the soldToCity to set
     */
    public void setSoldToCity(String soldToCity) {
        this.soldToCity = soldToCity;
    }

    /**
     * @return the soldToState
     */
    public String getSoldToState() {
        return soldToState;
    }

    /**
     * @param soldToState the soldToState to set
     */
    public void setSoldToState(String soldToState) {
        this.soldToState = soldToState;
    }

    /**
     * @return the soldToZip
     */
    public String getSoldToZip() {
        return soldToZip;
    }

    /**
     * @param soldToZip the soldToZip to set
     */
    public void setSoldToZip(String soldToZip) {
        this.soldToZip = soldToZip;
    }

    /**
     * @return the shipToName
     */
    public String getShipToName() {
        return shipToName;
    }

    /**
     * @param shipToName the shipToName to set
     */
    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    /**
     * @return the shipToAddr1
     */
    public String getShipToAddr1() {
        return shipToAddr1;
    }

    /**
     * @param shipToAddr1 the shipToAddr1 to set
     */
    public void setShipToAddr1(String shipToAddr1) {
        this.shipToAddr1 = shipToAddr1;
    }

    /**
     * @return the shipToAddr2
     */
    public String getShipToAddr2() {
        return shipToAddr2;
    }

    /**
     * @param shipToAddr2 the shipToAddr2 to set
     */
    public void setShipToAddr2(String shipToAddr2) {
        this.shipToAddr2 = shipToAddr2;
    }

    /**
     * @return the shipToCity
     */
    public String getShipToCity() {
        return shipToCity;
    }

    /**
     * @param shipToCity the shipToCity to set
     */
    public void setShipToCity(String shipToCity) {
        this.shipToCity = shipToCity;
    }

    /**
     * @return the shipToState
     */
    public String getShipToState() {
        return shipToState;
    }

    /**
     * @param shipToState the shipToState to set
     */
    public void setShipToState(String shipToState) {
        this.shipToState = shipToState;
    }

    /**
     * @return the shipToZip
     */
    public String getShipToZip() {
        return shipToZip;
    }

    /**
     * @param shipToZip the shipToZip to set
     */
    public void setShipToZip(String shipToZip) {
        this.shipToZip = shipToZip;
    }

    /**
     * @return the shipDt
     */
    public Date getShipDt() {
        return shipDt;
    }

    /**
     * @param shipDt the shipDt to set
     */
    public void setShipDt(Date shipDt) {
        this.shipDt = shipDt;
    }

    /**
     * @return the subTotal
     */
    public BigDecimal getSubTotal() {
        return subTotal;
    }

    /**
     * @param subTotal the subTotal to set
     */
    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * @return the taxAmount
     */
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    /**
     * @param taxAmount the taxAmount to set
     */
    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * @return the invAmount
     */
    public BigDecimal getInvAmount() {
        return invAmount;
    }

    /**
     * @param invAmount the invAmount to set
     */
    public void setInvAmount(BigDecimal invAmount) {
        this.invAmount = invAmount;
    }

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the orderno
     */
    public int getOrderno() {
        return orderno;
    }

    /**
     * @param orderno the orderno to set
     */
    public void setOrderno(int orderno) {
        this.orderno = orderno;
    }

    /**
     * @param ordersuf the ordersuf to set
     */
    public void setOrdersuf(int ordersuf) {
        this.ordersuf = ordersuf;
    }
    
}
