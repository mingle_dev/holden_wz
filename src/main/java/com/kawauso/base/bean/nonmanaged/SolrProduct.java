/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean.nonmanaged;

import java.util.Map;
import org.apache.solr.client.solrj.beans.Field;

/**
 *
 * @author chris
 */
public class SolrProduct
{
    @Field("id")
    private String id;

    @Field("name")
    private String sku;

    @Field("cat")
    private String[] categories;    
    
    @Field("category")
    private String category;
    
    @Field("manu")
    private String manufacturer;
    
    @Field("description")
    private String description;

    @Field("subject")
    private String details;
    
    @Field("author")
    private String type;

    @Field("url")
    private String image;

    @Field("attr_*")
    private Map<String, String> attributes;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the categories
     */
    public String[] getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(String[] categories) {
        this.categories = categories;
    }

//    /**
//     * @return the attributes
//     */
//    public List<SolrProductAttribute> getAttributes() {
//        return attributes;
//    }
//
//    /**
//     * @param attributes the attributes to set
//     */
//    public void setAttributes(List<SolrProductAttribute> attributes) {
//        this.attributes = attributes;
//    }

    /**
     * @return the attributes
     */
    public Map<String, String> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku the sku to set
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the details
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}