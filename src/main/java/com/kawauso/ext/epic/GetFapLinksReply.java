
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FapLine" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FapLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FapLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fapLine"
})
@XmlRootElement(name = "GetFapLinksReply")
public class GetFapLinksReply {

    @XmlElement(name = "FapLine")
    protected List<GetFapLinksReply.FapLine> fapLine;

    /**
     * Gets the value of the fapLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fapLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFapLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetFapLinksReply.FapLine }
     * 
     * 
     */
    public List<GetFapLinksReply.FapLine> getFapLine() {
        if (fapLine == null) {
            fapLine = new ArrayList<GetFapLinksReply.FapLine>();
        }
        return this.fapLine;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FapLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FapLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fapLabel",
        "fapLink"
    })
    public static class FapLine {

        @XmlElement(name = "FapLabel", required = true)
        protected String fapLabel;
        @XmlElement(name = "FapLink", required = true)
        protected String fapLink;

        /**
         * Gets the value of the fapLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFapLabel() {
            return fapLabel;
        }

        /**
         * Sets the value of the fapLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFapLabel(String value) {
            this.fapLabel = value;
        }

        /**
         * Gets the value of the fapLink property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFapLink() {
            return fapLink;
        }

        /**
         * Sets the value of the fapLink property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFapLink(String value) {
            this.fapLink = value;
        }

    }

}
