
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserBrands" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UserCompanyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ListPartsType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userBrands",
    "userCompanyType",
    "partNumber",
    "listPartsType"
})
@XmlRootElement(name = "ListParts")
public class ListParts {

    @XmlElement(name = "UserBrands", required = true)
    protected String userBrands;
    @XmlElement(name = "UserCompanyType", required = true)
    protected String userCompanyType;
    @XmlElement(name = "PartNumber", required = true)
    protected String partNumber;
    @XmlElement(name = "ListPartsType")
    protected String listPartsType;

    /**
     * Gets the value of the userBrands property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserBrands() {
        return userBrands;
    }

    /**
     * Sets the value of the userBrands property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserBrands(String value) {
        this.userBrands = value;
    }

    /**
     * Gets the value of the userCompanyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCompanyType() {
        return userCompanyType;
    }

    /**
     * Sets the value of the userCompanyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCompanyType(String value) {
        this.userCompanyType = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumber(String value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the listPartsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListPartsType() {
        return listPartsType;
    }

    /**
     * Sets the value of the listPartsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListPartsType(String value) {
        this.listPartsType = value;
    }

}
