
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartsNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartsLine" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partsNumber",
    "partsDescription",
    "partsLine"
})
@XmlRootElement(name = "ListPartsReply")
public class ListPartsReply {

    @XmlElement(name = "PartsNumber", required = true)
    protected String partsNumber;
    @XmlElement(name = "PartsDescription", required = true)
    protected String partsDescription;
    @XmlElement(name = "PartsLine")
    protected List<ListPartsReply.PartsLine> partsLine;

    /**
     * Gets the value of the partsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartsNumber() {
        return partsNumber;
    }

    /**
     * Sets the value of the partsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartsNumber(String value) {
        this.partsNumber = value;
    }

    /**
     * Gets the value of the partsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartsDescription() {
        return partsDescription;
    }

    /**
     * Sets the value of the partsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartsDescription(String value) {
        this.partsDescription = value;
    }

    /**
     * Gets the value of the partsLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partsLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartsLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListPartsReply.PartsLine }
     * 
     * 
     */
    public List<ListPartsReply.PartsLine> getPartsLine() {
        if (partsLine == null) {
            partsLine = new ArrayList<ListPartsReply.PartsLine>();
        }
        return this.partsLine;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "partNumber",
        "partDescription",
        "partApplication",
        "partLink"
    })
    public static class PartsLine {

        @XmlElement(name = "PartNumber", required = true)
        protected String partNumber;
        @XmlElement(name = "PartDescription", required = true)
        protected String partDescription;
        @XmlElement(name = "PartApplication", required = true)
        protected String partApplication;
        @XmlElement(name = "PartLink", required = true)
        protected String partLink;

        /**
         * Gets the value of the partNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartNumber() {
            return partNumber;
        }

        /**
         * Sets the value of the partNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartNumber(String value) {
            this.partNumber = value;
        }

        /**
         * Gets the value of the partDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartDescription() {
            return partDescription;
        }

        /**
         * Sets the value of the partDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartDescription(String value) {
            this.partDescription = value;
        }

        /**
         * Gets the value of the partApplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartApplication() {
            return partApplication;
        }

        /**
         * Sets the value of the partApplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartApplication(String value) {
            this.partApplication = value;
        }

        /**
         * Gets the value of the partLink property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartLink() {
            return partLink;
        }

        /**
         * Sets the value of the partLink property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartLink(String value) {
            this.partLink = value;
        }

    }

}
