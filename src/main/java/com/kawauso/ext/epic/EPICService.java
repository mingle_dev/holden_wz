
package com.kawauso.ext.epic;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "EPIC_Service", targetNamespace = "tag:xmlns.carrier.com,2012-11-01:rcd.epic:webservices", wsdlLocation = "file:///Users/chris/Dropbox/Projects/kawauso/src/main/java/com/kawauso/ext/epic/Epic_2_24_14.wsdl")
public class EPICService
    extends Service
{

    private final static URL EPICSERVICE_WSDL_LOCATION;
    private final static WebServiceException EPICSERVICE_EXCEPTION;
    private final static QName EPICSERVICE_QNAME = new QName("tag:xmlns.carrier.com,2012-11-01:rcd.epic:webservices", "EPIC_Service");

    static {
        URL url = null;
        WebServiceException e = null;

        try {
            //url = new URL("file:///Users/chris/Dropbox/Projects/kawauso/src/main/java/com/kawauso/ext/epic/Epic_2_24_14.wsdl");
            URL location = EPICService.class.getProtectionDomain().getCodeSource().getLocation();
            url = new URL( "file://" + location.getFile().replace("EPICService.class", "Epic_2_24_14.wsdl") );
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        EPICSERVICE_WSDL_LOCATION = url;
        EPICSERVICE_EXCEPTION = e;
    }

    public EPICService() {
        super(__getWsdlLocation(), EPICSERVICE_QNAME);
    }

    public EPICService(WebServiceFeature... features) {
        super(__getWsdlLocation(), EPICSERVICE_QNAME, features);
    }

    public EPICService(URL wsdlLocation) {
        super(wsdlLocation, EPICSERVICE_QNAME);
    }

    public EPICService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, EPICSERVICE_QNAME, features);
    }

    public EPICService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public EPICService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns EPICPortType
     */
    @WebEndpoint(name = "EPIC_Port")
    public EPICPortType getEPICPort() {
        return super.getPort(new QName("tag:xmlns.carrier.com,2012-11-01:rcd.epic:webservices", "EPIC_Port"), EPICPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EPICPortType
     */
    @WebEndpoint(name = "EPIC_Port")
    public EPICPortType getEPICPort(WebServiceFeature... features) {
        return super.getPort(new QName("tag:xmlns.carrier.com,2012-11-01:rcd.epic:webservices", "EPIC_Port"), EPICPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (EPICSERVICE_EXCEPTION!= null) {
            throw EPICSERVICE_EXCEPTION;
        }
        return EPICSERVICE_WSDL_LOCATION;
    }

}
