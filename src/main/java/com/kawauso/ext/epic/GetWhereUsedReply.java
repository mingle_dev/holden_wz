
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WhereUsedLine" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="WhereUsedModel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="WhereUsedItem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "whereUsedLine"
})
@XmlRootElement(name = "GetWhereUsedReply")
public class GetWhereUsedReply {

    @XmlElement(name = "WhereUsedLine")
    protected List<GetWhereUsedReply.WhereUsedLine> whereUsedLine;

    /**
     * Gets the value of the whereUsedLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the whereUsedLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWhereUsedLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetWhereUsedReply.WhereUsedLine }
     * 
     * 
     */
    public List<GetWhereUsedReply.WhereUsedLine> getWhereUsedLine() {
        if (whereUsedLine == null) {
            whereUsedLine = new ArrayList<GetWhereUsedReply.WhereUsedLine>();
        }
        return this.whereUsedLine;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="WhereUsedModel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="WhereUsedItem" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "whereUsedModel",
        "whereUsedItem"
    })
    public static class WhereUsedLine {

        @XmlElement(name = "WhereUsedModel", required = true)
        protected String whereUsedModel;
        @XmlElement(name = "WhereUsedItem", required = true)
        protected String whereUsedItem;

        /**
         * Gets the value of the whereUsedModel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWhereUsedModel() {
            return whereUsedModel;
        }

        /**
         * Sets the value of the whereUsedModel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWhereUsedModel(String value) {
            this.whereUsedModel = value;
        }

        /**
         * Gets the value of the whereUsedItem property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWhereUsedItem() {
            return whereUsedItem;
        }

        /**
         * Sets the value of the whereUsedItem property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWhereUsedItem(String value) {
            this.whereUsedItem = value;
        }

    }

}
