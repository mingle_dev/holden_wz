/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.bean;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author chris
 */
public class SyncCartItem implements Serializable
{
    private String itemName;
    private int quantity;

    public String getItemName() {
        return itemName;
    }

    @XmlElement(required=true, nillable=false)
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    @XmlElement(required=true, nillable=false)
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}