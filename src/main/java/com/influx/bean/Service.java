/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.bean;

import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author chris
 */
@Document
public class Service
{
    private String name;
        
    public Service() {}
    public Service(String name) { this.name = name; }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}
