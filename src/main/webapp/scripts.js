(function($){
    //https://github.com/teamdf/jquery-number/blob/master/jquery.number.js
    $.number = function(number, decimals, thousands_sep, dec_point) {
        thousands_sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
        dec_point = (typeof dec_point === 'undefined') ? '.' : dec_point;
        decimals = !isFinite(+decimals) ? 0 : Math.abs(decimals);
        //type = (typeof type === 'undefined') ? '' : type;
        
        // Work out the unicode representation for the decimal place and thousand sep.	
        var u_dec = ('\\u'+('0000'+(dec_point.charCodeAt(0).toString(16))).slice(-4));
        var u_sep = ('\\u'+('0000'+(thousands_sep.charCodeAt(0).toString(16))).slice(-4));

        // Fix the number, so that it's an actual number.
        number = (number + '')
                .replace(',', '') //strip out commas
                .replace('\.', dec_point) // because the number if passed in as a float (having . as decimal point per definition) we need to replace this with the passed in decimal point character
                .replace(new RegExp(u_sep,'g'),'')
                .replace(new RegExp(u_dec,'g'),'.')
                .replace(new RegExp('[^0-9+\-Ee.]','g'),'');

        var n = !isFinite(+number) ? 0 : +number,
            s = '',
            toFixedFix = function (n, decimals) {
                var k = Math.pow(10, decimals);
                return '' + Math.round(n * k) / k;
            };

        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (decimals ? toFixedFix(n, decimals) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, thousands_sep);
        }
        if ((s[1] || '').length < decimals) {
            s[1] = s[1] || '';
            s[1] += new Array(decimals - s[1].length + 1).join('0');
        }
        return s.join(dec_point);
    };

    $.fn.number = function(number, decimals) {
        this.text( $.number(number, decimals) );
    };

    $.fn.currency = function(number, decimals) {
        this.text( '$' + $.number(number, decimals) );
    };

    $.fn.percentage = function(number, decimals) {
        this.text( $.number(number, decimals) + '%' );
    }    
})(jQuery);
    
$(document).ready(function () {
    //$('form[name=cartItemForm]').on('submit', function(e) {

    $('#content').on('submit', 'form[name=cartItemForm]', function(e) {
        e.preventDefault();
        var prod = $('input[name="productCode"]', this);
        var qty = $('input[name="quantity"]',this);
        var values = $(this).serialize();

        if(USER_ID == '') {
            window.location.href = BASE_PATH + "user/login.htm";
            return false;
        }
        
        $.ajax({
            url: $('#cartItemForm').attr('action'),
            type: "post",
            data: values,
            beforeSend: function() {
                //$('#content').spin();
                //$("#cover").css("display", "block");
                //change button to indicate background process ?
            },
            success: function(data){
                data = JSON.parse(data);
                length = data.length;
                totalAmt = 0.00;                
                
                noty({type: 'information', text: prod.val() + ' has been added to your cart'});
                
                $("#myShoppingCartCount").html(length);

                //build cart
                if(length > 0) {
                    res = "";
                    for(i=0;i<length;i++) {
                        totalAmt += data[i].qty * data[i].unitPrice;
                        res += "<dt><a href='" + BASE_PATH + "product/detail.htm?prod=" + data[i].productCode + "' style='padding:0px;'>(" + data[i].qty + ") " + data[i].productDetail.description + "</a></dt>" +
                               "<dd><span class='nowrap'>$ " + parseFloat(data[i].qty * data[i].unitPrice, 10).toFixed(2) + "</span></dd>";
                    }
                    res += "<dt style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'>Total</span></dt><dd style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'>$ " + parseFloat(totalAmt, 10).toFixed(2) + "</span></dd>";

                    $("#myShoppingCartLines > li > dl").html(res);
                }else{
                    $("#myShoppingCartLines > li > dl").html('Your shopping cart is empty.');
                }
            },
            error:function(){
                noty({type: 'error', text: 'An error occurred adding ' + prod.val() + ' to your cart'});
            }
        }); 
    });
    
    $('.modalSaveList').on('click', function(e) {
        e.preventDefault();

        if(USER_ID == '') {
            window.location.href = BASE_PATH + "user/login.htm";
            return false;
        }

        $.ajax({
            //url: $('.modalSaveList').attr('href'), //this,
            url: e.target.href,
            beforeSend: function() {
                $('#dialog').jqm({overlay: 60});
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
                $('#dialog').jqmShow();
            },
            success: function(res) {
                $('.jqmWindow').html(res);
            }
        });
    });
    
    $('.modalAvail').on('click', function(e) {
        e.preventDefault();
         $.ajax({
            url:  $(this).attr('href'),
            beforeSend: function() {
                $('#dialog').jqm({overlay: 60});
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
                $('#dialog').jqmShow();
            },
            success: function(res) {
                $('.jqmWindow').html(res);
            }
         });
     });

    $(".hdr-sprite-search").click(function() {
        if($(".hdr-sprite-search").hasClass('js-active-dd')) {
            $(".searchbar").css('display','');
            $("#controls").css('height','');
            $(".hdr-sprite-search").removeClass("js-active-dd");
        }else{
            $(".searchbar").css('display','block');
            $("#controls").css('height','110px');
            $(".hdr-sprite-search").addClass("js-active-dd");
        }
    });

    $.noty.defaults = {
        layout: 'top',
        theme: 'defaultTheme',
        type: 'alert',
        text: '', // can be html or string
        dismissQueue: true, // If you want to use queue feature set this true
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing',
            speed: 100 // opening & closing animation speed
        },
        timeout: 2000, // delay for closing event. Set false for sticky notifications
        force: false, // adds notification to the beginning of queue when set to true
        modal: false,
        maxVisible: 5, // you can set max visible notification for dismissQueue true option,
        killer: false, // for close all notifications before show
        closeWith: ['click'], // ['click', 'button', 'hover']
        callback: {
            onShow: function() {},
            afterShow: function() {},
            onClose: function() {},
            afterClose: function() {}
        },
        buttons: false // an array of buttons
    };
});