<%-- 
    Document   : layout
    Created on : Jan 16, 2014, 4:59:55 PM
    Author     : chris.weaver
--%>
<%@tag description="WZ Portal Modal" pageEncoding="UTF-8"%>
<%@attribute name="modalTitle" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
<head>
        
<style>
    
.modalHead {
    background: #eaeaea;
    border-bottom: 1px solid #c8ccce;
    height: 30px;
    font-size: 150%;
    line-height: 40px;
    font-weight: bold;
    font-family: Verdana;
    color: #555;
}    
.modalBody {
    padding: 20px;
    font-size: 1.0em;
}

/*
.narrow {
    display: none;
}

.fake-table {
    display: table;
    width: 100%;
    border: 1px solid #c8ccce;
    border-radius: 5px;
    overflow: hidden;
    border-collapse: separate;
    border-spacing: 0;
  
}
.fake-table .header {
    display: table-header-group;
    background: #eaeaea;
}
.fake-table .row {
    display: table-row;
    width: auto;
}
.fake-table .row .cell {
    display: table-cell;
    line-height: 1.5em;
    padding: 0.5em 15px;
    vertical-align: top;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .header .cell {
    color: #555;
    font-weight: bold;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .body {
    display: table-row-group;
}
.txt-center {
    text-align: center;
}
.txt-right {
    text-align: right;
}
*/



/*
@media screen and (max-width: 700px) {
    .fake-table .header {
        display: none;
    }
    .fake-table .row .cell .narrow {
        width: 100%;
        display: block;
        clear: left;
        min-height: 4px;
        border-bottom: 0px;
        padding: 10px;
    }
    
    .narrow-hide {
        display: none;
    }
    .txt-center {
        text-align: left;
    }
    .fake-table .body .row:nth-child(even) .cell {
        background: #eaeaea;
    }
}
*/
</style>

<script>
    $('.jqmClose').on('click', function(e) {
        $('#dialog').jqmHide();
    });    
</script>


</head>

<body>
    <div class="modalHead" style="width:100%; padding-bottom:16px;">
        <div style="padding-left:10px; float:left;"><jsp:invoke fragment="modalTitle"/> </div>
        <div style="min-width:40px; max-width:40px; float:right;"><a class="jqmClose" style="color:#555; text-decoration:none;">X</a></div>
    </div>

    <div class="modalBody">
        <jsp:doBody/>
    </div>
</body>
</html>