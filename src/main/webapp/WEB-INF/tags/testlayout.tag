<%-- 
    Document   : layout
    Created on : Jan 16, 2014, 4:59:55 PM
    Author     : chris.weaver
--%>
<%@tag description="WZ Portal Layout" pageEncoding="UTF-8"%>
<%@attribute name="mainSearch" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>

<html>
<head>
    <title>
        Mingledorff's Portal
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1, minimum-scale=1">
    <meta name="decorator" content="home" />
    <meta name="Keywords" content="Mingledorff's HVAC Distribution Carrier Bryant Payne Heil" />
    <meta name="Description" content="Mingledorff's has been in the HVAC business for over 75 years. Serving Georgia, Florida, Alabama, South Carolina and Mississippi." />

    <link rel="shortcut icon" href="/web/static/images/favicon.ico" />

    <link rel="stylesheet" type="text/css" href="/web/site-test.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>    
    
    <script type="text/javascript">
        var BASE_PATH = '<c:url value="/"/>';
        var USER_ID = '<c:url value="${session.userId}"/>';
    </script>
    
    <script type="text/javascript" src="<c:url value="/scripts.js"/>"></script>
    
    <script type="text/javascript">
    $(document).ready(function () {
        $(".hdr-sprite-search").click(function() {
            if($(".hdr-sprite-search").hasClass('js-active-dd')) {
                $(".searchbar").css('display','');
                $("#controls").css('height','');
                $(".hdr-sprite-search").removeClass("js-active-dd");
            }else{
                $(".searchbar").css('display','block');
                $("#controls").css('height','110px');
                $(".hdr-sprite-search").addClass("js-active-dd");
            }
        });
        
        $.noty.defaults = {
            layout: 'top',
            theme: 'defaultTheme',
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'},
                close: {height: 'toggle'},
                easing: 'swing',
                speed: 100 // opening & closing animation speed
            },
            timeout: 2000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover']
            callback: {
                onShow: function() {},
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {}
            },
            buttons: false // an array of buttons
        }; 
        
        //$("#myShoppingCart").on('mouseover', function() {
        $("myShoppingCartLines").on('show', function() {

            console.log("myShoppingCartLines is now visible");
            //$("#control-menu-cart-data > div").text('asdf');
            
            $.ajax({
               url: "<c:url value="/cart/getItems.htm"/>",
               type: "post",
               beforeSend: function() {
                   $("#myShoppingCartLines").html('Retrieving Cart...');
               },
               success: function(data) {
                   data = JSON.parse(data);
                   length = data.length;
                   totalAmt = 0.00;
                   
                   if(length > 0) {
                       res = "<table style='width:400px;' border='0'>";
                       for(i=0;i<length;i++) {
                           res += "<tr>" +
                               "<td><a href='<c:url value="/product/detail.htm"/>?prod=" + data[i].productCode + "' style='padding:0px;'>(" + data[i].qty + ") " + data[i].productDetail.description + "</a></td>" +
                               "<td style='vertical-align:top; white-space:nowrap; text-align:right;'>$ " + parseFloat(data[i].qty * data[i].unitPrice, 10).toFixed(2) + "</td>" +
                               "</tr>";
                           totalAmt += data[i].qty * data[i].unitPrice;
                       }
                       res += "<tr><td style='font-weight:800;'>Total:</td><td style='font-weight:800; white-space:nowrap; text-align:right;'>$ " + parseFloat(totalAmt, 10).toFixed(2) + "</td></tr>";
                       res += "</table><hr/>";
                       res += "<div style='padding:0px 0px 6px 6px;'><a href='<c:url value="/cart/index.htm"/>' style='padding:0px;'>View Shopping Cart</a></div>";
                       
                       $("#myShoppingCartLines").html(res);
                   }else{
                       $("#myShoppingCartLines").html('Your shopping cart is empty.');
                   }
               },
               error:function() {
                   
               }
           });
        });    

    });  
    


    
    </script>    
</head>

<body>
<div id="dialog" class="jqmWindow"></div>    
    
<div id="wrap">

<div id="header">
    <div class="width-fix">
        <div id="logo">
            <a href="#">
                <img <%--class="print-only"--%> src="http://mgl1939.s3-website-us-east-1.amazonaws.com/img/mingle_logo_white.png" alt="Mingledorff's Inc." title="" height="44" />
            </a>
        </div>

        <div id="navigation">
            <ul class="menu">
                <li class="has-dd js-menuhover" data-role="mobile-menu-item">
                    <a href="<c:url value="/resources.htm"/>">Resources</a>
                    <ul>
                        <li>
                            <span class="big">Account Resources</span>
                            <ul>
                                <li>
                                    <a href="#">
                                        Account Information
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Order Inquiry
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has-dd js-menuhover" data-role="mobile-menu-item">
                    <a href="<c:url value="/help.htm"/>">Help</a>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="mobile-search">
        
            <div class="header-sprite hdr-sprite-search left"></div>
        
    </div>
</div>

<div id="controls">
    <div class="width-fix">
        
        <div class="searchbar">
            <form id="fastKeywordSearch" data-autocomplete="#" data-root="#" class="product-search relative" action="#" method="GET">
                <div>
                    <input
                        type="text"
                        id="term"
                        name="term"
                        placeholder="Keyword or Part Number"
                        class="term"
                        value=""
                        min="2"
                        max="255"
                        required />
                </div>
                <input type="hidden" id="searchMode" name="searchMode" value=""/>
                <input type="submit" value="Search" class="btn-search" />
            </form>
        </div>

        <ul class="menu nav-left">
            <li class="js-desktop js-menuhover">
                <div class="products">
                    <a href="<c:url value="/"/>">
                        <div class="header-sprite hdr-sprite-products left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">Products</span><br />
                            <span class="header-sublabel">Browse</span>
                        </div>
                    </a>
                </div>
                <ul data-role='nav-dd'>
                    <li>
                        <span class="big categories">Product Categories</span>
                        <ul>
                        <c:forEach items="${headProdMenu}" var="item">
                            <li><a href='<c:url value="/product/search.htm?id=${item.id}"/>'>${item.name}</a></li>
                        </c:forEach>
                        </ul>
                    </li>
                    <li>
                        <span class="big categories">Promotions &amp; Special Interests</span>
                        <ul>
                            <li><a href='#'>Pricebook Browser</a></li>
                            <li><a href='#'>Sale Items</a></li>
                            <li><a href='#'>Scratch and Dent</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="js-desktop js-menuhover">
                <div class="my-lists">
                    <a href="<c:url value="/user/lists/index.htm"/>">
                        <div class="header-sprite hdr-sprite-list left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Lists</span><br />
                            <span class="header-sublabel">Explore</span>
                        </div>
                    </a>
                </div>
                <ul data-role='nav-dd'>
                    <li>
                        <c:if test="${not empty saveLists}">
                        <span class="big categories">My Lists</span>
                        <ul>
                            <c:forEach items="${saveLists}" var="item" begin="0" end="14">
                                <li><a href="<c:url value="/user/lists/detail.htm?id=${item.id}"/>">${item.name}</a></li>
                            </c:forEach>
                            <c:if test="${fn:length(saveLists) gt 15}">
                                <li><a href="<c:url value="/user/lists/index.htm"/>">(More)</a></li>
                            </c:if>
                            <!--
                            <div class="subtitle">Shared Lists</div>
                            <li><a href='#'>Supplies</a></li>
                            <li><a href='#'>Furnace Accessories</a></li>
                            <li><a href='#'>Coils</a></li>
                            -->
                        </ul>
                        </c:if>                            
                    </li>
                </ul>
            </li> 
            <li class="mobile-only">
                <div>
                <a href="<c:url value="/resources.htm"/>">
                    <div class="header-sprite hdr-sprite-tools left"></div>
                </a>
                </div>
            </li>            
        </ul>

        <ul class="menu nav-right">
            <li id="myAccount" class="js-desktop js-menuhover">
                <div class="my-account">
                    <a href="<c:url value="/user/profile.htm"/>">
                        <div class="header-sprite hdr-sprite-account left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Account</span><br />
                            <span class="header-sublabel text-limit-100px myaccount">
                                 <c:choose>
                                 <c:when test="${not empty session.userId}">
                                     <c:out value="${fn:substring(session.fullName, 0, 12)}" />
                                 </c:when>
                                 <c:otherwise>
                                     Sign In
                                 </c:otherwise>
                                 </c:choose>
                            </span>                            
                        </div>
                    </a>
                </div>
                <ul class="primary-dd" data-role='nav-dd'>
                    <li class="no-padding">
                        <%--
                        <form action="" method="post" id="signInForm">
                            <p class="clear">
                                <label for="j_username" class="strong">
                                    Username:
                                </label>
                            </p>
                            <p>
                                <input id="j_username"
                                       name="j_username"
                                       type="text"
                                       maxlength="40"
                                       placeholder="Username"
                                       tabindex="1"
                                       autofocus
                                       required />
                            </p>
                            <p class="clear">

                                <label for="j_password" class="strong">
                                    Password:
                                </label>
                            </p>
                            <p>
                                <input id="j_password"
                                       name="j_password"
                                       type="password"
                                       maxlength="20"
                                       placeholder="Password"
                                       tabindex="2" required />
                            </p>
                            <p class="clear">
                                <label for="_spring_security_remember_me">
                                    <input type="checkbox"
                                           name="_spring_security_remember_me"
                                           id="_spring_security_remember_me" tabindex="3"
                                            />
                                    Keep me signed in
                                </label>
                            </p>
                            <p class="clear">
                                <input type="submit" value="Sign In" tabindex="5" />
                                or
                                <a href="#" class="text-button" tabindex="6">
                                    Register
                                </a>
                            </p>
                            <p>
                                Forgot your <a href="#" tabindex="7">username</a> or <a href="#" tabindex="8">password</a>?
                            </p>
                        </form>
                        --%>
                        <c:choose>
                        <c:when test="${not empty session.userId}">
                            <a href="<c:url value='/user/logout.htm'/>">Logout</a>
                        </c:when>
                        <c:otherwise>
                            <h3>Sign In</h3>
                            <form action="<c:url value='/user/login.htm'/>" method="post">
                                <table border='0' style="cellspacing:0px;">
                                    <tr>
                                        <td>Username:</td>
                                        <td>Password:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="p_username" style="width:200px" placeholder="Username" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                        <td><input type="password" name="p_password" style="width:150px" placeholder="Password" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                        <td><input type="submit" name="Submit" value="Submit"/></td>
                                    </tr>
                                </table>
                            </form>
                            <br/>
                            <span>Not already a user? <a href="<c:url value="/user/register.htm"/>" style="padding:0px; display:inline;">Register for Access</a></span>
                        </c:otherwise>
                        </c:choose>                        
                    </li>
                </ul>
            </li>

            <li id="myStore" class="js-menuhover js-desktop">
                <div>
                    <a href="<c:url value="/locations.htm"/>" id="menu-StoreLocator">
                        <div class="header-sprite hdr-sprite-store left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Store</span><br />
                            <span class="header-sublabel text-limit-100px">
                                <c:choose>
                                <c:when test="${not empty session.warehouse}">
                                    ${currentLocation.whseName}
                                </c:when>
                                <c:otherwise>
                                    Find a Branch
                                </c:otherwise>
                                </c:choose>                                
                            </span>
                        </div>
                    </a>
                </div>
                <ul class="primary-dd" data-role='nav-dd'>
                    <li>
                        <a href="<c:url value="/locations/detail.htm?code=${currentLocation.whseId}"/>" style="padding:0px;">${currentLocation.state}${currentLocation.whseId} - ${currentLocation.whseName}</a><br/>
                        ${currentLocation.addr}<br/>
                        ${currentLocation.whseName} ${currentLocation.state}, ${currentLocation.zipCd}<br/>
                        Phone: ${currentLocation.phoneNo}<br/>
                        Email: <a href="mailto:${currentLocation.emailAddr}" style="display:inline; padding:0px;">${currentLocation.emailAddr}</a>
                    </li>
                    <li style="padding:0px;">
                        <hr/>
                    </li>
                    <li>
                        <a href="<c:url value="/locations.htm"/>" style="padding:0px;">Find another Branch</a>
<%--                        
                    <c:choose>
                    <c:when test="${not empty session.warehouse}">
                        <div style="padding:6px; width:95%;">
                        <a href="<c:url value="/locations/detail.htm?code=${currentLocation.whseId}"/>" style="padding:0px;">${currentLocation.state}${currentLocation.whseId} - ${currentLocation.whseName}</a><br/>
                        ${currentLocation.addr}<br/>
                        ${currentLocation.whseName} ${currentLocation.state}, ${currentLocation.zipCd}<br/>
                        Phone: ${currentLocation.phoneNo}<br/>
                        Email: <a href="mailto:${currentLocation.emailAddr}" style="display:inline; padding:0px;">${currentLocation.emailAddr}</a>
                        </div>
                        <hr/>
                        <div style="padding:0px 0px 6px 6px;"><a href="<c:url value="/locations.htm"/>" style="padding:0px;">Find another Branch</a></div>
                    </c:when>
                    <c:otherwise>
                        <table width="100%">
                            <tr>
                                <td style="padding-left:10px;">
                                    <form action="<c:url value="/locations.htm"/>">    
                                        <input type="submit" value="Find a Branch">
                                    </form>
                                </td>
                                <td style="vertical-align:top; text-align:right; padding-right:10px;">
                                    You do not have<br/> a branch selected
                                </td>
                            </tr>
                        </table>
                    </c:otherwise>
                    </c:choose>    
--%>
                    </li>
                </ul>
            </li>

            <li id="myShoppingCart" class="js-menuhover js-desktop">
                <div class="my-shopping-cart">
                    <a href="<c:url value="/cart/index.htm"/>">
                        <div class="header-sprite hdr-sprite-cart left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Cart</span><br />
                            <span class="header-sublabel">Items (<span id="myShoppingCartCount">${fn:length(cartLines)}</span>)</span>
                        </div>
                    </a>
                </div>

                <ul class="primary-dd" id="myShoppingCartLines">
                    <li>
                        <c:choose>
                        <c:when test="${fn:length(cartLines) > 0}">                        
                            <dl class="cartSummary">
                                <c:set var="cartTotal" value="${0}"/>
                                <c:forEach var="item" items="${cartLines}">
                                    <c:set var="cartTotal" value="${cartTotal + (item.qty * item.unitPrice)}"/>
                                    <dt><a href='<c:url value="/product/detail.htm"/>?prod=${item.productCode}' style='padding:0px;'>(${item.qty}) ${item.productDetail.description}</a></dt>
                                    <dd><span class='nowrap'><fmt:formatNumber value="${item.qty * item.unitPrice}" type="currency" /></span></dd>
                                </c:forEach>

                                <dt style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'>Total</span></dt>
                                <dd style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'><fmt:formatNumber value="${cartTotal}" type="currency" /></span></dd>    
                            </dl>
                        </c:when>
                        <c:otherwise>
                            Your shopping cart is empty.
                        </c:otherwise>
                        </c:choose> 

                        <%--
                        
                            <dt>
                                <a href="">
                                    <span class="small">(1)</span>
                                    item 1
                                </a>
                            </dt>
                            <dd>
                                <span class="nowrap">$2.49</span>
                            </dd>
                            <dt>
                                <a href="#">
                                    <span class="small">(1)</span>
                                    item 2
                                </a>
                            </dt>
                            <dd>
                                <span class="nowrap">$3.32</span>
                            </dd>
                            <dt>
                                <a href="#">
                                    <span class="small">(1)</span>
                                    item 3
                                </a>
                            </dt>
                            <dd>
                                <span class="nowrap">$4.47</span>
                            </dd>
                            <dt>
                                <strong>
                                    Total (USD):
                                </strong>
                            </dt>
                            <dd>
                                <strong class="currency">
                                    <span class="nowrap">
                                        $10.28
                                    </span>
                                </strong>
                            </dd>
                        </dl>
                        --%>
                    </li>
                    <hr />
                    <li>
                        <a href="<c:url value="/cart/index.htm"/>">View Shopping Cart</a>
                    </li>
                </ul>
            </li>
            <%--
            <li class="mobile-only">
                <div  id="mobile-menu">
                    <a data-url="">
                        <div class="header-sprite hdr-sprite-mobile-menu left">
                        </div>
                        <ul data-role="nav-dd" class="hidden">
                        </ul>
                    </a>
                </div>
            </li>
            --%>
        </ul>
    </div>
</div>
    
<%--        
<div class="mobile-dd">
</div>
--%>

<div id="content">
    <c:if test="${fn:length(navTree) gt 0}">
    
    <div class="width-fix" style="font-size:110%; padding:6px 0px 2px 10px;">
    <a href="<c:url value="/index.htm"/>">Home</a>  
    <c:forEach items="${navTree}" var="item" varStatus="itemStatus">
        <c:choose>
            <c:when test="${itemStatus.last}">
                > ${item.name}
            </c:when>
            <c:otherwise>
                > <a href="<c:url value="/${item.url}.htm?${item.query}"/>">${item.name}</a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    </div>
    <hr/>
    </c:if>
</div>

<div id="content">
    <div class="width-fix" style="padding: 0px 10px 0px 10px;">

        <jsp:doBody/>

    </div>
</div>

<div id="test-foot" class="footer">
    <div class="width-fix">
        <table border="0" width="100%">
            <tr>
                <td width="175">
                    <ul class="foot-menu">
                        <div class="subtitle">COMPANY INFORMATION</div>
                        <li><a href="<c:url value="/about.htm"/>">About Us</a></li>
                        <li><a href="mailto:kperry@mingledorffs.com?subject=Career Opportunities">Career Opportunities</a></li>
                        <li><a href="<c:url value="/locations.htm"/>">Locations</a></li>
                    </ul>	
                    <ul id="foot-acct-wrap" class="foot-menu">
                        <div class="subtitle">MY ACCOUNT</div>
                        <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                        <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                        <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                    </ul>
                </td>
                <td width="175" style="vertical-align:top;">
                    <ul class="foot-menu">
                        <div class="subtitle">CUSTOMER SERVICE</div>
                        <li><a href="mailto:ccollins@mingledorffs.com?subject=Company Contacts">Company Contacts</a></li>
                        <li><a href="mailto:twall@mingledorffs.com?subject=Website Feedback">Feedback</a></li>
                        <li><a href="mailto:ccollins@mingledorffs.com?subject=Request Information">Request Information</a></li>
                    </ul>					
                </td>
                <td id="test-foot3" width="175">
                    <ul class="foot-menu">
                        <div class="subtitle">MY ACCOUNT</div>
                        <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                        <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                        <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                    </ul>					
                </td>
                <td id="test-foot4">
                    <div id="foot-social-wrap" class="social-container">
                        <div class="subtitle">KEEP CONNECTED</div>
                        <a href="http://www.facebook.com/mingledorffs" target="_blank" class="social left" id="facebook"></a>
                        <a href="http://www.linkedin.com/company/104105?trk=tyah&trkInfo=tas%3Amingledorff%2Cidx%3A2-1-6" target="_blank" class="social left" id="linkedin"></a>
                        <a href="http://twitter.com/mingledorffs" target="_blank" class="social left" id="twitter"></a>
                        <a href="http://www.youtube.com/mingledorffs" target="_blank" class="social left" id="youtube"></a>
                    </div>					
                </td>
            </tr>
        </table>
        <div style="padding-left:10px; color: #fff;" class="foot-menu">
            <a href="<c:url value="/terms.htm"/>">Terms & Conditions</a> | <a href="<c:url value="/privacy.htm"/>">Privacy Statement</a> | <a href="<c:url value="/sitemap.htm"/>">Sitemap</a><br/>
            Copyright &copy; 2013 Mingledorff's Inc. - All rights reserved. 
        </div> 
    </div>
</div>
                
</body>
</html>                