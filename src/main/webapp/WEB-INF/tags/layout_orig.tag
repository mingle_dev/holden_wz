<%-- 
    Document   : layout
    Created on : Jan 16, 2014, 4:59:55 PM
    Author     : chris.weaver
--%>
<%@tag description="WZ Portal Layout" pageEncoding="UTF-8"%>
<%@attribute name="mainSearch" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!doctype html>
<html class="no-js">
  <head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>MGL Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="HandheldFriendly" content="true">
    <%--
    <link rel="stylesheet" type="text/css" href="http://mgl1939.s3-website-us-east-1.amazonaws.com/css/style.css" media="screen, handheld" /> 
    <link rel="stylesheet" type="text/css" href="http://mgl1939.s3-website-us-east-1.amazonaws.com/css/enhanced.css" media="screen and (min-width: 40.5em)" />
    <!--[if (lt IE 9)&(!IEMobile)]>
    <link rel="stylesheet" type="text/css" href="http://mgl1939.s3-website-us-east-1.amazonaws.com/css/enhanced.css" />
    <![endif]-->    
    --%>
    
    <link rel="stylesheet" type="text/css" href="/web/style.css" media="screen, handheld" />
    <link rel="stylesheet" type="text/css" href="/web/enhanced.css" media="screen and (min-width: 40.5em)" />
    <!--[if (lt IE 9)&(!IEMobile)]>
    <link rel="stylesheet" type="text/css" href="/web/enhanced.css" />
    <![endif]-->
<style>
    .section, .content {
        min-width: 240px;
        overflow: hidden;
        position: relative;
    }
    
    /*
    body {
        font: 12px Verdana;
        color: #222;
    }
    */

    #product-categories, #product-top-results, #store-results {
        margin: 0 0 0 15px;
    }
    
    div {
        display: block;
    }

    .fourth:nth-child(4n+1) {
        clear: left;
    }

    .fourth {
        float: left;
        width: 25%;
    } 
    /*
    a:visited, a h3:visited {
        color: #003b69;
    } 
    */
    
    a:active, a:hover {
        color: #D11600;
        text-decoration: none;
    }
    
    .product {
        display: block;
        position: relative;
        border: 1px solid #c8ccce;
        border-radius: 5px;
        margin: 0 15px 15px 0;
        text-align: center;
        overflow: hidden;
        height: 127px;
    }

    .product img {
        display:block;
        margin:auto;
    }
    
    a, a h3 {
        color: #00599c;
        text-decoration: none;
    }
    
    /*
    a:visited, a h3:visited {
        color: #003b69;
    }
    */
@media screen and (max-width: 959px) {
    .fourth {
        width: 50%;
    }
    .fourth:nth-child(2n+1) {
        clear: left;
    }
    
    #product-attributes {
        display: none;
    }
}

@media screen and (max-width: 600px) {
    #product-categories .fourth, #product-top-results .fourth, #store-results .fourth {
        width: 100%;
    }

    .product {
        border-radius: 0;
        border: none;
        border-bottom: 1px solid #c8ccce;
        height: auto;
        text-align: left;
        margin: 0;
        height: 37px;
    }    

    .product img {
        display: none;
    }


}

/*
a:-webkit-any-link {
    color: -webkit-link;
    text-decoration: underline;
    cursor: auto;
}
*/

form input[type="text"] {
    font-size: 1.1em;
}

.ui-autocomplete { position: absolute; cursor: default; z-index:1000; margin-top: 20px;}	

/* workarounds */
* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

.ui-menu {
	list-style:none;
	padding: 0px;
	margin: 0;
	display: block;
	float: left;
}
.ui-menu .ui-menu {
	margin-top: 0px;
}
.ui-menu .ui-menu-item {
	margin:0;
	padding: 0;
	zoom: 1;
	float: left;
	clear: left;
	width: 100%;
}
.ui-menu .ui-menu-item a {
	text-decoration:none;
	display:block;
	padding: .2em .4em;
	line-height: 1.5;
	zoom:1;
}
.ui-menu .ui-menu-item a.ui-state-hover,
.ui-menu .ui-menu-item a.ui-state-active {
	font-weight: normal;
	margin: -1px;
}

.ui-widget-content { border: 1px solid #aaaaaa; background: #ffffff; color: #333333; }
.ui-widget-content a { color: #333333; }

.ui-widget-content .ui-state-default { background: #eeeeee; }
.ui-widget-content .ui-state-hover, .ui-widget-content .ui-state-focus { background: #f8f8f8; }
.ui-widget-content .ui-state-active { background: #999999; color: #ffffff; }
    
.ui-widget { font-family: Trebuchet MS, Helvetica, Arial, sans-serif; font-size: 1.1em; }
.ui-widget .ui-widget { font-size: 1em; }
.ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button { font-family: Trebuchet MS, Helvetica, Arial, sans-serif; font-size: 1em; }

.ui-helper-hidden { display: none; }
.ui-helper-hidden-accessible { position: absolute !important; clip: rect(1px 1px 1px 1px); clip: rect(1px,1px,1px,1px); }

	.ui-autocomplete-input { 
          width: 220px;
          padding: 1px;
          border: 1px solid #000;
          font-size: 12px;
          background: white url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/leftcap.gif') right center no-repeat;
        }
        .ui-autocomplete-loading {
            background: white url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/ui-anim_basic_16x16.gif') center center no-repeat;
            
        }
        
        
        .ui-widget-content {
            /*width: 220px;*/
        }
        
        
        .product-search-loading .term {
            background: white url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/ui-anim_basic_16x16.gif') right 50px center no-repeat;
        }
     
        /*www.csscheckbox.com/*/
        input[type=checkbox].css-checkbox {
                    position: absolute; 
                  overflow: hidden; 
                  clip: rect(0 0 0 0); 
                  height:1px; 
                  width:1px; 
                  margin:-1px; 
                  padding:0;
                  border:0;
          }

          input[type=checkbox].css-checkbox + label.css-label {
                  padding-left:20px;
                  /*height:15px;*/
                  display:inline-block;
                  line-height:15px;
                  background-repeat:no-repeat;
                  background-position: 0 0;
                  /*font-size:15px;*/
                  vertical-align:middle;
                  cursor:pointer;
          }

          input[type=checkbox].css-checkbox:checked + label.css-label {
                  background-position: 0 -15px;
          }

          .css-label{ background-image:url(//mgl1939.s3-website-us-east-1.amazonaws.com/img/lite-red-check.png); }
</style>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <%--<script type="text/javascript" src="http://www.mingledorffs.com/content/js/jquery-ui-1.8.23.custom.min.js"></script>--%>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>    
    
    <script type="text/javascript">
        var base_path = '<c:url value="/"/>';
        var user_id = '<c:url value="${session.userId}"/>';
    </script>
    
    <script type="text/javascript" src="<c:url value="/scripts.js"/>"></script>
    
    <script type="text/javascript">
        (function ($, window, document, undefined) {
            $.fn.extend({
                    dropmenu : function (options) {	
                            var _defaults = {};

                            return this.each(function() {
                                    var dropmenu = $(this);
                                    var metadata = dropmenu.data('options');
                                    var o = $.extend({}, _defaults, options, metadata);

                                    dropmenu.find('li').each(function() {
                                            var submenu = $('>ul', this);

                                            $(this).on({
                                                    mouseenter : function(e) {

                                                            //probably a better way to do this but it works...
                                                            if( $(window).width() < 648 )
                                                                    return;

                                                            //var padding = 40;
                                                            //var top = submenu.offset().top;

                                                            if(o.hover_class)
                                                                    $(this).addClass(o.hover_class);

                                                            $('.header-sprite', this).addClass('js-active-dd');

                                                            var subPos = $(dropmenu).position().left + $(this).position().left + submenu.width();
                                                            var calcWidth = ($(window).width() > 1280) ? $(window).width() - ( ($(window).width() - 1280) / 2) : $(window).width();

                                                            if( subPos + 40 > calcWidth ) {
                                                                            var leftPos = $(this).position().left + (calcWidth - (subPos + 40));
                                                                            submenu.css({ position: "absolute", left: (leftPos) + "px" });
                                                            }else{
                                                                            submenu.css({ position: "absolute", left: ($(this).position().left) + "px" });
                                                            }
                                                            submenu.show();
                                                    },
                                                    mouseleave : function(e) {
                                                            if(o.hover_class)
                                                                    $(this).removeClass(o.hover_class);		
                                                            $('.header-sprite', this).removeClass('js-active-dd');					
                                                            submenu.hide();
                                                    }
                                            });
                                    });
                            });
                    }
            });

    })( jQuery, window , document );

    /* http://stackoverflow.com/questions/1225102/jquery-event-to-trigger-action-when-a-div-is-made-visible */
    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
          var el = $.fn[ev];
          $.fn[ev] = function () {
            this.trigger(ev);
            return el.apply(this, arguments);
          };
        });
    })(jQuery);

    $(document).ready(function () {
        $("#menu-left").dropmenu({ hover_class : 'control-root'});
        $("#menu-right").dropmenu({ hover_class : 'control-root'});

        $("#menu-resources").mouseover(function (e) {
            var submenu = $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
            submenu.css({ 'right' : 59 });
            $('>span', this).addClass("dropdown-menu-root");
        });

        $("#menu-resources").mouseout(function (e) {
            $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
            $('>span', this).removeClass("dropdown-menu-root");
        });

        $(".hdr-sprite-search").click(function() {
            if($(".hdr-sprite-search").hasClass('js-active-dd')) {
                $(".searchbar").css('display','');
                $("#controls").css('height','');
                $(".product-search").css('margin','');
                $(".nav-left, .nav-right").css('top','');
                $(".hdr-sprite-search").removeClass("js-active-dd");
            }else{
                $("#controls").css('height','100px');
                $(".searchbar").css('display','block');
                $(".product-search").css('margin','0 4px 0 4px');
                $(".nav-left, .nav-right").css('top','50px');
                $(".hdr-sprite-search").addClass("js-active-dd");
            }
        });

        <c:if test="${empty mainSearch}">
        $("#mainSearchQX").autocomplete({
            minLength: 3,
            delay: 100,
            source: function( request, respond ) {
                $.post( "<c:url value="/product/quick.htm"/>", { term: request.term },
                    function( response ) {
                        $("#fastKeywordSearch").removeClass("product-search-loading");

                        var data = [ { product: request.term, description: "Search all products matching *" + request.term + "*" } ];
                        var res = JSON.parse(response);

                        if(res.length > 0)
                            data = data.concat(res);

                        respond( data );
                    }); 
            },
            search: function( event, ui ) {
                if($(".hdr-sprite-search").hasClass('js-active-dd'))
                    return false;
                $("#fastKeywordSearch").addClass("product-search-loading");
            },
            select: function( event, ui ) {
              $( "#mainSearchQX" ).val( ui.item.product );
              //$( "#mainSearchQX-id" ).val( ui.item.value );
              return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" ).append( "<a><b>" + item.product + "</b><br>" + item.description + "</a>" ).appendTo( ul );
        };
        </c:if>
        
        $.noty.defaults = {
            layout: 'top',
            theme: 'defaultTheme',
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'},
                close: {height: 'toggle'},
                easing: 'swing',
                speed: 100 // opening & closing animation speed
            },
            timeout: 2000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover']
            callback: {
                onShow: function() {},
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {}
            },
            buttons: false // an array of buttons
        };   
        
        $("#control-menu-cart-data").on('show', function() {
            //console.log("control-menu-cart-data is now visible");
            //$("#control-menu-cart-data > div").text('asdf');
            
            $.ajax({
               url: "<c:url value="/cart/getItems.htm"/>",
               type: "post",
               beforeSend: function() {
                   $("#control-menu-cart-data > div").html('<center>Working...</center>');
               },
               success: function(data) {
                   data = JSON.parse(data);
                   length = data.length;
                   totalAmt = 0.00;
                   
                   if(length > 0) {
                       res = "<table style='width:400px;' border='0'>";
                       for(i=0;i<length;i++) {
                           res += "<tr>" +
                               "<td><a href='<c:url value="/product/detail.htm"/>?prod=" + data[i].productCode + "' style='padding:0px;'>(" + data[i].qty + ") " + data[i].productDetail.description + "</a></td>" +
                               "<td style='vertical-align:top; white-space:nowrap; text-align:right;'>$ " + parseFloat(data[i].qty * data[i].unitPrice, 10).toFixed(2) + "</td>" +
                               "</tr>";
                           totalAmt += data[i].qty * data[i].unitPrice;
                       }
                       res += "<tr><td style='font-weight:800;'>Total:</td><td style='font-weight:800; white-space:nowrap; text-align:right;'>$ " + parseFloat(totalAmt, 10).toFixed(2) + "</td></tr>";
                       res += "</table><hr/>";
                       res += "<div style='padding:0px 0px 6px 6px;'><a href='<c:url value="/cart/index.htm"/>' style='padding:0px;'>View Shopping Cart</a></div>";
                       
                       $("#control-menu-cart-data > div").html(res);
                   }else{
                       $("#control-menu-cart-data > div").html('Your shopping cart is empty.');
                   }
               },
               error:function() {
                   
               }
           });
        });
    });
    </script>	
    
    <style>
/*
        .searchbar {
            display: block;
            width:100%;
            position: absolute;
            top: 0px;
        }
        
        .product-search {
            margin: 0 4px 0 4px;
        }
        .nav-left {
            top: 50px;
        }
        .nav-right {
            top: 50px;
        } 

        #controls {
            height: 100px;
        }
*/
    </style>
  </head>
  <body>
     
<div id="dialog" class="jqmWindow">
</div>
     
    <div id="header">
        <div class="width-fix">
            <div id="logo">
                <a href="<c:url value="/"/>"><img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/img/mingle_logo_white.png" height="44"></a>	
            </div>

            <div class="banner-nav">
                <ul class="dropdown-menu">
                    <li id="menu-resources">
                        <span class="nav-head">
                            <a href="<c:url value="/resources.htm"/>">Resources</a>
                        </span>
                        <ul class="dropdown-menu-items">
                            <div style="width:200px;" class="subtitle">Account Resources</div>
                            <li><a href="<c:url value="/account/balances.htm"/>">Account Information</a></li>
                            <li><a href="<c:url value="/account/orders.htm"/>">Order History</a></li>
                            <%--
                            <div class="subtitle">Sales Tools</div>
                            <li><a href="<c:url value="/quote/index.htm"/>">Quote Builder</a></li>
                            <li><a href="/account">Pricebooks - Dealer</a></li>
                            <li><a href="/account">Pricebooks - Retail</a></li>
                            <li><a href="/account">Retail Spiff Program</a></li>
                            
                            <div class="subtitle">Technical Services</div>
                            <li><a href="/account">Training Courses</a></li>
                            <li><a href="/orders">Service Bulletins</a></li>
                            <li><a href="/orders">Document Library</a></li>
                            --%>
                            <div class="subtitle">Sales Tools</div>
                            <li><a href="<c:url value="/resources.htm?f=qb"/>">Quote Builder</a></li>
                            <li><a href="<c:url value="/resources.htm?f=pbd"/>">Pricebooks - Dealer</a></li>
                            <li><a href="<c:url value="/resources.htm?f=pbr"/>">Pricebooks - Retail</a></li>
                            <li><a href="<c:url value="/resources.htm?f=rs"/>">Retail Spiff Program</a></li>
                            
                            <div class="subtitle">Technical Services</div>
                            <li><a href="<c:url value="/resources.htm?f=tc"/>">Training Courses</a></li>
                            <li><a href="<c:url value="/resources.htm?f=sb"/>">Service Bulletins</a></li>
                            <li><a href="<c:url value="/resources.htm?f=dl"/>">Document Library</a></li>                            
                            
                        </ul>
                    </li>
                    <li>
                        <span class="nav-head">
                            <a class="top" href="<c:url value="/help.htm"/>">Help</a>
                        </span>
                    </li>    
                </ul>
            </div>	
            <div class="banner-nav-lite">
                <a id="narrow-sprite-search" href="#">
                    <div class="header-sprite hdr-sprite-search left"></div>
                </a>
            </div>			
        </div>
    </div>		
    
    <div id="controls">
        <div class="width-fix">
            
            <div class="searchbar">
                <c:choose>
                <c:when test="${not empty mainSearch}">
                    <jsp:invoke fragment="mainSearch"/>
                </c:when>
                <c:otherwise>
                    
                    <form id="fastKeywordSearch" class="product-search relative" action="<c:url value="/product/search.htm"/>" method="GET">
                        <div>
                            <input type="text" id="mainSearchQX" name="term" placeholder="Keyword or Part Number" class="term ui-autocomplete-input" value="" min="2" max="255" required="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        </div>
                        <input type="hidden" id="searchMode" name="searchMode" value="">
                        <input type="submit" value="Search" class="btn-search">
                    </form>                    
                    
                    <%--
                    <form id="fastKeywordSearch" class="product-search relative" action="<c:url value="/product/search.htm"/>" method="GET">
                        <div>
                            <input type="text" id="mainSearchQX" name="term" placeholder="Keyword or Part Number" class="term ui-autocomplete-input" value="" min="2" max="255" required="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        </div>
                        <input type="hidden" id="searchMode" name="searchMode" value="">
                        <input type="submit" value="Search" class="btn-search">
                    </form>
                    --%>
                </c:otherwise>
                </c:choose>
            </div>
            
            <div id="navigation">
                <ul id="menu-left" class="dropdown-menu nav-left">
                    <li>
                        <span class="control">
                            <a href="<c:url value="/"/>">
                                <div class="header-sprite hdr-sprite-products left"></div>
                                <div class="left desktop-only">
                                    <span class="header-label">Products</span><br>
                                    <span class="header-sublabel text-limit-100px">Browse</span>
                                </div>
                            </a>
                        </span>
                        <ul class="dropdown-menu-items">
                            <div style="width:250px;" class="subtitle">Product Categories</div>
                            <c:forEach items="${headProdMenu}" var="item">
                                <li><a href='<c:url value="/product/search.htm?id=${item.id}"/>'>${item.name}</a></li>
                            </c:forEach>
                            <div class="subtitle">Promotions &amp; Special Interests</div>
                            <li><a href='#'>Pricebook Browser</a></li>
                            <li><a href='#'>Sale Items</a></li>
                            <li><a href='#'>Scratch and Dent</a></li>
                        </ul>
                    </li>			
                    <li>
                        <span class="control">
                            <c:choose>
                            <c:when test="${not empty session.userId}">
                            <a href="<c:url value="/user/lists/index.htm"/>">
                            </c:when>
                            <c:otherwise>
                            <a href="<c:url value="/user/login.htm"/>">
                            </c:otherwise>
                            </c:choose>
                                <div class="header-sprite hdr-sprite-list left"></div>
                                <div class="left desktop-only">						    	
                                    <span class="header-label">My Lists</span><br>
                                    <span class="header-sublabel text-limit-100px">Explore</span>
                                </div>
                            </a>
                        </span>
                        <c:if test="${not empty saveLists}">
                        <ul class="dropdown-menu-items">
                            <div style="width:350px;" class="subtitle">My Lists</div>
                            <c:forEach items="${saveLists}" var="item" begin="0" end="14">
                                <li><a href="<c:url value="/user/lists/detail.htm?id=${item.id}"/>">${item.name}</a></li>
                            </c:forEach>
                            <c:if test="${fn:length(saveLists) gt 15}">
                                <li><a href="<c:url value="/user/lists/index.htm"/>">(More)</a></li>
                            </c:if>
                            <!--
                            <div class="subtitle">Shared Lists</div>
                            <li><a href='#'>Supplies</a></li>
                            <li><a href='#'>Furnace Accessories</a></li>
                            <li><a href='#'>Coils</a></li>
                            -->
                        </ul>
                        </c:if>
                    </li>
                    <li class="mobile-only">
                        <span class="control">
                            <a href="<c:url value="/resources.htm"/>">
                            <div class="header-sprite hdr-sprite-tools left"></div>
                            </a>
                        </span>
                    </li>
                </ul>

                <ul id="menu-right" class="dropdown-menu nav-right">
                    <li>
                        <span class="control">
                            <c:choose>
                            <c:when test="${not empty session.userId}">
                            <a href="<c:url value="/user/profile.htm"/>">
                            </c:when>
                            <c:otherwise>
                            <a href="<c:url value="/user/login.htm"/>">
                            </c:otherwise>
                            </c:choose>
                                <div class="header-sprite hdr-sprite-account left"></div>
                                <div class="right desktop-only">						    	
                                    <span class="header-label">My Account</span><br>
                                    <span class="header-sublabel text-limit-100px">
                                         <c:choose>
                                         <c:when test="${not empty session.userId}">
                                             <c:out value="${fn:substring(session.fullName, 0, 12)}" />
                                         </c:when>
                                         <c:otherwise>
                                             Sign In
                                         </c:otherwise>
                                         </c:choose>
                                    </span>
                                </div>
                            </a>
                        </span>
                        <ul class="dropdown-menu-items">
                            <div style="padding:10px;">
                                <c:choose>
                                <c:when test="${not empty session.userId}">
                                    <a href="<c:url value='/user/logout.htm'/>">Logout</a>
                                </c:when>
                                <c:otherwise>
                                    <h3>Sign In</h3>
                                    <form action="<c:url value='/user/login.htm'/>" method="post">
                                        <table border='0' style="cellspacing:0px;">
                                            <tr>
                                                <td>Username:</td>
                                                <td>Password:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="p_username" style="width:200px" placeholder="Username" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                                <td><input type="password" name="p_password" style="width:150px" placeholder="Password" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                                <td><input type="submit" name="Submit" value="Submit"/></td>
                                            </tr>
                                        </table>
                                    </form>
                                    <br/>
                                    <span>Not already a user? <a href="<c:url value="/user/register.htm"/>" style="padding:0px; display:inline;">Register for Access</a></span>
                                </c:otherwise>
                                </c:choose>
                            </div>
                        </ul>
                    </li>					
                    <li>
                        <span class="control">
                            <a href="<c:url value="/locations.htm"/>">
                                <div class="header-sprite hdr-sprite-store left"></div>
                                <div class="right desktop-only">						    	
                                    <span class="header-label">My Branch</span><br>
                                    <span id="currentLocationName" class="header-sublabel text-limit-100px">
                                        <c:choose>
                                        <c:when test="${not empty session.warehouse}">
                                            ${currentLocation.whseName}
                                        </c:when>
                                        <c:otherwise>
                                            Find a Branch
                                        </c:otherwise>
                                        </c:choose>
                                        
                                    </span>
                                </div>
                            </a>
                        </span>
                        <ul class="dropdown-menu-items">
                            <div style="width:350px; padding-top:10px;">
                                <c:choose>
                                <c:when test="${not empty session.warehouse}">
                                    <div style="padding:6px; width:95%;">
                                    <a href="<c:url value="/locations/detail.htm?code=${currentLocation.whseId}"/>" style="padding:0px;">${currentLocation.state}${currentLocation.whseId} - ${currentLocation.whseName}</a>
                                    ${currentLocation.addr}<br/>
                                    ${currentLocation.whseName} ${currentLocation.state}, ${currentLocation.zipCd}<br/>
                                    Phone: ${currentLocation.phoneNo}<br/>
                                    Email: <a href="mailto:${currentLocation.emailAddr}" style="display:inline; padding:0px;">${currentLocation.emailAddr}</a>
                                    </div>
                                    <hr/>
                                    <div style="padding:0px 0px 6px 6px;"><a href="<c:url value="/locations.htm"/>" style="padding:0px;">Find another Branch</a></div>
                                </c:when>
                                <c:otherwise>
                                    <table width="100%">
                                        <tr>
                                            <td style="padding-left:10px;">
                                                <form action="<c:url value="/locations.htm"/>">    
                                                    <input type="submit" value="Find a Branch">
                                                </form>
                                            </td>
                                            <td style="vertical-align:top; text-align:right; padding-right:10px;">
                                                You do not have<br/> a branch selected
                                            </td>
                                        </tr>
                                    </table>
                                </c:otherwise>
                                </c:choose>
                            </div>			
                        </ul>
                    </li> 

                    <li id="control-menu-cart">
                        <span class="control">
                            <a href="<c:url value="/cart/index.htm"/>">
                                <div class="header-sprite hdr-sprite-cart left"></div>
                                <div class="right desktop-only">						    	
                                    <span class="header-label">My Cart</span><br>
                                    <span class="header-sublabel text-limit-100px">
                                        Items(${fn:length(cart)})
                                    </span>
                                </div>
                            </a>
                        </span>
                        <ul class="dropdown-menu-items" id="control-menu-cart-data">
                            <div style="width:400px; padding:10px;">
                                <c:choose>
                                    <c:when test="${fn:length(cart) > 0}">
                                        List cart lines here
                                    </c:when>
                                    <c:otherwise>
                                        Your shopping cart is empty.
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </ul>
                    </li>							       
                </ul>
            </div>			

        </div>	
    </div>

    <div id="content">
        <div class="width-fix">
            <c:if test="${fn:length(navTree) gt 0}">
            <div style="font-size:110%; padding:6px 0px 2px 10px;">
            <a href="<c:url value="/index.htm"/>">Home</a>  
            <c:forEach items="${navTree}" var="item" varStatus="itemStatus">
                <c:choose>
                    <c:when test="${itemStatus.last}">
                        > ${item.name}
                    </c:when>
                    <c:otherwise>
                        > <a href="<c:url value="/${item.url}.htm?${item.query}"/>">${item.name}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            </div>
            <hr/>
            </c:if>
            
            <div style="padding:10px;">
                <jsp:doBody/>
            </div>
        </div>
    </div>
    <br/><br/>

    <div id="test-foot" class="footer">
        <div class="width-fix">
            <table border="0" width="100%">
                <tr>
                    <td width="175">
                        <ul class="foot-menu">
                            <div class="subtitle">COMPANY INFORMATION</div>
                            <li><a href="<c:url value="/about.htm"/>">About Us</a></li>
                            <li><a href="mailto:kperry@mingledorffs.com?subject=Career Opportunities">Career Opportunities</a></li>
                            <li><a href="<c:url value="/locations.htm"/>">Locations</a></li>
                        </ul>	
                        <ul id="foot-acct-wrap" class="foot-menu">
                            <div class="subtitle">MY ACCOUNT</div>
                            <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                            <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                            <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                        </ul>
                    </td>
                    <td width="175" style="vertical-align:top;">
                        <ul class="foot-menu">
                            <div class="subtitle">CUSTOMER SERVICE</div>
                            <li><a href="mailto:ccollins@mingledorffs.com?subject=Company Contacts">Company Contacts</a></li>
                            <li><a href="mailto:twall@mingledorffs.com?subject=Website Feedback">Feedback</a></li>
                            <li><a href="mailto:ccollins@mingledorffs.com?subject=Request Information">Request Information</a></li>
                        </ul>					
                    </td>
                    <td id="test-foot3" width="175">
                        <ul class="foot-menu">
                            <div class="subtitle">MY ACCOUNT</div>
                            <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                            <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                            <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                        </ul>					
                    </td>
                    <td id="test-foot4">
                        <div id="foot-social-wrap" class="social-container">
                            <div class="subtitle">KEEP CONNECTED</div>
                            <a href="http://www.facebook.com/mingledorffs" target="_blank" class="social left" id="facebook"></a>
                            <a href="http://www.linkedin.com/company/104105?trk=tyah&trkInfo=tas%3Amingledorff%2Cidx%3A2-1-6" target="_blank" class="social left" id="linkedin"></a>
                            <a href="http://twitter.com/mingledorffs" target="_blank" class="social left" id="twitter"></a>
                            <a href="http://www.youtube.com/mingledorffs" target="_blank" class="social left" id="youtube"></a>
                        </div>					
                    </td>
                </tr>
            </table>
            <div style="padding-left:10px; color: #fff;" class="foot-menu">
                <a href="<c:url value="/terms.htm"/>">Terms & Conditions</a> | <a href="<c:url value="/privacy.htm"/>">Privacy Statement</a> | <a href="<c:url value="/sitemap.htm"/>">Sitemap</a><br/>
                Copyright &copy; 2013 Mingledorff's Inc. - All rights reserved. 
            </div> 
        </div>
    </div>

  </body>
</html>