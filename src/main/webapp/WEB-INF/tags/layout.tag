
<%@tag description="WZ Portal Layout" pageEncoding="UTF-8"%>
<%@attribute name="title" fragment="true"%>
<%@attribute name="mainSearch" fragment="true"%>
<%@attribute name="inlineScript" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<c:set var="root" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>

<html>
<head>
    <title>Holden Portal</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1, minimum-scale=1">
    <meta name="decorator" content="home" />
    <meta name="Keywords" content="Mingledorff's HVAC Distribution Carrier Bryant Payne Heil" />
    <meta name="Description" content="Mingledorff's has been in the HVAC business for over 75 years. Serving Georgia, Florida, Alabama, South Carolina and Mississippi." />
    <link rel="shortcut icon" href="http://www.mingledorffs.com/wp-content/themes/mingledorff/favicon.ico" />

    <!-- 08/7/2017<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js" async></script> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- 08/7/2017<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>-->
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" async></script>-->
    <!-- 08/7/2017<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" async></script> -->

    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />
    <!-- 08/7/2017 <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js" async></script> -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" async></script>
    <script type="text/javascript" src="http://mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js" async></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" async></script>

<!--
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstra
    p-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
-->
    <!--
    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    -->


    <!-- <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script> -->

    <!-- Bootstrap - Latest compiled and minified JavaScript -->
<!--
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!--
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="/web/jquery.ui.datepicker.min.js"></script> -->
<!--
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>
-->
    <script type="text/javascript">
        var BASE_PATH = '<c:url value="/"/>';
        var USER_ID = '<c:url value="${session.userId}"/>';
    </script>
    <script type="text/javascript" src="<c:url value="/jqModal.min.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/scripts.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/hogan.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/jquery.typeahead.js"/>" async></script>

    <jsp:invoke fragment="inlineScript"/>

<style>
    #cover {
        position: absolute;
        height: 100%;
        width: 100%;
        z-index: 1; /* make sure logout_box has a z-index of 2 */
        background-color: none;
        display: none;
    }

    .button-link {
        padding: 10px 15px;
        background: #4479BA;
        color: #FFF;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        border: solid 1px #20538D;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        -webkit-transition-duration: 0.2s;
        -moz-transition-duration: 0.2s;
        transition-duration: 0.2s;
        -webkit-user-select:none;
        -moz-user-select:none;
        -ms-user-select:none;
        user-select:none;
    }
    .button-link:hover {
        background: #356094;
        border: solid 1px #2A4E77;
        text-decoration: none;
    }
    .button-link:active {
        -webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        -moz-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        background: #2E5481;
        border: solid 1px #203E5F;
    }

</style>
    <style>
        .toggle.android { border-radius: 0px;}
        .toggle.android .toggle-handle { border-radius: 0px; }
    </style>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-86923084-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>
<script>
    $(document).ready(function() {
        var resizeTimer,
                $window = $(window);

        function imageresize()
        {
            if ($window.width() < 1200 && $window.width() > 900) {
                $('#img-logo-id').height("88px");
                $('#holden-logo-id').height("80px");
            }
            else if ($window.width() <= 900 && $window.width() > 650) {
                $('#img-logo-id').height("80px");
                $('#holden-logo-id').height("75px");
            }
            else if ($window.width() <= 650 && $window.width() > 440) {
                $('#img-logo-id').height("64px");
                $('#holden-logo-id').height("65px");
            }
            else if ($window.width() <= 440) {
                $('#img-logo-id').height("60px");
                $('#holden-logo-id').height("60px");
            }
            else {
                $('#img-logo-id').height("100px");
                $('#holden-logo-id').height("80px");
            }
        }
        imageresize();//Triggers when document first loads

        $window.resize(function() {
            //clearTimeout(resizeTimer);
            requestAnimationFrame(imageresize);
            //resizeTimer = setTimeout(imageresize, 100);
        });

        //get the products from the local storage
        var storedNumbers = JSON.parse(localStorage.getItem("snumbers"));
        //if it is empty
        if (storedNumbers == null || storedNumbers.length < 1) {
            // the array is not defined or has no elements
            storedNumbers = [];
            //get the products
            <c:forEach items="${productNames}" var="productName">
            //store the products in the local storage
            storedNumbers.push("${productName}");

            console.log("${productName}");
            </c:forEach>
            localStorage["snumbers"] = JSON.stringify(storedNumbers);
        }

        $('input.snumbers').typeahead({
            name: 'numbers',
            local: storedNumbers

        });

    });

</script>
<script>
    function clickQuerySkuNumber()
    {
        var protocol = window.location.protocol;
        var host     = window.location.host;
        var path     = window.location.pathname;
        var hostname = window.location.hostname;
        var sc = window.location.pathname.split( '/' );
        var context =  "/"+sc[1];
        //grab search term
        //if empty display a message indicating
        //get the term
        //submit the request
        var term       = document.getElementById("sku-input").value;
        var searchMode = document.getElementById("searchMode").value;
        var url = protocol + '//' + host + context + '/' + 'product/search.htm?searchMode=search&searchtyp=searchsku&term=' + term;
        window.location.href = url;
    }
</script>
<div id="cover"></div>

<div id="dialog" class="jqmWindow"></div>    
    
<div id="wrap">

<div id="header">
    <div class="width-fix">
        <div id="logo"  style="white-space: nowrap; width:80%;">
            <a href="<c:url value="/"/>">
                <c:choose>
                <c:when test="${(session.type == 'EMPLOYEE' || session.type == 'ADMINISTRATOR') && session.customerNumber != 3}">
                    <img  src="http://mgl1939.s3-website-us-east-1.amazonaws.com/img/Ming-Inc-logo-2015.png" alt="Mingledorff's Inc." title="" style="height:115%;"/>
                    <br/>
                <span style="font-size:100%; color:#014fa2; width:100%;">
                Profile Is Now Set To:${session.customerName}<br/>Customer #: ${session.customerNumber}<br/>
                <c:if test="${not empty session.shipTo}">
                Ship To #: ${session.shipTo}
                </c:if>
                </span>
                </c:when>
                    <c:when test='${param.cono == "2"}'>
                        <img id="holden-logo-id" src="/web/Holden-Transparant-logo.png" height="80px"/>
                    </c:when>
                    <c:when test='${session.cono == "2"}'>
                        <img id="holden-logo-id" src="/web/Holden-Transparant-logo.png" height="80px"/>
                    </c:when>
                <c:otherwise>
                        <img id="holden-logo-id" src="/web/Holden-Transparant-logo.png" height="80px"/>
                </c:otherwise>

                    <%-- <c:otherwise>
                        <img id="img-logo-id" src="http://mgl1939.s3-website-us-east-1.amazonaws.com/img/Ming-Inc-logo-2015.png" alt="Mingledorff's Inc." title="" height="100px"/>
                    </c:otherwise>--%>
                </c:choose>
            </a>
        </div>
        <%--<div id="navigation" style="vertical-align: text-bottom;">--%>

            <div id="navigation" style="padding-top:2em;">
            <ul class="menu">
                <input type="text" class="typeahead snumbers" id="sku-input" placeholder="Search SKU# Number" style="
    width: 200px;
    height: 34px;
    padding-left: 10px;
    display: inline;

"><input type="submit" style="background: url(//mgl1939.s3-website-us-east-1.amazonaws.com/img/search-icon-png-9982.png);background-size: 50%;background-repeat: no-repeat;background-position-x: 10px;background-position-y: 10px;color: transparent !important;  text-shadow:none !important;  border:none !important;  box-shadow:none !important;width:38px !important;height:38px !important;padding-left:0px; padding-right: 0px;" id="sku-btn" onclick="javascript:clickQuerySkuNumber()"/>              <!--<li class="has-dd js-menuhover" data-role="mobile-menu-item">-->
                    <!--<a href="<c:url value="/resources.htm"/>" style="color:black; font-size: large ; font-weight: bold; padding-top:2em;">Resources</a>--> <!-- uncomment this line to show Resource link -->
                <!--</li>-->
                <a class="button-link" href="<c:url value="/resources.htm"/>">Click Here To View Resources</a>

            </ul>

        </div>
    </div>
    <div class="mobile-search">
            <div class="header-sprite hdr-sprite-search left"></div>
    </div>
</div>

<div id="controls">
    <div class="width-fix">
        <div class="searchbar">
            <c:choose>
            <c:when test="${not empty mainSearch}">
                <jsp:invoke fragment="mainSearch"/>
            </c:when>
            <c:otherwise>
                <form id="fastKeywordSearch" class="product-search relative" action="<c:url value="/product/search.htm"/>" method="GET">
                    <input type="text" id="mainSearchQX" name="term" placeholder="Keyword or Part Number" class="term ui-autocomplete-input" value="" min="2" max="255" required="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                    <input type="hidden" id="searchMode" name="searchMode" value="">
                    <input type="submit" value="Search" class="btn-search">
                </form>                    
            </c:otherwise>
            </c:choose>            
        </div>

        <ul class="menu nav-left">
            <li class="js-desktop js-menuhover">
                <div class="products">
                    <a href="<c:url value="/pricebook/browse.htm"/>">
                        <div class="header-sprite hdr-sprite-products left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">Products</span><br />
                            <span class="header-sublabel">Browse</span>
                        </div>
                    </a>
                </div>
                <ul data-role='nav-dd'>
                    <li>
                        <span class="big categories">Product Categories</span>
                        <ul>
                        <c:forEach items="${headProdMenu}" var="item">
                            <li><a href='<c:url value="/pricebook/browse.htm?tid=${item.id}"/>'>${item.name}</a></li>
                        </c:forEach>
                            <li><a href='<c:url value="/pricebook/browse.htm"/>'>(More)</a></li>
                        </ul>
                    </li>
                    <%--
                    <li>
                        <span class="big categories">Promotions &amp; Special Interests</span>
                        <ul>
                            <li><a href='<c:url value="/pricebook/browse.htm"/>'>Pricebook Browser</a></li>
                            <li><a href='<c:url value="/product/sale.htm"/>'>Sale Items</a></li>
                            <li><a href='#'>Scratch and Dent</a></li>
                        </ul>
                    </li>
                    --%>
                </ul>
            </li>
            <li class="js-desktop js-menuhover">
                <div class="my-lists">
                    <a href="<c:url value="/user/lists/index.htm"/>">
                        <div class="header-sprite hdr-sprite-list left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Lists</span><br />
                            <span class="header-sublabel">Explore</span>
                        </div>
                    </a>
                </div>
                <c:if test="${not empty saveLists}">
                <ul data-role='nav-dd'>
                    <li>
                        <span class="big categories">My Lists</span>
                        <ul>
                            <c:forEach items="${saveLists}" var="item" begin="0" end="14">
                                <li><a href="<c:url value="/user/lists/detail.htm?id=${item.id}"/>">${item.name}</a></li>
                            </c:forEach>
                            <c:if test="${fn:length(saveLists) gt 15}">
                                <li><a href="<c:url value="/user/lists/index.htm"/>">(More)</a></li>
                            </c:if>
                        </ul>
                    </li>
                </ul>
                </c:if>
            </li> 
            <li class="mobile-only">
                <div>
                <a href="<c:url value="/resources.htm"/>">
                    <div class="header-sprite hdr-sprite-tools left"></div>
                </a>
                </div>
            </li>            
        </ul>

        <ul class="menu nav-right">
            <li id="myAccount" class="js-desktop js-menuhover">
                <div class="my-account">
                    <a href="<c:url value="/user/profile.htm"/>">
                        <div class="header-sprite hdr-sprite-account left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Account</span><br />
                            <span class="header-sublabel text-limit-100px myaccount">
                                 <c:choose>
                                 <c:when test="${not empty session.userId}">
                                     <%--<c:out value="${fn:substring(session.contact.firstName + ' ' + session.contact.lastName, 0, 12)}" />--%>
                                     ${session.contact.firstName} ${session.contact.lastName}
                                 </c:when>
                                 <c:otherwise>
                                     Sign In
                                 </c:otherwise>
                                 </c:choose>
                            </span>                            
                        </div>
                    </a>
                </div>
                <ul class="primary-dd" data-role='nav-dd'>
                    <li class="no-padding">
                        <c:choose>
                        <c:when test="${not empty session.userId}">
                            <li>
                                Username: ${session.user.username}<br/>
                                Customer #: ${session.customerNumber}<br/>
                                Sales Rep: ${session.slsRepName}<br/>
                            </li>
                            <li style="padding:0px;">
                                <hr/>
                            </li>
                            <li>
                                <a href="<c:url value='/user/logout.htm'/>">Logout</a>
                                &nbsp;|&nbsp;
                                <a href="<c:url value='/user/password.htm'/>">Change Password</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <h3>Sign In</h3>
                            <form action="<c:url value='/user/login.htm'/>" method="post">
                                <table border='0' style="cellspacing:0px;">
                                    <tr>
                                        <td>Username:</td>
                                        <td>Password:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="p_username" style="width:200px" placeholder="Username" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                        <td><input type="password" name="p_password" style="width:150px" placeholder="Password" value="" min="2" max="255" required="" autocomplete="off" /></td>
                                        <td><input type="submit" name="Submit" value="Submit"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><input type=checkbox name="cono" value="2" checked/>&nbsp;Holden Account?</td>
                                    </tr>
                                </table>
                            </form>
                            <br/>
                            <c:if test="${empty param.cono}">
                                <span>Not already a user? <a href="<c:url value="/user/register.htm"/>" style="padding:0px; display:inline;">Register for Access</a></span>
                            </c:if>
                            <c:if test="${not empty param.cono}">
                                <span>Not already a user? <a href="<c:url value="/user/register.htm?cono=2"/>" style="padding:0px; display:inline;">Register for Access</a></span>
                            </c:if>
                        </c:otherwise>
                        </c:choose>                        
                    </li>
                </ul>
            </li>

            <li id="myStore" class="js-menuhover js-desktop">
                <div>
                    <a href="<c:url value="/locations.htm"/>" id="menu-StoreLocator">
                        <div class="header-sprite hdr-sprite-store left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Store</span><br />
                            <span class="header-sublabel text-limit-100px">
                                <c:choose>
                                <c:when test="${not empty session.warehouse}">
                                    ${currentLocation.whseName}
                                </c:when>
                                <c:otherwise>
                                    Find a Branch
                                </c:otherwise>
                                </c:choose>                                
                            </span>
                        </div>
                    </a>
                </div>
                <ul class="primary-dd" data-role='nav-dd'>
                    <li>
                        <c:choose>
                        <c:when test="${not empty session.warehouse}">
                        <a href="<c:url value="/locations/detail.htm?code=${currentLocation.whseId}"/>" style="padding:0px;">${currentLocation.state}${currentLocation.whseId} - ${currentLocation.whseName}</a><br/>
                        ${currentLocation.addr}<br/>
                        ${currentLocation.whseName} ${currentLocation.state}, ${currentLocation.zipCd}<br/>
                        Phone: ${currentLocation.phoneNo}<br/>
                        Email: <a href="mailto:${currentLocation.emailAddr}" style="display:inline; padding:0px;">${currentLocation.emailAddr}</a>
                        </c:when>
                        <c:otherwise>
                        No location currently selected.
                        </c:otherwise>
                        </c:choose>
                    </li>
                    <li style="padding:0px;">
                        <hr/>
                    </li>
                    <li>
                        <a href="<c:url value="/locations.htm"/>" style="padding:0px;">Find another Branch</a>
                    </li>
                </ul>
            </li>

            <li id="myShoppingCart" class="js-menuhover js-desktop">
                <div class="my-shopping-cart">
                    <a href="<c:url value="/cart/index.htm"/>">
                        <div class="header-sprite hdr-sprite-cart left"></div>
                        <div class="left desktop-only">
                            <span class="header-label">My Cart</span><br />
                            <span class="header-sublabel">Items (<span id="myShoppingCartCount">${fn:length(cartLines)}</span>)</span>
                        </div>
                    </a>
                </div>

                <ul class="primary-dd" id="myShoppingCartLines">
                    <li>
                        <c:choose>
                        <c:when test="${fn:length(cartLines) > 0}">                        
                            <dl class="cartSummary">
                                <c:set var="cartTotal" value="${0}"/>
                                <c:forEach var="item" items="${cartLines}">
                                    <c:set var="cartTotal" value="${cartTotal + (item.qty * item.unitPrice)}"/>
                                    <dt><a href='<c:url value="/product/detail.htm"/>?prod=${util:base64Encode(item.productCode)}' style='padding:0px;'>(${item.qty}) ${item.productDetail.description}</a></dt>
                                    <dd><span class='nowrap'><fmt:formatNumber value="${item.qty * item.unitPrice}" type="currency" /></span></dd>
                                </c:forEach>

                                <dt style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'>Total</span></dt>
                                <dd style='padding-top:10px;'><span class='nowrap' style='font-weight:bold;'><fmt:formatNumber value="${cartTotal}" type="currency" /></span></dd>    
                            </dl>
                        </c:when>
                        <c:otherwise>
                            Your shopping cart is empty.
                        </c:otherwise>
                        </c:choose> 
                    </li>
                    <hr />
                    <li>
                        <a href="<c:url value="/cart/index.htm"/>">View Shopping Cart</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<c:if test="${fn:length(navTree) gt 0}">
<div id="breadcrumbs">
    <div class="width-fix" style="font-size:110%; padding:6px 0px 2px 10px;">
    <a href="<c:url value="/index.htm"/>">Home</a>  
    <c:forEach items="${navTree}" var="item" varStatus="itemStatus">
        <c:choose>
            <c:when test="${itemStatus.last}">
                > ${item.name}
            </c:when>
            <c:otherwise>
                > <a href="<c:url value="/${item.url}.htm?${item.query}"/>">${item.name}</a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    </div>
    <hr/>
</div>
</c:if>

<c:if test="${not empty title}">
<div id="title-bar">
    <div class="width-fix">
        <table width="100%" border="0">
            <tr>
                <td><h2><jsp:invoke fragment="title"/></h2></td>
                <td style="text-align:right; padding-right:4px;">
                    <c:choose>
                        <c:when test="${not empty favorite}">
                            <a href="<c:url value="/user/favorite-maint.htm?id=${favorite.id}"/>" title="Remove from Favorites">
                                <img src="https://s3.amazonaws.com/mgl1939/img/favorite_on.png" />
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="/user/favorite-add.htm?url=${util:base64Encode(currPage)}&name=${util:base64Encode(navTree[fn:length(navTree)-1].name)}"/>" title="Add to Favorites">
                                <img src="https://s3.amazonaws.com/mgl1939/img/favorite_off.png"/>
                            </a>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>
</c:if>

    <div id="content">

    <c:choose>
    <c:when test="${not empty ignoreWidthFix && ignoreWidthFix == true}">
    <div>
    </c:when>
    <c:otherwise>
    <div class="width-fix" style="padding: 0px 10px 0px 10px;">
    </c:otherwise>
    </c:choose>

        <jsp:doBody/>

    </div>
</div>

<div id="test-foot" class="footer">
    <div class="width-fix">
        <table border="0" width="100%">
            <tr>
                <td width="175">
                    <ul class="foot-menu">
                        <div class="subtitle">COMPANY INFORMATION</div>
                        <li><a href="<c:url value="/about.htm"/>">About Us</a></li>
                        <li><a href="<c:url value="/career/index.htm"/>">Career Opportunities</a></li>
                        <li><a href="<c:url value="/locations.htm"/>">Locations</a></li>
                    </ul>	
                    <ul id="foot-acct-wrap" class="foot-menu">
                        <div class="subtitle">MY ACCOUNT</div>
                        <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                        <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                        <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                    </ul>
                </td>
                <td width="175" style="vertical-align:top;">
                    <ul class="foot-menu">
                        <div class="subtitle">CUSTOMER SERVICE</div>
                        <li><a href="mailto:marketing@mingledorffs.com?subject=Company Contacts">Company Contacts</a></li>
                        <li><a href="mailto:wzsupport@mingledorffs.com?subject=Website Feedback">Feedback</a></li>
                        <li><a href="mailto:marketing@@mingledorffs.com?subject=Request Information">Request Information</a></li>
                    </ul>					
                </td>
                <td id="test-foot3" width="175">
                    <ul class="foot-menu">
                        <div class="subtitle">MY ACCOUNT</div>
                        <li><a href="<c:url value="/user/profile.htm"/>">My Account</a></li>
                        <li><a href="<c:url value="/user/register.htm"/>">Register</a></li>
                        <li><a href="<c:url value="/user/login.htm"/>">Login</a></li>
                    </ul>					
                </td>
                <td id="test-foot4">
                    <div id="foot-social-wrap" class="social-container">
                        <div class="subtitle">KEEP CONNECTED</div>
                        <a href="http://www.facebook.com/mingledorffs" target="_blank" class="social left" id="facebook"></a>
                        <a href="http://www.linkedin.com/company/104105?trk=tyah&trkInfo=tas%3Amingledorff%2Cidx%3A2-1-6" target="_blank" class="social left" id="linkedin"></a>
                        <a href="http://twitter.com/mingledorffs" target="_blank" class="social left" id="twitter"></a>
                        <a href="http://www.youtube.com/mingledorffs" target="_blank" class="social left" id="youtube"></a>
                    </div>					
                </td>
            </tr>
        </table>
        <div style="padding-left:10px; color: #fff;" class="foot-menu">
            <a href="<c:url value="/terms.htm"/>">Terms & Conditions</a> | <a href="<c:url value="/privacy.htm"/>">Privacy Statement</a> | <a href="<c:url value="/sitemap.htm"/>">Sitemap</a><br/>
            Copyright &copy;
            <c:choose>
                <c:when test='${param.cono == "2"}'>2018 Holden &amp; Associates</c:when>
                <c:when test='${session.cono == "2"}'>2018 Holden &amp; Associates</c:when>
                <c:otherwise>
                    2018 Holden &amp; Associates
                </c:otherwise>
            </c:choose>
            - All rights reserved.
        </div> 
    </div>
</div>
                
</body>
</html>