<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>

<div style="background-color:#eaeaea; padding:4px;">
    <p><b>PLEASE READ THESE TERMS AND CONDITIONS OF USE VERY CAREFULLY.</b></p>
    <p><b>By accessing or otherwise using this Web site (&quot;Web site &quot;), you agree to be bound by these terms and conditions of use.</b></p>
</div>
      
<p><b>Changes to the Terms and Conditions</b><br>
  Mingledorff's may change these terms and conditions of use at any time, without prior notice. If you access or use the Web site after Mingledorff's posts a change, you accept that change. If you do not accept the change, do not access or use the Web site. You should check these terms and conditions of use periodically.</p>

<p><b>Access and Use of the Web site</b><br>
  Mingledorff's grants you a limited license to access and use the Web site, including to display, copy, distribute and download its content, only for your personal, non-commercial use and provided that you do not modify the Web site, its content, or any copyright or other proprietary notices. This license terminates automatically if you breach any of these terms and conditions of use. Upon termination, you must immediately destroy all copies in your possession.</p>

<p>Unauthorized use of the Web site or any content may violate copyright laws, trademark laws, the laws of privacy and publicity, and communications regulations and statutes.</p>
<p><b>Restrictions on Use of the Web site</b><br>
  Except as expressly authorized above, you may not copy, modify, distribute, download, display, transfer, post, or transmit the Web site or its content in any form without Mingledorff's prior written permission.</p>

<p>The following activities are also expressly prohibited without Mingledorff's prior written permission: any non-personal or commercial use; use of any robot, spider, other automatic device, or manual process to monitor or copy the Web site or any of its content; &quot;mirroring&quot; the Web site or any content on any other server; collection or use of product listings, descriptions, or prices for a supplier of competitive or comparable products; and any action that imposes an unreasonable or disproportionately large load on the Web site or otherwise interferes with its functioning.</p>

<p><b>Posting and Transmitting to the Web site</b><br>
  Except for information that is subject to the Mingledorff's Privacy Policy, Mingledorff's will not treat information you post or transmit to the Web site as confidential or proprietary. By sending information to Mingledorff's through the Web site (by transmitting, posting, or otherwise), you agree that Mingledorff's may use that information in any way, for any purpose, including Mingledorff&#146;s own commercial purposes, subject only to the Mingledorff's Privacy Policy.</p>

<p>You may not post or transmit any threatening, defamatory, sexually graphic, inflammatory, profane, or other inappropriate material. Mingledorff's reserves the right to edit or remove any post or transmission that, in its judgment, is not appropriate.</p>
<p><b>Accuracy of Information and Disclaimer of Warranty</b><br>
  Mingledorff's has made every effort to present the content on the Web site accurately, but additions, deletions and changes may occur. Content on the Web site is provided &#147;as is&#148;; neither Mingledorff's nor its representatives make any representation or warranty with respect to the content.</p>

<p>Mingledorff's and its representatives specifically disclaim, to the fullest extent permitted by law, any and all warranties, express or implied, relating to the Web site or its content, including but not limited to, implied warranties of merchantability, completeness, timeliness, correctness, noninfringement, or fitness for any particular purpose.</p>

<p><b>Limitation of Liability</b><br>
  Under no circumstances will Mingledorff's or its affiliates have any liability with respect to any claims or damages (whether direct or indirect, special, incidental, consequential or punitive) as a result of your access or use of (or inability to access or use) this Web site or its content, even if they have been advised of the possibility of such damages. You access and use this Web site at your own risk.</p>

<p><b>Links to this Web site</b><br>
  Mingledorff's grants you a limited, revocable, and nonexclusive right to create a hyperlink to the home page of the Web site so long as the link does not portray Mingledorff's or its products or services in a false, misleading, derogatory, or offensive matter. You may not use the Mingledorff's logo, Mingledorff's trademark, or Mingledorff's name or trademarks, or other proprietary graphic in the link without the prior written permission of Mingledorff's.</p>
<p><b>Links to Third Party Web sites</b><br>
  Mingledorff's does not review or control third party Web sites that link to or from the Web site, is not responsible for their content, and does not represent that their content is accurate or appropriate. Your use of such third party site is on your own initiative and at your own risk and may be subject to the other sites&#146; terms of use.</p>

<p><b>Ownership</b><br>
  The material provided on this Web site is protected by law, including, but not limited to, United States copyright law and international treaties. The copyright in the content of the Web site is owned by Mingledorff's or others. Except for the limited rights granted above, all other rights are reserved.</p>

<p>All trademarks are the property of their respective owners. Some images are courtesy of manufacturers.</p>
<p><b>Privacy</b><br>
  Please review the Mingledorff's Privacy Policy, which also governs your visit to this Web site.</p>
<p><b>Last Update:</b> 08/03/04<br>

</p>      
      
  </jsp:body>
</t:layout>