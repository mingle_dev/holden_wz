<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>
      
<p>      
<b>Effective Date:</b> August 6, 2004
</p>
<p>
Mingledorff's is committed to protecting your privacy. That is because we base our business on the trust you place in us. This policy describes our practices regarding personal and account information collected through our Web site. In this policy, "personal and account information" means your name, company name, account number, address, telephone number, and e-mail address plus any other information that personally identifies you, including your industry, the number of employees at your company, and purchase history, or would permit us to contact you, and "we" and "us" means Mingledorff's as well as its affiliated companies.
</p>
<p>
We reserve the right to modify this privacy policy from time to time; the effective date noted above will indicate the date that this policy was last updated. You should visit our Web site periodically to review any changes.
</p>
<p>
<b>Is my personal or account information used for any other purpose?</b><br/>
We may use your personal and account information to provide products or services you have requested, respond to a communication from you, contact you, and as otherwise described in this policy.
</p>
<p>
We may occasionally send you promotional or product information. If you do not wish to receive promotional or product information, you may opt out of future communications by following the instructions in the e-mail communication or contacting your territory manager.
</p>
<p>
<b>Will my personal and account information be provided to any other party?</b><br/>
We restrict access to your personal and account information to those who need access to use it as set forth in this policy. Only authorized personnel are permitted to see your weatherzone account information. Your other personal and account information may be disclosed to third parties in limited circumstances. For example, we may share your personal and account information with our suppliers and other entities involved in transactions you have initiated. We may disclose your personal and account information to respond to subpoenas, court orders, or other legal process, as required by law, or to establish or exercise our legal rights or defend against legal claims. Your personal and account information may also be disclosed in connection with a sale of some or all of our business or a merger with another company.
</p>
<p>
<b>How do you protect my personal and account information?</b><br/>
We use 128-bit encryption technology and Secure Socket Layers ("SSL") in all areas where your personal and account information is required. In addition, your personal and account information and transaction history are kept behind our firewall on separate servers.
</p>
<p>
<b>How can I give you feedback or contact you?</b><br/>
Simply click here. Please note that messages become our property and, unless you direct otherwise, may be used by us for promotional purposes.
</p>
<p>
<b>How can I correct inaccurate information?</b><br/>
You may correct some personal and account information maintained in your Weatherzone Account through your Profile. To correct any other information, please contact your territory manager.
</p>
  </jsp:body>
</t:layout>