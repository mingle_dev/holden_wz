<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>

  <jsp:body>

  <h1>Accessories</h1>
    <form method="POST" action="acc-save.htm">
      <input type="hidden" name="qid" value="${qid}"/>
      <input type="hidden" name="sid" value="${sid}"/>
     <div class="fake-table">
        <div class="header">
          <div class="row">
            <div class="cell">Product</div>
            <div class="cell txt-center">Quantity</div>
            <div class="cell txt-right">Sell Price</div>
            <div class="cell txt-right">Margin %</div>
          </div>
        </div>
        <div class="body">
          <c:forEach items="${accessories}" var="acc">
              <div class="row">
                <div class="cell">
                    ${acc.product}<br/>
                </div>
                <div class="cell">
                  <input type="text" value="0"/>
                </div>
                <div class="cell">
                  <input type="text" value="${acc.sellPrice}"/>
                </div>
                <div class="cell">
                  <input type="text"/>
                </div>
              </div>
          </c:forEach>
        </div>
      </div><br/><br/>

      <div class="right">
        <input type="submit" name="submit" value="Cancel"/>
        <input type="submit" name="submit" value="Add"/>
      </div>
    </form>
  </jsp:body>
</t:layout>