<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
  <jsp:attribute name="title">
    ${navTree[fn:length(navTree)-1].name}
  </jsp:attribute>

  <jsp:body>
    <div class="section">
        <div>
            <div id="product-categories">
                <c:forEach items="${categories}" var="item">
                <div class="fourth">
                    <c:choose>
                    <c:when test="${not empty tid}">
                    <c:if test="${session.type == 'ADMINISTRATOR'}">
                    [ <a href="<c:url value="/pricebook/upload-image.htm?id=${item.id}&type=sheet"/>">Edit Image</a> ]
                    </c:if>
                    <a href="<c:url value="/pricebook/browse.htm?tid=${tid}&sid=${item.id}"/>" title="${item.name}" class="product">
                    </c:when>

                    <c:otherwise>
                    <c:if test="${session.type == 'ADMINISTRATOR'}">
                    [ <a href="<c:url value="/pricebook/upload-image.htm?id=${item.id}&type=template"/>">Edit Image</a> ]
                    </c:if>
                    <a href="<c:url value="/pricebook/browse.htm?tid=${item.id}"/>" title="${item.name}" class="product">
                    </c:otherwise>

                    </c:choose>
                    <h3>
                        <span class="img">
                            <c:choose>
                             <c:when test="${not empty item.image}">
                             <img src="data:image/jpeg;base64, ${item.image}"/>
                             </c:when>
                             <c:otherwise>
                             <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${item.name}">
                             </c:otherwise>
                             </c:choose>
                        </span>
                        ${item.name}
                    </h3>
                    </a>
                </div>
                </c:forEach>
            </div>
        </div>  
    </div> 
  </jsp:body>
</t:layout>