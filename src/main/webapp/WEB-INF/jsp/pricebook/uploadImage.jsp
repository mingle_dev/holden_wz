<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>

      <h3>Upload Image</h3>
      Images are saved exactly as they look and are not processed in any way. Images must be 75px x 75px.<br/><br/>
      <form action="<c:url value="/pricebook/upload-image.htm"/>" method="post" enctype="multipart/form-data">
          Select image to upload:
          <input type="hidden" name="id" value="${param.id}">
          <input type="hidden" name="type"  value="${param.type}">
          <input type="file" name="file" id="file">
          <input type="submit" value="Upload" name="submit">
      </form>

  </jsp:body>
</t:layout>