<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
  <jsp:body>

<style>
.fake-table {
    display: table;
    width: 100%;
    border: 1px solid #c8ccce;
    border-radius: 5px;
    overflow: hidden;
    border-collapse: separate;
    border-spacing: 0;
  
}
.fake-table .header {
    display: table-header-group;
    background: #eaeaea;
}
.fake-table .row {
    display: table-row;
    width: auto;
}
.fake-table .row .cell {
    display: table-cell;
    line-height: 1.5em;
    padding: 0.5em 15px;
    vertical-align: top;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .header .cell {
    color: #555;
    font-weight: bold;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .body {
    display: table-row-group;
}
.center {
    text-align: center;
}

@media screen and (max-width: 700px) {
    .fake-table .header {
        display: none;
    }
    .fake-table .row .cell.narrow-hide {
        display: none;
    }
}
</style>

<table width="100%">
    <tr>
        <td>
            <div style="font-size:200%; padding:10px;">Saved Shopping Lists</div>
        </td>
        <td style="text-align:right;">
            <a class="ui-link-button" href="<c:url value="/user/lists/shares.htm"/>" style="margin-top:6px;">Shared with Me</a>
        </td>
    </tr>
</table>

<c:choose>
    <c:when test="${fn:length(lists) > 0}">

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">List</div>
            <div class="cell"><div class="center">Items</div></div>
            <div class="cell"><div class="center">Tools</div></div>
        </div>
    </div>
    <div class="body">
        <c:forEach var="list" items="${lists}">
        <div class="row">
            <div class="cell" style="font-size:125%">
                <c:if test="${list.shared}">
                    <img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/shared_ico.png" title="This list is shared with others"/>
                </c:if>
                <a href="<c:url value="/user/lists/detail.htm?id=${list.id}"/>">${list.name}</a>
            </div>
            <div class="cell narrow-hide"><div class="center">${list.numItems}</div></div>
            <div class="cell narrow-hide">
                <div class="center">
                    <c:choose>
                    <c:when test="${session.type eq 'EMPLOYEE' || session.type eq 'ADMINISTRATOR'}">
                        <a href="<c:url value="/user/lists/admin-share.htm?id=${list.id}"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/share_ico.png" border="0"/></a>&nbsp;
                    </c:when>
                    <c:otherwise>
                        <a href="<c:url value="/user/lists/share.htm?id=${list.id}"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/share_ico.png" border="0"/></a>&nbsp;
                    </c:otherwise>
                    </c:choose>
                    <a href="<c:url value="/user/lists/clone.htm?id=${list.id}"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/clone_ico.png" border="0"/></a>&nbsp;
                    <a href="<c:url value="/user/lists/edit.htm?id=${list.id}"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/edit_ico.png" border="0"/></a>&nbsp;
                    <a href="<c:url value="/user/lists/delete.htm?id=${list.id}"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/delete_ico.png" border="0"/></a>
                </div>
            </div>
        </div>
        </c:forEach>
    </div>
</div>
      
    </c:when>
    <c:otherwise>
        <div style="font-size:125%;padding:20px;">
            <br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/yellow_alert.png" style="width:32px;vertical-align:middle;"/>
            You do not have any Saved Lists created. To create a list, start by using our
            Product browser or use the Search above.
            <br/><br/><br/><br/><br/><br/><br/><br/>
        </div>
    </c:otherwise>
</c:choose>

  </jsp:body>
</t:layout>