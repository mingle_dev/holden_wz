<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>
<style>
.border {
    border: 1px solid #c8ccce;
}
.pad10 {
    padding: 10px;
}

.nowrap {
    white-space:nowrap;
}

input[readonly] {
    background-color: #eaeaea;
}
</style>

<%-- ${session} --%>

<c:if test="${not empty message}">
    <img src="https://s3.amazonaws.com/mgl1939/img/green_check.png" style="width:32px; vertical-align:middle;"/>
    ${message}<br/><br/><br/>
</c:if>

<form:form method="POST" commandName="profileForm" action="/web/user/profile.htm"> 
    
<div id="details-main" class="container border">    
    <h2 class="header active">User Preferences</h2>
    <div class="section pad10">

<table border="0" width="100%">
    <tr>
        <td>
            <dl class="wide">
                <dt><label class="label-input">Username:</label></dt>
                <dd><input type="text" readonly="readonly" value="${user.username}" style="width:225px" /><br/><br/></dd>
                <dt><label class="label-input">Customer #:</label></dt>
                <dd>
                    <c:choose>
                    <%-- @todo; use enum Role for admin or employee group --%>
                    <c:when test="${user.groupId == '4d5c0d92-9630-4a23-b233-399ac0a80a21' or user.groupId == '4d6f9898-d0ac-4256-9069-5940c0a80a21'}">
                        <%--<form:input type="text" path="customerNumber" value="${session.customerNumber}" style="width:100px" />--%>
                        <input type="text" readonly="readonly" value="${session.customerNumber}" style="width:100px" />
                        <a href="<c:url value="/account/admin-search.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/search.png" style="height:36px; vertical-align:bottom; border:0px;"/></a>
                        <a href="<c:url value="/user/custno-reset.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/cancel.png" style="height:36px; vertical-align:bottom; border:0px;"/></a>
                        <br/><br/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" readonly="readonly" value="${session.customerNumber}" style="width:100px" /><br/><br/>
                    </c:otherwise>
                    </c:choose>
                </dd>
                <dt><label class="label-input">Assigned Warehouse:</label></dt>
                <dd>
                    <c:forEach var="location" items="${locations}">
                        <c:if test="${location.whseId == session.account.salesTerritory}">
                        <input type="text" readonly="readonly" value="${location.whseName} - ${location.state}${location.whseId}" /><br/><br/>
                        </c:if>
                    </c:forEach>
                </dd>
                <dt><label class="label-input">Current Warehouse:</label></dt>
                <dd>
                    <form:select path="warehouse">
                        <c:forEach var="location" items="${locations}">
                        <option value="${location.whseId}"<c:if test="${location.whseId == user.warehouse}">SELECTED</c:if>>${location.whseName} - ${location.state}${location.whseId}</option>
                        </c:forEach>
                    </form:select><br/><br/>
            </dd>
            <dt><label class="label-input">Job Account: <%--${user.shipTo}--%></label></dt>
            <dd>
                <form:select path="jobAccount">
                    <option value=""></option>
                    <c:forEach var="shipto" items="${shipToList}">
                    <option value="${shipto.id}"<c:if test="${shipto.id == user.shipTo}">SELECTED</c:if>>${shipto.name} - ${shipto.id}</option>
                    </c:forEach>
                </form:select><br/><br/>
            </dd>
            <dt><label class="label-input">Default ShipVia:</label></dt>
            <dd>
                <form:select id="shipVia" path="shipVia">
                    <option value=""></option>
                    <option value="MDS"<c:if test="${user.shipVia == 'MDS'}">SELECTED</c:if>>Mingledorff's Truck</option>
                    <option value="BWYB"<c:if test="${user.shipVia == 'BWYB'}">SELECTED</c:if>>Motor Freight</option>
                    <option value="PKP"<c:if test="${user.shipVia == 'PKP'}">SELECTED</c:if>>Pickup</option>
                    <option value="UPGB"<c:if test="${user.shipVia == 'UPGB'}">SELECTED</c:if>>UPS Ground</option>
                    <option value="UP2B"<c:if test="${user.shipVia == 'UP2B'}">SELECTED</c:if>>UPS 2nd Day</option>
                    <option value="UPAB"<c:if test="${user.shipVia == 'UPAB'}">SELECTED</c:if>>UPS Next Day</option>
                </form:select><br/><br/>         
            </dd>
            <dt><label class="label-input">Email Address:</label></dt>
            <dd>
                <form:input type="text" path="emailAddr" value="${contact.emailAddr}" style="width:225px" /><form:errors path="emailAddr" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt><label class="label-input">Office Phone:</label></dt>
            <dd><form:input type="text" path="officePhone" value="${contact.phoneWork}" /><br/><br/></dd>
            <dt><label class="label-input">Records Per Page:</label></dt>
            <dd>
                <form:select id="recPerPage" path="pageLimit">
                    <option value="20"<c:if test="${user.pageLimit == 20}">SELECTED</c:if>>20 Records</option>
                    <option value="40"<c:if test="${user.pageLimit == 40}">SELECTED</c:if>>40 Records</option>
                    <option value="60"<c:if test="${user.pageLimit == 60}">SELECTED</c:if>>60 Records</option>
                </form:select><br/><br/>        
            </dd>
            <dt><label class="label-input">Reset Warehouse:</label></dt>
            <dd>
                <form:select id="resetWhse" path="resetWhse">
                    <option value="0">False</option>
                    <option value="1"<c:if test="${user.resetWhse == true}">SELECTED</c:if>>True</option>
                </form:select><br/><br/> 
            </dd>
            <dt><label class="label-input">Shipment Notifications:</label></dt>
            <dd>
                <form:select id="shipNotify" path="shipNotifyType">
                    <option value="">None</option>
                    <option value="online"<c:if test="${user.shipNotifyType == 'online'}">SELECTED</c:if>>My Online Orders</option>
                    <option value="all"<c:if test="${user.shipNotifyType == 'all'}">SELECTED</c:if>>All Orders</option>
                </form:select><br/><br/>
            </dd>
                <dt><label class="label-input">Availability:</label></dt>
                <dd>
                    <form:select id="availType" path="availType">
                        <option value="local"<c:if test="${user.availType == 'local'}">SELECTED</c:if>>Local Availability</option>
                        <option value="all"<c:if test="${user.availType == 'all'}">SELECTED</c:if>>Company Availability</option>
                    </form:select><br/><br/>
                </dd>
        </dl>
    </td>
    <%--
     <td class="desktop-only" style="vertical-align:top;">
         <br/><br/>
            <div style="font-weight:800; padding-bottom:6px;">Job Roles:</div>
            <table border="0" cellpadding="4">
                <tr>
                  <td class="nowrap">
                      <input type="checkbox" id="attr:Manufacturer_ADP" name="jobRoles" class="css-checkbox" value="6a94f92f-b5c3-4d0a-b88f-7c779186c46d" checked="CHECKED">
                      <label for="attr:Manufacturer_ADP" name="attr:Manufacturer_ADP_lbl" class="css-label">Accounts Payable</label>
                  </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="6a94f92f-b5c3-4d0a-b88f-7c779186c46d"/> Administrative<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="fc9b0ee1-730e-4fca-9091-7cd36d4005ad" checked="checked"/> Architect<br/>
                      </td>
                </tr>
                 <tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="a30ee9de-9cf2-4494-876b-23ecfc8c8c8e"/> Bookkeeping<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="d64c9ad4-68cd-47bf-87bb-2ea93267cc59"/> Branch Manager<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="e50c5b67-460f-4e51-8dc3-61a7af8b811b"/> Building Maintenance<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="859f9013-88dc-42c0-90d2-948f2eb45a24"/> Engineer<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="e7de3d88-9404-483d-98d9-094420a672e0" checked="checked"/> General Manager<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="652b367c-520e-4ac8-8959-73fe906ef522" checked="checked"/> Information Technology<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="a0749fb0-d8ce-40be-bebd-d7b1c67c4619" checked="checked"/> Installation Manager<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="f0d2875b-3094-4a0a-a805-34e3a4c45bf8"/> Instructor/Trainer<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="6f279766-71e2-4b25-a5d1-2edf6004e5f3" checked="checked"/> Office Manager<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="17e88cf3-011a-499c-87a8-9441f06d4d4d" checked="checked"/> Principal/Owner<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="8198750e-6a21-4fa0-833a-96787c6df1a7"/> Purchasing<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="eee0d32e-b5a5-4f36-95e3-a58b788fdcba"/> QAD<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="327830d0-cd71-4547-9cbb-392e69429760" checked="checked"/> Retail Salesperson<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="91f59df3-5917-44f3-a7bb-4e2e570c19bd" checked="checked"/> Sales Manager<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="719f862e-6713-455e-86f6-a37ebe63a619" checked="checked"/> Service Manager<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="602daf9a-61f3-471a-b1b6-776ed962f830" checked="checked"/> Service Technician<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="93103c24-376d-4199-b189-bba0a1648e32" checked="checked"/> Student<br/>
                      </td>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="7867cb78-5435-48dd-8fd9-acc457e1dd4e"/> Warehouse<br/>
                      </td>
                  <tr>
                      </tr>
                  <td class="nowrap">
                      <input type="checkbox" name="jobRoles" value="9cb1ad4f-4a19-4132-a7b1-556d45729a9d" checked="checked"/> Warranty<br/>
                      </td>
                  </tr>
                  </table>                      a

                  </td>
                --%>
              </tr>
          </table>
    </div>        
</div>      
<br/>

<div class="section">
    <input type="submit" value="Update Profile"/>
    <%-- <a class="mobile-only ui-link-button" href="<c:url value="/user/logout.htm"/>">Log Off</a> --%>
    <input type="button" value="Change Password" onclick="window.location='${root}/web/user/password.htm'"/>
</div>
<br/>
</form:form>
  </jsp:body>
</t:layout>