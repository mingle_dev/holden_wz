<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<style>
#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
#sortable li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; height: 1.5em; }

.ui-state-highlight { 
    height: 80px !important;
    border: 1px solid #fcefa1;
    background: #fbf9ee;
    color: #363636;
}     

.ui-state-default {
    border: 1px solid #d3d3d3;
    background: #fff;
    color: #555555;
} 
.content-desktop-msg {
    display: none;
}    
    
.ui-link-button {
    background-color:#cc0000;
    text-indent:0;
    display:inline-block;
    color:#ffffff;
    font-size:12px;
    font-weight:normal;
    font-style:normal;
    padding: 10px 12px 10px 12px;
    text-decoration:none;
    text-align:center;
}.ui-link-button:hover {
    color:#ffffff;
    opacity:0.7;
    filter:alpha(opacity=70);
}

@media screen and (max-width: 700px) {
    .content-desktop {
        display: none;
    }
    .content-desktop-msg {
        display: block;
    }
}

</style>

<script>
$(function() {
    $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight"
    });
    $( "#sortable" ).disableSelection();
    
    $('form[name=userDescripForm]').on('change', function(e) {
        e.preventDefault();      

        var values = $(this).serialize();        

        console.log(values);

        //ajax to save item to list
        $.ajax({
            url: "/web/user/product/update-item.htm",
            type: "post",
            data: values,
            success: function(data){
                data = JSON.parse(data);
                if(data.status == true)
                    noty({type: 'information', text: data.message});
                else
                    noty({type: 'error', text: data.message});
            },
            error:function(){
                noty({type: 'error', text: 'An critical error occurred. Refresh page and try again.'});
            }
        });        
    });
    
    $('form[name=listNameForm]').on('change', function(e) {
        e.preventDefault();      

        var values = $(this).serialize();        

        console.log(values);

        //ajax to save item to list
        $.ajax({
            url: "/web/user/lists/rename.htm",
            type: "post",
            data: values,
            success: function(data){
                data = JSON.parse(data);
                if(data.status == true)
                    noty({type: 'information', text: data.message});
                else
                    noty({type: 'error', text: data.message});
            },
            error:function(){
                noty({type: 'error', text: 'An critical error occurred. Refresh page and try again.'});
            }
        });        
    });    
});
</script>      

<div class="content-desktop-msg" style="font-size:200%; margin: 20px 0px 20px 4px;">
    This feature is only available on displays with a resolution of 800x600 and greater.
</div>

<div class="container content-desktop" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
    <div class="left" style="font-size:150%; padding:10px;">
        <form name="listNameForm" id="listNameForm" class="cartForm">
            <input type="hidden" name="id" value="${list.id}"/>
            <input type="text" name="name" style="width:500px; padding-right: 10px;" value="${list.name}"/>
        </form>
    </div>

    <div class="right" style="font-size:150%; padding:10px;">
        <a class="ui-link-button" href="<c:url value="/user/lists/detail.htm?id=${list.id}"/>">Finish</a>
    </div>
</div>
<br/><br/>

<ul class="content-desktop" id="sortable">
    <c:forEach var="item" items="${list.listItems}">
        <li class="ui-state-default" style="height:80px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100px;"><img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.product.image}"></td>
                    <td>
                        <span style="font-size:125%; color:#00599c;">${item.product.product}</span><br/>
                        ${item.product.description}
                        <form name="userDescripForm" id="userDescripForm" class="cartForm">
                             <input type="hidden" name="prod" style="width:65px; text-align: right; padding-right: 10px;" value="${util:base64Encode(item.product.product)}"/>
                             <input type="text" name="descrip" value="${item.product.userDescription}" style="width:80%; padding-right: 10px;" placeholder="Enter a custom description"/>
                         </form>
                    </td>
                    <td style="width:100px;">
                        Default Qty:<br/>
                         <form name="cartItemForm" id="cartItemForm" class="cartForm">
                             <input type="hidden" name="prod" style="width:65px; text-align: right; padding-right: 10px;" value="${util:base64Encode(item.product.product)}"/>
                             <input type="text" name="qty" style="width:65px; text-align: right; padding-right: 10px;" value="1"/>
                         </form>
                    </td>
                    <td style="width:100px;">
                        <div class="right" style="font-size:150%; padding:10px;">
                            <a class="ui-link-button" href="<c:url value="/user/lists/detail.htm?id=${list.id}"/>">Remove</a>
                        </div>
                    </td>
                </tr>
            </table>
        </li>
    </c:forEach>    
</ul>

  </jsp:body>
</t:layout>