<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>
        <style>
        .formSuccess {
            padding: 10px 10px 10px 40px;
            border: 1px solid #90a03e;
            background: #bed352 url(//mgl1939.s3-website-us-east-1.amazonaws.com/img/success.gif) no-repeat 15px 12px;
            overflow: hidden;
            zoom: 1;
        }            
        .formError {
            padding: 10px 10px 10px 40px;
            border: 1px solid #d51d1d;
            background: #ffcece url(//mgl1939.s3-website-us-east-1.amazonaws.com/img/failure.png) no-repeat 15px 12px;
            overflow: hidden;
            zoom: 1;
        }
        .fieldError {
            color: #ff0000;
            padding-left: 4px;
            white-space: nowrap;
            display: inline-block;
        }
        </style>

        <h1>User Registration</h1>
      <c:if test="${not empty formError}">
          <div class="formError">
              There are one or more errors.
          </div><br/><br/>
      </c:if>

      <table style="width:100%"><tr><td>

      <c:if test="${param.cono == 2}">
          <c:set value="selected" var="cono2"></c:set>
      </c:if>
      <c:if test="${param.cono == null || param.cono != 2}">
          <c:set value="selected" var="cono1"></c:set>
      </c:if>

    <form:form method="POST" commandName="registerForm" action="/web/user/register.htm">

        <dl class="wide">
                <dt>
                    <label class="label-top">Distributor's Name:</label>
                </dt>
                <dd>
                    <form:select path="cono">
                        <form:option value="1" selected="${cono1}">Mingledorff's</form:option>
                        <form:option value="2" selected="${cono2}">Holden</form:option>
                    </form:select>
                </dd>
                <dt>
                    <label class="label-top">First / Last Name:</label>
                </dt>
                <dd>
                    <form:input type="text" path="firstName"/>
                    <form:input type="text" path="lastName"/><form:errors path="firstName" element="div" cssClass="fieldError" /><br/><br/>
                </dd>           
                <dt><label class="label-top">Phone #:</label></dt>
                <dd>
                    <form:input type="text" path="phoneNo"/><br/><br/>
                </dd>
                <dt><label class="label-top">Email Address:</label></dt>
                <dd>
                    <form:input type="text" path="emailAddr" style="width:200px"/><form:errors path="emailAddr" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt><label class="label-top">Desired Login Name:</label></dt>
                <dd>
                    <form:input type="text" path="username" style="width:200px" autocomplete="off"/><form:errors path="username" element="div" cssClass="fieldError" /><br/>
                    <span>Your username can be your email address.</span><br/><br/>
                </dd>
                <dt><label class="label-top">Choose a Password:</label></dt>
                <dd>
                    <form:input type="password" path="password1" style="width:200px" autocomplete="off"/><form:errors path="password1" element="div" cssClass="fieldError" /><br/>
                    <span>Minimum of 8 characters in length.</span><br/><br/>
                </dd>                
                <dt><label class="label-top">Re-Enter Password:</label></dt>
                <dd>
                    <form:input type="password" path="password2" style="width:200px" autocomplete="off"/><form:errors path="password2" element="div" cssClass="fieldError" /><br/><br/>
                </dd>                
                <dt><label class="label-top">Company Name:</label></dt>
                <dd>
                    <form:input type="text" path="companyName" style="width:200px"/><form:errors path="companyName" element="div" cssClass="fieldError" /><br/><br/>
                </dd>                
                <dt><label class="label-top">Account #:</label></dt>
                <dd>
                    <form:input type="text" path="accountNo" style="width:200px"/><form:errors path="accountNo" element="div" cssClass="fieldError" /><br/>
                    <span>Your account # can be found on your invoices and statements</span><br/><br/>
                </dd>
                <dt><label class="label-top">Invoices:</label></dt>
                <dd>
                    <form:input type="text" path="invoice1" style="width:200px"/>
                    <form:input type="text" path="invoice2" style="width:200px"/><form:errors path="invoice1" element="div" cssClass="fieldError" /><br/>
                    <span>Provide two invoice #'s from your recent purchase history</span><br/><br/>                    
                </dd>
            </dl>
            <div style="padding-left:15px;">
            <input id="tos_agreement" name="tos" class="css-checkbox" type="checkbox" value="true" <%--CHECKED="CHECKED"--%> />
            <label for="tos_agreement" name="tos_agreement_lbl" class="css-label">I have read and agree to the <a href="">Terms of Service</a>.</label><br/>
            <form:errors path="tos" element="div" cssClass="fieldError" /><br/><br/>
            <input type="submit" name="register" value="Register"><br/><br/>
            </div>
        </form:form>
            
        </td>
        <td class="desktop-only" style="vertical-align:top; width:400px;">
            <h2>Why Register?</h2>
            <ul style="line-height:1.5em;">
                <li><b>Fast Ordering Options</b> - </li>
                <li><b>Save Favorite Items</b> - </li>
                <li><b>Order Status</b> - View your order status online</li>
                <li><b>Store Availability</b> - Check product availability at any of our locations</li>
                <li><b>Invoices and Statements</b> - View invoices online and print them when you are ready to pay</li>
                <li><b>Invoice Gateway</b> - Pay your invoices quick and easy</li>
            </ul>
        </td></tr></table>
  </jsp:body>
</t:layout>

