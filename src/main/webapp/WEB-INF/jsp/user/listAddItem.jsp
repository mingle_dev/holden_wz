<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:modal>
    <jsp:attribute name="modalTitle">
        Save Item
    </jsp:attribute>
    
  <jsp:body>

<script>      
$(document).ready(function () {
    /* this can be moved to a global js file */
    $('form[name=saveListAddItemForm]').on('submit', function(e) {
        e.preventDefault();      

        var values = $(this).serialize();        
        
        //ajax to save item to list
        $.ajax({
            url: "/web/user/lists/add-item.htm",
            type: "post",
            data: values,
            beforeSend: function(){
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
            },
            success: function(data){
                data = JSON.parse(data);
                if(data.status == true)
                    noty({type: 'information', text: data.message});
                else
                    noty({type: 'error', text: data.message});
            },
            error:function(){
                noty({type: 'error', text: 'An error occurred while saving item'});
            }
        });        
        
        $('#dialog').jqmHide();
    });
});
</script>
      
    <style>
    .label-input {
        font-weight: 800;
        display:block;
        clear:both;
        padding-top:12px;
        padding-bottom: 8px;
    }
    
    @media screen and (min-width: 684px) {
        .label-input {
            float: left;
            width: 200px;
            text-align: left;
            margin-right: 0.5em;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
        }
        .form-field-no-caption {
            margin-left: 200px;
            padding-left: 0.5em;
        }        
    }    
    </style>

    <form name="saveListAddItemForm" id="saveListAddItemForm" class="cartForm">
        <input type="hidden" name="prod" value="${productCode}"/>
        <label class="label-input">Save List:</label>
        <select name="list_id" style="max-width:275px;">
            <option disabled selected>Choose an existing list</option>
            <c:forEach var="list" items="${lists}">
                <option value="${list.id}">${list.name}</option>
            </c:forEach>
        </select>
        <span style="padding:10px; font-size:125%">OR</span>
        <input type="text" name="new_listname" size="30" value="" placeholder="Enter new list name"  style="margin-top:10px;"/>
        <br/><br/>    
        
        <label class="label-input">Default Qty:</label>
        <input type="text" name="qty" value="0" style="width:65px; text-align: right; padding-right: 10px;"/>
        <br/><br/>       
        
        <span class="form-field-no-caption">     
            <input type="submit" class="btnCart" name="Add" value="Add"/>
        </span>
    </form>

  </jsp:body>
</t:modal>