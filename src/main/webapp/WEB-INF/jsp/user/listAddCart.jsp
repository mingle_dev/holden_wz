<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:modal>
    <jsp:attribute name="modalTitle">
        Save Shopping Cart
    </jsp:attribute>
    
  <jsp:body>

<script>      
$(document).ready(function () {
    /* this can be moved to a global js file */
    $('form[name=saveListAddItemForm]').on('submit', function(e) {
        e.preventDefault();      

        var values = $(this).serialize();        
        
        //ajax to save item to list
        $.ajax({
            url: "/web/user/lists/new-from-cart.htm",
            type: "post",
            data: values,
            beforeSend: function(){
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
            },
            success: function(data){
                data = JSON.parse(data);
                if(data.status == true)
                    noty({type: 'information', text: data.message});
                else
                    noty({type: 'error', text: data.message});
            },
            error:function(){
                noty({type: 'error', text: 'An error occurred while saving item'});
            }
        });        
        
        $('#dialog').jqmHide();
    });
});
</script>
      
    <style>
    .label-input {
        font-weight: 800;
        display:block;
        clear:both;
        padding-top:12px;
        padding-bottom: 8px;
    }
    
    @media screen and (min-width: 684px) {
        .label-input {
            float: left;
            width: 200px;
            text-align: left;
            margin-right: 0.5em;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
        }
        .form-field-no-caption {
            margin-left: 200px;
            padding-left: 0.5em;
        }        
    }    
    </style>

    <form name="saveListAddItemForm" id="saveListAddItemForm" class="cartForm">
        <input type="hidden" name="prod" value="${productCode}"/>
        <label class="label-input">Save List:</label>
        <input type="text" name="listName" size="50" value="" placeholder="Enter list name"  style="margin-top:10px;"/>
        <br/><br/>    
        
        <span class="form-field-no-caption">     
            <input type="submit" class="btnCart" name="Add" value="Add"/>
        </span>
    </form>

  </jsp:body>
</t:modal>