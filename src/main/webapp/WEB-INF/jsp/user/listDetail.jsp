<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

      <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>
      
<style>
.fake-table {
    display: table;
    width: 100%;
    border: 1px solid #c8ccce;
    border-radius: 5px;
    overflow: hidden;
    border-collapse: separate;
    border-spacing: 0;
  
}
.fake-table .header {
    display: table-header-group;
    background: #eaeaea;
}
.fake-table .row {
    display: table-row;
    width: auto;
}
.fake-table .row .cell {
    display: table-cell;
    line-height: 1.5em;
    padding: 0.5em 15px;
    vertical-align: top;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .header .cell {
    color: #555;
    font-weight: bold;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .body {
    display: table-row-group;
}
.center {
    text-align: center;
}
.narrow {
    display: none;
}
@media screen and (max-width: 700px) {
    .fake-table .header {
        display: none;
    }
    .fake-table .row .cell {
        width: 100%;
        display: block;
        clear: left;
        min-height: 4px;
        border-bottom: 0px;
        padding: 10px;
    }
    .narrow {
        display: inline;
    }
    .center {
        text-align: left;
    }
    .fake-table .body .row:nth-child(even) .cell {
    background: #eaeaea;
    }
}

@media screen and (max-width: 700px) {
    .content-desktop {
        display: none;
    }
    .content-desktop-msg {
        display: block;
    }
}
</style>
    
      
<div class="left" style="font-size:150%; padding:10px;">
    <a href="<c:url value="/user/lists/index.htm"/>">Home</a> > ${list.name}
</div>
      
<div class="content-desktop right" style="font-size:150%; padding:10px;">
    <a class="ui-link-button" href="<c:url value="/user/lists/edit.htm?id=${list.id}"/>">Edit List</a>
</div>

    
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Image</div>
            <div class="cell">Description</div>
            <div class="cell"><div class="center">Availability</div></div>
            <div class="cell">Price</div>
            <div class="cell"></div>
        </div>
    </div>
    <div class="body">
        <c:forEach var="item" items="${list.listItems}">
        <div class="row">
            <div class="cell">
                <c:choose>
                <c:when test="${not empty item.product.image}">
                <img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.product.image}">
                </c:when>
                <c:otherwise>
                    <img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png" width="70" height="70">
                </c:otherwise>
                </c:choose>
            </div>
            <div class="cell">
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.product.product)}"/>" style="font-size:125%">${item.product.description}</a><br/>
                <c:if test="${not empty item.product.userDescription}">
                    <b>My Description:</b> ${item.product.userDescription}<br/>
                </c:if>
                
                <b>Manufacturer:</b> <br/>
                <b>SKU:</b> ${item.product.product}<br/>
                <c:if test="${item.product.status eq 'S'}">
                    <span class="icon red-square">Item has been superseded. <a href="#">View replacement items.</a></span>
                </c:if>
            </div>
            <div class="cell">
                <div class="center">
                    <span class="narrow">Qty Available:</span>${item.product.avail}<br/>
                    <span class="narrow">
                    <c:choose>
                        <c:when test="${item.product.avail > 0}">
                            <span class="icon green-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                        </c:when>
                        <c:otherwise>
                            <span class="icon red-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                        </c:otherwise>
                    </c:choose>
                    </span>
                </div>
                <span class="narrow"><br/><a class="modalAvail" href="/web/product/avail.htm?prod=QkItMTExMzY1">Check other stores for availability</a></span>
            </div>
            <div class="cell">
                <span class="narrow">Your Price:</span>
                <c:choose>
                    <c:when test="${not empty item.product.price}">
                        <fmt:formatNumber value="${item.product.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                    </c:when>
                    <c:otherwise>
                        Call for Pricing
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="cell" style="white-space:nowrap;">
                    <form name="cartItemForm" id="cartItemForm" action="/cart/addItem.htm" class="cartForm">
                        <input type="hidden" name="productCode" value="${item.product.product}"/>
                        <input type="number" name="quantity" style="width:65px; text-align: right; padding-right: 10px;" value="${item.qty}"/>
                        <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart"/>
                    </form> 
            </div>
        </div>
        </c:forEach>
    </div>
</div>
    
  </jsp:body>
</t:layout>