<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
        <h1>Sign In</h1>
    
        <c:if test="${not empty message}">
            <div style="font-size:125%;">
                <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" border="0" style="width:32px; vertical-align:middle"/>
                ${message}<br/><br/>
            </div>
        </c:if>
        
        <form action="<c:url value="/user/login.htm"/>" method="post">
            <!--<b style="font-size:125%">Distributor's Name</b><br/><br/>
                <select name="cono" style="font-size:initial">
                    <option value="1">Mingledorff's</option>
                    <option value="2">Holden</option>
                </select>
            <br/><br/>-->
            <b style="font-size:125%">Username:</b><br/><input type="text" name="p_username" style="width:250px"><br/><br/>
            <b style="font-size:125%">Password:</b><br/><input type="password" name="p_password" style="width:250px"><br/><br/>

            <input type="checkbox" id="login_remember" name="login_remember" class="css-checkbox" value="true" CHECKED="CHECKED"/>
            <label for="login_remember" name="login_remember_lbl" class="css-label">Keep me signed in</label><br/><br/>
            <input type=checkbox id="cono" name="cono" value="2" class="css-checkbox checked"/>&nbsp;Holden Account?<br/><br/>
            <input type="submit" name="sign_in" value="Sign In"><br/><br/>

            <c:if test="${empty param.cono}">
                Not already a user? <a href="<c:url value="/user/register.htm?cono=2"/>">Register for Access</a>
            </c:if>
            <c:if test="${not empty param.cono}">
                Not already a user? <a href="<c:url value="/user/register.htm"/>">Register for Access</a>
            </c:if>
            <%--
            <a href="<c:url value="/user/recovery.htm"/>">Forgot your User ID?</a> |
            <a href="<c:url value="/user/recovery.htm?forgotpassword"/>">Forgot your Password?</a>
            --%>
        </form>
  </jsp:body>
</t:layout>