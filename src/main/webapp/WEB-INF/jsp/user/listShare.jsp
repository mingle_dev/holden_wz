<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

      <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>
      
<style>
.fake-table {
    display: table;
    width: 100%;
    border: 1px solid #c8ccce;
    border-radius: 5px;
    overflow: hidden;
    border-collapse: separate;
    border-spacing: 0;
  
}
.fake-table .header {
    display: table-header-group;
    background: #eaeaea;
}
.fake-table .row {
    display: table-row;
    width: auto;
}
.fake-table .row .cell {
    display: table-cell;
    line-height: 1.5em;
    padding: 0.5em 15px;
    vertical-align: top;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .header .cell {
    color: #555;
    font-weight: bold;
    border-bottom: 1px solid #c8ccce;
}
.fake-table .body {
    display: table-row-group;
}
.center {
    text-align: center;
}
.narrow {
    display: none;
}
@media screen and (max-width: 700px) {
    .fake-table .header {
        display: none;
    }
    .fake-table .row .cell {
        width: 100%;
        display: block;
        clear: left;
        min-height: 4px;
        border-bottom: 0px;
        padding: 10px;
    }
    .narrow {
        display: inline;
    }
    .center {
        text-align: left;
    }
    .fake-table .body .row:nth-child(even) .cell {
    background: #eaeaea;
    }
}

@media screen and (max-width: 700px) {
    .content-desktop {
        display: none;
    }
    .content-desktop-msg {
        display: block;
    }
}
</style>
    
      
<div style="font-size:150%; padding:10px;">
    <a href="<c:url value="/user/lists/index.htm"/>">Home</a> > ${list.name}
</div>
<br/>
      
<div style="font-size:150%; padding:10px;">
    <form action="<c:url value="/user/lists/admin-share.htm"/>">
        <input type="hidden" name="id" value="${list.id}" />
        Customer #: <input type="text" name="custno" value="" style="width:125px" />
        <a href=""><img src="https://s3.amazonaws.com/mgl1939/img/search.png" style="height:36px; vertical-align:middle; padding-bottom: 4px; border:0px;"/></a>
        <input type="submit" name="action" value="Share"/>
        <br/><br/>
    </form>
</div>

    
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Customer #</div>
            <div class="cell">Customer Name</div>
            <div class="cell">Type</div>
            <div class="cell">Sales Rep</div>
            <div class="cell">Location</div>
            <div class="cell"></div>
        </div>
    </div>
    <div class="body">
        <c:forEach var="item" items="${shares}">
        <div class="row">
            <div class="cell">
                ${item.custno}
            </div>
            <div class="cell">
                ${item.name}
            </div>
            <div class="cell">
                ${fn:toUpperCase(item.custType)}
            </div>
            <div class="cell">
                ${item.salesRep}
            </div>
            <div class="cell">
                ${item.whse}
            </div>
            <div class="cell"">
                <a href="<c:url value="/user/lists/admin-share.htm?id=${list.id}&custno=${item.custno}&action=unshare"/>"><img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/delete_ico.png" border="0" title="Remove Share"/></a>
            </div>
        </div>
        </c:forEach>
    </div>
</div>
    
  </jsp:body>
</t:layout>