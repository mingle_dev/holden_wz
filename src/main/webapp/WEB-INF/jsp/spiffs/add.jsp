<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>

<script>
var groups = ${jsonData};

$(function() {
    $('.spiff_category').on('change', function() {
        var id = $(this).attr('id');
        var category = id.substr(id.length - 1);
        var products = groups[$(this).val()];
        var option = '';

        $('#spiff_product_' + category).find('option').remove().end().append('<option value=""></option>');
        if(typeof products !== "undefined") {
            for (i = 0; i < products.length; i++) {
                option += '<option value="' + products[i] + '">' + products[i] + '</option>';
            }
            $('#spiff_product_' + category).append(option);
        }
    });

    $( "#instDate" ).datepicker({ minDate: '01/01/2016', maxDate: '12/31/2017' });

    <c:forTokens items="A,B,C,D,E,F" delims="," var="t">
    <c:if test="${not empty mapData['spiff_category_'.concat(t)]}">
        $('#spiff_category_${t}').trigger("change");
        $('#spiff_product_${t}').val("${mapData['spiff_product_'.concat(t)]}");
    </c:if>
    </c:forTokens>
});
</script>

<style>
    .ui-datepicker table {
        width: 100%;
        font-size: .9em;
        border-collapse: collapse;
        margin: 0 0 .4em;
    }

    .ui-datepicker th {
        padding: .7em .3em;
        text-align: center;
        font-weight: bold;
        border: 0;
    }

    .ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next {
        position: absolute;
        top: 2px;
        width: 1.8em;
        height: 1.8em;
    }

    .ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
        display: block;
        position: absolute;
        left: 50%;
        margin-left: -8px;
        top: 50%;
        margin-top: -8px;
    }

    .ui-datepicker .ui-datepicker-next {
        right: 2px;
    }

    .ui-datepicker .ui-datepicker-prev {
        left: 2px;
    }

    .ui-datepicker .ui-datepicker-title {
        margin: 0 2.3em;
        line-height: 1.8em;
        text-align: center;
    }

    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        border: 1px solid #d3d3d3;
        background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
        font-weight: normal;
        color: #555555;
    }

    .ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled {
        opacity: .35;
        filter: Alpha(Opacity=35);
        background-image: none;
    }

    .ui-datepicker td span, .ui-datepicker td a {
        display: block;
        padding: .2em;
        text-align: right;
        text-decoration: none;
    }

    .ui-widget-content {
        border: 1px solid #aaaaaa;
        background: #ffffff url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png") 50% 50% repeat-x;
        color: #222222;
    }

    .ui-widget-header {
        border: 1px solid #aaaaaa;
        background: #cccccc url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png") 50% 50% repeat-x;
        color: #222222;
        font-weight: bold;
    }

    .ui-widget-header .ui-icon {
        background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_222222_256x240.png");
    }

    .ui-icon-circle-triangle-e {
        background-position: -48px -192px;
    }

    .ui-icon-circle-triangle-w {
        background-position: -80px -192px;
    }

    .ui-icon {
        width: 16px;
        height: 16px;
    }
    .ui-icon {
        display: block;
        text-indent: -99999px;
        overflow: hidden;
        background-repeat: no-repeat;
    }

</style>

<h1>2016-2017 Retail Spiff Claim Entry</h1>
Please be aware of the deadline for submitting claims:<br/>
* All claims for equipment sold this year must be received no later than Jan 31 the following year. Claims received after this date cannot be honored.
<br/><br/>

<c:if test="${not empty message}">
<span style="font-size:125%; color:#ff0000;">${message}</span><br/><br/>
</c:if>

<form action="<c:url value="/spiffs/add.htm"/>" method="POST">
    <input type="hidden" id="claim_type" name="claim_type" value="${mapData['claim_type']}"/>
    <div class="left">    
    <dl class="enroll">
        <dt><label class="label-input">Homeowner Name:</label></dt>
        <dd><input type="text" id="fullName" name="fullName" value="${mapData['fullName']}" style="width:200px" /></dd>

        <dt><label class="label-input">Street Address:</label></dt>
        <dd><input type="text" id="addrStreet" name="addrStreet" value="${mapData['addrStreet']}" style="width:200px;" /></dd>
        
        <dt><label class="label-input">City, St Zip:</label></dt>
        <dd>
            <input type="text" id="addrCity" name="addrCity" value="${mapData['addrCity']}" style="width:125px;" />,
            <input type="text" id="addrState" name="addrState" value="${mapData['addrState']}" style="width:50px;" />
            <input type="text" id="addrZip" name="addrZip" value="${mapData['addrZip']}" style="width:75px;" /><br/>
        </dd>
        
        <dt><label class="label-input readonly">Installation Date:</label></dt>
        <dd><input type="text" id="instDate" name="instDate" value="${mapData['instDate']}" style="width:100px;"/></dd>
    </dl>
    </div>
    <div class="right" style="padding-right:40px;">
        <div class="desktop-only">
        <c:choose>
            <c:when test="${mapData['claim_type'] eq 'B'}">
                <img src="https://s3.amazonaws.com/mgl1939/content/images/new_bryant_opt.jpeg"/>
            </c:when>
            <c:when test="${mapData['claim_type'] eq 'C'}">
                <img src="https://s3.amazonaws.com/mgl1939/content/images/new_carrier_opt.jpeg"/>
            </c:when>
        </c:choose>
        </div>
    </div>
    
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Category</div>
            <div class="cell">Model Group</div>
            <div class="cell">Model #</div>
            <div class="cell">Serial #</div>
            <div class="cell">Status</div>
        </div>
    </div>      
    <div class="body">
        
        <c:forTokens items="A,B,C,D,E,F" delims="," var="name">
        <div class="row">
            <div class="cell" style="vertical-align:middle; font-size:125%;">
                <c:choose>
                    <c:when test="${name == 'A'}">Outdoor Unit</c:when>
                    <c:when test="${name == 'B'}">Indoor Unit</c:when>
                    <c:when test="${name == 'C'}">Package Unit</c:when>
                    <c:when test="${name == 'D'}">Controller</c:when>
                    <c:when test="${name == 'E'}">Air Cleaner</c:when>
                    <c:when test="${name == 'F'}">Miscellaneous</c:when>
                </c:choose>
            </div>            
            <div class="cell" style="vertical-align:middle;">
                <div class="narrow">Category: </div>
                <select class="spiff_category" id="spiff_category_${name}" name="spiff_category_${name}" style="min-width:100px;">
                    <option></option>
                    <c:forEach var="group" items="${categories[name]}">
                    <option <c:if test="${group.prodGroup eq mapData['spiff_category_'.concat(name)]}">SELECTED</c:if>>${group.prodGroup}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="cell" style="vertical-align:middle;">
                <div class="narrow">Model #: </div>
                <select id="spiff_product_${name}" name="spiff_product_${name}" style="min-width:150px;">
                </select>
            </div>
            <div class="cell">
                <div class="narrow">Serial #: </div>
                <input type="text" id="spiff_serial_${name}" name="spiff_serial_${name}" value="${mapData['spiff_serial_'.concat(name)]}" style="width:200px;" />
            </div>
            <div class="cell">
                <div class="narrow">Order #: </div>
                ${mapData['spiff_message_'.concat(name)]}
            </div>
        </div>
        </c:forTokens>
    </div>
</div>
<br/><br/>

<div>
    <input id="submit_claim_btn" name="submit_claim_btn" type="submit" value="Submit Claim">
    <a class="ui-link-button" href="<c:url value="/spiffs/index.htm"/>">Cancel</a>
</div>

</form>
<script>
    $(document).ready( function() {

                $("#submit_claim_btn").click( function(){

                    $('#submit_claim_btn').prop('disabled',true);
                    $(this).delay(2000);
                    $('#submit_claim_btn').prop('disabled',false);
                });
            }
    );
</script>
</script>
  </jsp:body>
</t:layout>