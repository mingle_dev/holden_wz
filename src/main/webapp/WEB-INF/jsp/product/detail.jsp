<%-- 
    Document   : detail
    Created on : Jan 23, 2014, 10:22:45 PM
    Author     : chris
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>

     <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

<script>
$(document).ready(function () {
    $("#accordion").accordion({
        animate: 20,
        active: 0,
        collapsible: true,
        heightStyle: content,
        beforeActivate: function(event, ui){
            var target = $("#" + event.currentTarget.id).attr('data-name');
            $("[data-name=" + target + "Panel]").html('<div class="section"><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/></div>');
        },
        activate: function(event, ui){
            var clicked = $(this).find('.ui-state-active').attr('data-name');

            if(typeof clicked == 'undefined') 
                return;

            $.ajax({
               type: "GET",
               url: "/web/product/" + clicked + ".htm",
               data: "prod='${util:base64Encode(product.product)}'",
               success: function(res){
                 $("[data-name=" + clicked + "Panel]").html(res);
               },
               error: function(e){
                   $("[data-name=" + clicked + "Panel]").html('An error occurred');
               }
             });            
        },
        autoHeight: false
    });
});     
</script>
        
        
<style>

.jqmWindow {
display: none;
top: 10%;
position: absolute;
z-index: 10000;
left: 8px;
right: 8px;
margin-left: auto;
margin-right: auto;

height: auto;
background-color: #fff;
border: 1px solid black;
padding: 12px;
}

.jqmOverlay {
background-color:#000;
}

.accordion {
border: 1px solid #c8ccce;
}

.header.active {
background: #eaeaea;
border-bottom: 1px solid #c8ccce;
}
.accordion .header {
background: #fff;
color: #00599c;
cursor: pointer;
}

.accordion .header.active {
background: #eaeaea;
border-bottom: 1px solid #c8ccce;
}
h1, #footer h2 {
padding-top: 15px;
}

h1, h2, h3, h4, h5, h6, p, ul, ol, dl, .indent, .indent2x {
padding: 0.5em 15px;
margin: 0;
line-height: 1.5em;
}


#details-image {
float: left;
width: 300px;
margin: 15px;
}

.sb-right, .img-col.right, col-right {
width: 250px;
float: right;
margin: 0 0 1em -15px;
}

#details-image img {
    width: auto;
    margin: auto;
    clear: left;
    display: block;
}

@media screen and (max-width: 600px) {
    #details-image {
        float: none;
        width: auto;
    }
    .sb-right, .img-col.right, col-right {
        width: auto;
        float: none;
        margin: 0 0 1em;
    }
    dl dt {
        width: 100%;
    }
}
</style>            

<div id="details-main" class="container accordion">
    <h1 class="header active">${product.description}</h1>
        <div class="section">
            
            <div id="details-image">
                <c:choose>
                    <c:when test="${not empty product.image}">
                       <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/products/${product.image}' class="img-large"> 
                    </c:when>
                    <c:otherwise>
                        <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png' class="img-large"> 
                    </c:otherwise>
                </c:choose>
            </div>
                
            <!--Pricing-->
            <div class="sb-right">
                <div style="padding:6px; line-height:1.5em;">
                    <c:if test="${not empty product.price}">
                        <div style="padding:10px 0 10px 0; font-size:125%;"><b>Your Price:</b>
                        <fmt:formatNumber value="${product.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></div>
                    </c:if>
                    
                    <%--
                    @todo; need to code retail pricing into this
                    <div style="padding:0 0 20px 0; font-size:125%;">
                        <b>Retail Price:</b> $100.00
                        <img src="http://i.neoseeker.com/mgv/20291/291/107/question_icon_small_display.png">
                    </div>
                    --%>
                    
                    <form name="cartItemForm" id="cartItemForm" action="/cart/addItem.htm" class="cartForm">
                        <input type="hidden" name="productCode" value="${product.product}"/>
                        <input type="text" name="quantity" style="width:65px; text-align: right; padding-right: 10px;" value="1"/>
                        <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart"/>
                    </form>                      

                    <div style="padding:10px 0 10px 0; font-size:125%;"><b>Qty Available:</b> ${product.avail}</div>

                    <div>
                        <c:choose>
                            <c:when test="${product.avail > 0}">
                                <span class="icon green-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="icon red-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    
                    <div style="padding-bottom:10px;">
                        <a class="modalAvail" href="<c:url value="/product/avail.htm?prod=${util:base64Encode(product.product)}"/>">Check other stores for availability</a>
                    </div>                    
                    
                    <a class="modalSaveList ui-button" href="<c:url value="/user/lists/add-item.htm?prod=${util:base64Encode(product.product)}"/>">Add to Saved List</a>

                </div>
            </div>

            <!-- General Information -->
            <div class="section">
                <div style="padding:6px; line-height:1.5em;">
                    <dt style="padding-top:4px; font-size:125%;"><b>SKU:</b> ${product.product}</dt>
                    <dt style="padding-top:4px; font-size:125%;">
                        <b>Manufacturer:</b>
                        <a href="<c:url value="/product/search.htm?term=${product.vendor}"/>" title="${product.vendor}">${product.vendor}</a>
                    </dt>

                    <div style="padding:4px 20px 4px 0px;"><b>Category:</b>     
                        <c:forEach items="${navTree}" var="item" varStatus="itemStatus">
                            <a href="<c:url value="${item.url}.htm?${item.query}"/>">${item.name}</a>
                            <c:if test="${not itemStatus.last}"> > </c:if>
                        </c:forEach>
                    </div>
                    <br/>

                    <c:if test="${product.status eq 'S'}">
                        <span class="icon red-square">Item has been superseded. <a href="#">View replacement items.</a></span>
                    </c:if>
                </div>
            </div>
    </div>
</div>
<br/>

<%--
<style>
.ui-accordion-header {
    border: 1px solid #c8ccce;
    background: #eaeaea;
    font-size: 150%;
    color: #00599c;
}

.ui-accordion-content {
    border: 1px solid #c8ccce;
    line-height: 1.5em;
}
</style>


<script>
  $(function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      heightStyle: 'content'
    });
    
    $("#accordion").click(function(e) {
        var activeIdx = $( "#accordion" ).accordion( "option", "active" );
        var activeTxt = $("#accordion h3").eq(activeIdx).text();
        
        if(activeTxt == 'Accessories') {
            //$('.ui-accordion-content-active').load('<c:url value="/"/>');
        }else if(activeTxt == 'Related Products') {
            //$('.ui-accordion-content-active').load('<c:url value="/"/>');
        }else if(activeTxt == 'Component List') {
            //$('.ui-accordion-content-active').load('<c:url value="/"/>');
        }
    });
  });
</script>
--%>

<style>
.ui-link-button:hover
{
background: #c8ccce;
}

.ui-link-button {
border: 1px solid #c8ccce;
background: #eaeaea;
font-size: 150%;
color: #00599c;
padding: 0.5em 15px;
margin: 0;
line-height: 1.5em;
}

.ui-box-basic {
border: 1px solid #c8ccce;
line-height: 1.5em;
}

.ui-accordion {
border: 1px solid #c8ccce;
}

.ui-accordion-header {
background: #eaeaea;
border-bottom: 1px solid #c8ccce;
color: #00599c;
text-decoration: none;
font-size: 150%;
line-height: 1.5em;
}
</style>

<div id="accordion">
  <%--
  <h3>Product Details</h3>
  <div>
      <p>
        ${product.details}
      </p>

  </div>
  --%>
  <h3 data-name="accessories">Accessories</h3>
  <div data-name="accessoriesPanel" data-state="new" style="padding:20px;"></div>
  
  <%--  
  <h3 data-name="related">Related Products</h3>
  <div data-name="relatedPanel">
    <p>
    
    </p>
  </div>

  <h3 data-name="components">Component List</h3>
  <div data-name="componentsPanel">
    <p>
    
    </p>
  </div>
  --%>
</div>

<a href="<c:url value="/product/components.htm?prod=${util:base64Encode(product.product)}"/>">
    <h3 data-name="components" style="background:#eaeaea; border:1px solid #c8ccce; border-top:0px; color:#00599c; text-decoration:none; font-size:150%; line-height:1.5em;" tabindex="-1">
        Component List
    </h3> 
</a>
    </jsp:body>
</t:layout>