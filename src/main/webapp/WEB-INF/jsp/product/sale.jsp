<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

<style>
    .box { padding: 12px; }
    .yellowbg { background-color: #FEF6D3; }
</style>
    
<h1>Sale Items</h1>

<div class="box border yellowbg">
At times, Mingledorff's will reach a surplus or obsolete status on products in our inventory. 
This page highlights that material for you to review and purchase at discounted rates. Limited 
quantities are available on these items, so you can only order from available stock 
(no backorders allowed). For questions, please contact <a href="#">support@mingledorffs.com</a>
</div>
<br/>

<c:choose>
<c:when test="${searchCount > 0}">
    <div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">    
        <c:if test="${not empty term}">    
        <div class="left" style="font-size:1.4em;">
            Search results for <b>${term}</b>
            <a href="<c:url value="/product/search.htm?id=${menuId}&page=${page}&limit=${limit}&sort=${sort}${attrStr}"/>"><img src="http://www.qc.cuny.edu/PublishingImages/close_red.gif" border="0"></a>
        </div>
        </c:if>

        <div class="right" style="font-size:1.4em;">
             <%--${page} - ${numPages} of ${searchCount}--%>
             ${searchCount} Products
        </div>
    </div>

    <div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
        <div class="left">
            <ul class="dropdown-menu">
                <li id="search-results-page">
                    <span class="fake-select-head">
                        <a href="#">${limit} Results Per Page</a>
                    </span>
                    <ul class="fake-select-items">
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=20${attrStr}"/>">20 Results Per Page</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=40${attrStr}"/>">40 Results Per Page</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=60${attrStr}"/>">60 Results Per Page</a></li>
                    </ul>
                </li>

                <li id="search-results-sort">
                    <span class="fake-select-head">
                        <a href="#">Sorted by ${sortKey} <c:choose><c:when test="${sortDir eq 'desc'}">(Z-A)</c:when><c:otherwise>(A-Z)</c:otherwise></c:choose></a>
                    </span>
                    <ul class="fake-select-items">
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:asc${attrStr}"/>">Sorted by Product (A-Z)</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:desc${attrStr}"/>">Sorted by Product (Z-A)</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:asc${attrStr}"/>">Sorted by Description (A-Z)</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:desc${attrStr}"/>">Sorted by Description (Z-A)</a></li>
                        <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=popular:desc${attrStr}"/>">Sorted by Popularity</a></li>
                    </ul>
                </li>       
            </ul>
        </div>

        <div style="float:right;">
            <div class="ui-buttonset">
                <c:if test="${page > 1}">
                  <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page-1}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> < </a>
                </c:if>
                <c:choose>
                    <c:when test="${numPages < 6}">
                      <c:forEach var="i" begin="1" end="${numPages}" step="1">
                        <c:choose>
                          <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                          <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                        </c:choose>
                      </c:forEach>
                    </c:when>
                    <c:when test="${page < 6}">
                      <c:forEach var="i" begin="1" end="5" step="1">
                        <c:choose>
                          <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                          <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                        </c:choose>
                      </c:forEach>
                    </c:when>          
                    <c:when test="${(page + 3) > numPages}">
                      <c:forEach var="i" begin="${numPages - 4}" end="${numPages}" step="1">
                        <c:choose>
                          <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                          <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                        </c:choose>
                      </c:forEach>
                    </c:when>
                    <c:otherwise>
                    <c:forEach var="i" begin="${page - 2}" end="${page + 2}" step="1">
                      <c:choose>
                          <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                          <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                      </c:choose>
                    </c:forEach>
                    </c:otherwise>
                </c:choose>
                <c:if test="${page < numPages}">
                  <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page+1}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> > </a>
                </c:if>
            </div>          
        </div>  
    </div>
    <br/><br/>

    <div class="fake-table">
        <div class="header">
            <div class="row">
                <div class="cell">Image</div>
                <div class="cell">Description</div>
                <div class="cell"><div class="center">Availability</div></div>
                <div class="cell">Price</div>
                <div class="cell"></div>
            </div>
        </div>
        <div class="body">
            <c:forEach items="${products}" var="item">
            <div class="row">
                <div class="cell">
                    <c:choose>
                    <c:when test="${not empty item.image}">
                    <img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.image}">
                    </c:when>
                    <c:otherwise>
                        <img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png" width="70" height="70">
                    </c:otherwise>
                    </c:choose>
                </div>
                <div class="cell">
                    <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.product)}"/>" style="font-size:125%">${item.description}</a><br/>
                    <b>Manufacturer:</b> <br/>
                    <b>SKU:</b> ${item.product}<br/>
                    <c:if test="${item.status eq 'S'}">
                        <span class="icon red-square">Item has been superseded. <a href="#">View replacement items.</a></span>
                    </c:if>
                </div>
                <div class="cell">
                    <div class="center">
                        <span class="narrow">Qty Available:</span>
                        <c:choose>
                            <c:when test="${not empty session.warehouse}">
                                ${item.netAvail}<br/>
                                <span class="narrow">
                                <c:choose>
                                    <c:when test="${product.avail > 0}">
                                        <span class="icon green-square">Norcross, GA</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="icon red-square">Norcross, GA</span>
                                    </c:otherwise>
                                </c:choose>
                                </span>                        
                            </c:when>
                            <c:otherwise><a href="<c:url value="/locations.htm"/>">Select Branch</a></c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="cell">
                    <span class="narrow">Your Price:</span>
                    <c:choose>
                        <c:when test="${not empty item.price}">
                            <fmt:formatNumber value="${item.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                        </c:when>
                        <c:otherwise>
                            Call for Pricing
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="cell" style="white-space:nowrap;">
                    <form name="cartItemForm" id="cartItemForm" action="/cart/addItem.htm" class="cartForm">
                        <input type="hidden" name="productCode" style="width:65px; text-align: right; padding-right: 10px;" value="${item.product}"/>
                        <input type="text" name="quantity" style="width:65px; text-align: right; padding-right: 10px;" value="1"/>
                        <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart"/>
                    </form>

                    <a class="modalSaveList ui-button" href="<c:url value="/user/lists/add-item.htm?prod=${util:base64Encode(item.product)}"/>">Add to Saved List</a>
                </div>
            </div>
            </c:forEach>
        </div>
    </div>
</c:when>
<c:otherwise>
    <br/><br/>
    <center><h2>
    Sorry, there are no products currently available. Please check back often as items
    are added frequently.
    </h2></center>
    <br/><br/><br/><br/>
</c:otherwise>
</c:choose>

  </jsp:body>
</t:layout>