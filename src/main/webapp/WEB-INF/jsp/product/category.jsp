<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
    <div class="section">
      <div>
          <div id="product-categories">
              <c:forEach items="${categories}" var="item">
                  <div class="fourth">
                      <c:choose>
                      <c:when test="${item.children == '0'}">
                      <a href="<c:url value="/product/search.htm?id=${item.id}"/>" title="${item.name}" class="product">
                          </c:when>
                          <c:otherwise>
                          <a href="<c:url value="/product/search.htm?id=${item.id}"/>" title="${item.name}" class="product">
                              </c:otherwise>
                              </c:choose>
                              <h3>
                        <span class="img">
                            <c:choose>
                                <c:when test="${not empty item.image}">
                                    <img src="http://www.mingledorffs.com/content/nav/${item.image}" alt="${item.name}">
                                </c:when>
                                <c:otherwise>
                                    <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${item.name}">
                                </c:otherwise>
                            </c:choose>
                        </span>
                                      ${item.name}
                              </h3>
                          </a>
                  </div>
              </c:forEach>
          </div>
      </div>
  </div>
  </jsp:body>
</t:layout>