<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>

<div class="section-title mobile-only">Product Categories</div>
<div class="section">
    <div>
        <div id="product-categories">
            <c:forEach items="${headProdMenu}" var="item">
            <div class="fourth">
                <a href="<c:url value="/product/search.htm?id=${item.id}"/>" title="" class="product" id="Screws">
                <h3>
                    <span class="img">
                        <c:choose>
                         <c:when test="${not empty item.image}">
                         <img src="http://www.mingledorffs.com/content/nav/${item.image}" alt="${item.name}">
                         </c:when>
                         <c:otherwise>
                         <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${item.name}">
                         </c:otherwise>
                         </c:choose>
                    </span>
                    ${item.name}
                </h3>
                </a>
            </div>
            </c:forEach>
            
            <%--
            <div class="fourth">
                <a href="" title="" class="product" id="pricebook">
                <h3>
                    <span class="img">
                         <img src="http://b.dryicons.com/images/icon_sets/shine_icon_set/png/256x256/address_book_search.png" width="75" height="75" alt="${item.name}">
                    </span>
                    Pricebooks
                </h3>
                </a>
            </div>
            --%>
        </div>
    </div>  
</div>   

<div class="section-title mobile-only">Promotions & Special Interests</div> 
<div class="section mobile-only">
    <div>
        <div id="product-categories">
            <div class="fourth">
                <a href="<c:url value="/pricebook/browse.htm"/>" title="" class="product" id="pricebook">
                <h3>
                    <span class="img">
                         <img src="http://b.dryicons.com/images/icon_sets/shine_icon_set/png/256x256/address_book_search.png" width="75" height="75" alt="${item.name}">
                    </span>
                    Pricebooks
                </h3>
                </a>
            </div>
                    
            <div class="fourth">
                <a href="<c:url value="/pricebook/browse.htm"/>" title="" class="product" id="pricebook">
                <h3>
                    <span class="img">
                         <img src="http://hostbillapp.com/features/apps/cartpromotion/pictures/sale.png" width="75" height="75" alt="${item.name}">
                    </span>
                    Sale and Promotions
                </h3>
                </a>
            </div>
  
            <div class="fourth">
                <a href="<c:url value="/product/scratch-and-dent.htm"/>" title="" class="product" id="pricebook">
                <h3>
                    <span class="img">
                         <img src="http://www.alpinehomeair.com/images/Scratch-N-Dent_page.jpg" width="75" height="75" alt="${item.name}">
                    </span>
                    Scratch & Dent
                </h3>
                </a>
            </div>                    
        </div>        
    </div>
</div>

<!-- Tree categories w/ images -->
<%--
            <table cellspacing="12" border="0" cellpadding="0">
              <tr>
              <c:forEach var="item" items="${headProdMenu}" varStatus="itemStatus">
                <td style="text-align:center; width:126px">
                    <table class="ecn_imgBox" cellspacing="0" border="0" cellpadding="0" width="100%">
                      <tr>
                          <td style="text-align: center;">
                            <c:choose>
                            <c:when test="${item.image != ''}">
                            <img src="http://www.mingledorffs.com/content/nav/${item.image}">
                            </c:when>
                            <c:otherwise>
                            <img src="http://www.mingledorffs.com/img/photo_pending.jpg">
                            </c:otherwise>
                            </c:choose>
                          </td>
                      </tr><tr>
                        <td style="vertical-align:top; text-align: center;">
                            <c:choose>
                            <c:when test="${item.children == '0'}">
                            <a href="Catalog.do?op=itmlst&cid=${item.id}" style="font-size:11px;">${item.name}</a>
                            </c:when>
                            <c:otherwise>
                            <a href="Catalog.do?op=cat&cid=${item.id}" style="font-size:11px;">${item.name}</a>
                            </c:otherwise>
                            </c:choose>
                        </td>
                      </tr>
                    </table>
                </td>
              <c:if test="${itemStatus.count % 5 == 0}">
              </tr>
              <tr style="height:115px;">
              </c:if>
              </c:forEach>
              </tr>
            </table>        

        
    <c:forEach items="${locations}" var="item">
        ${item.whseName}<br/>
    </c:forEach>

    <hr>
    Your Session ID is: ${session.sessionID}<br/>
    Name: ${session.fullName}<br/>
    User ID: ${session.userId}
--%>


  </jsp:body>    
</t:layout>


