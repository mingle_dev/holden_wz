<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

    <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

    <style>

      .ui-widget-header {

        font-weight: bold;
      }

      .ui-widget-header .ui-icon {
        background-image: url('http://code.jquery.com/ui/1.10.4/themes/smoothness/images/ui-icons_222222_256x240.png');
      }

      .ui-icon-minusthick {
        background-position: 0px 0px;
      }
      .ui-icon-plusthick {
        background-position: -64px 0px;
      }

      .ui-icon {
        width: 16px;
        height: 16px;
      }
      .ui-icon {
        display: block;
        text-indent: -99999px;
        overflow: hidden;
        background-repeat: no-repeat;
      }



    </style>

    <script>
      $(function() {
        $( ".portlet" )
                .addClass( "ui-widget ui-widget-content" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header portlet-header-border" )
                .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

        $( ".portlet-toggle" ).click(function() {
          var icon = $(this);
          icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
          icon.closest( ".portlet" ).find( ".portlet-header" ).toggleClass( "portlet-header-border portlet-header-noborder" );
          icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
        });
      });
    </script>

    <style>
      .fake-select-head {
        display: inline-block;
        background-color: #eaeaea;
        border: 1px solid #c8ccce;
        cursor: pointer;
        line-height: 24px;
        padding: 8px;

      }

      .fake-select-head a {
        font-size: 14px;
        color: #555;
        font-weight: bold;
      }

      .fake-select {
        white-space: nowrap;
        display: inline-block;
        /* IE7 inline-block fix */
        *display: inline;
        *zoom: 1;
      }

      .fake-select li {
        margin: 0;
        padding: 0;
        display: inline-block;
        /* IE7 inline-block and padding fix */
        *display: inline;
        *zoom: 1;
        *vertical-align: bottom;
      }

      .fake-select,
      .fake-select ul {
        margin: 0;
        padding: 4px;
        list-style: none;
      }
      .fake-select ul {
        display: none;
        position: absolute;
        z-index: 1000000;
      }

      .fake-select-items {
        font-size: 14px;
        background: #fff;
        border: 1px solid #000;
        line-height: 1.5;
      }

      .fake-select-items li {
        padding: 0px 15px 0px 4px;
      }

      .fake-select li a {
        display: block;
      }
      .fake-select ul li {
        position: relative;
        display: block;
      }


      .square.icon {
        background: url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/red_square.gif') 0px 50% no-repeat;
        padding-left: 12px;
      }

      .narrow {
        display: none;
      }


    </style>


    <script>
      $(document).ready(function () {

        $("#search-results-page, #search-results-sort").mouseover(function (e) {
          $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
        });

        $("#search-results-page, #search-results-sort").mouseout(function (e) {
          $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
        });

        $('input[type=checkbox]').change(function (e) {
          e.preventDefault();

          var checkValues = $('input[type=checkbox]:checked').map(function() {
            return $(this).val();
          }).get();

          var attr = "";
          for(var i=0; i<checkValues.length; i++)
            attr += "&attr=" + checkValues[i];

          window.location = "<c:url value=""/>?term=${term}&id=${menuId}&page=1&limit=${limit}&" + attr;
        });
      });
    </script>

    <table width="100%" style="padding-bottom:6px;">
      <tr>
        <td><h2>${navTree[fn:length(navTree)-1].name}</h2></td>
        <td style="text-align:right;">
          <c:if test="${not empty viewAllButton}">
            <a class="ui-link-button" href="<c:url value="/pricebook/browse-all.htm?tid=${param.tid}&sid=${param.sid}"/>">View All Items</a>
          </c:if>
        </td>
      </tr>
    </table>

    <table style="width:100%; border:0;"><tr>
      <c:if test="${fn:length(attributes) gt 0}">
        <td id="product-attributes" width="210" style="vertical-align:top; padding-right:8px;">
          <c:forEach var="entry" items="${attributes}">
            <div class="portlet">
              <div class="portlet-header">${entry.name}</div>
              <div class="portlet-content">
                <c:forEach var="value" items="${entry.values}">
                  <input type="checkbox" id="attr:${entry.name}_${value.name}" name="${entry.name}" class="css-checkbox" value="${entry.name}:${value.name}" <c:if test="${value.checked == true}">CHECKED="CHECKED"</c:if> />
                  <label for="attr:${entry.name}_${value.name}" name="attr:${entry.name}_${value.name}_lbl" class="css-label">${value.name}</label><br/>
                </c:forEach>
              </div>
            </div>
          </c:forEach>
        </td>
      </c:if>

      <td style="vertical-align:top;">

        <div class="fake-table">
          <div class="header">
            <div class="row">
              <div class="cell"> </div>
              <div class="cell">Description</div>
              <div class="cell"><div class="center">Availability</div></div>
              <div class="cell">Price</div>
              <div class="cell"></div>
            </div>
          </div>
          <div class="body">
            <c:forEach items="${products}" var="item">
              <div class="row">
                <div class="cell">
                  <c:choose>
                    <c:when test="${not empty item.image}">
                      <img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.image}">
                    </c:when>
                    <c:otherwise>
                      <img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png" width="70" height="70">
                    </c:otherwise>
                  </c:choose>
                </div>
                <div class="cell">
                  <c:choose>
                    <c:when test="${item.type eq 'kit'}">
                      <a href="<c:url value="/product/kit-detail.htm?id=${util:base64Encode(item.product)}"/>" style="font-size:125%">${item.description}</a><br/>
                    </c:when>
                    <c:otherwise>
                      <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.product)}"/>" style="font-size:125%">${item.description}</a><br/>
                    </c:otherwise>
                  </c:choose>
                  <c:if test="${not empty item.vendor}">
                    <b>Manufacturer:</b> <a href="<c:url value="/product/search.htm?term=${item.vendor}"/>">${item.vendor}</a><br/>
                  </c:if>
                  <b>SKU:</b> ${item.product}<br/>
                  <c:if test="${item.status eq 'S'}">
                    <span class="icon red-square">Item has been superseded. <a href="<c:url value='/product/alternates.htm?prod=${util:base64Encode(item.product)}' />">View replacement items.</a></span>
                  </c:if>
                </div>
                <div class="cell">
                  <div class="center">
                    <span class="narrow">
                        <a class="modalAvail" href="/web/product/avail.htm?prod=${util:base64Encode(item.product)}">Qty Available:</a>
                    </span>
                    <c:choose>
                      <c:when test="${not empty session.warehouse}">
                        <%-- <a class="modalAvail" href="/web/product/avail.htm?prod=${util:base64Encode(item.product)}"> --%>
                        <a class="modalAvail" href="<c:url value="/product/avail.htm?prod=${util:base64Encode(item.product)}"/>">
                          <c:choose>
                            <c:when test="${not empty session.user.availType && session.user.availType eq 'all'}">
                              ${item.netAvail}
                            </c:when>
                            <c:otherwise>
                              ${item.avail}
                            </c:otherwise>
                          </c:choose>
                        </a>
                            <span class="narrow">
                            <c:choose>
                              <c:when test="${item.avail > 0}">
                                <span class="icon green-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                              </c:when>
                              <c:otherwise>
                                <span class="icon red-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                              </c:otherwise>
                            </c:choose>
                            </span>
                      </c:when>
                      <c:otherwise><a href="<c:url value="/locations.htm"/>">Select Branch</a></c:otherwise>
                    </c:choose>
                  </div>
                </div>
                <div class="cell">
                  <span class="narrow">Your Price:</span>
                  <c:choose>
                    <c:when test="${not empty item.price}">
                      <fmt:formatNumber value="${item.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                    </c:when>
                    <c:otherwise>
                      Call for Pricing
                    </c:otherwise>
                  </c:choose>
                </div>
                <div class="cell" style="white-space:nowrap;">
                  <form name="cartItemForm" id="cartItemForm" action="/cart/addItem.htm" class="cartForm">
                    <input type="hidden" name="productCode" style="width:65px; text-align: right; padding-right: 10px;" value="${item.product}"/>
                    <input type="text" name="quantity" style="width:65px; text-align: right; padding-right: 10px;" value="1"/>
                    <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart"/>
                  </form>

                  <a class="modalSaveList ui-button" href="<c:url value="/user/lists/add-item.htm?prod=${util:base64Encode(item.product)}"/>">Add to Saved List</a>

                    <%--
                    <a id="modalSaveList" class="ui-button" href="<c:url value="/user/lists/add-item.htm?prod=${util:base64Encode(item.product)}"/>">Add to Saved List</a>
                    --%>

                    <%--
                        <ul class="dropdown-menu">
                            <li id="search-results-page">
                                <span class="fake-select-head">
                                    <a href="#">Add to Saved List</a>
                                </span>
                                <ul class="fake-select-items" style="right:56px;">
                                    <c:forEach items="${saveLists}" var="item" begin="0" end="14">
                                        <li><a href='#'>${item.name}</a></li>
                                    </c:forEach>
                                    <c:if test="${fn:length(saveLists) gt 15}">
                                        <li><a href='#'>+ More Saved Lists</a></li>
                                    </c:if>
                                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=60${attrStr}"/>">+ Create another Saved List</a></li>
                                </ul>
                            </li>
                        </ul>
                    --%>
                </div>
              </div>
            </c:forEach>
          </div>
        </div>
      </td>

    </tr></table>

  </jsp:body>
</t:layout>