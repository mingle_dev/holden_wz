<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<style>
.narrow {
    display: none;
}

.ui-button-enabled {
    background: #D11600;
    color: #fff !important;
}
</style>

<h3>Component Parts View: ${util:base64Decode(prod)}</h3><br/>

<c:choose>
    <c:when test="${not empty models}">
        ! More than one model matches your search parameters. Please choose unit from list below.
        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell">Model</div>
                    <div class="cell">Voltage</div>
                </div>
            </div>
            <div class="body">
            <c:forEach items="${models}" var="item">
                <div class="row">
                    <div class="cell"><a href="<c:url value="/product/components.htm?prod=${prod}&sub=${util:base64Encode(item[0])}"/>">${item[0]}</a></div>
                    <div class="cell">${item[1]}</div>
                </div>
            </c:forEach>
            </div>
        </div>
    </c:when>
    <c:otherwise>

        <div>
            <c:forEach items="${groups}" var="group">
                <a href="<c:url value="/product/components.htm?prod=${prod}&group=${group.groupCode}"/>" class="ui-button <c:if test="${group.groupCode == param.group}">ui-button-enabled</c:if>" style="margin:2px 0;">${group.groupDescription}</a>
            </c:forEach>
        </div>

        <c:choose>
            <c:when test="${fn:length(products) gt 0}">
                <div class="fake-table">
                    <div class="header">
                        <div class="row">
                            <div class="cell">Image</div>
                            <div class="cell">Description</div>
                            <div class="cell"><div class="center">Availability</div></div>
                            <div class="cell">Price</div>
                            <div class="cell"></div>
                        </div>
                    </div>
                    <div class="body">
                        <c:forEach items="${products}" var="item">
                            <div class="row">
                                <div class="cell">
                                    <c:choose>
                                        <c:when test="${not empty item.image}">
                                            <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.image}' class="border" style='height:70px; width:auto;'>
                                        </c:when>
                                        <c:otherwise>
                                            <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png' class="border" style='height:70px; width:auto;'>
                                        </c:otherwise>
                                    </c:choose>
                                </div>

                                <div class="cell">
                                    <a href="" style="font-size:125%">${item.description}</a><br/>
                                    <b>Manufacturer:</b> <br/>
                                    <b>SKU:</b> ${item.product}<br/>
                                    <c:if test="${empty item.price}">
                                        <span class="icon red-square">This is not a standard Mingledorff's product - Non-catalog</span>
                                    </c:if>
                                </div>

                                <div class="cell">
                                    <div class="center">
                                        <c:choose>
                                            <c:when test="${not empty item.avail}">
                                                <span class="narrow">Qty Available:</span>${item.avail}
                                            </c:when>
                                            <c:otherwise>N/S</c:otherwise>
                                        </c:choose><br/>

                    <span class="narrow">
                    <c:choose>
                        <c:when test="${item.avail > 0}">
                            <span class="icon green-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                        </c:when>
                        <c:otherwise>
                            <span class="icon red-square">${currentLocation.whseName}, ${currentLocation.state}</span>
                        </c:otherwise>
                    </c:choose>
                    </span>
                                    </div>
                                </div>
                                <div class="cell">
                                    <span class="narrow">Your Price:</span>
                                    <c:choose>
                                        <c:when test="${not empty item.price}">${item.price}</c:when>
                                        <c:otherwise>Call</c:otherwise>
                                    </c:choose>
                                </div>

                                <div class="cell" style="white-space:nowrap;">
                                    <form name="cartItemForm" id="cartItemForm" class="cartForm"  action="/cart/addItem.htm">
                                        <input type="text" name="qty" style="width:65px; text-align: right; padding-right: 10px;" value="1"/>
                                        <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart"/>
                                    </form>

                                    <ul class="dropdown-menu">
                                        <li id="search-results-page">
                        <span class="fake-select-head">
                            <a href="#">Add to Saved List</a>
                        </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:when>
            <c:when test="${fn:length(groups) gt 0}">
                <br/><br/><center><span style="font-size:125%;">Choose a category from the list above</span></center><br/><br/>
            </c:when>
            <c:otherwise>
                <br/><br/><center><span style="font-size:125%;">No components found in database</span></center><br/><br/>
            </c:otherwise>
        </c:choose>

    </c:otherwise>
</c:choose>

  </jsp:body>
</t:layout>