<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
     <%-- <div class="modal fade" id="myModal" style="display:block;width:452px;background:green;height:750px;font-size: 15px;/* font-weight: bold; *//* font-family: -webkit-body; */color: white;">
          <div class="modal-header" style="border-bottom: none">
              <a class="close" data-dismiss="modal" onclick="$('.modal').hide();">×</a>
          </div>
          <div class="modal-body">
              <p>Welcome back to Weatherzone - Hosted by Mingledorffs.

                  <br><br>While we are sorry for the inconvenience that has been presented to you over the course of the last two weeks.

                  <br><br>At this point we have restored basic functionality, to order and pay your bills online.  We still recommend SKU Search, and you can use your saved list to order as well.

                  <br><br>We know that over the course of the next several weeks we will need to clean up our websites connections to supporting systems.  If you should run into any issues with previously available functionality please send an e-mail to <a href="mailto:wzsupport@mingledorffs.com?Subject=WeatherZone%20Issue%20-%20" target="_top">wzsupport@mingledorffs.com</a> with your account information, the steps you took to expose the deficiency, and provide the date/time that it occurred.  We will work on repairing the defect in a timely fashion to have a full restoral of all services.

                  <br><br>Thanks as always for your partnership, and continued patience as we continue to work to provide customer service through our eCommerce platform.

                  <br><br>Click <a href="#" onclick="$('.modal').hide();">OK</a> to continue
          </div>
          <!--<div class="modal-footer" style="border-top: none">
              <a data-dismiss="modal" class="btn" onclick="$('.modal').hide();">OK</a>
          </div>-->
      </div>--%>
<c:choose>
    <c:when test="${not empty favorites}">
        <h1>My Favorites</h1>
        <div class="section">
            <div>
                <div id="product-categories">
                    <c:forEach items="${favorites}" var="favorite">
                        <div class="fourth">

                            <a href="<c:url value="${favorite.url}"/>" title="${favorite.name}" class="product">
                            <h3>
                            <span class="img">
                                <c:choose>
                                    <c:when test="${not empty favorite.linkIcon}">
                                        <img src="https://s3.amazonaws.com/mgl1939/img/${favorite.linkIcon}" alt="${favorite.name}">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${favorite.name}">
                                    </c:otherwise>
                                </c:choose>
                            </span>
                            ${favorite.name}
                            </h3>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

    </c:when>
    <c:otherwise>

<style>
.featured-products {
    text-align: center;
    padding-bottom: 40px;
}
.featured-products h1 {
    margin: 40px auto 10px auto;
    max-width: 680px;
    text-align: center;
    font-size: 36px;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>
<div class="featured-products desktop-only">
        <h1>Connect with us, anywhere, anytime.</h1>
        <img src="https://s3.amazonaws.com/mgl1939/img/connect.jpg" style="border:0"/>
</div> 

<div class="desktop-only" style="position: relative; width: 100%; min-width: 100%; height: 436px; min-height: 436px;">
    <div style="background-image: url(https://s3.amazonaws.com/mgl1939/img/whse.jpg); background-size: cover; min-height: 436px; max-width: 100%; min-width: 100%; width: 100%; height: 436px; display: block; background-position: initial initial; background-repeat: no-repeat no-repeat;">
        <div style="width: 984px; margin: auto; padding: 0;">
            <div style="position: absolute; width: 600px; top: 100px; font-family:'proxima-nova',Arial,Helvetica,sans-serif;"> 
                <h1 style="color: #FFFFFF; float: left; font-size: 50px; font-style: normal; font-weight: 700; line-height: 60px; padding-bottom: 10px;"><%--Intelligent, integrated, beautiful online interactions--%>Service, Solutions, Success since 1939</h1>
                <div class="accessoryImg"></div>
            </div>
        </div>
    </div>
</div>

<div class="width-fix content" style="padding: 0px 10px 0px 10px;">                
<div style="max-width: 975px; margin: auto;">
    <h1 style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 36px; line-height: 40px; text-transform: none; border: none;">
        We've raised the bar on raising the bar. 
    </h1>
    <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; margin-bottom:40px; font-size: 125%;">
        Our goal is to provide best-in-class tools to make your job easier. That's why we re-designed our site from the ground up to deliver
        intelligent, integrated features that work on all your modern devices.
    </p>
</div>
    
<div class="section" style="max-width:1000px; margin:auto;"> 

    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="<c:url value="/product/index.htm"/>">
                <img src="https://s3.amazonaws.com/mgl1939/img/products.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Products</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                Mingledorff's is a leading provider of top-quality HVAC equipment and supplies. 
                We carry the brands you trust, along with money-saving alternatives to suit any budget.
                We add new products nearly every day to make sure we offer the latest, high-quality products.
            </p>   
            </div>
    </div>   
    
    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="<c:url value="/user/lists/index.htm"/>">
                <img src="https://s3.amazonaws.com/mgl1939/img/shopping_carts.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Saved Shopping Lists</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                Saved shopping lists are a great way to capture search results that you would like to reference later. Create as many 
                lists as you want, with a name of your choice and save products from anywhere you see the "Save to List" button. 
                Share lists with colleagues that you find useful.
            </p>   
            </div>
    </div>
    
    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="<c:url value="/account/orders.htm"/>">
                <img src="https://s3.amazonaws.com/mgl1939/img/stack-bills.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Order History</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                Search for current and historical invoices by date, product, and even serial number. View PDF versions of the
                invoice and packing slips signed by the order recipient.
            </p>   
            </div>
    </div>
    
    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="#">
                <img src="https://s3.amazonaws.com/mgl1939/img/sales_tools.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Sales Tools</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                <%-- quotebuilder, pricebooks, retail spiffs --%>
                 Create a PDF with the most popular items we carry along with YOUR pricing. 
                 Take things a step further and develop retail pricing for use on your mobile phone and print in PDF format. 
            </p>   
            </div>
    </div>    
    
    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="<c:url value="/account/balances.htm"/>">
                <img src="https://s3.amazonaws.com/mgl1939/img/bill_pay.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Online Bill Pay</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                Using advanced security, we are PCI Level 1 Compliant to ensure both you enjoy a safe and secure online experience
                within our payment gateway. Research bills by key criteria such as date, amount, open balance, location, PO# and
                make ACH payments directly from your computer.
            </p>   
            </div>
    </div>
    
    <div class="half">
        <div style="max-width: 450px; margin: auto; padding-right:20px;">
            <a href="http://www.mingledorffs.com/technical-support">
                <img src="https://s3.amazonaws.com/mgl1939/img/tech_service.jpg" style="height: auto; width: auto; border:1px solid #ccc; max-width: 100%; vertical-align: middle;">
            </a>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 200%; margin-bottom:0;">Technical Services</p>
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 125%; margin-bottom: 40px;">
                Our Technical Services department mission is to provide a level of customer assurance and 
                technical support services in the HVAC industry that meet or exceed our customer's expectations. In addition, we
                offer technical training and education services designed for individuals, companies, and public 
                organizations to acquire, maintain, and optimize their HVAC skills.
            </p>   
            </div>
    </div>    
</div>
<br/><br/><br/>

    </c:otherwise>
</c:choose>

  </jsp:body>    
</t:layout>


