<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:modal>
    
  <jsp:attribute name="modalTitle">${title}</jsp:attribute>
    
  <jsp:body>
      <center>
          <div style="font-size:125%;">${message}</div>
      <br/><br/>
      
      <c:if test="${not empty button_yes}">
          <a class="ui-link-button" href="<c:url value="${button_yes}"/>">Yes</a>
      </c:if>
      
      <c:if test="${not empty button_no}">
      <a class="ui-link-button"  href="<c:url value="${button_no}"/>">No</a>
      </c:if>
      
      <c:if test="${not empty button_continue}">
      <a class="ui-link-button"  href="<c:url value="${button_continue}"/>">Continue</a>
      </c:if>
      </center>
  </jsp:body>
</t:modal>