<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>

        <script>
            $(document).ready(function () {
                $('#shipToId').on('change', function(e) {

                    if($('#shipToId').val() == '') {
                        $('#shipToName').val('');
                        $('#shipToAddr1').val('');
                        $('#shipToAddr2').val('');
                        $('#shipToCity').val('');
                        $('#shipToState').val('');
                        $('#shipToZip').val('');
                        return;
                    }

                    $.ajax({
                        url: '<c:url value="/account/get-shipto.htm"/>',
                        data: {
                            custno : ${session.customerNumber},
                            shipto: $('#shipToId').val()
                        },
                        type: "POST",
                        dataType: "json",
                        beforeSend: function() {
                            //$('#dialog').jqm({overlay: 60});
                            //$('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
                            //$('#dialog').jqmShow();
                        },
                        success: function(res) {
                            $('#shipToName').val( res.name );
                            $('#shipToAddr1').val( res.addr1 );
                            $('#shipToAddr2').val( res.addr2 );
                            $('#shipToCity').val( res.city );
                            $('#shipToState').val( res.state );
                            $('#shipToZip').val( res.zip );
                            //$('#dialog').jqmHide();
                        },
                        error: function() {
                            alert('error');
                        }
                    });
                });
            });
        </script>
        <script>
            $(function() {
                $( "#datepicker" ).datepicker();
            });
        </script>

        <h2>Step 1: Shipping Information</h2>
       
        <c:if test="${not empty formError}">
        <div class="formError">
            There are one or more errors.
        </div><br/><br/>
        </c:if>

<form:form method="POST" commandName="cartForm" action="/web/cart/checkout.htm">   
<form:input type="hidden" path="step" value="shipInfo"/>
    
<div id="details-main" class="container border">    
    <h2 class="header active">Shipping Address</h2>
    <div class="section pad10">
        <div class="half">
            <dl>
                <dt>
                    <label class="label-top">Ship To:</label>
                </dt>
                <dd>
                    <form:select path="shipToId">
                        <form:option value=""></form:option>
                        <c:forEach var="shipto" items="${shipToList}">
                        <option value="${shipto.id}" <c:if test="${shipto.id == cartData.shipToId}">SELECTED</c:if>>${shipto.name} (${shipto.id})</option>
                        </c:forEach>
                    </form:select><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Ship To Name:</label>
                </dt>
                <dd>
                    <form:input type="text" path="shipToName" value="${cartData.shipToName}" style="width:200px;"/><form:errors path="shipToName" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Address 1:</label>
                </dt>
                <dd>
                    <form:input type="text" path="shipToAddr1" value="${cartData.shipToAddr1}" style="width:250px;" /><form:errors path="shipToAddr1" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Address 2:</label>
                </dt>
                <dd>
                    <form:input type="text" path="shipToAddr2" value="${cartData.shipToAddr2}" style="width:250px;" /><form:errors path="shipToAddr2" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">City/State/Zip:</label>
                </dt>
                <dd>
                    <form:input type="text" path="shipToCity" value="${cartData.shipToCity}" style="width:125px;" />
                    <form:input type="text" path="shipToState" value="${cartData.shipToState}" style="width:50px;" />
                    <form:input type="text" path="shipToZip" value="${cartData.shipToZip}" style="width:75px;" />
                    <form:errors path="shipToCity" element="div" cssClass="fieldError" />
                </dd>
            </dl>
        </div>  
        <div class="half">
            <br/><br/>
            <img src="http://www.hivrisk.info/RedInfoIcon.png" width="24" height="24" style="vertical-align: middle;">
            <span style="padding-left: 4px; font-weight: bold;">Shipping Address:</span><br/>
            <div style="padding-left:32px; line-height: 1.5em;">
            To use a Ship-To address, click the drop-down and choose a location. 
            You can also enter a shipping address which will only be stored 
            for this order. If you would like to have more Ship-To addresses 
            available please contact support.
            </div>
        </div>
    </div>
</div>
<br/>            

<div id="details-main" class="container border">   
    <h2 class="header active">Shipping / Billing Details</h2>
    <div class="section pad10">
        <div class="half">
            <dl>
                <dt>
                    <label class="label-top">Purchase Order:</label>
                </dt>
                <dd>
                <form:input type="text" path="custPo" value="${cartData.custPo}" style="width:250px;" /><form:errors path="custPo" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Reference:</label>
                </dt>
                <dd>
                    <form:input type="text" path="reference" value="${cartData.reference}" style="width:250px;" /><form:errors path="reference" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Ship Date:</label>
                </dt>
                <dd>
                    <form:input id="datepicker" type="text" path="shipDate" value="${cartData.shipDate}" placeholder="Enter date in format mm/dd/yyyy" pattern="\d{1,2}/\d{1,2}/\d{4}" style="width:250px;" />
                    <form:errors path="shipDate" element="div" cssClass="fieldError" />                
                    <br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Instructions:</label>
                </dt>
                <dd>
                    <input type="text" name="instructions" value="${cartData.instructions}" style="width:250px;" /><br/><br/>
                </dd>
                <dt>
                    <label class="label-top">Ship Via:</label>
                </dt>
                <dd>
                    <form:select path="shipVia">
                        <option></option>
                        <c:forEach var="shipvia" items="${shipViaList}">
                            <c:choose>
                            <c:when test="${not empty cartData.shipVia}">
                                <option value="${shipvia.code}"<c:if test="${shipvia.code == cartData.shipVia}">SELECTED</c:if>>${shipvia.description}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${shipvia.code}"<c:if test="${shipvia.code == defaultShipVia}">SELECTED</c:if>>${shipvia.description}</option>
                            </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                    <form:errors path="shipVia" element="div" cssClass="fieldError" />
                </dd>
            </dl>
        </div>
        <div class="half">
            <dl>
                <dt>
                    <label class="label-top" style="width:100px;">Order Type:</label>
                </dt>
                <dd>
                    <form:select path="orderType">
                        <option value="SO">Sales Order</option>
                        <option value="QU"<c:if test="${cartData.orderType eq 'QU'}">SELECTED</c:if>>Quote</option>
                    </form:select><br/><br/>
                </dd>
                <dt>
                    <label class="label-top" style="width:125px;">Bill To Account:</label>
                </dt>
                <dd>
                    <form:select path="jobId">
                        <option></option>
                        <c:forEach var="shipTo" items="${shipToList}">
                            <c:if test="${shipTo.id != 'main'}">
                            <c:choose>
                                <c:when test="${empty cartData.jobId}">
                                    <option value="${shipTo.id}"<c:if test="${shipTo.id == session.shipTo}">SELECTED</c:if>>${shipTo.name} (${shipTo.id})</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${shipTo.id}"<c:if test="${shipTo.id == cartData.jobId}">SELECTED</c:if>>${shipTo.name} (${shipTo.id})</option>
                                </c:otherwise>
                            </c:choose>
                            </c:if>
                        </c:forEach>
                    </form:select>
                    <form:errors path="jobId" element="div" cssClass="fieldError" /><br/><br/>
                </dd>
            </dl>
        </div>
    </div>    
</div>
<br/>

<div class="section">
    <div class="right">
        <input type="submit" value="Next Step"/>
    </div>
</div>
<br/>
        </form:form>
        
    </jsp:body>
</t:layout>