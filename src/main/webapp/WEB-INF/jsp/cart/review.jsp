<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>

<h2>Step 2: Review Order</h2>

<div id="details-main" class="container border">    
    <h2 class="header active">Order Information</h2>
    <div class="section pad10" style="line-height: 1.5em;">
        <div class="half">
            <div style="padding: 6px 15px;">
            <b>Bill To Address:</b><br/>
            ${accountInfo.name}<br/>
            ${accountInfo.address1}<br/>
            ${accountInfo.city}, ${accountInfo.state} ${accountInfo.zip}<br/>
            <br/>
            <b>Ship To Address:</b><br/>
            ${cartForm.shipToName}<br/>
            ${cartForm.shipToAddr1}<br/>
            ${cartForm.shipToAddr2}<br/>
            ${cartForm.shipToCity}, ${cartForm.shipToState} ${cartForm.shipToZip}<br/><br/>
            </div>
        </div>
        
        <div class="half">
            <dl>
                <dt>Bill to Account:</dt><dd>${cartForm.jobId}&nbsp;</dd>
                <dt>Purchase Order:</dt><dd>${cartForm.custPo}&nbsp;</dd>
                <dt>Warehouse:</dt><dd>${currentLocation.whseName} (${session.warehouse}) <%--${cartForm.shipWhseId}--%>&nbsp;</dd>
                <dt>Ship Date:</dt><dd>${cartForm.shipDate}&nbsp;</dd>
                <dt>Ship Via:</dt><dd>${cartForm.shipVia}&nbsp;</dd>
                <dt>Reference:</dt><dd>${cartForm.reference}&nbsp;</dd>
                <dt>Instructions:</dt><dd>${cartForm.instructions}&nbsp;</dd>
            </dl>
            <br/>
        </div>

    </div>
</div>
<br/>

<form:form method="POST" commandName="cartForm" action="/web/cart/checkout.htm">   
<form:input type="hidden" path="step" value="commit"/>
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell"></div>
            <div class="cell">Product</div>
            <div class="cell"><div class="center">Order Qty</div></div>
            <div class="cell"><div class="center">Qty Avail</div></div>
            <div class="cell"><div class="right">Price</div></div>
            <div class="cell"><div class="right">Ext. Price</div></div>
        </div>
    </div>
    <div class="body">
        <c:forEach var="line" items="${cartLines}" varStatus="itemStatus">
        <div class="row">
            <div class="cell">
                <c:choose>
                <c:when test="${not empty line.productDetail.image}">
                    <img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${line.productDetail.image}">
                </c:when>
                <c:otherwise>
                    <img src="//mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png" width="70" height="70">
                </c:otherwise>
                </c:choose>
            </div>
            <div class="cell">
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(line.productDetail.product)}"/>" style="font-size:125%">${line.productDetail.description}</a><br/>
                <c:if test="${not empty line.productDetail.userDescription}">
                    <b>My Description: </b>${line.productDetail.userDescription}<br/>
                </c:if>

                <b>SKU: </b>${line.productDetail.product}<br/>

            </div>
            <div class="cell">
                <div class="center">
                    <span class="mobile-only">Order Qty: </span>${line.qty}
                </div>
            </div>
            <div class="cell">
                <div class="center">
                    <span class="mobile-only">Qty Avail: </span>${line.productDetail.avail}
                    <span class="mobile-only">
                    <c:if test="${line.productDetail.avail - line.qty < 0}">
                        Backordered: ${(line.productDetail.avail - line.qty) * -1}
                    </c:if>
                    </span>
                </div>
            </div>
            <div class="cell">
                <div class="right">
                    <span class="mobile-only">Price: </span>
                    <fmt:formatNumber value="${line.unitPrice}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                </div>
            </div>
            <div class="cell" style="white-space:nowrap;">
                <div class="right">
                <span class="mobile-only">Ext Price: </span>
                <fmt:formatNumber value="${line.qty * line.unitPrice}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                </div>
            </div>
        </div>
        </c:forEach>
    </div>
</div>
<br/>

<div class="right" style="padding-right:5px;">
<b>Order Total: <fmt:formatNumber value="${subTotal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></b>
</div><br/><br/>

<div class="section">
    <div class="right">
        <input type="submit" value="Place Order"/>
    </div>
</div>
<br/>


</form:form>
        
    </jsp:body>
</t:layout>