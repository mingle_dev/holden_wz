<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>      
      
<script>
$(function() {
    $(".removeCartItem").on("click", function () {
        $.post("/web/cart/index.htm",
            { productCode: $(this).data('val'), quantity: 0 },
            function(data) {
                window.location.reload();
            }
        );
    });   

    $('#modalSaveCart').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            url: '/web/user/lists/new-from-cart.htm',
            beforeSend: function() {
                $('#dialog').jqm({overlay: 60});
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
                $('#dialog').jqmShow();
            },
            success: function(res) {
                $('.jqmWindow').html(res);
            }
        });
    });    
    
    $('#modalClearCart').on('click', function(e) {
        e.preventDefault();
        
        window.scrollTo(0, 0);

        $.ajax({
            url: '/web/cart/clear.htm',
            beforeSend: function() {
                $('#dialog').jqm({overlay: 60});
                $('.jqmWindow').html('<br/><br/><br/><center><img src="http://www.4metal.pl/images/wait_animation.gif"/><br/>Loading...</center><br/><br/><br/>');
                $('#dialog').jqmShow();
            },
            success: function(res) {
                $('.jqmWindow').html(res);
            }
        });
    });     
});
</script>      

<h2>Shopping Cart</h2>

<c:if test="${not empty message}">
    <img src="http://www.familyfirstalerts.com/alert-images/emergency-alert-system.gif" border="0" style="width:32px; height:32px; vertical-align:middle"/>
    <span style="font-size:125%;">${message}</span><br/><br/>
</c:if>

<c:choose>
<c:when test="${fn:length(items) > 0}">
<table border='0' cellspacing="0" cellpadding="0" style="width:100%">
    <tr>
        <td style="padding-bottom:8px;">
            <a id="modalSaveCart" class="ui-button" href="/web">Save to a List</a>
        </td>
        <td style="padding-bottom:8px; text-align:right;">
            <a class="ui-link-button" href="<c:url value="/cart/checkout.htm"/>">Checkout</a>
        </td>
    </tr>
</table>    
          
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell"></div>
            <div class="cell">Product</div>
            <div class="cell"><div class="center">Quantity</div></div>
            <div class="cell"><div class="right">Price</div></div>
            <div class="cell"><div class="right">Total Price</div></div>
        </div>
    </div>
    <div class="body">            
        <c:forEach var="item" items="${items}">
        <div class="row">
            <div class="cell">
                <div class="center">
                    <c:choose>
                    <c:when test="${not empty item.productDetail.image}">
                    <img src="http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.productDetail.image}"/>
                    </c:when>
                    <c:otherwise>
                    <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="70" height="70"/>
                    </c:otherwise>
                    </c:choose>
                </div>
            </div>            
            <div class="cell">
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.productCode)}"/>" class="prod-link">${item.productDetail.description}</a><br/>
                <b>SKU:</b> ${item.productDetail.product}<br/>
                <b>Manufacturer:</b> 
            </div>
            <div class="cell">
                <div class="center">
                    <form action="/web/cart/index.htm" method="post">
                        <input type="hidden" name="productCode" value="${util:base64Encode(item.productCode)}"/>
                        <input type="text" class="qty" name="quantity" onchange="this.form.submit()" style="width:50px; text-align:right; padding-right:8px;" value="${item.qty}"><br/>
                        <div class="removeCartItem fakeLink" data-val="${util:base64Encode(item.productCode)}">Remove</div>
                    </form>
                </div>
            </div>
            <div class="cell">
                <div class="right">
                    <span class="narrow">Your Price:</span>
                    <fmt:formatNumber value="${item.unitPrice}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                </div>
            </div>
            <div class="cell">
                <div class="right">
                    <span class="narrow">Extended Price:</span>
                    <fmt:formatNumber value="${item.qty * item.unitPrice}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                </div>
            </div>            
        </div>
        </c:forEach>
    </div>
</div>

<table border='0' cellspacing="0" cellpadding="0" style="width:100%">
    <tr>
        <td colspan="2" style="padding:8px; text-align:right;"><b>Order Total: <fmt:formatNumber value="${subTotal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></b></td>
    </tr>
    <tr>
        <td style="padding-top:8px;">
            <a id="modalClearCart" class="ui-button" href="/web" data-value="32">Clear Cart</a>
        </td>
        <td style="padding-top:8px; text-align:right;">
            <a class="ui-link-button" href="<c:url value="/cart/checkout.htm"/>">Checkout</a>
        </td>
    </tr>
</table>    
</div>
</c:when>
<c:otherwise>
<div style="font-size:125%; padding:20px;">
    <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" style="width:32px; vertical-align:middle;">
    Your cart is empty.
    <br/><br/><br/><br/><br/><br/>
</div>
</c:otherwise>
</c:choose>

  </jsp:body>
</t:layout>