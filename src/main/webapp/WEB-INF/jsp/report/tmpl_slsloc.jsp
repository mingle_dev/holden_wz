<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>

    <jsp:attribute name="title">
        Sales Totals
    </jsp:attribute>

  <jsp:body>

<c:set var="net" value="0"/>
<c:set var="mgn" value="0"/>

<table style="margin-left:auto; margin-right:auto; border-collapse: collapse;padding : 0; background : #fff; font: normal 18px Helvetica;-webkit-border-radius : 8px; width : 96%;">
  <c:forEach items="${results.results}" var="row">
      <c:set var="net" value="${net + row[4]}"/>
      <c:set var="mgn" value="${mgn + row[6]}"/>
  <tr>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
          <a href="<c:url value="/reports/orders.htm?cono=${param.cono}&key=whse&val=${row[2]}"/>">${row[3]} (${row[2]})</a><br/>
          Count: ${row[0]}
      </td>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
          <span style="font:bold 18px Helvetica;">
              <fmt:formatNumber type="currency" value="${row[4]}" />
          </span><br/>
          <c:choose>
              <c:when test="${row[4] == 0 || row[4] eq '0.00'}">
                  0% Margin
              </c:when>
              <c:otherwise>
                  <fmt:formatNumber type="percent" maxFractionDigits="2" value="${row[6] / row[4]}" /> Margin
              </c:otherwise>
          </c:choose>
      </td>
  </tr>
  </c:forEach>
</table>
<br/><br/><br/>

<table style="margin-left:auto; margin-right:auto; border-collapse: collapse;padding : 0; background : #fff; font: normal 18px Helvetica;-webkit-border-radius : 8px; width : 96%;">
  <tr>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
          Total Net Sales:
      </td>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
          <fmt:formatNumber value='${net}' type="currency" groupingUsed='true' />
      </td>
  </tr>
    <tr>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
            Margin:
        </td>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
            <fmt:formatNumber value='${mgn}' type="currency" groupingUsed='true' />
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
            Margin Percent:
        </td>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
            <c:choose>
                <c:when test="${net == 0}">
                    0%
                </c:when>
                <c:otherwise>
                    <fmt:formatNumber value='${mgn / net}' type="percent" maxFractionDigits="2" groupingUsed='true' />
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>

    <br/><br/><br/><br/>
  </jsp:body>
</t:layout>