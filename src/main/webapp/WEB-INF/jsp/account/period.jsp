<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:attribute name="title">
    <h2>
        Period Balance -
        <c:choose>
            <c:when test="${period == 0}">Future</c:when>
            <c:when test="${period eq '1'}">Current</c:when>
            <c:when test="${period == 2}">30 Days</c:when>
            <c:when test="${period == 3}">60 Days</c:when>
            <c:when test="${period == 4}">90 Days</c:when>
            <c:when test="${period == 5}">120+ Days</c:when>
        </c:choose>
    </h2>
  </jsp:attribute>

  <jsp:body>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Invoice #</div>
            <div class="cell">Customer PO #</div>
            <div class="cell">Reference</div>
            <div class="cell center">Invoice Date</div>
            <div class="cell center">Due Date</div>
            <div class="cell right">Debit Amount</div>
            <div class="cell right">Credit Amount</div>
        </div>
    </div>
    <div class="body">
        <c:forEach items="${orders}" var="order" varStatus="itemStatus">
        <div class="row">
            <div class="cell"><span class="narrow"><b>Order #:</b></span>
                <a href="<c:url value="/account/orders/detail.htm?orderno=${order.orderNo}&ordersuf=${order.orderSuf}"/>">${order.orderNo}-<c:if test="${order.orderSuf < 10}">0</c:if>${order.orderSuf}</a>
            </div>
            <div class="cell"><span class="narrow">PO #:</span> ${order.custPo}</div>
            <div class="cell"><span class="narrow">Reference:</span> ${order.reference}</div>
            <div class="cell center"><span class="narrow">Invoice Date:</span> <fmt:formatDate pattern="MM/dd/yyyy" value="${order.invoiceDt}"/></div>
            <div class="cell center"><span class="narrow">Due Date:</span> <fmt:formatDate pattern="MM/dd/yyyy" value="${order.dueDt}"/></div>
            <div class="cell right"><span class="narrow">Debit Amount:</span>
                <c:if test="${order.invAmount > 0}">
                <fmt:formatNumber value="${order.invAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                </c:if>
            </div>
            <div class="cell right"><span class="narrow">Credit Amount:</span>
                <c:if test="${order.invAmount < 0}">
                <fmt:formatNumber value="${order.invAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                </c:if>
            </div>            
        </div>
        </c:forEach>
    </div>
</div>

  </jsp:body>
</t:layout>