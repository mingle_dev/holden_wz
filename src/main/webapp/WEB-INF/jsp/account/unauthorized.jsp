<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
        <h1>Unauthorized Access</h1>
        You do not have the appropriate security level to access this section.
        <br/><br/>

        To gain access, contact <a href="mailto:${billpaySendToEmail}?subject=Request%20for%20BillPay%20authorization%20Token&Body=${emailBodyMessage}&FROM=${emailReplyAddress}&CC=twall@mingledorffs.com">Customer Support</a> for an authorization token.
        <!--To gain access, contact <a href="mailto:billpay@mingledorffs.com">Customer Support</a> for an authorization token.-->
        <br/><br/>

        <c:if test="${not empty message}">
            <div style="font-size:125%;">
                <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" border="0" style="width:32px; vertical-align:middle"/>
                ${message}<br/><br/>
            </div>
        </c:if>        
        
        <form action="<c:url value="/user/grant-acct.htm"/>" method="post">
            <b style="font-size:125%">Token:</b><br/>
            <input type="text" name="token" style="width:250px"><br/>
            <br/><br/>
            <input type="submit" name="continue" value="Continue"><br/><br/>
        </form>
        
        <%--
        To gain access, enter the Token ID # found at the top of your latest statement in the box below or contact <a href="mailto:billpay@mingledorffs.com">Customer Support</a>.
        <br/><br/>

        <form action="<c:url value="/user/grant-acct.htm"/>" method="post">
            <b style="font-size:125%">Statement Token:</b><br/>
            <input type="text" name="token" style="width:250px"><br/>
            <br/><br/>
            <input type="submit" name="continue" value="Continue"><br/><br/>
        </form>
        --%>
  </jsp:body>
</t:layout>