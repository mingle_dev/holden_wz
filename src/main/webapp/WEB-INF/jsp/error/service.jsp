<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>
        
        <h2>${messageTitle} - ${messageNum}</h2>
        
        <div style="font-size:125%; padding:20px;">
            <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" style="width:32px; vertical-align:middle;"/>
            ${message}<br/><br/><br/>
        </div>
        
        <!--
        <c:forEach var="line" items="${stackTrace}">
            ${line}
        </c:forEach>
        -->
        
        <c:if test="${not empty linkUrl}">
        <center><a class="ui-link-button" href="<c:url value="${linkUrl}"/>">${linkText}</a></center>
        </c:if>
        <br/><br/><br/><br/>
        
        <%--
        ${results.key.orderno} - ${results.key.ordersuf}<br/>
        
        errorMsg: ${results.errorMsg}<br/>
        ackMsg: ${results.ackMsg}<br/>
        ackData: ${results.ackData}<br/>
        ackErrorNum: ${results.ackErrorNum}<br/>
        --%>
    </jsp:body>
</t:layout>