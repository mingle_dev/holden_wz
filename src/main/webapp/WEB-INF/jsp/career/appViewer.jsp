<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>

<%--
<a class="ui-link-button" href="<c:url value="/career/view-app.htm?id=${application.id}&view=print"/>" target="print">Print</a>
--%>

<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment
        </h2>

          <dl class="wide">
            <dt><label class="label-top">Legal Full Name:</label></dt>
            <dd>${application.firstName} ${application.middleName} ${application.lastName}</dd>
            
            <dt><label class="label-top">Address:</label></dt>
            <dd>
                ${application.streetAddr}<br/>
                ${application.city}, ${application.state} ${application.zip}
            </dd>
            
            <dt><label class="label-top">Email Address:</label></dt>
            <dd>${application.emailAddr}</dd>
            
            <dt><label class="label-top">Home Phone:</label></dt>
            <dd>${application.homePhone}</dd>
            
            <dt><label class="label-top">Cell Phone:</label></dt>
            <dd>${application.cellPhone}</dd>
            
            <dt><label class="label-top">Work Phone:</label></dt>
            <dd>${application.workPhone}</dd>            
          </dl>
            
          <dl class="wide">
            <dt style="width:70%"><label class="label-top">Are you at least 18 years of age and legally eligible for work in the United States:</label></dt>
            <dd>${application.eligible}</dd>
            
            <dt style="width:70%"><label class="label-top">Will you work overtime if necessary:</label></dt>
            <dd>${application.overtime}</dd>
            
            <dt style="width:70%">
                <label class="label-top">Are you on layoff and subject to recall:</label>
            </dt>
            <dd>${application.recall}</dd>            
            
            <dt style="width:70%">
                <label class="label-top">Do you have a valid driver's license:</label>
            </dt>
            <dd>${application.validLicense}</dd>
            
            <dt style="width:70%">
                <label class="label-top">Have you ever been discharged or asked to resign from a job:</label>
            </dt>
            <dd>${application.discharged}</dd>
            
            <dt style="width:70%">
                <label class="label-top">Have you ever been convicted of or pled guilty to a felony:</label>
            </dt>
            <dd>${application.felony}</dd>
            
            <dt style="width:70%">
                <label class="label-top">Have you previously been employed by Mingledorff's Inc. or a subsidiary:</label>
            </dt>
            <dd>${application.prevEmploy}</dd>
            
            <dt style="width:70%">
                <label class="label-top">Do you have any relatives working for Mingledorff's Inc. or a subsidiary:</label>
            </dt>
            <dd>${application.relatives}</dd>
        </dl>
            
        <div style="padding:16px;">
            <div class="fake-table">
                <div class="header">
                    <div class="row">
                        <div class="cell"></div>
                        <div class="cell">School Name</div>
                        <div class="cell">Location</div>
                        <div class="cell">Area of Study</div>
                        <div class="cell">Degree/Certificate/Diploma</div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">                
                        <div class="cell">High School</div>
                        <div class="cell">${application.eduHighSchool}</div>
                        <div class="cell">${application.eduHighState}</div>
                        <div class="cell"></div>
                        <div class="cell">${application.eduHighDiploma}</div>
                    </div>
                    <div class="row">                
                        <div class="cell">College</div>
                        <div class="cell">${application.eduUgradSchool}</div>
                        <div class="cell">${application.eduUgradState}</div>
                        <div class="cell">${application.eduUgradField}</div>
                        <div class="cell">${application.eduUgradDegree}</div>
                    </div>
                    <div class="row">                
                        <div class="cell">Graduate School</div>
                        <div class="cell">${application.eduGradSchool}</div>
                        <div class="cell">${application.eduGradState}</div>
                        <div class="cell">${application.eduGradField}</div>
                        <div class="cell">${application.eduGradDegree}</div>
                    </div>        
                    <div class="row">                
                        <div class="cell">Trade, Business, Other</div>
                        <div class="cell">${application.eduTradeSchool}</div>
                        <div class="cell">${application.eduTradeState}</div>
                        <div class="cell">${application.eduTradeField}</div>
                        <div class="cell">${application.eduTradeDegree}</div>
                    </div>  
                </div>                
            </div><br/>
                  
            <p>
                <b>Professional licenses, designations, certifications, etc.</b><br/>
                ${application.certifications}
            </p>
            
            <p>
                <b>Special skills (include software skills, language skills, etc.)</b><br/>
                ${application.skills}
            </p> 
        </div>
            
        <dl class="wide">
            <dt style="width:70%"><label class="label-top">May we contact your present employer:</label></dt>
            <dd>${application.contactEmployer}</dd>
        </dl>
        
        <div style="padding:16px;">
            <p style="font-size:125%; color:#00599c; margin:0;">Employment History</p>
            <hr/>
            <c:forEach var="jobs" items="${application.previousJobs}">
                <dl class="wide">
                    <dt><label class="label-top">Employer Name:</label></dt>
                    <dd>${jobs.name}</dd>
                    <dt><label class="label-top">Location:</label></dt>
                    <dd>${jobs.city}, ${jobs.state} ${jobs.zip}</dd>      
                    <dt><label class="label-top">Phone:</label></dt>
                    <dd>${jobs.phone}</dd>           
                    <dt><label class="label-top">Position Held:</label></dt>
                    <dd>${jobs.positionHeld}</dd>        
                    <dt><label class="label-top">Employment Dates:</label></dt>
                    <dd>${jobs.employDates}</dd>      
                    <dt><label class="label-top">Reason for Leaving:</label></dt>
                    <dd>${jobs.reasonLeave}</dd>
                    <dt><label class="label-top">Pay Rate upon Leaving:</label></dt>
                    <dd>${jobs.payRate}</dd>
                    <dt><label class="label-top">Supervisor:</label></dt>
                    <dd>${jobs.supervisor}</dd>
                    <dt><label class="label-top">Job Duties:</label></dt>
                    <dd>${jobs.duties}</dd>                          
                </dl>                
            </c:forEach>
        </div>
        
        <hr/>
        
        <div style="padding:16px;">
            <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
                <form:input type="hidden" path="step" value="7"></form:input>
                <form:checkbox path="certifyFacts"/>
                I certify that the facts contained in this application are true and complete to the best of my knowledge. I understand that
                any falsified statements on this application or omission of fact on either this application or during the pre-employment
                process may result in my application being rejected, or, if I am hired, in my employment being terminated.<br/>
                <form:errors path="certifyFacts" element="div" cssClass="fieldError" />
                <br/>

                <div style="text-align:right;">
                    <input type="submit" name="submit" value="Submit Application"><br/><br/>
                </div>
            </form:form>
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>        
        