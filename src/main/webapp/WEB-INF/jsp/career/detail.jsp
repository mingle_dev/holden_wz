<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:career>
    <jsp:body>
        
<div id="details-main" class="container border">    
    <h2 class="header active">Open Position</h2>
    <div class="section pad10">
        
    <table border="0" width="100%">
        <tr>
            <td>
                <dl>
                    <dt><label class="label-input">Title:</label></dt><dd>${position.name}<br/><br/></dd>
                    <dt><label class="label-input">Reports To:</label></dt><dd>${position.report}<br/><br/></dd>
                    <dt><label class="label-input">Classification:</label></dt><dd>${position.classification}<br/><br/></dd>
                    <dt><label class="label-input">Location:</label></dt><dd>${position.location}</dd>
                </dl><br/>
                
                <div style="padding:6px 15px; line-height:1.5">
                    <label style="font-weight:bold;">Purpose:</label><p>${position.purpose}</p><br/>
                    <label style="font-weight:bold;">Principal Accountabilities:</label><div style="line-height:1.5;">${position.accountabilities}</div><br/>
                    <label style="font-weight:bold;">Qualifications:</label><div style="line-height:1.5;">${position.qualifications}</div><br/>
                    <label style="font-weight:bold;">Other Information:</label><div style="line-height:1.5;">${position.otherInfo}</div><br/>
                </div>
            </td>
        </tr>
    </table>
                
    </div>        
</div>      
<br/>         

<div class="section">
    <a class="ui-link-button" href="<c:url value="/career/apply.htm?id=${position.id}"/>">Apply Now</a>
</div>

    </jsp:body>
</t:career>