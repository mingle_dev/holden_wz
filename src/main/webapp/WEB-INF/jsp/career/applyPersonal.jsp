<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Personal Information</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Personal Information
        </h2>
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">
        

    <p>
        The following information will allow us to contact you regarding possible 
        job opportunities. If you are offered a position, you will be required 
        to provide documentation verifying your responses to some of these questions.<br/><br/>
        NOTE: Items marked with an <span style="color:#FF0000">*</span> asterisk are required fields.<br/><br/>
        <b>IMPORTANT: Please provide your full legal name - entering nicknames 
           and abbreviations will delay the status of your application.</b><br/><br/>
    </p>
    
    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
        <form:input type="hidden" path="step" value="2"></form:input>
        <dl class="wide">
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>Legal First Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="firstName" style="width:250px"/><form:errors path="firstName" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Middle Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="middleName" style="width:250px"/><form:errors path="middleName" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>Legal Last Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="lastName" style="width:250px"/><form:errors path="lastName" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>Address:</label>
            </dt>
            <dd>
                <form:input type="text" path="streetAddr" style="width:300px"/><form:errors path="streetAddr" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>City:</label>
            </dt>
            <dd>
                <form:input type="text" path="city" style="width:200px"/><form:errors path="city" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>State:</label>
            </dt>
            <dd>
                <form:input type="text" path="state" style="width:150px"/><form:errors path="state" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>Zip/Postal Code:</label>
            </dt>
            <dd>
                <form:input type="text" path="zip" style="width:150px"/><form:errors path="zip" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            
            <dt>
                <label class="label-top">Email Address:</label>
            </dt>
            <dd>
                <form:input type="text" path="emailAddr" style="width:250px"/><form:errors path="emailAddr" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top"><span style="color:#FF0000">*</span>Home Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="homePhone" style="width:150px"/><form:errors path="homePhone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Cell Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="cellPhone" style="width:150px"/><form:errors path="cellPhone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Work Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="workPhone" style="width:150px"/><form:errors path="workPhone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            
            
            
            
            
        </dl>
        <div style="text-align:right;">
            <input type="submit" name="next" value="Next"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>