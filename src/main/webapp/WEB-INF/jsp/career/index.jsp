<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:career>
    <jsp:body>
        <h1>Career Opportunities</h1>
        
        <p style="font-size:125%">
        Mingledorff’s, Inc. is always searching for talented people to join our workforce. 
        With the help of our amazing, employees we celebrated 75 years in business in 2014.  
        Our culture is based on a value system of Customer Focused, 
        Results Driven, Collaboration, Solutions & Integrity.<br/><br/>

        Mingledorff’s offers all full time employees a competitive benefit package 
        to include Health, Dental and Vision insurance plans, Life & ADAD plans, 
        401K retirement with company match, paid time off, education assistance and much more...<br/><br/>

        If you are interested in a rewarding career with an industry leader, 
        we invite you to explore the opportunities we have available.<br/>
        </p>

        <table width="100%" border="0">
        <tr>
        <td style="text-align:center; vertical-align:top !important;">
            <br/><br/>
            <a href="http://www.jobs.net/jobs/mingledorffs/en-us/search/">
                <img src="http://www.mingledorffs.com/wp-content/uploads/2014/07/btn-careers.png" border="0"/>
            </a>
            <%--
            <a href="mailto:jpowell@mingledorffs.com?subject=Career Opportunities">
                <img src="http://www.mingledorffs.com/wp-content/uploads/2014/07/btn-careers.png" border="0"/>
            </a>
            --%>
        <%--
        <div class="fake-table" style="width:100%;">
            <div class="header">
                <div class="row">
                    <div class="cell">Position</div>
                    <div class="cell">Location</div>
                    <div class="cell">Date Posted</div>
                </div>
            </div>
            <div class="body">            
                <c:forEach var="position" items="${availPositions}">
                <div class="row">
                    <div class="cell"><a href="/web/career/detail.htm?id=${position.id}">${position.name}</a></div>
                    <div class="cell">${position.location}</div>
                    <div class="cell">${position.submittedDate} 7/14/2014</div>            
                </div>
                </c:forEach>
            </div>
        </div>
        --%>
        </td>
        <td class="desktop-only" style="width:435px; text-align:right; vertical-align:top !important;">
            <img src="https://s3.amazonaws.com/mgl1939/img/career_people.jpg" style="max-width:425px;"/>
        </td>
        </tr></table><br/>
    
  
<div>        
        <p style="font-size:125%;">
            *Mingledorff’s, Inc. is an Equal Opportunity Employer.  All qualified candidates will be considered.  
            We do not discriminate on the basis of race, color, religion, gender, 
            national origin, age, disability, ethnicity, citizenship, sexual preference, 
            veteran status, marital status, genetic information, or any characteristic protected by law.<br/><br/>

            **All positions are subject to the Americans with Disability Specifications: <br/>
            The physical demands described here are representative of those that must be 
            met by an employee to successfully perform the essential functions of this job. 
            Reasonable accommodations may be made to enable individuals with disabilities 
            to perform the essential functions. While performing the duties of this job, 
            the employee is occasionally required to stand; walk; sit; use hands to finger,
            handle, or feel objects, tools or controls; reach with hands and arms; climb 
            stairs; balance; stoop, kneel, crouch or crawl; talk or hear; taste or smell. 
            The employee must occasionally lift and/or move up to 60 pounds (this will vary based on position). 
            Specific vision abilities required by the job include close vision, distance vision, 
            color vision, peripheral vision, depth perception, and the ability to adjust focus.
        </p>        
</div>        
    </jsp:body>
</t:career>