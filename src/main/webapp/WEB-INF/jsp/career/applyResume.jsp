<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Resume</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Resume
        </h2>
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">

    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
        <form:input type="hidden" path="step" value="6"></form:input>
        You can attach a resume by uploading a document from your machine or cutting and pasting it below. 
        If you do not wish to attach a resume you can continue through the application process.<br/><br/>
        
        <input type="file"></input>
        
        
        
        <div style="text-align:right;">
            <input type="submit" name="next" value="Next"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>        
        